use crate::movement::CameraControl;
use feldspat::ecs::{Entities, System, Transform};
use feldspat::engine::screen::{Mode, Screen};
use feldspat::engine::Engine;
use feldspat::input::Key;
use feldspat::math::{Mat4, Quat, Vec3, Vec4};
use feldspat::rendering::light::{DirectionalLight, PointLight};
use feldspat::rendering::OnScreenPass;
use std::cell::Cell;
use std::sync::Arc;

pub struct Demo {
    debug_voxelization_was_pressed: Cell<bool>,
    debug_voxelization: Cell<bool>,
    grab_cursor_was_pressed: Cell<bool>,
    grab_cursor: Cell<bool>,
    entities: Entities,
    camera_control: CameraControl,
}

impl Demo {
    pub fn new(engine: &Engine) -> Arc<dyn Screen> {
        Arc::new(Self {
            debug_voxelization_was_pressed: Cell::new(false),
            debug_voxelization: Cell::new(false),
            grab_cursor_was_pressed: Cell::new(false),
            grab_cursor: Cell::new(false),
            entities: Entities::new(),
            camera_control: CameraControl::new(
                engine.time().clone(),
                engine.graphics_context().window().clone(),
                engine.input().clone(),
            ),
        })
    }
}

impl Screen for Demo {
    fn name(&self) -> &'static str {
        "Demo"
    }

    fn mode(&self) -> Mode {
        Mode::Opaque
    }

    fn on_enter(&self, _engine: &Engine) -> Result<(), Box<dyn std::error::Error>> {
        let camera = self.entities.from_template("entities/camera.ent")?;
        let sun = self.entities.with_name("sun");
        sun.insert(
            Transform::new().with_orientation(
                Mat4::look_at(
                    Vec3::new(1.0, 1.0, 1.0),
                    Vec3::new(0.0, 0.0, 0.0),
                    Vec3::new(0.0, 1.0, 0.0),
                )
                .into(),
            ),
        );
        sun.insert(DirectionalLight::new(Vec3::new(1.0, 1.0, 1.0), 3.0));
        self.entities.from_template("entities/sponza.ent")?;
        Ok(())
    }

    fn update(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>> {
        let keyboard = engine.input().keyboard();
        let window = engine.graphics_context().window();

        if self.grab_cursor_was_pressed.get() != keyboard.is_pressed(Key::F2)
            && keyboard.is_pressed(Key::F2)
        {
            self.grab_cursor.replace(!self.grab_cursor.get());
            window.set_cursor_grab(self.grab_cursor.get());
        }

        if self.grab_cursor.get() {
            self.camera_control.update(&self.entities);
            window.center_cursor_position();
        }

        if self.debug_voxelization_was_pressed.get() != keyboard.is_pressed(Key::F3)
            && keyboard.is_pressed(Key::F3)
        {
            self.debug_voxelization
                .replace(!self.debug_voxelization.get());
            if self.debug_voxelization.get() {
                engine
                    .renderer()
                    .set_on_screen_pass(OnScreenPass::OctreeDebug);
            } else {
                engine.renderer().set_on_screen_pass(OnScreenPass::PbrFinal);
            }
        }

        self.grab_cursor_was_pressed
            .replace(keyboard.is_pressed(Key::F2));
        self.debug_voxelization_was_pressed
            .replace(keyboard.is_pressed(Key::F3));
        self.entities.commit();
        Ok(())
    }

    fn draw(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>> {
        engine.renderer().update(&self.entities)?;
        Ok(())
    }

    fn on_leave(&self, _engine: &Engine) -> Result<(), Box<dyn std::error::Error>> {
        Ok(())
    }
}
