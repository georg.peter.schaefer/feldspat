use approx::relative_eq;
use serde::Deserialize;

use feldspat::ecs::{Entities, System, Transform};
use feldspat::engine::Time;
use feldspat::graphics::window::Window;
use feldspat::input::{Input, Key};
use feldspat::math::{Mat4, Vec2, Vec3};
use feldspat::rendering::camera::Camera;
use feldspat::Component;
use std::sync::Arc;

#[derive(Component, Deserialize, Copy, Clone)]
pub struct Movement {
    lateral_velocity: f32,
    angular_velocity: f32,
    azimuth: f32,
    elevation: f32,
}

impl Movement {
    pub fn with_azimuth(mut self, azimuth: f32) -> Self {
        self.azimuth = azimuth;
        self
    }

    pub fn with_elevation(mut self, elevation: f32) -> Self {
        self.elevation = elevation;
        self
    }

    pub fn lateral_velocity(&self) -> f32 {
        self.lateral_velocity
    }

    pub fn angular_velocity(&self) -> f32 {
        self.angular_velocity
    }

    pub fn azimuth(&self) -> f32 {
        self.azimuth
    }

    pub fn elevation(&self) -> f32 {
        self.elevation
    }
}

pub struct CameraControl {
    input: Arc<Input>,
    window: Arc<Window>,
    time: Arc<Time>,
}

impl CameraControl {
    pub fn new(time: Arc<Time>, window: Arc<Window>, input: Arc<Input>) -> Self {
        Self {
            time,
            window,
            input,
        }
    }
}

impl System for CameraControl {
    fn update(&self, entities: &Entities) {
        for (entity, transform, movement, _) in
            entities.components::<(Transform, Movement, Camera)>()
        {
            let r = 10.0;
            let look_at = Vec3::new(
                r * f32::cos(movement.elevation()) * f32::sin(movement.azimuth()),
                r * f32::sin(movement.elevation()),
                r * f32::cos(movement.elevation()) * -f32::cos(movement.azimuth()),
            );
            let forward = look_at.normalize();
            let up = (Vec3::new(
                f32::cos(movement.elevation() + 0.01) * f32::sin(movement.azimuth()),
                f32::sin(movement.elevation() + 0.01),
                f32::cos(movement.elevation() + 0.01) * -f32::cos(movement.azimuth()),
            ))
            .normalize();
            let up = (up - up.dot(forward) * forward).normalize();
            let right = forward.cross(up).normalize();

            let mut direction = Vec3::zero();
            if self.input.keyboard().is_pressed(Key::W) {
                direction += forward;
            }
            if self.input.keyboard().is_pressed(Key::S) {
                direction -= forward;
            }
            if self.input.keyboard().is_pressed(Key::D) {
                direction += right;
            }
            if self.input.keyboard().is_pressed(Key::A) {
                direction -= right;
            }
            if !relative_eq!(direction, Vec3::zero()) {
                direction = direction.normalize();
            }

            let mut mouse = self.input.mouse().value();
            let resolution = self.window.resolution();
            let half_size = Vec2::new(resolution[0] as f32 / 2.0, resolution[1] as f32 / 2.0);
            mouse -= half_size;
            mouse /= half_size;
            mouse[1] *= -1.0;

            let azimuth =
                movement.azimuth() + mouse[0] * movement.angular_velocity() * self.time.delta();
            let elevation =
                movement.elevation() + mouse[1] * movement.angular_velocity() * self.time.delta();

            let rotation =
                Mat4::look_at(transform.position(), transform.position() + look_at, up).inverse();
            let orientation = rotation.into();

            entity.insert(
                Movement::from(*movement)
                    .with_azimuth(azimuth)
                    .with_elevation(elevation),
            );

            entity.insert(
                Transform::new()
                    .with_position(
                        transform.position()
                            + direction * movement.lateral_velocity() * self.time.delta(),
                    )
                    .with_orientation(orientation)
                    .with_scale(transform.scale()),
            );
        }
    }
}
