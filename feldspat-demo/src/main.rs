mod demo;
mod movement;

use crate::demo::Demo;
use crate::movement::Movement;
use feldspat::ecs::{Entities, Transform};
use feldspat::engine::Engine;
use feldspat::rendering::camera::Camera;
use feldspat::rendering::light::{DirectionalLight, PointLight};
use feldspat::rendering::static_mesh::StaticMesh;
use fern::colors::ColoredLevelConfig;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error + 'static>> {
    Entities::register::<Transform>();
    Entities::register::<Camera>();
    Entities::register::<StaticMesh>();
    Entities::register::<DirectionalLight>();
    Entities::register::<PointLight>();
    Entities::register::<Movement>();

    setup_logger().unwrap();
    Engine::builder().screen(Demo::new).run()
}

fn setup_logger() -> Result<(), fern::InitError> {
    let colors = ColoredLevelConfig::new();
    fern::Dispatch::new()
        .format(move |out, message, record| {
            if record.target() == "feldspat::engine::splash" {
                out.finish(format_args!("{}", message))
            } else {
                out.finish(format_args!(
                    "{}[{}][{}] {}",
                    chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                    record.target(),
                    colors.color(record.level()),
                    message
                ))
            }
        })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .chain(fern::log_file("output.log")?)
        .apply()?;
    Ok(())
}
