//! Procedural macro for compiling shaders during source compilation and including the resulting
//! SPIR-V binary into the compiled source.

#![warn(missing_docs)]
#![warn(missing_doc_code_examples)]
#![warn(broken_intra_doc_links)]

use ash::vk;
use proc_macro::TokenStream;
use proc_macro2::{Delimiter, Group, Ident, Literal, Punct, Spacing, Span};
use quote::{ToTokens, TokenStreamExt};
use shaderc::{CompilationArtifact, IncludeType, ResolvedInclude};
use std::fs;
use std::path::{Path, PathBuf};
use syn::parse::{Parse, ParseStream, Parser};
use syn::punctuated::Punctuated;
use syn::LitStr;
use syn::Token;
use syn::{parse_macro_input, Field};

/// Compiles the specified shader source and provides a type which creates a shader module from the
/// compiled source.
#[proc_macro]
pub fn shader(input: TokenStream) -> TokenStream {
    let shader_definition = parse_macro_input!(input as ShaderDefinition);
    let root = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    let root = Path::new(&root).to_path_buf();
    let full_path = root.join(&shader_definition.path);
    let parent = full_path.parent().unwrap().to_path_buf();
    let source = fs::read_to_string(full_path).expect(&format!(
        "Failed to read source from {:?}",
        shader_definition.path
    ));
    let mut compiler = shaderc::Compiler::new().unwrap();
    let mut compile_options = shaderc::CompileOptions::new().unwrap();
    compile_options.set_include_callback(|include, ty, src, _| {
        let full_paths = match ty {
            IncludeType::Relative => vec![parent.join(include)],
            IncludeType::Standard => shader_definition
                .include_dirs
                .iter()
                .map(|include_dir| root.join(include_dir).join(include))
                .collect(),
        };
        for possible_path in &full_paths {
            if possible_path.exists() {
                return Ok(ResolvedInclude {
                    resolved_name: possible_path
                        .canonicalize()
                        .unwrap()
                        .to_str()
                        .unwrap()
                        .to_string(),
                    content: fs::read_to_string(possible_path).unwrap(),
                });
            }
        }

        Err("Unable to find file".to_string())
    });
    let compilation_artifact = match compiler.compile_into_spirv(
        &source,
        shader_definition.type_as_kind(),
        shader_definition.path.to_str().unwrap(),
        "main",
        Some(&compile_options),
    ) {
        Ok(compilation_artifact) => compilation_artifact,
        Err(e) => panic!("{}", e),
    };
    let compiled_shader = CompiledShader::from(compilation_artifact);
    let shader_type = ShaderType(shader_definition.ty);
    return quote::quote! {
        #compiled_shader

        #[allow(missing_docs)]
        pub struct Shader;

        impl Shader {
            #[allow(missing_docs)]
            pub fn load(device: &std::sync::Arc<feldspat::graphics::Device>) -> Result<std::sync::Arc<feldspat::graphics::ShaderModule>, feldspat::graphics::ShaderModuleCreationError> {
                feldspat::graphics::ShaderModule::new(device, &BINARY, #shader_type, "main")
            }
        }
    }
    .into();
}

struct ShaderDefinition {
    ty: vk::ShaderStageFlags,
    path: PathBuf,
    include_dirs: Vec<PathBuf>,
}

impl ShaderDefinition {
    fn type_as_kind(&self) -> shaderc::ShaderKind {
        match self.ty {
            vk::ShaderStageFlags::VERTEX => shaderc::ShaderKind::Vertex,
            vk::ShaderStageFlags::FRAGMENT => shaderc::ShaderKind::Fragment,
            vk::ShaderStageFlags::GEOMETRY => shaderc::ShaderKind::Geometry,
            vk::ShaderStageFlags::TESSELLATION_CONTROL => shaderc::ShaderKind::TessControl,
            vk::ShaderStageFlags::TESSELLATION_EVALUATION => shaderc::ShaderKind::TessEvaluation,
            vk::ShaderStageFlags::COMPUTE => shaderc::ShaderKind::Compute,
            _ => unreachable!("Unhandled shader type"),
        }
    }
}

impl Parse for ShaderDefinition {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        struct Output {
            ty: Option<vk::ShaderStageFlags>,
            path: Option<PathBuf>,
            include_dirs: Option<Vec<PathBuf>>,
        }
        let mut output = Output {
            ty: None,
            path: None,
            include_dirs: None,
        };

        while !input.is_empty() {
            let ident: Ident = input.parse()?;
            input.parse::<Token![:]>()?;
            let ident = ident.to_string();

            match ident.as_str() {
                "ty" => {
                    if output.ty.is_some() {
                        panic!("Multiple definitions of `ty`");
                    }

                    let ty: LitStr = input.parse()?;
                    let ty = match ty.value().as_ref() {
                        "vertex" => vk::ShaderStageFlags::VERTEX,
                        "fragment" => vk::ShaderStageFlags::FRAGMENT,
                        "geometry" => vk::ShaderStageFlags::GEOMETRY,
                        "tesselation_control" => vk::ShaderStageFlags::TESSELLATION_CONTROL,
                        "tesselation_evaluation" => vk::ShaderStageFlags::TESSELLATION_EVALUATION,
                        "compute" => vk::ShaderStageFlags::COMPUTE,
                        _ => panic!("Unexpected shader type"),
                    };
                    output.ty = Some(ty);
                }
                "path" => {
                    if output.path.is_some() {
                        panic!("Multiple definitions of `path`");
                    }

                    let path: LitStr = input.parse()?;
                    output.path = Some(path.value().into());
                }
                "include_dirs" => {
                    if output.include_dirs.is_some() {
                        panic!("Multiple definitions of `include_dirs`");
                    }

                    let content;
                    syn::bracketed!(content in input);
                    let include_dirs: Punctuated<LitStr, Token![,]> =
                        content.parse_terminated(|buffer| buffer.parse::<LitStr>())?;
                    let include_dirs = include_dirs
                        .iter()
                        .map(|literal| literal.value().into())
                        .collect();
                    output.include_dirs = Some(include_dirs);
                }
                unhandled => unreachable!("Unexpected field {:?}", unhandled),
            }

            if !input.is_empty() {
                input.parse::<Token![,]>()?;
            }
        }

        Ok(ShaderDefinition {
            ty: output.ty.expect("Missing shader type"),
            path: output.path.expect("Missing shader path"),
            include_dirs: output.include_dirs.unwrap_or(Vec::new()),
        })
    }
}

struct CompiledShader {
    inner: CompilationArtifact,
}

impl ToTokens for CompiledShader {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        let binary = self.inner.as_binary();

        tokens.append(Ident::new("const", Span::call_site()));
        tokens.append(Ident::new("BINARY", Span::call_site()));
        tokens.append(Punct::new(':', Spacing::Joint));

        let mut array = proc_macro2::TokenStream::new();
        array.append(Ident::new("u32", Span::call_site()));
        array.append(Punct::new(';', Spacing::Joint));
        array.append(Literal::usize_unsuffixed(binary.len()));

        tokens.append(Group::new(Delimiter::Bracket, array));
        tokens.append(Punct::new('=', Spacing::Alone));

        let mut bytes = proc_macro2::TokenStream::new();
        for byte in binary {
            bytes.append(Literal::u32_unsuffixed(*byte));
            bytes.append(Punct::new(',', Spacing::Joint));
        }

        tokens.append(Group::new(Delimiter::Bracket, bytes));
        tokens.append(Punct::new(';', Spacing::Joint));
    }
}

impl From<CompilationArtifact> for CompiledShader {
    fn from(inner: CompilationArtifact) -> Self {
        Self { inner }
    }
}

struct ShaderType(vk::ShaderStageFlags);

impl ToTokens for ShaderType {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        tokens.append(Ident::new("ash", Span::call_site()));
        tokens.append(Punct::new(':', Spacing::Joint));
        tokens.append(Punct::new(':', Spacing::Joint));
        tokens.append(Ident::new("vk", Span::call_site()));
        tokens.append(Punct::new(':', Spacing::Joint));
        tokens.append(Punct::new(':', Spacing::Joint));
        tokens.append(Ident::new("ShaderStageFlags", Span::call_site()));
        tokens.append(Punct::new(':', Spacing::Joint));
        tokens.append(Punct::new(':', Spacing::Joint));
        match self.0 {
            vk::ShaderStageFlags::VERTEX => tokens.append(Ident::new("VERTEX", Span::call_site())),
            vk::ShaderStageFlags::FRAGMENT => {
                tokens.append(Ident::new("FRAGMENT", Span::call_site()))
            }
            vk::ShaderStageFlags::GEOMETRY => {
                tokens.append(Ident::new("GEOMETRY", Span::call_site()))
            }
            vk::ShaderStageFlags::TESSELLATION_CONTROL => {
                tokens.append(Ident::new("TESSELLATION_CONTROL", Span::call_site()))
            }
            vk::ShaderStageFlags::TESSELLATION_EVALUATION => {
                tokens.append(Ident::new("TESSELLATION_EVALUATION", Span::call_site()))
            }
            vk::ShaderStageFlags::COMPUTE => {
                tokens.append(Ident::new("COMPUTE", Span::call_site()))
            }
            _ => unreachable!("Unhandled shader type"),
        }
    }
}
