//! Generic matrix type and operations.

use std::ops::{
    Add, AddAssign, Deref, DerefMut, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Sub,
    SubAssign,
};

use approx::{AbsDiffEq, RelativeEq};
use num::traits::NumAssign;
use num::{Float, Num};

use crate::math::projection::{Ortho, PerspectiveFov};
use crate::math::quat::TQuat;
use crate::math::vec::{Column, Row, TVec};

/// A generic n x m-dimensional matrix type.
#[derive(Copy, Clone, PartialEq, Debug)]
pub struct TMat<T, const N: usize, const M: usize>
where
    T: Num,
{
    columns: [TVec<Column, T, M>; N],
}

impl<T, const N: usize, const M: usize> TMat<T, N, M>
where
    T: Num + Copy,
{
    /// Creates a matrix with all components set to 0.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use feldspat::math::Mat2;
    ///
    /// assert_relative_eq!(Mat2::zero(), Mat2::new(0.0, 0.0, 0.0, 0.0));
    /// ```
    pub fn zero() -> Self {
        Self {
            columns: [TVec::<Column, T, M>::zero(); N],
        }
    }

    /// Transposes a matrix.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use feldspat::math::{Mat2x3, Mat3x2};
    ///
    /// let mat = Mat2x3::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
    /// assert_relative_eq!(Mat2x3::transpose(mat), Mat3x2::new(1.0, 4.0, 2.0, 5.0, 3.0, 6.0));
    /// ```
    pub fn transpose(self) -> TMat<T, M, N> {
        let mut result = TMat::zero();
        for i in 0..N {
            for j in 0..M {
                result[j][i] = self[i][j];
            }
        }
        result
    }
}

impl<T, const N: usize> TMat<T, N, N>
where
    T: Num + Copy,
{
    /// Creates the identity matrix.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use feldspat::math::Mat2;
    ///
    /// assert_relative_eq!(Mat2::identity(), Mat2::new(1.0, 0.0, 0.0, 1.0));
    /// ```
    pub fn identity() -> Self {
        let mut result = Self::zero();
        for i in 0..N {
            result[i][i] = T::one();
        }
        result
    }
}

impl<T, const N: usize> TMat<T, N, N>
where
    T: NumAssign + Float + RelativeEq,
{
    /// Calculates the inverse of a matrix.
    pub fn inverse(self) -> Self {
        self.gauss_jordan()
    }

    /// Gauss jordan for matrix inversion.
    fn gauss_jordan(mut self) -> Self {
        let mut inverse = Self::identity();
        for i in 0..N {
            if approx::relative_eq!(self[i][i], T::zero()) {
                let non_zero_row = self.find_non_zero_row(i).expect("Matrix is not invertible");
                self.swap_rows(i, non_zero_row);
                inverse.swap_rows(i, non_zero_row);
            }
            let div = self[i][i];
            self.div_row(i, div);
            inverse.div_row(i, div);

            for j in i + 1..N {
                let coefficient = self[i][j];
                self.sub_multiple_of_row(j, i, coefficient);
                inverse.sub_multiple_of_row(j, i, coefficient);
            }
        }
        for i in (1..N).rev() {
            for j in (0..i).rev() {
                let coefficient = self[i][j];
                self.sub_multiple_of_row(j, i, coefficient);
                inverse.sub_multiple_of_row(j, i, coefficient);
            }
        }
        inverse
    }

    fn find_non_zero_row(&self, column: usize) -> Option<usize> {
        for j in column + 1..N {
            if approx::relative_ne!(self[column][j], T::zero()) {
                return Some(j);
            }
        }
        None
    }

    fn swap_rows(&mut self, a: usize, b: usize) {
        for i in 0..N {
            let tmp = self[i][a];
            self[i][a] = self[i][b];
            self[i][b] = tmp;
        }
    }

    fn div_row(&mut self, row: usize, div: T) {
        for i in 0..N {
            self[i][row] /= div;
        }
    }

    fn sub_multiple_of_row(&mut self, target_row: usize, row: usize, coefficient: T) {
        for i in 0..N {
            let value = coefficient * self[i][row];
            self[i][target_row] -= value;
        }
    }
}

impl<T> TMat<T, 2, 2>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(a00: T, a01: T, a10: T, a11: T) -> Self {
        Self {
            columns: [[a00, a01].into(), [a10, a11].into()],
        }
    }
}

impl<T> TMat<T, 3, 3>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(a00: T, a01: T, a02: T, a10: T, a11: T, a12: T, a20: T, a21: T, a22: T) -> Self {
        Self {
            columns: [
                [a00, a01, a02].into(),
                [a10, a11, a12].into(),
                [a20, a21, a22].into(),
            ],
        }
    }
}

impl<T> TMat<T, 4, 4>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(
        a00: T,
        a01: T,
        a02: T,
        a03: T,
        a10: T,
        a11: T,
        a12: T,
        a13: T,
        a20: T,
        a21: T,
        a22: T,
        a23: T,
        a30: T,
        a31: T,
        a32: T,
        a33: T,
    ) -> Self {
        Self {
            columns: [
                [a00, a01, a02, a03].into(),
                [a10, a11, a12, a13].into(),
                [a20, a21, a22, a23].into(),
                [a30, a31, a32, a33].into(),
            ],
        }
    }
}

impl<T> TMat<T, 4, 4>
where
    T: Float + NumAssign,
{
    /// Constructs a perspective projection matrix from a field of view.
    pub fn perspective(fov_y: T, aspect_ratio: T, near: T, far: T) -> Self {
        PerspectiveFov::new(fov_y, aspect_ratio, near, far).into()
    }

    /// Constructs a orthographic projection matrix.
    pub fn ortho(left: T, right: T, bottom: T, top: T, near: T, far: T) -> Self {
        Ortho::new(left, right, bottom, top, near, far).into()
    }

    /// Creates a right hand view matrix from the `eye`, `center` and `up` vectors.
    pub fn look_at(
        eye: TVec<Column, T, 3>,
        center: TVec<Column, T, 3>,
        up: TVec<Column, T, 3>,
    ) -> Self {
        let forward = TVec::normalize(center - eye);
        let right = TVec::normalize(TVec::cross(forward, up));
        let up = TVec::normalize(TVec::cross(right, forward));
        Self::new(
            right[0],
            up[0],
            -forward[0],
            T::zero(),
            right[1],
            up[1],
            -forward[1],
            T::zero(),
            right[2],
            up[2],
            -forward[2],
            T::zero(),
            -eye.dot(right),
            -eye.dot(up),
            eye.dot(forward),
            T::one(),
        )
    }
}

impl<T> TMat<T, 2, 3>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(a00: T, a01: T, a02: T, a10: T, a11: T, a12: T) -> Self {
        Self {
            columns: [[a00, a01, a02].into(), [a10, a11, a12].into()],
        }
    }
}

impl<T> TMat<T, 3, 2>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(a00: T, a01: T, a10: T, a11: T, a20: T, a21: T) -> Self {
        Self {
            columns: [[a00, a01].into(), [a10, a11].into(), [a20, a21].into()],
        }
    }
}

impl<T> TMat<T, 2, 4>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(a00: T, a01: T, a02: T, a03: T, a10: T, a11: T, a12: T, a13: T) -> Self {
        Self {
            columns: [[a00, a01, a02, a03].into(), [a10, a11, a12, a13].into()],
        }
    }
}

impl<T> TMat<T, 4, 2>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(a00: T, a01: T, a10: T, a11: T, a20: T, a21: T, a30: T, a31: T) -> Self {
        Self {
            columns: [
                [a00, a01].into(),
                [a10, a11].into(),
                [a20, a21].into(),
                [a30, a31].into(),
            ],
        }
    }
}

impl<T> TMat<T, 3, 4>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(
        a00: T,
        a01: T,
        a02: T,
        a03: T,
        a10: T,
        a11: T,
        a12: T,
        a13: T,
        a20: T,
        a21: T,
        a22: T,
        a23: T,
    ) -> Self {
        Self {
            columns: [
                [a00, a01, a02, a03].into(),
                [a10, a11, a12, a13].into(),
                [a20, a21, a22, a23].into(),
            ],
        }
    }
}

impl<T> TMat<T, 4, 3>
where
    T: Num + Copy,
{
    /// Creates a new matrix with the given components.
    pub fn new(
        a00: T,
        a01: T,
        a02: T,
        a10: T,
        a11: T,
        a12: T,
        a20: T,
        a21: T,
        a22: T,
        a30: T,
        a31: T,
        a32: T,
    ) -> Self {
        Self {
            columns: [
                [a00, a01, a02].into(),
                [a10, a11, a12].into(),
                [a20, a21, a22].into(),
                [a30, a31, a32].into(),
            ],
        }
    }
}

impl<T, const N: usize, const M: usize> From<[TVec<Column, T, M>; N]> for TMat<T, N, M>
where
    T: Num,
{
    fn from(columns: [TVec<Column, T, M>; N]) -> Self {
        Self { columns }
    }
}

impl<T, const N: usize, const M: usize> From<[TVec<Row, T, N>; M]> for TMat<T, N, M>
where
    T: Num + Copy,
{
    fn from(rows: [TVec<Row, T, N>; M]) -> Self {
        let mut result = Self::zero();
        for i in 0..N {
            for j in 0..M {
                result[i][j] = rows[j][i];
            }
        }
        result
    }
}

impl<T> From<TQuat<T>> for TMat<T, 4, 4>
where
    T: Float,
{
    fn from(quat: TQuat<T>) -> Self {
        let two = T::from(2).unwrap();
        Self::new(
            T::one() - two * (quat[2] * quat[2] + quat[3] * quat[3]),
            two * (quat[1] * quat[2] + quat[3] * quat[0]),
            two * (quat[1] * quat[3] - quat[2] * quat[0]),
            T::zero(),
            two * (quat[1] * quat[2] - quat[3] * quat[0]),
            T::one() - two * (quat[1] * quat[1] + quat[3] * quat[3]),
            two * (quat[2] * quat[3] + quat[1] * quat[0]),
            T::zero(),
            two * (quat[1] * quat[3] + quat[2] * quat[0]),
            two * (quat[2] * quat[3] - quat[1] * quat[0]),
            T::one() - two * (quat[1] * quat[1] + quat[2] * quat[2]),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::one(),
        )
    }
}

impl<T, const N: usize, const M: usize> From<TMat<T, N, M>> for [[T; N]; M]
where
    T: Num + Copy,
{
    fn from(mat: TMat<T, N, M>) -> Self {
        let mut result = [[T::zero(); N]; M];
        for i in 0..N {
            for j in 0..M {
                result[j][i] = mat[i][j]
            }
        }
        result
    }
}

impl<I, T, const N: usize, const M: usize> Index<I> for TMat<T, N, M>
where
    I: std::slice::SliceIndex<[TVec<Column, T, M>]>,
    T: Num,
{
    type Output = I::Output;

    fn index(&self, index: I) -> &Self::Output {
        Index::index(&**self, index)
    }
}

impl<I, T, const N: usize, const M: usize> IndexMut<I> for TMat<T, N, M>
where
    I: std::slice::SliceIndex<[TVec<Column, T, M>]>,
    T: Num,
{
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        IndexMut::index_mut(&mut **self, index)
    }
}

impl<T, const N: usize, const M: usize> Add for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    type Output = Self;

    fn add(mut self, rhs: Self) -> Self::Output {
        self += rhs;
        self
    }
}

impl<T, const N: usize, const M: usize> AddAssign for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    fn add_assign(&mut self, rhs: Self) {
        for i in 0..N {
            for j in 0..M {
                self[i][j] += rhs[i][j]
            }
        }
    }
}

impl<T, const N: usize, const M: usize> Sub for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    type Output = Self;

    fn sub(mut self, rhs: Self) -> Self::Output {
        self -= rhs;
        self
    }
}

impl<T, const N: usize, const M: usize> SubAssign for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    fn sub_assign(&mut self, rhs: Self) {
        for i in 0..N {
            for j in 0..M {
                self[i][j] -= rhs[i][j]
            }
        }
    }
}

impl<T, const N: usize, const M: usize, const K: usize> Mul<TMat<T, M, K>> for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    type Output = TMat<T, N, K>;

    fn mul(self, rhs: TMat<T, M, K>) -> Self::Output {
        let mut result = Self::Output::zero();
        for i in 0..N {
            for k in 0..K {
                for j in 0..M {
                    result[i][k] += self[i][j] * rhs[j][k];
                }
            }
        }
        result
    }
}

impl<T, const N: usize, const M: usize> Mul<TVec<Column, T, M>> for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    type Output = TVec<Column, T, N>;

    fn mul(self, rhs: TVec<Column, T, M>) -> Self::Output {
        let mut result = TVec::zero();
        for i in 0..N {
            for j in 0..M {
                result[i] += self[i][j] * rhs[j];
            }
        }
        result
    }
}

impl<T, const N: usize, const M: usize> Mul<TMat<T, N, M>> for TVec<Row, T, N>
where
    T: NumAssign + Copy,
{
    type Output = TVec<Row, T, M>;

    fn mul(self, rhs: TMat<T, N, M>) -> Self::Output {
        let mut result = TVec::zero();
        for j in 0..M {
            for i in 0..N {
                result[j] += self[i] * rhs[i][j];
            }
        }
        result
    }
}

impl<T, const N: usize, const M: usize> Mul<T> for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    type Output = Self;

    fn mul(mut self, rhs: T) -> Self::Output {
        self *= rhs;
        self
    }
}

impl<T, const N: usize, const M: usize> MulAssign<T> for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    fn mul_assign(&mut self, rhs: T) {
        for i in 0..N {
            for j in 0..M {
                self[i][j] *= rhs;
            }
        }
    }
}

macro_rules! impl_scalar_mul_mat {
    ($type:ty) => {
        impl<const N: usize, const M: usize> Mul<TMat<$type, N, M>> for $type {
            type Output = TMat<$type, N, M>;

            fn mul(self, mut rhs: TMat<$type, N, M>) -> Self::Output {
                rhs *= self;
                rhs
            }
        }
    };
}

impl_scalar_mul_mat!(i8);
impl_scalar_mul_mat!(i16);
impl_scalar_mul_mat!(i32);
impl_scalar_mul_mat!(i64);
impl_scalar_mul_mat!(i128);
impl_scalar_mul_mat!(isize);
impl_scalar_mul_mat!(u8);
impl_scalar_mul_mat!(u16);
impl_scalar_mul_mat!(u32);
impl_scalar_mul_mat!(u64);
impl_scalar_mul_mat!(u128);
impl_scalar_mul_mat!(usize);
impl_scalar_mul_mat!(f32);
impl_scalar_mul_mat!(f64);

impl<T, const N: usize, const M: usize> Div<T> for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    type Output = Self;

    fn div(mut self, rhs: T) -> Self::Output {
        self /= rhs;
        self
    }
}

impl<T, const N: usize, const M: usize> DivAssign<T> for TMat<T, N, M>
where
    T: NumAssign + Copy,
{
    fn div_assign(&mut self, rhs: T) {
        for i in 0..N {
            for j in 0..M {
                self[i][j] /= rhs;
            }
        }
    }
}

impl<T, const N: usize, const M: usize> Deref for TMat<T, N, M>
where
    T: Num,
{
    type Target = [TVec<Column, T, M>];

    fn deref(&self) -> &Self::Target {
        &self.columns
    }
}

impl<T, const N: usize, const M: usize> DerefMut for TMat<T, N, M>
where
    T: Num,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.columns
    }
}

impl<T, const N: usize, const M: usize> AbsDiffEq for TMat<T, N, M>
where
    T: AbsDiffEq + Float,
    T::Epsilon: Copy,
{
    type Epsilon = T::Epsilon;

    fn default_epsilon() -> Self::Epsilon {
        T::default_epsilon()
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        for i in 0..N {
            for j in 0..M {
                if !T::abs_diff_eq(&self[i][j], &other[i][j], epsilon) {
                    return false;
                }
            }
        }
        true
    }
}

impl<T, const N: usize, const M: usize> RelativeEq for TMat<T, N, M>
where
    T: RelativeEq + Float,
    T::Epsilon: Copy,
{
    fn default_max_relative() -> Self::Epsilon {
        T::default_max_relative()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        for i in 0..N {
            for j in 0..M {
                if !T::relative_eq(&self[i][j], &other[i][j], epsilon, max_relative) {
                    return false;
                }
            }
        }
        true
    }
}

#[cfg(test)]
mod tests {
    use crate::math::mat::TMat;
    use crate::math::vec::{Row, TVec};
    use crate::math::{
        Mat2, Mat2x3, Mat2x4, Mat3, Mat3x2, Mat3x4, Mat4, Mat4x2, Mat4x3, Vec2, Vec3, Vec4,
    };

    #[test]
    fn test_zero() {
        let mat = Mat2::zero();
        assert_relative_eq!(mat[0][0], 0.0);
        assert_relative_eq!(mat[0][1], 0.0);
        assert_relative_eq!(mat[1][0], 0.0);
        assert_relative_eq!(mat[1][1], 0.0);
    }

    #[test]
    fn test_transpose() {
        let mat = Mat2x3::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        assert_relative_eq!(
            Mat2x3::transpose(mat),
            Mat3x2::new(1.0, 4.0, 2.0, 5.0, 3.0, 6.0)
        );
    }

    #[test]
    fn test_identity() {
        let mat = Mat4::identity();
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 0.0);
        assert_relative_eq!(mat[0][2], 0.0);
        assert_relative_eq!(mat[0][3], 0.0);
        assert_relative_eq!(mat[1][0], 0.0);
        assert_relative_eq!(mat[1][1], 1.0);
        assert_relative_eq!(mat[1][2], 0.0);
        assert_relative_eq!(mat[1][3], 0.0);
        assert_relative_eq!(mat[2][0], 0.0);
        assert_relative_eq!(mat[2][1], 0.0);
        assert_relative_eq!(mat[2][2], 1.0);
        assert_relative_eq!(mat[2][3], 0.0);
        assert_relative_eq!(mat[3][0], 0.0);
        assert_relative_eq!(mat[3][1], 0.0);
        assert_relative_eq!(mat[3][2], 0.0);
        assert_relative_eq!(mat[3][3], 1.0);
    }

    #[test]
    fn test_mat2_inverse() {
        let m = Mat2::new(1.0, 4.0, 7.0, -14.0);
        let inv = TMat::inverse(m);
        assert_relative_eq!(
            inv,
            Mat2::new(1.0 / 3.0, 2.0 / 21.0, 1.0 / 6.0, -1.0 / 42.0),
            max_relative = 0.0000001
        );
    }

    #[test]
    fn test_mat3_inverse() {
        let m = Mat3::new(4.0, -1.0, 4.0, 2.0, 3.0, -3.0, 0.0, 6.0, 5.0);
        let inv = TMat::inverse(m);
        assert_relative_eq!(
            inv,
            Mat3::new(
                33.0 / 190.0,
                29.0 / 190.0,
                -9.0 / 190.0,
                -1.0 / 19.0,
                2.0 / 19.0,
                2.0 / 19.0,
                6.0 / 95.0,
                -12.0 / 95.0,
                7.0 / 95.0
            ),
            max_relative = 0.0000001
        );
    }

    #[test]
    fn test_mat4_inverse() {
        let m = Mat4::new(
            1.0, 0.0, 1.0, 1.0, -4.0, 0.0, -3.0, -1.0, -1.0, 0.0, -2.0, 4.0, 3.0, 1.0, -1.0, 1.0,
        );
        let inv = TMat::inverse(m);
        assert_relative_eq!(
            inv,
            Mat4::new(
                -7.0 / 4.0,
                -3.0 / 4.0,
                1.0 / 4.0,
                0.0,
                27.0 / 4.0,
                11.0 / 4.0,
                -5.0 / 4.0,
                1.0,
                17.0 / 8.0,
                5.0 / 8.0,
                -3.0 / 8.0,
                0.0,
                5.0 / 8.0,
                1.0 / 8.0,
                1.0 / 8.0,
                0.0
            ),
            max_relative = 0.0000001
        );
    }

    #[test]
    fn test_mat2_new() {
        let mat = Mat2::new(1.0, 2.0, 3.0, 4.0);
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[1][0], 3.0);
        assert_relative_eq!(mat[1][1], 4.0);
    }

    #[test]
    fn test_mat3_new() {
        let mat = Mat3::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0);
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[0][2], 3.0);
        assert_relative_eq!(mat[1][0], 4.0);
        assert_relative_eq!(mat[1][1], 5.0);
        assert_relative_eq!(mat[1][2], 6.0);
        assert_relative_eq!(mat[2][0], 7.0);
        assert_relative_eq!(mat[2][1], 8.0);
        assert_relative_eq!(mat[2][2], 9.0);
    }

    #[test]
    fn test_mat4_new() {
        let mat = Mat4::new(
            1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0,
        );
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[0][2], 3.0);
        assert_relative_eq!(mat[0][3], 4.0);
        assert_relative_eq!(mat[1][0], 5.0);
        assert_relative_eq!(mat[1][1], 6.0);
        assert_relative_eq!(mat[1][2], 7.0);
        assert_relative_eq!(mat[1][3], 8.0);
        assert_relative_eq!(mat[2][0], 9.0);
        assert_relative_eq!(mat[2][1], 10.0);
        assert_relative_eq!(mat[2][2], 11.0);
        assert_relative_eq!(mat[2][3], 12.0);
        assert_relative_eq!(mat[3][0], 13.0);
        assert_relative_eq!(mat[3][1], 14.0);
        assert_relative_eq!(mat[3][2], 15.0);
        assert_relative_eq!(mat[3][3], 16.0);
    }

    #[test]
    fn test_mat2x3_new() {
        let mat = Mat2x3::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[0][2], 3.0);
        assert_relative_eq!(mat[1][0], 4.0);
        assert_relative_eq!(mat[1][1], 5.0);
        assert_relative_eq!(mat[1][2], 6.0);
    }

    #[test]
    fn test_mat3x2_new() {
        let mat = Mat3x2::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[1][0], 3.0);
        assert_relative_eq!(mat[1][1], 4.0);
        assert_relative_eq!(mat[2][0], 5.0);
        assert_relative_eq!(mat[2][1], 6.0);
    }

    #[test]
    fn test_mat2x4_new() {
        let mat = Mat2x4::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0);
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[0][2], 3.0);
        assert_relative_eq!(mat[0][3], 4.0);
        assert_relative_eq!(mat[1][0], 5.0);
        assert_relative_eq!(mat[1][1], 6.0);
        assert_relative_eq!(mat[1][2], 7.0);
        assert_relative_eq!(mat[1][3], 8.0);
    }

    #[test]
    fn test_mat4x2_new() {
        let mat = Mat4x2::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0);
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[1][0], 3.0);
        assert_relative_eq!(mat[1][1], 4.0);
        assert_relative_eq!(mat[2][0], 5.0);
        assert_relative_eq!(mat[2][1], 6.0);
        assert_relative_eq!(mat[3][0], 7.0);
        assert_relative_eq!(mat[3][1], 8.0);
    }

    #[test]
    fn test_mat3x4_new() {
        let mat = Mat3x4::new(
            1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0,
        );
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[0][2], 3.0);
        assert_relative_eq!(mat[0][3], 4.0);
        assert_relative_eq!(mat[1][0], 5.0);
        assert_relative_eq!(mat[1][1], 6.0);
        assert_relative_eq!(mat[1][2], 7.0);
        assert_relative_eq!(mat[1][3], 8.0);
        assert_relative_eq!(mat[2][0], 9.0);
        assert_relative_eq!(mat[2][1], 10.0);
        assert_relative_eq!(mat[2][2], 11.0);
        assert_relative_eq!(mat[2][3], 12.0);
    }

    #[test]
    fn test_mat4x3_new() {
        let mat = Mat4x3::new(
            1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0,
        );
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[0][2], 3.0);
        assert_relative_eq!(mat[1][0], 4.0);
        assert_relative_eq!(mat[1][1], 5.0);
        assert_relative_eq!(mat[1][2], 6.0);
        assert_relative_eq!(mat[2][0], 7.0);
        assert_relative_eq!(mat[2][1], 8.0);
        assert_relative_eq!(mat[2][2], 9.0);
        assert_relative_eq!(mat[3][0], 10.0);
        assert_relative_eq!(mat[3][1], 11.0);
        assert_relative_eq!(mat[3][2], 12.0);
    }

    #[test]
    fn test_perspective() {
        let fov_y = 90f32.to_radians();
        let aspect_ratio = 1920.0 / 1080.0;
        let near = 0.01;
        let far = 100.0;
        let projection = Mat4::perspective(fov_y, aspect_ratio, near, far);
        let expected = Mat4::from([
            Vec4::new(0.5625, 0.0, 0.0, 0.0),
            Vec4::new(0.0, -1.0, 0.0, 0.0),
            Vec4::new(0.0, 0.0, -1.00010001, -1.0),
            Vec4::new(0.0, 0.0, -0.010001, 0.0),
        ]);
        assert_relative_eq!(projection, expected);
    }

    #[test]
    fn ortho() {
        let left = -10.0;
        let right = 10.0;
        let bottom = -10.0;
        let top = 10.0;
        let near = -10.0;
        let far = 10.0;
        let ortho = Mat4::ortho(left, right, bottom, top, near, far);
        let expected = Mat4::from([
            Vec4::new(0.1, 0.0, 0.0, 0.0),
            Vec4::new(0.0, -0.1, 0.0, 0.0),
            Vec4::new(0.0, 0.0, -0.05, 0.0),
            Vec4::new(0.0, 0.0, 0.5, 1.0),
        ]);
        assert_relative_eq!(ortho, expected);
    }

    #[test]
    fn test_look_at() {
        let mat = Mat4::look_at(
            Vec3::new(0.0, 0.0, 3.0),
            Vec3::zero(),
            Vec3::new(0.0, 1.0, 0.0),
        );
        let expected = Mat4::from([
            Vec4::new(1.0, 0.0, 0.0, 0.0),
            Vec4::new(0.0, 1.0, 0.0, 0.0),
            Vec4::new(0.0, 0.0, 1.0, 0.0),
            Vec4::new(0.0, 0.0, -3.0, 1.0),
        ]);
        assert_relative_eq!(mat, expected);
    }

    #[test]
    fn test_from_rows() {
        let mat: Mat4x2 = [
            TVec::<Row, f32, 4>::new(1.0, 2.0, 3.0, 4.0),
            TVec::<Row, f32, 4>::new(5.0, 6.0, 7.0, 8.0),
        ]
        .into();
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[1][0], 2.0);
        assert_relative_eq!(mat[2][0], 3.0);
        assert_relative_eq!(mat[3][0], 4.0);
        assert_relative_eq!(mat[0][1], 5.0);
        assert_relative_eq!(mat[1][1], 6.0);
        assert_relative_eq!(mat[2][1], 7.0);
        assert_relative_eq!(mat[3][1], 8.0);
    }

    #[test]
    fn test_from_columns() {
        let mat: Mat3x2 = [[1.0, 3.0, 5.0].into(), [2.0, 4.0, 6.0].into()].into();
        assert_relative_eq!(mat[0][0], 1.0);
        assert_relative_eq!(mat[0][1], 2.0);
        assert_relative_eq!(mat[1][0], 3.0);
        assert_relative_eq!(mat[1][1], 4.0);
        assert_relative_eq!(mat[2][0], 5.0);
        assert_relative_eq!(mat[2][1], 6.0);
    }

    #[test]
    fn test_index_mut() {
        let mut mat = Mat4::identity();
        assert_relative_eq!(mat, Mat4::identity());
        mat[2][2] = 5.0;
        assert_relative_ne!(mat, Mat4::identity());
        assert_relative_eq!(mat[2][2], 5.0);
    }

    #[test]
    fn test_add() {
        let mut mat = Mat2x3::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        mat += Mat2x3::new(0.5, 1.0, 1.5, 2.0, 2.5, 3.0);
        assert_relative_eq!(mat, Mat2x3::new(1.5, 3.0, 4.5, 6.0, 7.5, 9.0));
        assert_relative_eq!(
            mat,
            Mat2x3::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0) + Mat2x3::new(0.5, 1.0, 1.5, 2.0, 2.5, 3.0)
        );
    }

    #[test]
    fn test_sub() {
        let mut mat = Mat3x2::new(1.5, 3.0, 4.5, 6.0, 7.5, 9.0);
        mat -= Mat3x2::new(0.5, 1.0, 1.5, 2.0, 2.5, 3.0);
        assert_relative_eq!(mat, Mat3x2::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0));
        assert_relative_eq!(
            mat,
            Mat3x2::new(1.5, 3.0, 4.5, 6.0, 7.5, 9.0) - Mat3x2::new(0.5, 1.0, 1.5, 2.0, 2.5, 3.0)
        );
    }

    #[test]
    fn test_mul_mat() {
        let a = Mat2x3::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        let b = Mat3x4::new(
            1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0,
        );
        let result = a * b;
        let expected = Mat2x4::new(38.0, 44.0, 50.0, 56.0, 83.0, 98.0, 113.0, 128.0);
        assert_relative_eq!(result, expected);
    }

    #[test]
    fn test_mul_vec() {
        let mat = Mat2x3::new(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        let v = Vec3::new(1.0, 2.0, 3.0);
        let u = TVec::<Row, _, 2>::from([1.0, 2.0]);
        assert_relative_eq!(mat * v, Vec2::new(14.0, 32.0));
        assert_relative_eq!(u * mat, TVec::<Row, _, 3>::from([9.0, 12.0, 15.0]));
    }

    #[test]
    fn test_mul_scalar() {
        let mut mat = Mat2::identity();
        mat *= 42.0;
        assert_relative_eq!(mat, Mat2::new(42.0, 0.0, 0.0, 42.0));
        assert_relative_eq!(mat, Mat2::identity() * 42.0);
        assert_relative_eq!(mat, 42.0 * Mat2::identity());
    }

    #[test]
    fn test_div_scalar() {
        let mut mat = Mat2::identity();
        mat /= 42.0;
        assert_relative_eq!(mat, Mat2::new(1.0 / 42.0, 0.0, 0.0, 1.0 / 42.0));
        assert_relative_eq!(mat, Mat2::identity() / 42.0);
    }
}
