use std::cell::UnsafeCell;
use std::collections::HashMap;

use winit::dpi::Pixel;
use winit::event::{MouseButton, MouseScrollDelta, WindowEvent};

use crate::input::State;
use crate::math::Vec2;

/// Represents the current state of the mouse.
pub struct Mouse {
    inner: UnsafeCell<InnerMouse>,
}

impl Mouse {
    pub(super) fn new() -> Self {
        Self {
            inner: UnsafeCell::new(InnerMouse::new()),
        }
    }

    /// Returns if a mouse button is pressed.
    pub fn is_pressed(&self, button: Button) -> bool {
        *self.get().buttons.get(&button).unwrap_or(&State::Released) == State::Pressed
    }

    /// Returns the direction of the mouse wheel.
    pub fn wheel(&self) -> f32 {
        self.get().wheel
    }

    /// Returns the delta position of the mouse.
    pub fn value(&self) -> Vec2 {
        let inner = self.get();
        match inner.mode {
            Mode::Absolute => inner.current,
            Mode::Delta => inner.current - inner.last,
        }
    }

    pub(super) fn update(&self, event: &WindowEvent) {
        match event {
            WindowEvent::MouseInput { button, state, .. } => {
                self.get_mut()
                    .buttons
                    .insert((*button).into(), (*state).into());
            }
            WindowEvent::MouseWheel { delta, .. } => {
                if let MouseScrollDelta::LineDelta(_, wheel) = delta {
                    self.get_mut().wheel = wheel.signum()
                }
            }
            WindowEvent::CursorMoved { position, .. } => {
                let inner = self.get_mut();
                inner.last = inner.current;
                inner.current = Vec2::new(position.x.cast(), position.y.cast());
            }
            _ => {}
        }
    }

    pub(crate) fn reset(&self) {
        let inner = self.get_mut();
        inner.wheel = 0.0;
    }

    fn get(&self) -> &InnerMouse {
        unsafe { &*self.inner.get() }
    }

    fn get_mut(&self) -> &mut InnerMouse {
        unsafe { &mut *self.inner.get() }
    }
}

unsafe impl Send for Mouse {}

unsafe impl Sync for Mouse {}

struct InnerMouse {
    mode: Mode,
    buttons: HashMap<Button, State>,
    wheel: f32,
    current: Vec2,
    last: Vec2,
}

impl InnerMouse {
    fn new() -> Self {
        Self {
            mode: Mode::Absolute,
            buttons: HashMap::new(),
            wheel: 0.0,
            current: Vec2::zero(),
            last: Vec2::zero(),
        }
    }
}

#[allow(dead_code)]
enum Mode {
    Absolute,
    Delta,
}

/// Mouse buttons.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Hash, Copy, Clone)]
pub enum Button {
    /// Left mouse button.
    Left,
    /// Right mouse button.
    Right,
    /// Middle mouse button.
    Middle,
    /// Other mouse button.
    Other(u16),
}

impl From<MouseButton> for Button {
    fn from(button: MouseButton) -> Self {
        match button {
            MouseButton::Left => Button::Left,
            MouseButton::Right => Button::Right,
            MouseButton::Middle => Button::Middle,
            MouseButton::Other(id) => Button::Other(id),
        }
    }
}
