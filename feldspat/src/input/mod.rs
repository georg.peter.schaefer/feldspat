//! Input handling.

use winit::event::{ElementState, WindowEvent};

pub use keyboard::Key;
pub use keyboard::Keyboard;
pub use mouse::Button as MouseButton;
pub use mouse::Mouse;
use std::sync::Arc;

mod keyboard;
mod mouse;

/// State of a button or key.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Hash, Copy, Clone)]
pub enum State {
    /// The key or button is pressed.
    Pressed,
    /// The key or button is released.
    Released,
}

impl From<ElementState> for State {
    fn from(state: ElementState) -> Self {
        match state {
            ElementState::Pressed => State::Pressed,
            ElementState::Released => State::Released,
        }
    }
}

/// Keeps track of all input devices.
pub struct Input {
    keyboard: Keyboard,
    mouse: Mouse,
}

impl Input {
    pub(crate) fn new() -> Arc<Self> {
        Arc::new(Self {
            keyboard: Keyboard::new(),
            mouse: Mouse::new(),
        })
    }

    /// Returns the keyboard.
    pub fn keyboard(&self) -> &Keyboard {
        &self.keyboard
    }

    /// Returns the mouse.
    pub fn mouse(&self) -> &Mouse {
        &self.mouse
    }

    pub(crate) fn handle_window_event(&self, event: &WindowEvent) {
        match event {
            WindowEvent::KeyboardInput { input, .. } => {
                self.keyboard.update(&input);
            }
            event => {
                self.mouse.update(&event);
            }
        }
    }

    pub(crate) fn reset(&self) {
        self.mouse.reset();
    }
}
