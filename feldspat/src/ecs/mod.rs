//! Types and facilities for the entity component system.

pub use component::Component;
pub use component::Ref;
pub use component::RefMut;
pub use entity::Entities;
pub use entity::Facade;
pub use entity::Handle;
pub use system::System;
pub use transform::Transform;

pub use self::serde::Deserializer;

mod component;
mod entity;
mod parser;
mod serde;
mod system;
mod transform;
