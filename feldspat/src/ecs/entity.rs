use std::any::TypeId;
use std::cell::RefCell;
use std::collections::HashMap;
use std::io;
use std::path::Path;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Mutex};

use crate::asset::{AssetError, PlainText};
use crate::ecs::serde::Deserializer;
use crate::ecs::{Component, Ref, RefMut};

/// A handle to an entity.
///
/// This identifies an entity either with a unique id or name.
///
/// For more information see the [module level documentation](super).
#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, Hash, Debug)]
pub enum Handle {
    /// A id based `Handle`.
    Id(u64),

    /// A name based `Handle`.
    Name(String),
}

/// A facade for an entity.
///
/// Entities are more or less only an association between [`components`](super::Component) and a
/// [`handle`](Handle). For convenience this facade type is provided so that the properties of an
/// entity can be accessed and modified as if it was a real instance of a type.
///
/// For more information see the [module level documentation](super).
#[derive(Clone)]
pub struct Facade<'a> {
    handle: Handle,
    inner: &'a Entities,
}

unsafe impl<'a> Send for Facade<'a> {}

unsafe impl<'a> Sync for Facade<'a> {}

impl<'a> Facade<'a> {
    /// Returns the handle to the entity.
    pub fn handle(&self) -> &Handle {
        &self.handle
    }

    /// Inserts a [`Component`](super::Component) into the entity.
    ///
    /// An entity can only have one [`Component`](super::Component) of each type. Adding a
    /// [`Component`] is deferred until [`commit`](Entities::commit) is called.
    ///
    /// If the entity already has a [`Component`](super::Component) of that type, the existing one
    /// will be overwritten.
    ///
    /// For more information see the [module level documentation](super).
    pub fn insert<C>(&self, component: C)
    where
        C: Component,
    {
        self.inner.insert_component(self.handle(), component);
    }

    /// Removes a [`Component`](super::Component) from the entity.
    ///
    /// Removing a [`Component`] is deferred until [`commit`](Entities::commit) is called.
    ///
    /// For more information see the [module level documentation](super).
    pub fn remove<C>(&self)
    where
        C: Component,
    {
        self.inner.remove_component::<C>(self.handle())
    }

    /// Gets an immutable borrow to a [`Component`](super::Component) of an entity.
    ///
    /// If the entity does not contain the requested [`Component`](super::Component) `None` is
    /// returned.
    pub fn get<C>(&self) -> Option<Ref<C>>
    where
        C: Component,
    {
        self.inner.get_component::<C>(&self.handle)
    }

    /// Gets a mutable borrow to a [`Component`](super::Component) of an entity.
    ///
    /// If the entity does not contain the requested [`Component`](super::Component) `None` is
    /// returned.
    pub fn get_mut<C>(&self) -> Option<RefMut<C>>
    where
        C: Component + Clone,
    {
        self.inner.get_mut_component::<C>(&self.handle)
    }
}

/// Type alias for the signature of a component factory.
pub type ComponentFactory = dyn Fn(&Facade, &mut Deserializer) -> Result<(), ()> + Sync + Send;

lazy_static::lazy_static! {
    /// Contains the registered component factories.
    static ref FACTORIES: Arc<Mutex<HashMap<String, Arc<ComponentFactory>>>> = Arc::new(Mutex::new(HashMap::new()));
}

/// A collection of entities.
///
/// For more information see the [module level documentation](super).
pub struct Entities {
    components: RefCell<HashMap<Handle, HashMap<TypeId, Arc<dyn Component>>>>,
    inserts: Arc<Mutex<Vec<(Handle, Arc<dyn Component>)>>>,
    deletes: Arc<Mutex<Vec<(Handle, TypeId)>>>,
    next_id: AtomicU64,
}

impl Entities {
    /// Creates a new entity collection.
    pub fn new() -> Self {
        Self {
            components: RefCell::new(HashMap::new()),
            inserts: Arc::new(Mutex::new(Vec::new())),
            deletes: Arc::new(Mutex::new(Vec::new())),
            next_id: AtomicU64::new(0),
        }
    }

    /// Creates a new entity returning a [`Facade`](Facade) for the entity.
    ///
    /// The entity is created with an id based handle.
    ///
    /// Note that this does not actually create an entity, but rather a proxy through which
    /// [`components`](super::Component) associated with the handle can be accessed or modified.
    ///
    /// For more information see the [module level documentation](super).
    pub fn create(&self) -> Facade {
        Facade {
            handle: Handle::Id(self.next_id.fetch_add(1, Ordering::Relaxed)),
            inner: &self,
        }
    }

    /// Creates a new entity returning a [`Facade`](Facade) for the entity.
    ///
    /// The entity is created with a name based handle.
    ///
    /// Note that this does not actually create an entity, but rather a proxy through which
    /// [`components`](super::Component) associated with the handle can be accessed or modified. In
    /// consequence, calling this function twice with the same name returns a new proxy for the same
    /// entity each time.
    ///
    /// For more information see the [module level documentation](super).
    pub fn with_name(&self, name: impl AsRef<str>) -> Facade {
        Facade {
            handle: Handle::Name(name.as_ref().into()),
            inner: &self,
        }
    }

    /// Creates a new entity from a template.
    ///
    /// The entity is created with the components defined in the template.
    ///
    /// Note that this does not actually create an entity, but rather a proxy through which
    /// [`components`](super::Component) associated with the handle can be accessed or modified.
    /// Also, the created component follow the same principle as manually created components.
    /// Meaning the [`components`](super::Component) are first visible in the next frame.
    ///
    /// For more information see the [module level documentation](super).
    pub fn from_template(&self, path: impl AsRef<Path>) -> Result<Facade, AssetError> {
        let plain_text = crate::asset::ASSETS
            .lock()
            .unwrap()
            .as_ref()
            .unwrap()
            .read::<PlainText, _>(path)?;
        let plain_text = plain_text.unwrap();
        super::serde::from_str(&self, plain_text.as_ref().unwrap().get())
            .map_err(|error| AssetError::IoError(io::Error::new(io::ErrorKind::Other, error)))
    }

    /// Removes an entity.
    ///
    /// This is just a convenience method for removing all [`Components`](super::Component)
    /// associated with the [`Handle`](Handle) of an entity.
    pub fn remove(&self, facade: &Facade) {
        let handle = facade.handle();
        if let Some(components) = self.components.borrow().get(handle) {
            self.deletes.lock().unwrap().extend(
                components
                    .keys()
                    .map(|component_type| (handle.clone(), component_type.clone())),
            );
        }
    }

    /// Gets an entity for the given [`Handle`](Handle).
    pub fn get(&self, handle: &Handle) -> Facade {
        Facade {
            handle: handle.clone(),
            inner: &self,
        }
    }

    /// Returns a list of component tuple.
    pub fn components<'a, Comps>(&'a self) -> Vec<Comps::Item>
    where
        Comps: Components<'a>,
    {
        Comps::components(&self)
    }

    /// Commits all deferred inserts and deletes.
    pub fn commit(&self) {
        let mut components = self.components.borrow_mut();
        while let Some((handle, component)) = self.inserts.lock().unwrap().pop() {
            components
                .entry(handle)
                .or_insert(HashMap::new())
                .insert((*component.clone().as_any()).type_id(), component);
        }
        while let Some((handle, type_id)) = self.deletes.lock().unwrap().pop() {
            components
                .get_mut(&handle)
                .and_then(|entity| entity.remove(&type_id));
        }
        components.retain(|_, components| !components.is_empty());
    }

    pub(super) fn factory(typename: &str) -> Option<Arc<ComponentFactory>> {
        FACTORIES.lock().unwrap().get(typename).cloned()
    }

    /// Registers a component type.
    ///
    /// This is required if a component shall be created from an entity template.
    ///
    /// For more information see the [module level documentation](super).
    pub fn register<C: Component>() {
        C::register(&mut FACTORIES.lock().unwrap());
    }

    /// Inserts a `component` for the entity behind this `handle`.
    fn insert_component<C>(&self, handle: &Handle, component: C)
    where
        C: Component,
    {
        self.inserts
            .lock()
            .unwrap()
            .push((handle.clone(), Arc::new(component)))
    }

    /// Removes a component of type `C` for the entity behind this `handle`.
    fn remove_component<C>(&self, handle: &Handle)
    where
        C: Component,
    {
        self.deletes
            .lock()
            .unwrap()
            .push((handle.clone(), TypeId::of::<C>()));
    }

    fn get_component<C>(&self, handle: &Handle) -> Option<Ref<C>>
    where
        C: Component,
    {
        self._get_component(handle).map(Ref::from)
    }

    fn get_mut_component<'a, C>(&'a self, handle: &'a Handle) -> Option<RefMut<C>>
    where
        C: Component + Clone,
    {
        self._get_component::<C>(handle)
            .map(|component| RefMut::new(handle, &self, (*component).clone()))
    }

    fn _get_component<C>(&self, handle: &Handle) -> Option<Arc<C>>
    where
        C: Component,
    {
        self.components
            .borrow()
            .get(handle)
            .and_then(|components| components.get(&TypeId::of::<C>()))
            .cloned()
            .map(|component| component.as_any())
            .map(|component| component.downcast::<C>().unwrap())
    }
}

/// Helper trait to allow iterating over all entities that have a certain set of components.
pub trait Components<'a> {
    /// The output type if the iterator.
    ///
    /// This has to be a tuple (facade, components...).
    type Item;

    /// Returns an iterator over (facade, components...).
    fn components(entities: &'a Entities) -> Vec<Self::Item> {
        entities
            .components
            .borrow()
            .keys()
            .map(|handle| entities.get(handle))
            .filter(Self::filter)
            .map(|entity| Self::to_item(entity, &entities))
            .collect()
    }

    /// Checks if the given components contain all components for `Self::Item`.
    fn filter(entity: &Facade<'a>) -> bool;

    /// Constructs the `Self::Item`.
    fn to_item(entity: Facade<'a>, entities: &'a Entities) -> Self::Item;
}

macro_rules! impl_components_tuple {
    ($($name:ident)+) => {
        impl<'a, $($name: Component),+> Components<'a> for ($($name,)+) {
            type Item = (Facade<'a>, $(Ref<'a, $name>,)+);

            fn filter(entity: &Facade<'a>) -> bool {
                $(entity.get::<$name>().is_some())&&+
            }

            fn to_item(entity: Facade<'a>, entities: &'a Entities) -> Self::Item {
                (entity.clone(), $(entities.get_component::<$name>(entity.handle()).unwrap(),)+)
            }
        }
    };
}

impl_components_tuple! { A }
impl_components_tuple! { A B }
impl_components_tuple! { A B C }
impl_components_tuple! { A B C D }
impl_components_tuple! { A B C D E }
impl_components_tuple! { A B C D E F }
impl_components_tuple! { A B C D E F G }
impl_components_tuple! { A B C D E F G H }
impl_components_tuple! { A B C D E F G H I }
impl_components_tuple! { A B C D E F G H I J }
impl_components_tuple! { A B C D E F G H I J K }
impl_components_tuple! { A B C D E F G H I J K L }

#[cfg(test)]
mod tests {
    use rayon::prelude::*;
    use serde::Deserialize;

    use crate::asset::Assets;
    use crate::ecs::{Entities, Handle};
    use crate::Component;

    #[derive(Component, Deserialize, Eq, PartialEq, Clone, Debug)]
    struct Text {
        inner: String,
    }

    #[test]
    fn test_create() {
        let entities = Entities::new();
        let entity = entities.create();
        assert!(matches!(entity.handle(), Handle::Id(_)));
    }

    #[test]
    fn test_with_name() {
        let entities = Entities::new();
        let entity = entities.with_name("test");
        assert!(matches!(entity.handle(), Handle::Name(name) if name == "test"));
    }

    #[test]
    fn test_from_template() {
        Entities::register::<Text>();

        let _assets = Assets::new_dummy("rsc/test");
        let entities = Entities::new();
        let entity = entities.from_template("ecs/text.ent").unwrap();
        entities.commit();
        let text = entity.get::<Text>().unwrap();
        assert_eq!(text.inner, "Some test text.");
    }

    #[test]
    fn test_remove() {
        let entities = Entities::new();
        let entity = entities.create();
        entity.insert(Text {
            inner: "test".into(),
        });
        entities.commit();
        assert!(matches!(entity.get::<Text>(), Some(component) if component.inner == "test"));
        entities.remove(&entity);
        assert!(matches!(entity.get::<Text>(), Some(component) if component.inner == "test"));
        entities.commit();
        assert_eq!(entity.get::<Text>(), None);
    }

    #[test]
    fn test_get() {
        let entities = Entities::new();
        let entity = entities.with_name("test");
        let result = entities.get(entity.handle());
        assert_eq!(result.handle(), entity.handle());
    }

    #[test]
    fn test_iterate_over_entities() {
        let entities = Entities::new();
        let entity = entities.create();
        entity.insert(Text {
            inner: "a component".into(),
        });
        let other = entities.create();
        other.insert(Text {
            inner: "another component".into(),
        });
        entities.commit();
        entities
            .components::<(Text,)>()
            .par_iter()
            .for_each(|(entity, text)| {
                if text.inner == "a component" {
                    entity.get_mut::<Text>().unwrap().inner = "foo".to_string();
                }
            });
        entities.commit();
        assert!(matches!(entity.get::<Text>(), Some(component) if component.inner == "foo"));
        assert!(
            matches!(other.get::<Text>(), Some(component) if component.inner == "another component")
        );
    }

    #[test]
    fn test_insert_component() {
        let entities = Entities::new();
        let entity = entities.create();
        entity.insert(Text {
            inner: "text".to_string(),
        });
        assert_eq!(entity.get::<Text>(), None);
        entities.commit();
        assert!(
            matches!(entity.get::<Text>(), Some(component) if component.inner == "text".to_string())
        );
    }

    #[test]
    fn test_remove_component() {
        let entities = Entities::new();
        let entity = entities.create();
        entity.insert(Text {
            inner: "text".to_string(),
        });
        entities.commit();
        entity.remove::<Text>();
        assert!(
            matches!(entity.get::<Text>(), Some(component) if component.inner == "text".to_string())
        );
        entities.commit();
        assert_eq!(entity.get::<Text>(), None);
    }

    #[test]
    fn test_get_component() {
        let entities = Entities::new();
        let entity = entities.create();
        entity.insert(Text {
            inner: "text".to_string(),
        });
        entities.commit();
        assert!(
            matches!(entity.get::<Text>(), Some(component) if component.inner == "text".to_string())
        );
    }

    #[test]
    fn test_get_component_none() {
        let entities = Entities::new();
        let entity = entities.create();
        assert_eq!(entity.get::<Text>(), None);
    }

    #[test]
    fn test_get_mut_component() {
        let entities = Entities::new();
        let entity = entities.create();
        entity.insert(Text {
            inner: "text".to_string(),
        });
        entities.commit();
        {
            let text = entity.get_mut::<Text>();
            assert!(matches!(text.as_ref(), Some(component) if component.inner == "text"));
            let mut text = text.unwrap();
            text.inner = "another text".to_string();
        }
        entities.commit();
        assert!(
            matches!(entity.get::<Text>(), Some(component) if component.inner == "another text")
        );
    }

    #[test]
    fn test_get_mut_component_none() {
        let entities = Entities::new();
        let entity = entities.create();
        assert_eq!(entity.get_mut::<Text>(), None);
    }
}
