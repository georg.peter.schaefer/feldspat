pub use self::parser::*;

#[derive(Debug)]
pub struct EntityStmt {
    pub components: Vec<ComponentExpr>,
}

#[derive(Debug)]
pub struct ComponentExpr {
    pub strct: StructExpr,
}

#[derive(Debug)]
pub enum Expr {
    StructExpr(StructExpr),
    LiteralExpr(LiteralExpr),
    ListExpr(ListExpr),
}

#[derive(Debug)]
pub struct StructExpr {
    pub ident: String,
    pub fields: Vec<StructExprField>,
}

#[derive(Debug)]
pub struct StructExprField {
    pub ident: String,
    pub expr: Expr,
}

#[derive(Debug)]
pub enum LiteralExpr {
    StringLiteral(String),
    BooleanLiteral(bool),
    IntegerLiteral(IntegerLiteral),
    FloatLiteral(FloatLiteral),
    VectorLiteral(VectorLiteral),
}

#[derive(Debug)]
pub struct IntegerLiteral {
    pub literal: String,
    pub suffix: Option<IntegerSuffix>,
}

impl IntegerLiteral {
    pub fn is_i8(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::I8)
    }

    pub fn is_i16(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::I16)
    }

    pub fn is_i32(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::I32)
    }

    pub fn is_i64(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::I64)
    }

    pub fn is_i128(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::I128)
    }

    pub fn is_isize(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::Isize)
    }

    pub fn is_u8(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::U8)
    }

    pub fn is_u16(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::U16)
    }

    pub fn is_u32(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::U32)
    }

    pub fn is_u64(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::U64)
    }

    pub fn is_u128(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::U128)
    }

    pub fn is_usize(&self) -> bool {
        self.suffix == None || self.suffix == Some(IntegerSuffix::Usize)
    }
}

#[derive(Eq, PartialEq, Debug)]
pub enum IntegerSuffix {
    U8,
    U16,
    U32,
    U64,
    U128,
    Usize,
    I8,
    I16,
    I32,
    I64,
    I128,
    Isize,
}

#[derive(Debug)]
pub struct FloatLiteral {
    pub literal: String,
    pub suffix: Option<FloatSuffix>,
}

impl FloatLiteral {
    pub fn is_f32(&self) -> bool {
        self.suffix == None || self.suffix == Some(FloatSuffix::F32)
    }

    pub fn is_f64(&self) -> bool {
        self.suffix == None || self.suffix == Some(FloatSuffix::F64)
    }
}

#[derive(Eq, PartialEq, Debug)]
pub enum FloatSuffix {
    F32,
    F64,
}

#[derive(Debug)]
pub enum VectorLiteral {
    Vec2(Vec<FloatLiteral>),
    Vec3(Vec<FloatLiteral>),
    Vec4(Vec<FloatLiteral>),
    Quat(Vec<FloatLiteral>),
}

#[derive(Debug)]
pub struct ListExpr {
    pub list: Vec<Expr>,
}

lalrpop_mod!(pub parser, "/ecs/parser.rs");

#[cfg(test)]
mod test {
    use crate::ecs::parser::{
        EntityStmt, Expr, FloatLiteral, FloatSuffix, IntegerLiteral, IntegerSuffix, LiteralExpr,
        StructExpr, VectorLiteral,
    };

    use super::EntityStmtParser;

    #[test]
    fn test_parse() {
        let desc = std::fs::read_to_string("rsc/test/ecs/full.ent").unwrap();
        let entity: EntityStmt = EntityStmtParser::new().parse(&desc).unwrap();

        assert_eq!(entity.components.len(), 5);

        let transform = &entity.components[0].strct;
        assert_eq!(transform.ident, "Transform");
        assert_eq!(transform.fields.len(), 3);

        let position = &transform.fields[0];
        assert_eq!(position.ident, "position");

        let components = assert_vec3_literal(&position.expr);
        let x = &components[0];
        assert_eq!(x.literal, "10.0");
        assert_eq!(x.suffix, None);
        let y = &components[1];
        assert_eq!(y.literal, "-1");
        assert_eq!(y.suffix, Some(FloatSuffix::F32));
        let z = &components[2];
        assert_eq!(z.literal, "19.123");
        assert_eq!(z.suffix, Some(FloatSuffix::F32));

        let orientation = &transform.fields[1];
        assert_eq!(orientation.ident, "orientation");

        let components = assert_quat_literal(&orientation.expr);
        let x = &components[0];
        assert_eq!(x.literal, "1.0");
        assert_eq!(x.suffix, None);
        let y = &components[1];
        assert_eq!(y.literal, "2.0");
        assert_eq!(y.suffix, None);
        let z = &components[2];
        assert_eq!(z.literal, "3.0");
        assert_eq!(z.suffix, None);
        let w = &components[3];
        assert_eq!(w.literal, "4.0");
        assert_eq!(w.suffix, None);

        let scale = &transform.fields[2];
        assert_eq!(scale.ident, "scale");

        let components = assert_vec3_literal(&scale.expr);
        let x = &components[0];
        assert_eq!(x.literal, "1");
        assert_eq!(x.suffix, Some(FloatSuffix::F32));

        let static_mesh = &entity.components[1].strct;
        assert_eq!(static_mesh.ident, "StaticMesh");
        assert_eq!(static_mesh.fields.len(), 1);

        let path = &static_mesh.fields[0];
        assert_eq!(path.ident, "mesh");
        assert_eq!(assert_string_literal(&path.expr), "asset/text");

        let rigid_body = &entity.components[2].strct;
        assert_eq!(rigid_body.ident, "RigidBody");
        assert_eq!(rigid_body.fields.len(), 2);

        let shape = &rigid_body.fields[0];
        assert_eq!(shape.ident, "shape");

        let shape = assert_struct_expr(&shape.expr);
        assert_eq!(shape.ident, "Cylinder");

        let radius = &shape.fields[0];
        assert_eq!(radius.ident, "radius");

        let radius = assert_float_literal(&radius.expr);
        assert_eq!(radius.literal, "1.5");
        assert_eq!(radius.suffix, Some(FloatSuffix::F32));

        let height = &shape.fields[1];
        assert_eq!(height.ident, "height");

        let height = assert_float_literal(&height.expr);
        assert_eq!(height.literal, "1.0");
        assert_eq!(height.suffix, None);

        let active = &rigid_body.fields[1];
        assert_eq!(active.ident, "active");
        assert_eq!(assert_boolean_literal(&active.expr), true);

        let health = &entity.components[3].strct;
        assert_eq!(health.ident, "Health");
        assert_eq!(health.fields.len(), 1);

        let value = &health.fields[0];
        assert_eq!(value.ident, "health");

        let value = assert_integer_literal(&value.expr);
        assert_eq!(value.literal, "1000");
        assert_eq!(value.suffix, None);

        let damage = &entity.components[4].strct;
        assert_eq!(damage.ident, "Damage");
        assert_eq!(damage.fields.len(), 1);

        let value = &damage.fields[0];
        assert_eq!(value.ident, "damage");

        let value = assert_integer_literal(&value.expr);
        assert_eq!(value.literal, "10");
        assert_eq!(value.suffix, Some(IntegerSuffix::I64));
    }

    fn assert_vec3_literal(expr: &Expr) -> &Vec<FloatLiteral> {
        match expr {
            Expr::LiteralExpr(LiteralExpr::VectorLiteral(VectorLiteral::Vec3(components))) => {
                components
            }
            _ => panic!("expected vec3 literal"),
        }
    }

    fn assert_quat_literal(expr: &Expr) -> &Vec<FloatLiteral> {
        match expr {
            Expr::LiteralExpr(LiteralExpr::VectorLiteral(VectorLiteral::Quat(components))) => {
                components
            }
            _ => panic!("expected quat literal"),
        }
    }

    fn assert_string_literal(expr: &Expr) -> &String {
        match expr {
            Expr::LiteralExpr(LiteralExpr::StringLiteral(string)) => string,
            _ => panic!("expected string literal"),
        }
    }

    fn assert_struct_expr(expr: &Expr) -> &StructExpr {
        match expr {
            Expr::StructExpr(strct) => strct,
            _ => panic!("expected struct expression"),
        }
    }

    fn assert_float_literal(expr: &Expr) -> &FloatLiteral {
        match expr {
            Expr::LiteralExpr(LiteralExpr::FloatLiteral(float)) => float,
            _ => panic!("expected float literal"),
        }
    }

    fn assert_boolean_literal(expr: &Expr) -> bool {
        match expr {
            Expr::LiteralExpr(LiteralExpr::BooleanLiteral(boolean)) => *boolean,
            _ => panic!("expected boolean literal"),
        }
    }

    fn assert_integer_literal(expr: &Expr) -> &IntegerLiteral {
        match expr {
            Expr::LiteralExpr(LiteralExpr::IntegerLiteral(integer)) => integer,
            _ => panic!("expected integer literal"),
        }
    }
}
