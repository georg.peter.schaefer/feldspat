use serde::Deserialize;

use crate::math::{Mat4, Quat, Vec3, Vec4};
use crate::Component;

/// Spatial information for an entity.
#[derive(Component, Deserialize, Copy, Clone)]
pub struct Transform {
    position: Vec3,
    orientation: Quat,
    scale: Vec3,
}

impl Transform {
    /// Creates a new empty transform.
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    /// Sets the position.
    pub fn with_position(mut self, position: Vec3) -> Self {
        self.position = position;
        self
    }

    /// Sets the orientation.
    pub fn with_orientation(mut self, orientation: Quat) -> Self {
        self.orientation = orientation;
        self
    }

    /// Sets the scale.
    pub fn with_scale(mut self, scale: Vec3) -> Self {
        self.scale = scale;
        self
    }

    /// Returns the position.
    pub fn position(&self) -> Vec3 {
        self.position
    }

    /// Returns the orientation.
    pub fn orientation(&self) -> Quat {
        self.orientation
    }

    /// Returns the scale.
    pub fn scale(&self) -> Vec3 {
        self.scale
    }

    /// Calculates the model matrix from the transforms position, orientation and scale.
    pub fn model(&self) -> Mat4 {
        let scale = Mat4::from([
            Vec4::new(self.scale[0], 0.0, 0.0, 0.0),
            Vec4::new(0.0, self.scale[1], 0.0, 0.0),
            Vec4::new(0.0, 0.0, self.scale[2], 0.0),
            Vec4::new(0.0, 0.0, 0.0, 1.0),
        ]);
        let rotation = Mat4::from(self.orientation);
        let mut model = rotation * scale;

        model[3][0] = self.position[0];
        model[3][1] = self.position[1];
        model[3][2] = self.position[2];
        model
    }
}

impl Default for Transform {
    fn default() -> Self {
        Self {
            position: Default::default(),
            orientation: Default::default(),
            scale: Vec3::new(1.0, 1.0, 1.0),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::ecs::Transform;
    use crate::math::{Mat4, Quat, Vec3, Vec4};

    #[test]
    fn build() {
        let transform = create_transform();

        assert_relative_eq!(transform.position, Vec3::new(1.0, 2.0, 3.0));
        assert_relative_eq!(
            transform.orientation,
            Quat::from_angle_axis(45f32.to_radians(), Vec3::new(1.0, 0.0, 0.0))
        );
        assert_relative_eq!(transform.scale, Vec3::new(1.5, 1.5, 1.5));
    }

    #[test]
    fn model() {
        let transform = create_transform();

        assert_relative_eq!(
            transform.model(),
            Mat4::from([
                Vec4::new(1.5, 0.0, 0.0, 0.0),
                Vec4::new(0.0, 1.0606602, 1.0606602, 0.0),
                Vec4::new(0.0, -1.0606602, 1.0606602, 0.0),
                Vec4::new(1.0, 2.0, 3.0, 1.0)
            ])
        );
    }

    fn create_transform() -> Transform {
        Transform::new()
            .with_position(Vec3::new(1.0, 2.0, 3.0))
            .with_orientation(Quat::from_angle_axis(
                45f32.to_radians(),
                Vec3::new(1.0, 0.0, 0.0),
            ))
            .with_scale(Vec3::new(1.5, 1.5, 1.5))
    }
}
