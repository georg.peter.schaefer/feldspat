use core::fmt;
use std::error;
use std::fmt::{Display, Formatter};
use std::num::{ParseFloatError, ParseIntError};
use std::str::FromStr;

use serde::de::{DeserializeSeed, MapAccess, SeqAccess, Visitor};
use serde::{de, ser};

use crate::ecs::parser::{
    ComponentExpr, EntityStmt, Expr, FloatLiteral, ListExpr, LiteralExpr, StructExprField,
    VectorLiteral,
};
use crate::ecs::parser::{EntityStmtParser, StructExpr};
use crate::ecs::{Entities, Facade};
use std::slice::Iter;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug, PartialEq)]
pub enum Error {
    Message(String),
    ExpectedStructExpr,
    ExpectedStructExprField,
    ExpectedStringLiteral,
    ExpectedBooleanLiteral,
    ExpectedI8Literal,
    ExpectedI16Literal,
    ExpectedI32Literal,
    ExpectedI64Literal,
    ExpectedI128Literal,
    ExpectedIsizeLiteral,
    ExpectedU8Literal,
    ExpectedU16Literal,
    ExpectedU32Literal,
    ExpectedU64Literal,
    ExpectedU128Literal,
    ExpectedUsizeLiteral,
    ExpectedF32Literal,
    ExpectedF64Literal,
    ExpectedListExpr,
}

impl ser::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Error::Message(msg.to_string())
    }
}

impl de::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Error::Message(msg.to_string())
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Error::Message(msg) => f.write_str(msg),
            Error::ExpectedStructExpr => f.write_str("expected struct expr"),
            Error::ExpectedStructExprField => f.write_str("expected struct expr field"),
            Error::ExpectedStringLiteral => f.write_str("expected string literal"),
            Error::ExpectedBooleanLiteral => f.write_str("expected boolean literal"),
            Error::ExpectedI8Literal => f.write_str("expected i8 literal"),
            Error::ExpectedI16Literal => f.write_str("expected i8 literal"),
            Error::ExpectedI32Literal => f.write_str("expected i16 literal"),
            Error::ExpectedI64Literal => f.write_str("expected i32 literal"),
            Error::ExpectedI128Literal => f.write_str("expected i64 literal"),
            Error::ExpectedIsizeLiteral => f.write_str("expected i128 literal"),
            Error::ExpectedU8Literal => f.write_str("expected u8 literal"),
            Error::ExpectedU16Literal => f.write_str("expected u16 literal"),
            Error::ExpectedU32Literal => f.write_str("expected u32 literal"),
            Error::ExpectedU64Literal => f.write_str("expected u64 literal"),
            Error::ExpectedU128Literal => f.write_str("expected u128 literal"),
            Error::ExpectedUsizeLiteral => f.write_str("expected usize literal"),
            Error::ExpectedF32Literal => f.write_str("expected f32 literal"),
            Error::ExpectedF64Literal => f.write_str("expected f64 literal"),
            Error::ExpectedListExpr => f.write_str("expected list expression"),
        }
    }
}

impl error::Error for Error {}

impl From<ParseIntError> for Error {
    fn from(error: ParseIntError) -> Self {
        Error::Message(error.to_string())
    }
}

impl From<ParseFloatError> for Error {
    fn from(error: ParseFloatError) -> Self {
        Error::Message(error.to_string())
    }
}

enum Item<'a> {
    StructExpr(&'a StructExpr),
    StructExprField(&'a StructExprField),
    Expr(&'a Expr),
    FloatLiteral(&'a FloatLiteral),
}

/// Deserializer for component expressions from entity templates.
pub struct Deserializer<'de> {
    hierarchy: Vec<Item<'de>>,
}

impl<'de> Deserializer<'de> {
    /// Creates a new deserializer for the `component_expr`.
    fn from_component_expr(component_expr: &'de ComponentExpr) -> Self {
        Deserializer {
            hierarchy: vec![Item::StructExpr(&component_expr.strct)],
        }
    }
}

pub fn from_str<'a, 'b>(entities: &'a Entities, input: &'b str) -> Result<Facade<'a>> {
    let parser = EntityStmtParser::new();
    let entity_stmt: EntityStmt = parser.parse(input).unwrap();
    let entity = entities.create();
    for component_expr in &entity_stmt.components {
        let mut deserializer = Deserializer::from_component_expr(component_expr);
        let ident = &component_expr.strct.ident;
        let factory = Entities::factory(ident).unwrap();
        factory(&entity, &mut deserializer).unwrap();
    }
    Ok(entity)
}

impl<'de, 'a> de::Deserializer<'de> for &'a mut Deserializer<'de> {
    type Error = Error;

    fn deserialize_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::BooleanLiteral(boolean)))) => {
                visitor.visit_bool(*boolean)
            }
            _ => Err(Error::ExpectedBooleanLiteral),
        }
    }

    fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_i8() =>
            {
                visitor.visit_i8(i8::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedI8Literal),
        }
    }

    fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_i16() =>
            {
                visitor.visit_i16(i16::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedI16Literal),
        }
    }

    fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_i32() =>
            {
                visitor.visit_i32(i32::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedI32Literal),
        }
    }

    fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_i64() || literal.is_isize() =>
            {
                visitor.visit_i64(i64::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedI64Literal),
        }
    }

    fn deserialize_i128<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_i128() =>
            {
                visitor.visit_i128(i128::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedI64Literal),
        }
    }

    fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_u8() =>
            {
                visitor.visit_u8(u8::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedU8Literal),
        }
    }

    fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_u16() =>
            {
                visitor.visit_u16(u16::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedU16Literal),
        }
    }

    fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_u32() =>
            {
                visitor.visit_u32(u32::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedU32Literal),
        }
    }

    fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_u64() || literal.is_usize() =>
            {
                visitor.visit_u64(u64::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedU32Literal),
        }
    }

    fn deserialize_u128<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::IntegerLiteral(literal))))
                if literal.is_u128() =>
            {
                visitor.visit_u128(u128::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedI64Literal),
        }
    }

    fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::FloatLiteral(literal))))
            | Some(Item::FloatLiteral(literal))
                if literal.is_f32() =>
            {
                visitor.visit_f32(f32::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedF32Literal),
        }
    }

    fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::FloatLiteral(literal))))
            | Some(Item::FloatLiteral(literal))
                if literal.is_f64() =>
            {
                visitor.visit_f64(f64::from_str(&literal.literal)?)
            }
            _ => Err(Error::ExpectedF64Literal),
        }
    }

    fn deserialize_char<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_str<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::StringLiteral(string)))) => {
                visitor.visit_str(&string)
            }
            _ => Err(Error::ExpectedStringLiteral),
        }
    }

    fn deserialize_string<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::LiteralExpr(LiteralExpr::StringLiteral(string)))) => {
                visitor.visit_string(string.clone())
            }
            _ => Err(Error::ExpectedStringLiteral),
        }
    }

    fn deserialize_bytes<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_byte_buf<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_option<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit_struct<V>(self, _name: &'static str, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_newtype_struct<V>(self, _name: &'static str, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.hierarchy.pop() {
            Some(Item::Expr(Expr::ListExpr(ListExpr { list }))) => {
                visitor.visit_seq(List::new(&mut *self, list.iter()))
            }
            _ => Err(Error::ExpectedListExpr),
        }
    }

    fn deserialize_tuple<V>(self, _len: usize, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_tuple_struct<V>(
        self,
        _name: &'static str,
        _len: usize,
        _visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_map<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_struct<V>(
        self,
        _name: &'static str,
        _fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        return match self.hierarchy.pop().unwrap() {
            Item::StructExpr(struct_expr) | Item::Expr(Expr::StructExpr(struct_expr)) => {
                self.hierarchy.extend(
                    struct_expr
                        .fields
                        .iter()
                        .map(|field| Item::StructExprField(field))
                        .rev(),
                );
                visitor.visit_map(Fields::new(&mut *self, struct_expr.fields.len()))
            }
            Item::Expr(Expr::LiteralExpr(LiteralExpr::VectorLiteral(literal))) => match literal {
                VectorLiteral::Vec2(components)
                | VectorLiteral::Vec3(components)
                | VectorLiteral::Vec4(components)
                | VectorLiteral::Quat(components) => {
                    self.hierarchy.extend(
                        components
                            .iter()
                            .map(|component| Item::FloatLiteral(component))
                            .rev(),
                    );
                    visitor.visit_seq(VectorComponents::new(&mut *self))
                }
            },
            _ => Err(Error::ExpectedStructExpr),
        };
    }

    fn deserialize_enum<V>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        _visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_identifier<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        if let Some(Item::StructExprField(field)) = self.hierarchy.last() {
            return visitor.visit_string(field.ident.clone());
        }
        Err(Error::ExpectedStructExprField)
    }

    fn deserialize_ignored_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }
}

struct Fields<'a, 'de> {
    de: &'a mut Deserializer<'de>,
    count: usize,
}

impl<'a, 'de> Fields<'a, 'de> {
    fn new(de: &'a mut Deserializer<'de>, count: usize) -> Self {
        Self { de, count }
    }
}

impl<'de, 'a> MapAccess<'de> for Fields<'a, 'de> {
    type Error = Error;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
    where
        K: DeserializeSeed<'de>,
    {
        if self.count == 0 {
            return Ok(None);
        }

        if let Some(Item::StructExprField(_)) = self.de.hierarchy.last() {
            self.count -= 1;
            return seed.deserialize(&mut *self.de).map(Some);
        }
        Ok(None)
    }

    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
    where
        V: DeserializeSeed<'de>,
    {
        if let Some(Item::StructExprField(field)) = self.de.hierarchy.pop() {
            self.de.hierarchy.push(Item::Expr(&field.expr));
            return seed.deserialize(&mut *self.de);
        }
        Err(Error::ExpectedStructExprField)
    }
}

struct VectorComponents<'a, 'de> {
    de: &'a mut Deserializer<'de>,
}

impl<'a, 'de> VectorComponents<'a, 'de> {
    fn new(de: &'a mut Deserializer<'de>) -> Self {
        Self { de }
    }
}

impl<'de, 'a> SeqAccess<'de> for VectorComponents<'a, 'de> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        return if let Some(Item::FloatLiteral(_)) = self.de.hierarchy.last() {
            seed.deserialize(&mut *self.de).map(Some)
        } else {
            Ok(None)
        };
    }
}

struct List<'a, 'de> {
    de: &'a mut Deserializer<'de>,
    elements: Iter<'de, Expr>,
}

impl<'a, 'de> List<'a, 'de> {
    fn new(de: &'a mut Deserializer<'de>, elements: Iter<'de, Expr>) -> Self {
        Self { de, elements }
    }
}

impl<'de, 'a> SeqAccess<'de> for List<'a, 'de> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        if let Some(expr) = self.elements.next() {
            self.de.hierarchy.push(Item::Expr(expr));
            seed.deserialize(&mut *self.de).map(Some)
        } else {
            Ok(None)
        }
    }
}

// Test fails like every second run with no apparent reason.
// #[cfg(test)]
// mod tests {
//     use std::io::Error;
//     use std::path::Path;
//
//     use serde::Deserialize;
//
//     use crate::asset::{Asset, AssetRef, Assets};
//     use crate::ecs::serde::from_str;
//     use crate::ecs::Entities;
//     use crate::engine::Engine;
//     use crate::math::{Quat, Vec3};
//     use crate::Component;
//     use std::sync::Arc;
//
//     struct Mesh {
//         inner: String,
//     }
//
//     impl Asset for Mesh {
//         fn try_from_bytes(
//             _engine: Arc<Engine>,
//             _path: &Path,
//             bytes: Vec<u8>,
//         ) -> Result<Self, Error> {
//             Ok(Mesh {
//                 inner: String::from_utf8(bytes).unwrap(),
//             })
//         }
//     }
//
//     #[derive(Component, Deserialize)]
//     struct Transform {
//         position: Vec3,
//         orientation: Quat,
//         scale: Vec3,
//     }
//
//     #[derive(Component, Deserialize)]
//     struct StaticMesh {
//         mesh: AssetRef<Mesh>,
//     }
//
//     #[derive(Component, Deserialize)]
//     struct RigidBody {
//         shape: Cylinder,
//         active: bool,
//     }
//
//     #[derive(Deserialize)]
//     struct Cylinder {
//         radius: f32,
//         height: f32,
//     }
//
//     #[derive(Component, Deserialize)]
//     struct Health {
//         health: u32,
//     }
//
//     #[derive(Component, Deserialize)]
//     struct Damage {
//         damage: i64,
//     }
//
//     #[test]
//     fn test_deserialize() {
//         Entities::register::<Transform>();
//         Entities::register::<StaticMesh>();
//         Entities::register::<RigidBody>();
//         Entities::register::<Health>();
//         Entities::register::<Damage>();
//
//         let assets = Assets::new("rsc/test/");
//         let _engine = Engine::create_dummy(&assets);
//         let entities = Entities::new(assets);
//         let input = std::fs::read_to_string("rsc/test/ecs/full.ent").unwrap();
//         let entity = from_str(&entities, &input).unwrap();
//
//         entities.commit();
//
//         let transform = entity.get::<Transform>().unwrap();
//         assert_relative_eq!(transform.position, Vec3::new(10.0, -1.0, 19.123));
//         assert_relative_eq!(transform.orientation, Quat::new(1.0, 2.0, 3.0, 4.0));
//         assert_relative_eq!(transform.scale, Vec3::new(1.0, 1.0, 1.0));
//
//         let static_mesh = entity.get::<StaticMesh>().unwrap();
//         assert_eq!(
//             static_mesh.mesh.unwrap().as_ref().unwrap().inner,
//             "Some test text."
//         );
//
//         let rigid_body = entity.get::<RigidBody>().unwrap();
//         assert_relative_eq!(rigid_body.shape.radius, 1.5);
//         assert_relative_eq!(rigid_body.shape.height, 1.0);
//         assert_eq!(rigid_body.active, true);
//
//         let health = entity.get::<Health>().unwrap();
//         assert_eq!(health.health, 1000);
//
//         let damage = entity.get::<Damage>().unwrap();
//         assert_eq!(damage.damage, 10);
//     }
// }
