use crate::ecs::Entities;

/// Trait for a system that handles entities.
///
/// To create a new system this trait has to be implemented and the system needs to be registered
/// with the engine. For more information see
/// [`Engine::register_system`](super::super::core::Engine::register_system).
pub trait System: Send + Sync + 'static {
    /// Updates the system.
    ///
    /// This function is called by the main loop of the engine. This is the place where a system
    /// applies it's logic to entities that have the components required by the system.
    fn update(&self, entities: &Entities);
}
