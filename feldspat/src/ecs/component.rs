use std::any::Any;
use std::fmt::Debug;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::sync::Arc;

use crate::ecs::entity::ComponentFactory;
use crate::ecs::{Entities, Handle};
use std::collections::HashMap;

/// Wrapper type for an immutably borrowed component from an entity.
///
/// See the [module level documentation](super) for more information.
#[derive(Eq, PartialEq, Debug)]
pub struct Ref<'a, C>
where
    C: Component,
{
    inner: Arc<C>,
    _phantom_data: PhantomData<&'a C>,
}

impl<'a, C> From<Arc<C>> for Ref<'a, C>
where
    C: Component,
{
    fn from(ptr: Arc<C>) -> Self {
        Self {
            inner: ptr,
            _phantom_data: Default::default(),
        }
    }
}

impl<'a, C> Deref for Ref<'a, C>
where
    C: Component,
{
    type Target = C;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

/// Wrapper type for an mutably borrowed component from an entity.
///
/// See the [module level documentation](super) for more information.
pub struct RefMut<'a, C>
where
    C: Component + Clone,
{
    handle: &'a Handle,
    entities: &'a Entities,
    component: C,
}

impl<'a, C> RefMut<'a, C>
where
    C: Component + Clone,
{
    /// Creates a new mutable component reference.
    pub(crate) fn new(handle: &'a Handle, entities: &'a Entities, component: C) -> Self {
        Self {
            handle,
            entities,
            component,
        }
    }
}

impl<'a, C> Deref for RefMut<'a, C>
where
    C: Component + Clone,
{
    type Target = C;

    fn deref(&self) -> &Self::Target {
        &self.component
    }
}

impl<'a, C> DerefMut for RefMut<'a, C>
where
    C: Component + Clone,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.component
    }
}

impl<'a, C> Drop for RefMut<'a, C>
where
    C: Component + Clone,
{
    fn drop(&mut self) {
        let entity = self.entities.get(self.handle);
        entity.insert(self.component.clone())
    }
}

#[cfg(test)]
impl<'a, C> PartialEq for RefMut<'a, C>
where
    C: Component + Clone,
{
    fn eq(&self, _: &Self) -> bool {
        true
    }
}

#[cfg(test)]
impl<'a, C> Debug for RefMut<'a, C>
where
    C: Component + Clone,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "")
    }
}

/// The component trait.
///
/// A type that implements this trait is considered a component. Components are the building blocks
/// that make up game objects.
///
/// For more information see the [module level documentation](super).
pub trait Component: Send + Sync + 'static {
    /// Returns a `Component` pointer as an `Any` pointer.
    fn as_any(self: Arc<Self>) -> Arc<dyn Any + Sync + Send>;

    /// Registers a factory to construct this component.
    fn register(factories: &mut HashMap<String, Arc<ComponentFactory>>)
    where
        Self: Sized;
}
