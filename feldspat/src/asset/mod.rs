//! Asset management facilities.

use std::any::Any;
use std::cell::UnsafeCell;
use std::collections::{HashMap, VecDeque};
use std::fmt::{Debug, Display, Formatter};
use std::io::{Error, ErrorKind};
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::path::{Path, PathBuf};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Arc, Mutex};
use std::{fmt, fs};

use notify::{Event, EventKind, RecommendedWatcher};
use serde::de::Visitor;
use serde::{de, Deserialize, Deserializer};

use crate::graphics::Device;

lazy_static::lazy_static! {
    /// Global [`Assets`](Assets) reference.
    ///
    /// This global state is required to be able to deserialize [`AssetRef`](AssetRef) from a path.
    pub (crate) static ref ASSETS: Arc<Mutex<Option<Arc<Assets>>>> = Arc::new(Mutex::new(None));
}

/// Asset error type.
pub enum AssetError {
    /// General IO error.
    IoError(std::io::Error),
    /// Asset specific error.
    Custom(Box<dyn std::error::Error + 'static>),
}

impl std::error::Error for AssetError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self {
            AssetError::IoError(e) => Some(e),
            AssetError::Custom(e) => Some(e.as_ref()),
        }
    }
}

impl Debug for AssetError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(&self, f)
    }
}

impl Display for AssetError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self {
            AssetError::IoError(e) => Display::fmt(e, f),
            AssetError::Custom(e) => Display::fmt(e, f),
        }
    }
}

impl From<std::io::Error> for AssetError {
    fn from(error: Error) -> Self {
        AssetError::IoError(error)
    }
}

/// The general asset trait.
///
/// Any type that implements this trait is considered an asset. An implementation has to provide an
/// implementation of the conversion function, that tries to construct an asset from bytes.
pub trait Asset: Sized + 'static {
    /// Tries to convert some bytes into `Self`.
    fn try_from_bytes(assets: Arc<Assets>, path: &Path, bytes: Vec<u8>)
        -> Result<Self, AssetError>;
}

/// A shared reference of an possibly loaded [`Asset`](Asset).
pub struct AssetRef<A>
where
    A: Asset,
{
    handle: Arc<Handle<A>>,
}

impl<A> Clone for AssetRef<A>
where
    A: Asset,
{
    fn clone(&self) -> Self {
        Self {
            handle: self.handle.clone(),
        }
    }
}

impl<A> AssetRef<A>
where
    A: Asset,
{
    /// Returns the [`Assets`](Asset) path.
    pub fn path(&self) -> &Path {
        self.handle.path()
    }

    /// Returns weather the asset is ready.
    pub fn is_ready(&self) -> bool {
        self.handle.try_borrow().is_ok()
    }

    /// Applies a function to the `Result` of an asset loading task (if available).
    ///
    /// For more information see the [module level documentation](super).
    pub fn map<F, E>(&self, mut func: F) -> Result<(), E>
    where
        F: FnMut(&Result<A, AssetError>) -> Result<(), E>,
    {
        if let Ok(result) = self.handle.try_borrow() {
            return func(result.deref());
        }
        Ok(())
    }

    /// Applies a function to an asset (if available).
    ///
    /// For more information see the [module level documentation](super).
    pub fn map_ok<F, E>(&self, mut func: F) -> Result<(), E>
    where
        F: FnMut(&A) -> Result<(), E>,
    {
        self.map(|result| {
            if let Ok(asset) = result {
                return func(asset);
            }
            Ok(())
        })
    }

    /// Returns the result of an asset loading task.
    ///
    /// Because this function may block until the asset is available, its use is generally
    /// discouraged. Instead, prefer to use [`map`](AssetRef::map) or [`map_ok`](AssetRef::map_ok).
    ///
    /// For more information see the [module level documentation](super).
    pub fn unwrap(&self) -> Ref<'_, A> {
        while let Err(_) = self.handle.try_borrow() {
            std::hint::spin_loop();
        }
        self.handle.try_borrow().unwrap()
    }

    /// Joins the current asset with another.
    pub fn join<R>(self, other: AssetRef<R>) -> JoinAsset<(AssetRef<A>, AssetRef<R>)>
    where
        R: Asset,
    {
        JoinAsset {
            inner: (self, other),
        }
    }
}

impl<'de, A> Deserialize<'de> for AssetRef<A>
where
    A: Asset,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(AssetRefVisitor(PhantomData))
    }
}

struct AssetRefVisitor<A>(PhantomData<A>)
where
    A: Asset;

impl<'de, A> Visitor<'de> for AssetRefVisitor<A>
where
    A: Asset,
{
    type Value = AssetRef<A>;

    fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
        formatter.write_str("AssetRef")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(ASSETS
            .lock()
            .unwrap()
            .as_ref()
            .unwrap()
            .read::<A, _>(&v)
            .unwrap())
    }
}

/// Joins some assets.
pub fn join<J>(join: J) -> JoinAsset<J> {
    JoinAsset { inner: join }
}

/// Joined assets.
pub struct JoinAsset<T> {
    inner: T,
}

impl<A0, A1> JoinAsset<(AssetRef<A0>, AssetRef<A1>)>
where
    A0: Asset,
    A1: Asset,
{
    /// Maps the joined assets with `func` if their results are available.
    pub fn map<F>(&self, mut func: F)
    where
        F: FnMut(&Result<A0, AssetError>, &Result<A1, AssetError>),
    {
        if let (Ok(result0), Ok(result1)) = (
            self.inner.0.handle.try_borrow(),
            self.inner.1.handle.try_borrow(),
        ) {
            func(result0.deref(), result1.deref());
        }
    }

    /// Maps the joined assets with `func` if thier results are available and ok.
    pub fn map_ok<F>(&self, mut func: F)
    where
        F: FnMut(&A0, &A1),
    {
        self.map(|result0, result1| {
            if let (Ok(asset0), Ok(asset1)) = (result0, result1) {
                func(asset0, asset1)
            }
        })
    }

    /// Joins the joined assets with another asset.
    pub fn join<A2>(self, a2: AssetRef<A2>) -> JoinAsset<(AssetRef<A0>, AssetRef<A1>, AssetRef<A2>)>
    where
        A2: Asset,
    {
        JoinAsset {
            inner: (self.inner.0, self.inner.1, a2.clone()),
        }
    }
}

impl<A0, A1, A2> JoinAsset<(AssetRef<A0>, AssetRef<A1>, AssetRef<A2>)>
where
    A0: Asset,
    A1: Asset,
    A2: Asset,
{
    /// Maps the joined assets with `func` if their results are available.
    pub fn map<F>(&self, mut func: F)
    where
        F: FnMut(&Result<A0, AssetError>, &Result<A1, AssetError>, &Result<A2, AssetError>),
    {
        if let (Ok(result0), Ok(result1), Ok(result2)) = (
            self.inner.0.handle.try_borrow(),
            self.inner.1.handle.try_borrow(),
            self.inner.2.handle.try_borrow(),
        ) {
            func(result0.deref(), result1.deref(), result2.deref());
        }
    }

    /// Maps the joined assets with `func` if thier results are available and ok.
    pub fn map_ok<F>(&self, mut func: F)
    where
        F: FnMut(&A0, &A1, &A2),
    {
        self.map(|result0, result1, result2| {
            if let (Ok(asset0), Ok(asset1), Ok(asset2)) = (result0, result1, result2) {
                func(asset0, asset1, asset2)
            }
        })
    }
}

impl<A0, A1, A2>
    JoinAsset<(
        Option<&AssetRef<A0>>,
        Option<&AssetRef<A1>>,
        Option<&AssetRef<A2>>,
    )>
where
    A0: Asset,
    A1: Asset,
    A2: Asset,
{
    /// Maps the joined assets with `func` if their results are available.
    pub fn map<F, E>(&self, mut func: F) -> Result<(), E>
    where
        F: FnMut(Option<Ref<A0>>, Option<Ref<A1>>, Option<Ref<A2>>) -> Result<(), E>,
    {
        let (option0, option1, option2) = (
            self.inner
                .0
                .map(|asset| &asset.handle)
                .map(|handle| handle.try_borrow())
                .map(|borrow| {
                    if let Ok(result0) = borrow {
                        Some(result0)
                    } else {
                        None
                    }
                })
                .flatten(),
            self.inner
                .1
                .map(|asset| &asset.handle)
                .map(|handle| handle.try_borrow())
                .map(|borrow| {
                    if let Ok(result0) = borrow {
                        Some(result0)
                    } else {
                        None
                    }
                })
                .flatten(),
            self.inner
                .2
                .map(|asset| &asset.handle)
                .map(|handle| handle.try_borrow())
                .map(|borrow| {
                    if let Ok(result0) = borrow {
                        Some(result0)
                    } else {
                        None
                    }
                })
                .flatten(),
        );
        func(option0, option1, option2)
    }

    /// Maps the joined assets with `func` if their results are available and ok.
    pub fn map_ok<F, E>(&self, mut func: F) -> Result<(), E>
    where
        F: FnMut(Option<&A0>, Option<&A1>, Option<&A2>) -> Result<(), E>,
    {
        self.map(|option0, option1, option2| {
            let param0 = if let Some(r0) = &option0 {
                Some(r0.deref())
            } else {
                None
            };
            let param1 = if let Some(r1) = &option1 {
                Some(r1.deref())
            } else {
                None
            };
            let param2 = if let Some(r2) = &option2 {
                Some(r2.deref())
            } else {
                None
            };

            func(
                param0
                    .map(|result| if let Ok(a0) = result { Some(a0) } else { None })
                    .flatten(),
                param1
                    .map(|result| if let Ok(a0) = result { Some(a0) } else { None })
                    .flatten(),
                param2
                    .map(|result| if let Ok(a0) = result { Some(a0) } else { None })
                    .flatten(),
            )
        })
    }
}

struct BorrowError;

impl Display for BorrowError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "already mutably borrowed")
    }
}

impl Debug for BorrowError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

struct BorrowMutError;

impl Display for BorrowMutError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "already immutably borrowed")
    }
}

impl Debug for BorrowMutError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

/// A handle to a maybe loaded asset.
///
/// This type provides thread-safe interior mutability for the result of an asset loading task.
struct Handle<A>
where
    A: Asset,
{
    path: PathBuf,
    borrows: AtomicUsize,
    inner: UnsafeCell<Option<Result<A, AssetError>>>,
}

impl<A> Handle<A>
where
    A: Asset,
{
    /// Create a new empty shared handle.
    fn new(path: PathBuf) -> Arc<Self> {
        Arc::new(Self {
            path,
            borrows: 0.into(),
            inner: UnsafeCell::new(None),
        })
    }

    /// Returns the path to this handles [`Asset](Asset).
    fn path(&self) -> &Path {
        &self.path
    }

    /// Initializes the mutable memory location.
    ///
    /// # Safety
    /// Calling this method while there is any borrow on the result is undefined behavior.
    unsafe fn init(&self, result: Result<A, AssetError>) {
        debug_assert_eq!(self.borrows.load(Ordering::Relaxed), 0);
        *self.inner.get() = Some(result);
        self.borrows.store(1, Ordering::Relaxed);
    }

    /// Tries to immutably borrow the asset.
    fn try_borrow(&self) -> Result<Ref<'_, A>, BorrowError> {
        match self
            .borrows
            .fetch_update(Ordering::Relaxed, Ordering::Relaxed, |borrows| {
                if borrows == 0 {
                    None
                } else {
                    Some(borrows + 1)
                }
            }) {
            Ok(_) => Ok(Ref { borrower: &self }),
            Err(_) => Err(BorrowError),
        }
    }

    /// Tries to mutably borrow the asset.
    fn try_borrow_mut(&self) -> Result<RefMut<'_, A>, BorrowMutError> {
        match self
            .borrows
            .compare_exchange_weak(1, 0, Ordering::Relaxed, Ordering::Relaxed)
        {
            Ok(_) => Ok(RefMut { borrower: &self }),
            Err(_) => Err(BorrowMutError),
        }
    }
}

impl<A> Reload for Handle<A>
where
    A: Asset,
{
    fn as_any(self: Arc<Self>) -> Arc<dyn Any + Send + Sync + 'static> {
        self
    }

    fn reload(self: Arc<Self>, source: Arc<dyn Source>) {
        let assets = ASSETS.lock().unwrap().clone().unwrap();
        rayon::spawn(move || {
            if let Ok(mut result) = self.try_borrow_mut() {
                println!("reloading assset {}", self.path.to_str().unwrap());
                *result = Some(
                    source
                        .read(&self.path)
                        .and_then(|bytes| A::try_from_bytes(assets, &self.path, bytes)),
                );
            }
        });
    }
}

unsafe impl<A> Send for Handle<A> where A: Asset {}

unsafe impl<A> Sync for Handle<A> where A: Asset {}

/// A wrapper type for an immutably borrowed [`Asset`](Asset).
pub struct Ref<'a, A>
where
    A: Asset,
{
    borrower: &'a Handle<A>,
}

impl<'a, A> Deref for Ref<'a, A>
where
    A: Asset,
{
    type Target = Result<A, AssetError>;

    fn deref(&self) -> &Self::Target {
        unsafe { (*self.borrower.inner.get()).as_ref() }.unwrap()
    }
}

impl<'a, A> Drop for Ref<'a, A>
where
    A: Asset,
{
    fn drop(&mut self) {
        self.borrower.borrows.fetch_sub(1, Ordering::Relaxed);
    }
}

/// A wrapper type for an mutably borrowed [`Asset`](Asset).
struct RefMut<'a, A>
where
    A: Asset,
{
    borrower: &'a Handle<A>,
}

impl<'a, A> Deref for RefMut<'a, A>
where
    A: Asset,
{
    type Target = Option<Result<A, AssetError>>;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.borrower.inner.get() }
    }
}

impl<'a, A> DerefMut for RefMut<'a, A>
where
    A: Asset,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.borrower.inner.get() }
    }
}

impl<'a, A> Drop for RefMut<'a, A>
where
    A: Asset,
{
    fn drop(&mut self) {
        self.borrower.borrows.store(1, Ordering::Relaxed);
    }
}

/// The reload operation for an [asset handle](Handle).
trait Reload: Send + Sync {
    /// Returns an [asset handle](Handle) as [`Any`](std::any::Any).
    ///
    /// This is required to be able to downcast the trait object as the concrete [`Handle`](Handle)
    /// for an asset.
    fn as_any(self: Arc<Self>) -> Arc<dyn Any + Send + Sync + 'static>;

    /// Reloads an asset from the given source.
    fn reload(self: Arc<Self>, source: Arc<dyn Source>);
}

/// A source from which an [`Asset`](Asset) can be read.
///
/// Assets can be read from a custom source by implementing the trait and registering it with the
/// [`asset management`](Assets).
///
/// By default there is a filesystem source registered with the [`asset management`](Assets), for
/// reading an asset from a file.
pub trait Source: Send + Sync + 'static {
    /// Checks if `path` points to an asset in a source.
    fn is_asset(&self, path: &Path) -> bool;

    /// Reads the bytes of an asset from a source.
    ///
    /// # Errors
    /// This function will return an error if an error is encountered while reading from the source.
    fn read(&self, path: &Path) -> Result<Vec<u8>, AssetError>;
}

/// The os filesystem as a source for assets.
struct FilesystemSource {
    base_dir: PathBuf,
}

impl Source for FilesystemSource {
    fn is_asset(&self, path: &Path) -> bool {
        self.base_dir.join(path).is_file()
    }

    fn read(&self, path: &Path) -> Result<Vec<u8>, AssetError> {
        Ok(fs::read(self.base_dir.join(path))?)
    }
}

/// The asset management.
///
/// Allows reading an [`Asset`](Asset) from registered [`Sources`](Source). Red [`Assets`](Asset)
/// are cached and not read again as long as they are cached. The actual reading of an
/// [`Asset`](Asset) is submitted as a task and executed on another thread.
///
/// For more information see the [module level documentation](super).
pub struct Assets {
    device: Option<Arc<Device>>,
    sources: Arc<Mutex<Vec<Arc<dyn Source>>>>,
    cache: Arc<Mutex<HashMap<PathBuf, Arc<dyn Reload>>>>,
    modified: Arc<Mutex<VecDeque<PathBuf>>>,
    _watcher: RecommendedWatcher,
}

impl Assets {
    /// Creates a new instance.
    ///
    /// The passed path is the base dir from where to read file based assets.
    pub fn new(device: Arc<Device>, base_dir: impl AsRef<Path>) -> Arc<Assets> {
        let modified = Arc::new(Mutex::new(VecDeque::new()));
        let modified2 = modified.clone();
        let base = base_dir.as_ref().to_path_buf();
        let watcher: RecommendedWatcher =
            notify::recommended_watcher(move |result: Result<Event, _>| match result {
                Ok(event) => {
                    if let EventKind::Modify(_) = event.kind {
                        let mut modified = modified2.lock().unwrap();
                        for path in event.paths {
                            modified.push_back(path.strip_prefix(&base).unwrap().to_path_buf());
                        }
                    }
                }
                Err(_) => {}
            })
            .unwrap();
        // watcher
        //     .watch(base_dir.as_ref(), RecursiveMode::Recursive)
        //     .unwrap();
        let assets = Arc::new(Assets {
            device: Some(device),
            sources: Arc::new(Mutex::new(Vec::new())),
            cache: Arc::new(Mutex::new(HashMap::new())),
            modified,
            _watcher: watcher,
        });
        assets.register_source(FilesystemSource {
            base_dir: base_dir.as_ref().to_path_buf(),
        });
        ASSETS.lock().unwrap().replace(assets.clone());
        assets
    }

    #[cfg(test)]
    pub fn new_dummy(base_dir: impl AsRef<Path>) -> Arc<Self> {
        use notify::Watcher;

        let assets = Arc::new(Self {
            device: None,
            sources: Arc::new(Mutex::new(vec![Arc::new(FilesystemSource {
                base_dir: base_dir.as_ref().to_path_buf(),
            })])),
            cache: Arc::new(Mutex::new(Default::default())),
            modified: Arc::new(Mutex::new(Default::default())),
            _watcher: RecommendedWatcher::new(|_| {}).unwrap(),
        });
        ASSETS.lock().unwrap().replace(assets.clone());
        assets
    }

    /// Registers a [`Source`](Source).
    ///
    /// Note that sources are processed in the order they have been registered. When reading an
    /// asset, the first source that can read the asset will be selected for reading.
    ///
    /// For more information see [`Source`](Source).
    pub fn register_source<S>(&self, source: S)
    where
        S: Source,
    {
        self.sources.lock().unwrap().push(Arc::new(source));
    }

    /// Reads an [`Asset`].
    ///
    /// The reading of an asset is submitted to another thread. The returned [`AssetRef`](AssetRef)
    /// may not give immediate access to the underlying asset. For more information see the
    /// [module level documentation](super)
    ///
    /// # Errors
    /// This function will return an error if `path` does not point to an asset in any of the
    /// registered sources.
    ///
    /// It will also return an error if an asset is read from the cache with a different type than
    /// it was initially read.
    pub fn read<A, P>(self: &Arc<Self>, path: P) -> Result<AssetRef<A>, AssetError>
    where
        A: Asset,
        P: AsRef<Path>,
    {
        let path = path.as_ref().to_path_buf();
        let mut cache = self.cache.lock().unwrap();
        let handle = if let Some(handle) = cache.get(&path) {
            handle.clone()
        } else {
            let handle = Handle::<A>::new(path.as_path().into());
            let result = handle.clone();
            let source = self.select_source(&path)?;
            let path2 = path.clone();
            let assets = self.clone();
            rayon::spawn(move || {
                log::debug!("Start loading asset {:?}", &path2);
                let result = source
                    .read(&path2)
                    .and_then(|bytes| A::try_from_bytes(assets, &path2, bytes));
                unsafe {
                    handle.init(result);
                }
                log::debug!("Finished loading asset {:?}", &path2);
            });
            cache.insert(path.clone(), result.clone());
            result
        };
        return if let Ok(handle) = handle.as_any().downcast::<Handle<A>>() {
            Ok(AssetRef { handle })
        } else {
            Err(Error::new(
                ErrorKind::NotFound,
                path.to_str().unwrap().to_string(),
            ))?
        };
    }

    /// Selects the first source that can load the asset pointed to by `path`.
    fn select_source(&self, path: &Path) -> Result<Arc<dyn Source>, AssetError> {
        self.sources
            .lock()
            .unwrap()
            .iter()
            .filter(|source| source.is_asset(path))
            .next()
            .cloned()
            .ok_or(AssetError::IoError(Error::new(
                ErrorKind::NotFound,
                path.to_str().unwrap().to_string(),
            )))
    }

    /// Reloads all modified assets.
    ///
    /// Assets that are currently in use, will not be reloaded.
    ///
    /// # Safety
    /// Calling this function is guaranteed to be safe. However, it is not recommended to call this
    /// function during a frame. If an asset that was modified is still in use, i.e. is still
    /// borrowed, while this function is called, the asset will not be reloaded. The reloading
    /// mechanism needs a mutable borrow to the asset, which can not happen while immutable borrows
    /// are still present. It is best to call this function at the end of a frame, after all systems
    /// have been updated.
    pub fn reload(self: &Arc<Self>) {
        let mut modified = self.modified.lock().unwrap();
        let cache = self.cache.lock().unwrap();
        while !modified.is_empty() {
            let path = modified.pop_front().unwrap();
            if let Some(handle) = cache.get(&path) {
                if let Ok(source) = self.select_source(&path) {
                    handle.clone().reload(source);
                }
            }
        }
    }

    /// Returns the device.
    pub fn device(&self) -> &Arc<Device> {
        self.device.as_ref().unwrap()
    }
}

unsafe impl Sync for Assets {}

/// An asset containing only plain text.
pub struct PlainText {
    inner: String,
}

impl PlainText {
    /// Returns the `str`slice.
    pub fn get(&self) -> &str {
        &self.inner
    }
}

impl Asset for PlainText {
    fn try_from_bytes(
        _assets: Arc<Assets>,
        _path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, AssetError> {
        Ok(Self {
            inner: String::from_utf8(bytes).unwrap(),
        })
    }
}

/// An asset containing some binary data.
pub struct Blob {
    inner: Vec<u8>,
}

impl Blob {
    /// Returns the `str`slice.
    pub fn get(&self) -> &Vec<u8> {
        &self.inner
    }
}

impl Asset for Blob {
    fn try_from_bytes(
        _assets: Arc<Assets>,
        _path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, AssetError> {
        Ok(Self { inner: bytes })
    }
}

#[cfg(test)]
mod tests {
    use std::io::{Error, ErrorKind};
    use std::path::Path;
    use std::sync::Arc;

    use crate::asset::{Asset, AssetError, Assets, Handle};

    struct Text {
        inner: String,
    }

    impl Asset for Text {
        fn try_from_bytes(
            _assets: Arc<Assets>,
            path: &Path,
            bytes: Vec<u8>,
        ) -> Result<Self, AssetError> {
            match String::from_utf8(bytes) {
                Ok(text) => Ok(Text { inner: text }),
                Err(_) => Err(AssetError::IoError(Error::new(
                    ErrorKind::Other,
                    format!("{}: Invalid utf8 encoding", path.to_str().unwrap()),
                ))),
            }
        }
    }

    struct Other;

    impl Asset for Other {
        fn try_from_bytes(_: Arc<Assets>, _: &Path, _: Vec<u8>) -> Result<Self, AssetError> {
            Ok(Other)
        }
    }

    #[test]
    fn test_borrow() {
        let handle = Handle::<Text>::new("test".into());

        assert!(handle.try_borrow().is_err());
        assert!(handle.try_borrow_mut().is_err());
        unsafe {
            handle.init(Ok(Text {
                inner: "testText".to_string(),
            }));
        }

        {
            let borrow = handle.try_borrow().unwrap();
            assert_eq!(borrow.as_ref().unwrap().inner, "testText");
            let borrow = handle.try_borrow().unwrap();
            assert_eq!(borrow.as_ref().unwrap().inner, "testText");
            assert!(handle.try_borrow_mut().is_err());
        }

        {
            let borrow = handle.try_borrow_mut().unwrap();
            assert_eq!(borrow.as_ref().unwrap().as_ref().unwrap().inner, "testText");
            assert!(handle.try_borrow_mut().is_err());
            assert!(handle.try_borrow().is_err());
        }
        assert!(handle.try_borrow().is_ok());
    }

    #[test]
    fn test_read_not_found() {
        let assets = Assets::new_dummy("rsc/test/asset");
        let result = assets.read::<Text, _>("not_found");

        assert!(result.is_err());
        let result = result.err().unwrap();
        assert!(
            matches!(result, AssetError::IoError(error) if error.kind() == ErrorKind::NotFound)
        );
    }

    #[test]
    fn test_read_conversion_error() {
        let assets = Assets::new_dummy("rsc/test/asset");
        let result = assets.read::<Text, _>("non_utf8");

        assert!(result.is_ok());

        let non_utf8 = result.unwrap();
        assert!(non_utf8.unwrap().is_err());
    }

    #[test]
    fn test_read_other_asset_type() {
        let assets = Assets::new_dummy("rsc/test/asset");
        let result = assets.read::<Text, _>("text");
        assert!(result.is_ok());

        let result = assets.read::<Other, _>("text");
        assert!(result.is_err());
    }

    #[test]
    fn test_read() {
        let assets = Assets::new_dummy("rsc/test/asset");
        let result = assets.read::<Text, _>("text");

        assert!(result.is_ok());

        let text = result.unwrap();

        assert_eq!(text.unwrap().as_ref().unwrap().inner, "Some test text.");

        text.map_ok::<_, ()>(|text| {
            assert_eq!(text.inner, "Some test text.");
            Ok(())
        })
        .unwrap();

        let text = assets.read::<Text, _>("text").unwrap();
        assert_eq!(text.unwrap().as_ref().unwrap().inner, "Some test text.");
    }
}
