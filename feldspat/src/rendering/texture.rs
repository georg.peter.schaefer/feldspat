//! Textures.

use std::error::Error;
use std::fmt::{Display, Formatter};
use std::io::Cursor;
use std::path::Path;
use std::sync::Arc;

use ash::vk;
use image::io::Reader;
use image::{GenericImageView, ImageError};

use crate::asset::{Asset, AssetError, Assets};
use crate::graphics::image::{
    Image, ImageCreationError, ImageView, ImageViewCreationError, ImmutableImage, Sampler,
    SamplerCreationError,
};
use crate::graphics::Device;

/// A texture consisting of an image, a view and a sampler.
pub struct Texture {
    sampler: Arc<Sampler>,
    view: Arc<ImageView<Arc<ImmutableImage>>>,
    _image: Arc<ImmutableImage>,
}

impl Texture {
    /// Creates a two by two texture.
    pub fn two_by_two(device: Arc<Device>) -> Result<Self, TextureCreationError> {
        let image = ImmutableImage::from_iter(
            device.clone(),
            vk::Format::R8G8B8A8_SRGB,
            vk::Extent3D::builder().width(2).height(2).depth(1).build(),
            false,
            [
                0u8, 0u8, 0u8, 1u8, 0u8, 0u8, 0u8, 1u8, 0u8, 0u8, 0u8, 1u8, 0u8, 0u8, 0u8, 1u8,
            ],
        )?;
        let view = ImageView::from_image(image.clone())?;
        let sampler = Sampler::builder().build(device.clone())?;

        Ok(Self {
            sampler,
            view,
            _image: image,
        })
    }

    /// Returns the sampler.
    pub fn sampler(&self) -> &Arc<Sampler> {
        &self.sampler
    }

    /// Returns the view.
    pub fn view(&self) -> &Arc<ImageView<Arc<ImmutableImage>>> {
        &self.view
    }
}

impl Asset for Texture {
    fn try_from_bytes(
        assets: Arc<Assets>,
        _path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, AssetError> {
        let image = Reader::new(Cursor::new(bytes))
            .with_guessed_format()?
            .decode()?;
        let extent = vk::Extent3D::builder()
            .width(image.width())
            .height(image.height())
            .depth(1);
        let image = ImmutableImage::from_iter(
            assets.device().clone(),
            vk::Format::R8G8B8A8_UNORM,
            *extent,
            true,
            image.into_rgba8().iter().cloned(),
        )?;
        let view = ImageView::from_image(image.clone())?;
        let sampler = Sampler::builder()
            .max_lod(image.mip_levels() as _)
            .build(assets.device().clone())?;
        Ok(Self {
            sampler,
            view,
            _image: image,
        })
    }
}

/// Error that can occur during texture creation.
#[derive(Debug)]
pub enum TextureCreationError {
    /// Image creation has failed.
    ImageCreationFailed(ImageCreationError),
    /// Image view creation has failed.
    ImageViewCreationFailed(ImageViewCreationError),
    /// Sampler creation has failed.
    SamplerCreationFailed(SamplerCreationError),
}

impl Display for TextureCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            TextureCreationError::ImageCreationFailed(e) => Display::fmt(e, f),
            TextureCreationError::ImageViewCreationFailed(e) => Display::fmt(e, f),
            TextureCreationError::SamplerCreationFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for TextureCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            TextureCreationError::ImageCreationFailed(e) => Some(e),
            TextureCreationError::ImageViewCreationFailed(e) => Some(e),
            TextureCreationError::SamplerCreationFailed(e) => Some(e),
        }
    }
}

impl From<ImageCreationError> for TextureCreationError {
    fn from(e: ImageCreationError) -> Self {
        TextureCreationError::ImageCreationFailed(e)
    }
}

impl From<ImageViewCreationError> for TextureCreationError {
    fn from(e: ImageViewCreationError) -> Self {
        TextureCreationError::ImageViewCreationFailed(e)
    }
}

impl From<SamplerCreationError> for TextureCreationError {
    fn from(e: SamplerCreationError) -> Self {
        TextureCreationError::SamplerCreationFailed(e)
    }
}

impl From<ImageError> for AssetError {
    fn from(e: ImageError) -> Self {
        AssetError::Custom(e.into())
    }
}

impl From<ImageCreationError> for AssetError {
    fn from(e: ImageCreationError) -> Self {
        AssetError::Custom(e.into())
    }
}

impl From<ImageViewCreationError> for AssetError {
    fn from(e: ImageViewCreationError) -> Self {
        AssetError::Custom(e.into())
    }
}

impl From<SamplerCreationError> for AssetError {
    fn from(e: SamplerCreationError) -> Self {
        AssetError::Custom(e.into())
    }
}
