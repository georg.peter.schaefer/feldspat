//! Vertex definition and vertex shader for fullscreen passes.

use crate::graphics::buffer::{Buffer, BufferCreationError, ImmutableBuffer};
use crate::graphics::Device;
use crate::math::Vec2;
use std::sync::Arc;

/// Fullscreen pass vertex buffer.
///
/// Contains geometry that covers the whole screen.
pub struct FullscreenVertexBuffer {
    inner: Arc<ImmutableBuffer<[Vertex]>>,
}

impl FullscreenVertexBuffer {
    /// Creates a new vertex buffer with a geometry covering the whole screen.
    pub fn new(device: Arc<Device>) -> Result<Arc<Self>, BufferCreationError> {
        let vertices = vec![
            Vertex {
                position: Vec2::new(-1.0, -1.0),
            },
            Vertex {
                position: Vec2::new(-1.0, 3.0),
            },
            Vertex {
                position: Vec2::new(3.0, -1.0),
            },
        ];
        Ok(Arc::new(Self {
            inner: ImmutableBuffer::from_iter(
                device.clone(),
                ash::vk::BufferUsageFlags::VERTEX_BUFFER,
                vertices,
            )?,
        }))
    }
}

impl Buffer for Arc<FullscreenVertexBuffer> {
    fn inner(&self) -> ash::vk::Buffer {
        self.inner.inner()
    }
}

/// Vertex definition for fullscreen pass.
#[derive(Copy, Clone, Default)]
pub struct Vertex {
    /// Position.
    pub position: Vec2,
}

crate::vertex!(Vertex, position);

/// Vertex shader for fullscreen pass.
pub mod vertex_shader {
    feldspat_shaders::shader! {
        ty: "vertex",
        path: "src/rendering/fullscreen.vert"
    }
}
