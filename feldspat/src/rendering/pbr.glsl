const float PI = 3.14159265358979323846;

float trowbridge_reitz_ggx_distribution(vec3 N, vec3 H, float roughness) {
    float alpha = roughness * roughness;
    float alpha2 = alpha * alpha;
    float NdotH = clamp(dot(N, H), 0.0, 1.0);
    float NdotH2 = NdotH * NdotH;

    float numerator = alpha2;
    float denominator = NdotH2 * (alpha2 - 1.0) + 1.0;
    denominator = PI * denominator * denominator;

    return numerator / denominator;
}

float geometry_schlick(float cos_theta, float roughness) {
    float r = roughness + 1.0;
    float k = (r * r) / 8.0;

    float numerator = cos_theta;
    float denominator = cos_theta * (1.0 - k) + k;

    return numerator / denominator;
}

float geometry_smith(vec3 N, vec3 V, vec3 L, float roughness) {
    float NdotL = clamp(dot(N, L), 0.0, 1.0);
    float NdotV = clamp(dot(N, V), 0.0, 1.0);

    return geometry_schlick(NdotL, roughness) * geometry_schlick(NdotV, roughness);
}

vec3 fresnel_schlick(vec3 V, vec3 H, vec3 F0) {
    float VdotH = clamp(dot(V, H), 0.0, 1.0);
    float exponent = (-5.55473 * VdotH - 6.98316) * VdotH;

    return F0 + (1.0 - F0) * pow(2, exponent);
}

vec3 pbr_brdf(vec3 V, vec3 L, vec3 radiance, vec3 Cdiff, vec3 N, float roughness, float metallic) {
    vec3 H = normalize(L + V);

    float NdotL = clamp(dot(N, L), 0.0, 1.0);
    float NdotV = clamp(dot(N, V), 0.0, 1.0);

    vec3 fD = Cdiff / PI;

    float D = trowbridge_reitz_ggx_distribution(N, H, roughness);

    vec3 F0 = mix(vec3(0.04), Cdiff, 0.0);
    vec3 F = fresnel_schlick(V, H, F0);

    float G = geometry_smith(N, V, L, roughness);

    vec3 fS = (D * F * G) / max((4.0 * NdotL * NdotV), 0.001);

    vec3 kD = (1.0 - F) * (1.0 - metallic);

    return (kD * fD + fS) * radiance * NdotL;
}