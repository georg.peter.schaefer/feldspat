#version 450

#include <pbr.glsl>

layout(push_constant) uniform Light {
    vec3 position;
    vec3 color;
    float intensity;
    vec3 eye;
} light;

layout(origin_upper_left) in vec4 gl_FragCoord;

layout(input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput position;
layout(input_attachment_index = 1, set = 0, binding = 1) uniform subpassInput base_color;
layout(input_attachment_index = 2, set = 0, binding = 2) uniform subpassInput normal;
layout(input_attachment_index = 3, set = 0, binding = 3) uniform subpassInput metallic_roughness;

layout(location = 0) out vec4 out_color;

void main() {
    vec3 Cdiff = subpassLoad(base_color).rgb;
    vec3 N = subpassLoad(normal).xyz;

    if (length(N) <= 0.01) {
        out_color = vec4(Cdiff, 1.0);
    } else {
        N = normalize(N);
        vec3 P = subpassLoad(position).xyz;
        vec3 L = normalize(light.position - P);
        vec3 V = normalize(light.eye - P);
        float roughness = subpassLoad(metallic_roughness).g;
        float metallic = subpassLoad(metallic_roughness).b;
        float distance = length(light.position - P);
        float intensity = light.intensity / (distance * distance);
        vec3 radiance = light.color * intensity;

        vec3 Lo = pbr_brdf(V, L, radiance, Cdiff, N, roughness, metallic);

        out_color = vec4(Lo, 1.0);
    }
}
