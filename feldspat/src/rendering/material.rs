//! Material.

use itertools::Itertools;

use crate::asset::AssetRef;
use crate::math::Vec4;
use crate::rendering::texture::Texture;

/// PBR-Roughness-Metallic Material.
#[derive(Clone)]
pub struct Material {
    base_color: Vec4,
    metallic: f32,
    roughness: f32,
    base_color_texture: Option<AssetRef<Texture>>,
    normal_texture: Option<AssetRef<Texture>>,
    metallic_roughness_texture: Option<AssetRef<Texture>>,
}

impl Material {
    /// Creates a new material builder.
    pub fn start(base_color: Vec4, metallic: f32, roughness: f32) -> MaterialBuilder {
        MaterialBuilder {
            base_color,
            metallic,
            roughness,
            base_color_texture: None,
            normal_texture: None,
            metallic_roughness_texture: None,
        }
    }

    /// Returns the base color.
    pub fn base_color(&self) -> Vec4 {
        self.base_color
    }

    /// Returns the metallic factor.
    pub fn metallic(&self) -> f32 {
        self.metallic
    }

    /// Returns the roughness.
    pub fn roughness(&self) -> f32 {
        self.roughness
    }

    /// Returns the optional base color texture.
    pub fn base_color_texture(&self) -> Option<&AssetRef<Texture>> {
        self.base_color_texture.as_ref()
    }

    /// Returns the optional normal texture.
    pub fn normal_texture(&self) -> Option<&AssetRef<Texture>> {
        self.normal_texture.as_ref()
    }

    /// Returns the optional metallic roughness texture.
    pub fn metallic_roughness_texture(&self) -> Option<&AssetRef<Texture>> {
        self.metallic_roughness_texture.as_ref()
    }

    /// Determines the index of the pipeline to use for the given material.
    pub fn pipeline_index(&self) -> usize {
        (0..3)
            .map(|_| vec![false, true].into_iter())
            .multi_cartesian_product()
            .position(|specialization_constants| {
                specialization_constants
                    == vec![
                        self.base_color_texture.is_some(),
                        self.normal_texture.is_some(),
                        self.metallic_roughness_texture.is_some(),
                    ]
            })
            .unwrap()
    }
}

/// Builder for a `Material`.
pub struct MaterialBuilder {
    base_color: Vec4,
    metallic: f32,
    roughness: f32,
    base_color_texture: Option<AssetRef<Texture>>,
    normal_texture: Option<AssetRef<Texture>>,
    metallic_roughness_texture: Option<AssetRef<Texture>>,
}

impl MaterialBuilder {
    /// Adds a base color texture to the material.
    pub fn with_base_color_texture(&mut self, base_color_texture: AssetRef<Texture>) -> &Self {
        self.base_color_texture = Some(base_color_texture);
        self
    }

    /// Adds a normal texture to the material.
    pub fn with_normal_texture(&mut self, normal_texture: AssetRef<Texture>) -> &Self {
        self.normal_texture = Some(normal_texture);
        self
    }

    /// Adds a metallic roughness texture to the material.
    pub fn with_metallic_roughness_texture(
        &mut self,
        metallic_roughness_texture: AssetRef<Texture>,
    ) -> &Self {
        self.metallic_roughness_texture = Some(metallic_roughness_texture);
        self
    }

    /// Creates the material.
    pub fn build(self) -> Material {
        Material {
            base_color: self.base_color,
            metallic: self.metallic,
            roughness: self.roughness,
            base_color_texture: self.base_color_texture,
            normal_texture: self.normal_texture,
            metallic_roughness_texture: self.metallic_roughness_texture,
        }
    }
}
