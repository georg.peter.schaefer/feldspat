//! Gather pass for deferred shading.

use std::sync::{Arc, Mutex};

use ash::vk;
use itertools::Itertools;
use rayon::prelude::*;
use thread_local::ThreadLocal;

use crate::asset::join;
use crate::ecs::{Entities, Transform};
use crate::graphics::command_buffer::{CommandBuffer, CommandBufferBuilder};
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorSetLayoutCreationError,
    GraphicsPipelineCreationError,
};
use crate::graphics::{Device, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, Swapchain};
use crate::math::Mat4;
use crate::rendering::camera::Camera;
use crate::rendering::geometry_pass::shader::SpecializationConstants;
use crate::rendering::static_mesh::{StaticMesh, Vertex};
use crate::rendering::texture::Texture;
use crate::rendering::{PipelineCreationError, PipelineExecutionError};

/// Gather pass for deferred shading.
pub struct GeometryPass {
    default_texture: Texture,
    pipelines: Vec<Arc<GraphicsPipeline>>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    render_pass: Arc<RenderPass>,
    device: Arc<Device>,
}

impl GeometryPass {
    /// Creates the geometry pass.
    pub fn new(
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
    ) -> Result<Self, PipelineCreationError> {
        let vertex_shader_module = vertex_shader::Shader::load(&device)?;
        let fragment_shader_module = fragment_shader::Shader::load(&device)?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(&device)?;
        let pipelines = Self::create_pipelines(
            &device,
            swapchain,
            render_pass.clone(),
            vertex_shader_module.clone(),
            fragment_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let default_texture = Texture::two_by_two(device.clone())?;

        Ok(Self {
            device,
            render_pass,
            descriptor_set_layout,
            pipelines,
            default_texture,
        })
    }

    fn create_descriptor_set_layout(
        device: &Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipelines(
        device: &Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        fragment_shader: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Vec<Arc<GraphicsPipeline>>, GraphicsPipelineCreationError> {
        let mut pipelines = Vec::new();
        let specialization_constants =
            (0..3)
                .map(|_| 0..2)
                .multi_cartesian_product()
                .map(|specialization_constants| SpecializationConstants {
                    base_color_texture: specialization_constants[0],
                    normal_texture: specialization_constants[1],
                    metallic_roughness_texture: specialization_constants[2],
                });

        for constants in specialization_constants {
            pipelines.push(Self::create_pipeline(
                &device,
                swapchain,
                render_pass.clone(),
                vertex_shader.clone(),
                fragment_shader.clone(),
                constants,
                descriptor_set_layout,
            )?);
        }

        Ok(pipelines)
    }

    fn create_pipeline(
        device: &Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        fragment_shader: Arc<ShaderModule>,
        specialization_constants: SpecializationConstants,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        let extent = swapchain.extent();
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader)
            .fragment_shader(fragment_shader)
            .fragment_shader_specialization_constants(specialization_constants)
            .vertex_input::<Vertex>()
            .triangle_list()
            .cull_mode_back()
            .front_face_counter_clockwise()
            .depth_test_enabled()
            .viewports(vec![vk::Viewport::builder()
                .width(extent.width as _)
                .height(extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .render_pass(render_pass.clone(), 0)
            .push_constants::<shader::Transform>(
                vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
            )
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    /// Executes the geometry pass.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
        framebuffer: &Arc<Framebuffer>,
        entities: &Entities,
    ) -> Result<(), PipelineExecutionError> {
        let cameras = entities.components::<(Transform, Camera)>();
        if let Some((_, camera_transform, camera)) = cameras.first() {
            let extent = framebuffer.extent();
            let aspect_ration = extent.width as f32 / extent.height as f32;
            let projection = camera.projection(aspect_ration);
            let view = camera_transform.model().inverse();
            let per_thread_builder = ThreadLocal::new();

            let results = entities
                .components::<(Transform, StaticMesh)>()
                .par_iter()
                .map(|(_, transform, static_mesh)| {
                    let mut builder = per_thread_builder
                        .get_or(|| {
                            Mutex::new(Some(
                                CommandBuffer::secondary(
                                    self.device.clone(),
                                    vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
                                    self.device.graphics_queue().family(),
                                    self.render_pass.clone(),
                                    0,
                                    framebuffer.clone(),
                                )
                                .unwrap(),
                            ))
                        })
                        .lock()
                        .unwrap();
                    self.static_mesh(
                        &mut builder.as_mut().unwrap(),
                        projection,
                        view,
                        &transform,
                        &static_mesh,
                    )
                })
                .collect::<Vec<_>>();

            for result in results {
                result?;
            }

            for secondary_builder in per_thread_builder.iter() {
                let mut secondary_builder = secondary_builder.lock().unwrap();
                if let Some(secondary_builder) = secondary_builder.take() {
                    builder.execute_commands(secondary_builder.build()?);
                }
            }
        }

        Ok(())
    }

    fn static_mesh(
        &self,
        builder: &mut CommandBufferBuilder,
        projection: Mat4,
        view: Mat4,
        transform: &Transform,
        static_mesh: &StaticMesh,
    ) -> Result<(), PipelineExecutionError> {
        static_mesh
            .mesh()
            .map_ok::<_, PipelineExecutionError>(|mesh| {
                builder
                    .bind_vertex_buffer(mesh.vertex_buffer.clone())
                    .bind_index_buffer(mesh.index_buffer.clone(), vk::IndexType::UINT32);

                for sub_mesh in &mesh.sub_meshes {
                    join((
                        sub_mesh.material.base_color_texture().clone(),
                        sub_mesh.material.normal_texture().clone(),
                        sub_mesh.material.metallic_roughness_texture().clone(),
                    ))
                    .map_ok::<_, PipelineExecutionError>(
                        |base_color, normal, metallic_roughness| {
                            let base_color = base_color.unwrap_or(&self.default_texture);
                            let normal = normal.unwrap_or(&self.default_texture);
                            let metallic_roughness =
                                metallic_roughness.unwrap_or(&self.default_texture);

                            let pipeline = &self.pipelines[sub_mesh.material.pipeline_index()];

                            let transform_constants = shader::Transform {
                                model: transform.model(),
                                view,
                                projection,
                                base_color: sub_mesh.material.base_color(),
                                metallic: sub_mesh.material.metallic(),
                                roughness: sub_mesh.material.roughness(),
                            };

                            let descriptor_set = DescriptorSet::builder()
                                .add_sampled_image(
                                    0,
                                    base_color.view().clone(),
                                    base_color.sampler().clone(),
                                )
                                .add_sampled_image(
                                    1,
                                    normal.view().clone(),
                                    normal.sampler().clone(),
                                )
                                .add_sampled_image(
                                    2,
                                    metallic_roughness.view().clone(),
                                    metallic_roughness.sampler().clone(),
                                )
                                .build(self.device.clone(), self.descriptor_set_layout.clone())?;

                            builder
                                .bind_pipeline(pipeline.clone())
                                .push_constants(
                                    pipeline.layout(),
                                    vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
                                    transform_constants,
                                )
                                .bind_descriptor_set(pipeline.layout(), descriptor_set.clone())
                                .draw_indexed(sub_mesh.count as _, 1, sub_mesh.offset as _, 0, 0);

                            Ok(())
                        },
                    )?
                }

                Ok(())
            })?;

        Ok(())
    }
}

mod vertex_shader {
    feldspat_shaders::shader! {
        ty: "vertex",
        path: "src/rendering/geometry.vert"
    }
}

mod fragment_shader {
    feldspat_shaders::shader! {
        ty: "fragment",
        path: "src/rendering/geometry.frag"
    }
}

mod shader {
    use ash::vk;

    use crate::math::{Mat4, Vec4};

    #[allow(unused)]
    pub struct SpecializationConstants {
        pub base_color_texture: u32,
        pub normal_texture: u32,
        pub metallic_roughness_texture: u32,
    }

    impl crate::graphics::pipeline::SpecializationConstants for SpecializationConstants {
        fn specialization_map_entries(&self) -> Vec<vk::SpecializationMapEntry> {
            vec![
                vk::SpecializationMapEntry::builder()
                    .constant_id(0)
                    .offset(0)
                    .size(4)
                    .build(),
                vk::SpecializationMapEntry::builder()
                    .constant_id(1)
                    .offset(4)
                    .size(4)
                    .build(),
                vk::SpecializationMapEntry::builder()
                    .constant_id(2)
                    .offset(8)
                    .size(4)
                    .build(),
            ]
        }
    }

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct Transform {
        pub model: Mat4,
        pub view: Mat4,
        pub projection: Mat4,
        pub base_color: Vec4,
        pub metallic: f32,
        pub roughness: f32,
    }
}
