//! Rendering pipeline.

use std::cell::{RefCell, UnsafeCell};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::sync::{Arc, Weak};

use ash::vk;

use crate::ecs::Entities;
use crate::engine::Config;
use crate::graphics::buffer::BufferCreationError;
use crate::graphics::command_buffer::{CommandBuffer, CommandBufferCreationError};
use crate::graphics::framebuffer::FramebufferCreationError;
use crate::graphics::image::{
    ImageCreationError, ImageView, ImageViewCreationError, ImmutableImage, Sampler,
    SamplerCreationError,
};
use crate::graphics::pipeline::{
    DescriptorSetCreationError, DescriptorSetLayoutCreationError, GraphicsPipelineCreationError,
};
use crate::graphics::queue::{SubmitError, WaitIdleError};
use crate::graphics::render_pass::RenderPassCreationError;
use crate::graphics::sync::{FenceCreationError, ResetError, SemaphoreCreationError, WaitError};
use crate::graphics::{
    AcquireNextImageError, Device, Fence, GraphicsContext, PresentError, Semaphore,
    ShaderModuleCreationError, Swapchain,
};
use crate::rendering::copy_pipeline::CopyPipeline;
use crate::rendering::gi::octree_debug_pass::OctreeDebugPass;
use crate::rendering::gi::{GiData, GiPipeline};
use crate::rendering::pbr_pipeline::PbrPipeline;
use crate::rendering::texture::TextureCreationError;

pub mod ao_pass;
pub mod camera;
pub mod copy_pipeline;
pub mod final_pass;
pub mod fullscreen_pass;
pub mod geometry_pass;
pub mod gi;
pub mod light;
pub mod light_pass;
pub mod material;
pub mod pbr_pipeline;
pub mod static_mesh;
pub mod texture;

/// The renderer.
pub struct Renderer {
    copying_finished_semaphore: Semaphore,
    pipeline_fence: Arc<Fence>,
    image_available_semaphore: Semaphore,
    rendering_finished_semaphore: Semaphore,
    swapchain_dependent: UnsafeCell<SwapchainDependent>,
    gi_pipeline: GiPipeline,
    state: RefCell<State>,
    graphics_context: Weak<GraphicsContext>,
}

impl Renderer {
    /// Creates a new renderer.
    pub fn new(
        config: Arc<Config>,
        graphics_context: Arc<GraphicsContext>,
    ) -> Result<Arc<Self>, RendererCreationError> {
        let device = graphics_context.device().clone();
        let gi_pipeline = GiPipeline::new(config.clone(), device.clone())?;
        let swapchain_dependent = UnsafeCell::new(SwapchainDependent::new(
            config.clone(),
            device.clone(),
            graphics_context.swapchain(),
            gi_pipeline.gi_data().clone(),
        )?);
        let renderer = Arc::new(Self {
            copying_finished_semaphore: Semaphore::new(device.clone())?,
            pipeline_fence: Fence::new(device.clone(), vk::FenceCreateFlags::empty())?,
            image_available_semaphore: Semaphore::new(device.clone())?,
            rendering_finished_semaphore: Semaphore::new(device.clone())?,
            swapchain_dependent,
            gi_pipeline,
            state: RefCell::new(State::new()),
            graphics_context: Arc::downgrade(&graphics_context),
        });
        let renderer2 = renderer.clone();

        unsafe {
            graphics_context
                .clone()
                .swapchain()
                .register_recreate_callback(move |swapchain| {
                    *renderer2.swapchain_dependent.get() = SwapchainDependent::new(
                        config.clone(),
                        device.clone(),
                        swapchain,
                        renderer2.gi_pipeline.gi_data().clone(),
                    )?;
                    Ok(())
                });
        }

        Ok(renderer)
    }

    /// Updates the renderer.
    pub fn update(&self, entities: &Entities) -> Result<(), PipelineExecutionError> {
        let graphics_context = self.graphics_context();
        let device = graphics_context.device();
        let graphics_queue = device.graphics_queue();
        let present_queue = device.present_queue();
        let swapchain_dependent = self.get_swapchain_dependent();

        self.gi_pipeline.execute(entities)?;

        let (on_screen_command_buffer, src_image) = match self.state.borrow().on_screen_pass {
            OnScreenPass::OctreeDebug => (
                swapchain_dependent.octree_debug_pass.execute(entities)?,
                swapchain_dependent.octree_debug_pass.final_attachment(),
            ),
            OnScreenPass::PbrFinal => (
                swapchain_dependent.pbr_pipeline.execute(entities)?,
                swapchain_dependent.pbr_pipeline.final_attachment(),
            ),
        };
        graphics_queue.submit(
            on_screen_command_buffer.clone(),
            vec![],
            vec![],
            Some(&self.rendering_finished_semaphore),
            None,
        )?;

        let image_index = match graphics_context
            .swapchain()
            .acquire_next_image(&self.image_available_semaphore)
        {
            Ok((_, true)) | Err(AcquireNextImageError::OutOfDate(_)) => unsafe {
                graphics_context.swapchain().recreate();
                return Ok(());
            },
            Ok((image_index, false)) => image_index,
            Err(e) => panic!("Failed to acquire next image {}", e),
        };

        let copy_command_buffer = swapchain_dependent
            .copy_pipeline
            .execute(src_image.clone(), image_index as _)?;
        graphics_queue.submit(
            copy_command_buffer.clone(),
            vec![
                &self.rendering_finished_semaphore,
                &self.image_available_semaphore,
            ],
            vec![
                vk::PipelineStageFlags::FRAGMENT_SHADER,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            ],
            Some(&self.copying_finished_semaphore),
            Some(self.pipeline_fence.clone()),
        )?;

        self.pipeline_fence.wait()?;
        self.pipeline_fence.reset()?;

        match present_queue.present(
            graphics_context.swapchain(),
            image_index,
            &self.copying_finished_semaphore,
        ) {
            Ok(true) | Err(PresentError::OutOfDate(_)) => {
                unsafe { graphics_context.swapchain().recreate() };
            }
            Err(e) => panic!("Failed to present {}", e),
            _ => {}
        }

        Ok(())
    }

    fn graphics_context(&self) -> Arc<GraphicsContext> {
        self.graphics_context.upgrade().unwrap()
    }

    fn get_swapchain_dependent(&self) -> &SwapchainDependent {
        unsafe { &*self.swapchain_dependent.get() }
    }

    /// Sets which pass will be presented on screen.
    pub fn set_on_screen_pass(&self, on_screen_pass: OnScreenPass) {
        self.state.borrow_mut().on_screen_pass = on_screen_pass;
    }
}

unsafe impl Send for Renderer {}

unsafe impl Sync for Renderer {}

struct SwapchainDependent {
    copy_pipeline: CopyPipeline,
    pbr_pipeline: PbrPipeline,
    octree_debug_pass: OctreeDebugPass,
}

impl SwapchainDependent {
    fn new(
        config: Arc<Config>,
        device: Arc<Device>,
        swapchain: &Swapchain,
        gi_data: Arc<GiData>,
    ) -> Result<Self, RendererCreationError> {
        let octree_debug_pass =
            OctreeDebugPass::new(config.clone(), device.clone(), swapchain, gi_data.clone())?;
        let pbr_pipeline =
            PbrPipeline::new(config.clone(), device.clone(), swapchain, gi_data.clone())?;
        let copy_pipeline = CopyPipeline::new(device.clone(), swapchain)?;

        Ok(Self {
            pbr_pipeline,
            copy_pipeline,
            octree_debug_pass,
        })
    }
}

/// Error that can occur during renderer creation.
#[derive(Debug)]
pub enum RendererCreationError {
    /// Semaphore creation has failed.
    SemaphoreCreationFailed(SemaphoreCreationError),
    /// Fence creation has failed.
    FenceCreationFailed(FenceCreationError),
    /// Pipeline creation has failed.
    PipelineCreationFailed(PipelineCreationError),
}

impl Display for RendererCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            RendererCreationError::SemaphoreCreationFailed(e) => Display::fmt(e, f),
            RendererCreationError::FenceCreationFailed(e) => Display::fmt(e, f),
            RendererCreationError::PipelineCreationFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for RendererCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            RendererCreationError::SemaphoreCreationFailed(e) => Some(e),
            RendererCreationError::FenceCreationFailed(e) => Some(e),
            RendererCreationError::PipelineCreationFailed(e) => Some(e),
        }
    }
}

impl From<SemaphoreCreationError> for RendererCreationError {
    fn from(e: SemaphoreCreationError) -> Self {
        RendererCreationError::SemaphoreCreationFailed(e)
    }
}

impl From<FenceCreationError> for RendererCreationError {
    fn from(e: FenceCreationError) -> Self {
        RendererCreationError::FenceCreationFailed(e)
    }
}

impl From<PipelineCreationError> for RendererCreationError {
    fn from(e: PipelineCreationError) -> Self {
        RendererCreationError::PipelineCreationFailed(e)
    }
}

/// Error that can occur during rendering pripeline creation.
#[derive(Debug)]
pub enum PipelineCreationError {
    /// Render pass creation has failed.
    RenderPassCreationFailed(RenderPassCreationError),
    /// Attachment image creation has failed.
    ImageCreationFailed(ImageCreationError),
    /// Attachment image view creation has failed.
    ImageViewCreationFailed(ImageViewCreationError),
    /// Framebuffer creation has failed.
    FramebufferCreationFailed(FramebufferCreationError),
    /// Descriptor set layout creation has failed.
    DescriptorSetLayoutCreationFailed(DescriptorSetLayoutCreationError),
    /// Shader module creation has failed.
    ShaderModuleCreationFailed(ShaderModuleCreationError),
    /// Graphics pipeline creation has failed.
    GraphicsPipelineCreationFailed(GraphicsPipelineCreationError),
    /// Buffer creation has failed.
    BufferCreationFailed(BufferCreationError),
    /// Sampler creation has failed.
    SamplerCreationFailed(SamplerCreationError),
    /// Descriptor set creation has failed.
    DescriptorSetCreationFailed(DescriptorSetCreationError),
    /// Command buffer creation has failed.
    CommandBufferCreationFailed(CommandBufferCreationError),
    /// Submit has failed.
    SubmitFailed(SubmitError),
    /// Wait idle on queue has failed.
    WaitIdleFailed(WaitIdleError),
    /// Fence creation has failed.
    FenceCreationFailed(FenceCreationError),
}

impl Display for PipelineCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            PipelineCreationError::RenderPassCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::ImageCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::ImageViewCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::FramebufferCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::DescriptorSetLayoutCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::ShaderModuleCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::GraphicsPipelineCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::BufferCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::SamplerCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::DescriptorSetCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::CommandBufferCreationFailed(e) => Display::fmt(e, f),
            PipelineCreationError::SubmitFailed(e) => Display::fmt(e, f),
            PipelineCreationError::WaitIdleFailed(e) => Display::fmt(e, f),
            PipelineCreationError::FenceCreationFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for PipelineCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            PipelineCreationError::RenderPassCreationFailed(e) => Some(e),
            PipelineCreationError::ImageCreationFailed(e) => Some(e),
            PipelineCreationError::ImageViewCreationFailed(e) => Some(e),
            PipelineCreationError::FramebufferCreationFailed(e) => Some(e),
            PipelineCreationError::DescriptorSetLayoutCreationFailed(e) => Some(e),
            PipelineCreationError::ShaderModuleCreationFailed(e) => Some(e),
            PipelineCreationError::GraphicsPipelineCreationFailed(e) => Some(e),
            PipelineCreationError::BufferCreationFailed(e) => Some(e),
            PipelineCreationError::SamplerCreationFailed(e) => Some(e),
            PipelineCreationError::DescriptorSetCreationFailed(e) => Some(e),
            PipelineCreationError::CommandBufferCreationFailed(e) => Some(e),
            PipelineCreationError::SubmitFailed(e) => Some(e),
            PipelineCreationError::WaitIdleFailed(e) => Some(e),
            PipelineCreationError::FenceCreationFailed(e) => Some(e),
        }
    }
}

impl From<RenderPassCreationError> for PipelineCreationError {
    fn from(e: RenderPassCreationError) -> Self {
        PipelineCreationError::RenderPassCreationFailed(e)
    }
}

impl From<ImageCreationError> for PipelineCreationError {
    fn from(e: ImageCreationError) -> Self {
        PipelineCreationError::ImageCreationFailed(e)
    }
}

impl From<ImageViewCreationError> for PipelineCreationError {
    fn from(e: ImageViewCreationError) -> Self {
        PipelineCreationError::ImageViewCreationFailed(e)
    }
}

impl From<FramebufferCreationError> for PipelineCreationError {
    fn from(e: FramebufferCreationError) -> Self {
        PipelineCreationError::FramebufferCreationFailed(e)
    }
}

impl From<DescriptorSetLayoutCreationError> for PipelineCreationError {
    fn from(e: DescriptorSetLayoutCreationError) -> Self {
        PipelineCreationError::DescriptorSetLayoutCreationFailed(e)
    }
}

impl From<ShaderModuleCreationError> for PipelineCreationError {
    fn from(e: ShaderModuleCreationError) -> Self {
        PipelineCreationError::ShaderModuleCreationFailed(e)
    }
}

impl From<GraphicsPipelineCreationError> for PipelineCreationError {
    fn from(e: GraphicsPipelineCreationError) -> Self {
        PipelineCreationError::GraphicsPipelineCreationFailed(e)
    }
}

impl From<BufferCreationError> for PipelineCreationError {
    fn from(e: BufferCreationError) -> Self {
        PipelineCreationError::BufferCreationFailed(e)
    }
}

impl From<SamplerCreationError> for PipelineCreationError {
    fn from(e: SamplerCreationError) -> Self {
        PipelineCreationError::SamplerCreationFailed(e)
    }
}

impl From<DescriptorSetCreationError> for PipelineCreationError {
    fn from(e: DescriptorSetCreationError) -> Self {
        PipelineCreationError::DescriptorSetCreationFailed(e)
    }
}

impl From<CommandBufferCreationError> for PipelineCreationError {
    fn from(e: CommandBufferCreationError) -> Self {
        PipelineCreationError::CommandBufferCreationFailed(e)
    }
}

impl From<SubmitError> for PipelineCreationError {
    fn from(e: SubmitError) -> Self {
        PipelineCreationError::SubmitFailed(e)
    }
}

impl From<WaitIdleError> for PipelineCreationError {
    fn from(e: WaitIdleError) -> Self {
        PipelineCreationError::WaitIdleFailed(e)
    }
}

impl From<FenceCreationError> for PipelineCreationError {
    fn from(e: FenceCreationError) -> Self {
        PipelineCreationError::FenceCreationFailed(e)
    }
}

impl From<TextureCreationError> for PipelineCreationError {
    fn from(e: TextureCreationError) -> Self {
        match e {
            TextureCreationError::ImageCreationFailed(e) => {
                PipelineCreationError::ImageCreationFailed(e)
            }
            TextureCreationError::ImageViewCreationFailed(e) => {
                PipelineCreationError::ImageViewCreationFailed(e)
            }
            TextureCreationError::SamplerCreationFailed(e) => {
                PipelineCreationError::SamplerCreationFailed(e)
            }
        }
    }
}

/// Error that can occur during pipeline execution.
#[derive(Debug)]
pub enum PipelineExecutionError {
    /// Submit has failed.
    SubmitFailed(SubmitError),
    /// Waiting for fence has failed.
    WaitFailed(WaitError),
    /// Resetting fence has failed.
    ResetFailed(ResetError),
    /// Command buffer creation failed.
    CommandBufferCreationFailed(CommandBufferCreationError),
    /// Descriptor set creation has failed.
    DescriptorSetCreationFailed(DescriptorSetCreationError),
}

impl Display for PipelineExecutionError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            PipelineExecutionError::SubmitFailed(e) => Display::fmt(e, f),
            PipelineExecutionError::WaitFailed(e) => Display::fmt(e, f),
            PipelineExecutionError::ResetFailed(e) => Display::fmt(e, f),
            PipelineExecutionError::CommandBufferCreationFailed(e) => Display::fmt(e, f),
            PipelineExecutionError::DescriptorSetCreationFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for PipelineExecutionError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            PipelineExecutionError::SubmitFailed(e) => Some(e),
            PipelineExecutionError::WaitFailed(e) => Some(e),
            PipelineExecutionError::ResetFailed(e) => Some(e),
            PipelineExecutionError::CommandBufferCreationFailed(e) => Some(e),
            PipelineExecutionError::DescriptorSetCreationFailed(e) => Some(e),
        }
    }
}

impl From<SubmitError> for PipelineExecutionError {
    fn from(e: SubmitError) -> Self {
        PipelineExecutionError::SubmitFailed(e)
    }
}

impl From<WaitError> for PipelineExecutionError {
    fn from(e: WaitError) -> Self {
        PipelineExecutionError::WaitFailed(e)
    }
}

impl From<ResetError> for PipelineExecutionError {
    fn from(e: ResetError) -> Self {
        PipelineExecutionError::ResetFailed(e)
    }
}

impl From<CommandBufferCreationError> for PipelineExecutionError {
    fn from(e: CommandBufferCreationError) -> Self {
        PipelineExecutionError::CommandBufferCreationFailed(e)
    }
}

impl From<DescriptorSetCreationError> for PipelineExecutionError {
    fn from(e: DescriptorSetCreationError) -> Self {
        PipelineExecutionError::DescriptorSetCreationFailed(e)
    }
}

struct State {
    on_screen_pass: OnScreenPass,
}

impl State {
    fn new() -> Self {
        Self {
            on_screen_pass: OnScreenPass::PbrFinal,
        }
    }
}

/// Describes the passes that can be presented on to the screen.
pub enum OnScreenPass {
    /// Debug visualization of the octree.
    OctreeDebug,
    /// Final output of the pbr pipeline.
    PbrFinal,
}

/// Voxelized scene.
pub struct Volume {
    sampler: Arc<Sampler>,
    view: Arc<ImageView<Arc<ImmutableImage>>>,
    image: Arc<ImmutableImage>,
}

impl Volume {
    /// Creates a new volume.
    pub fn new(device: Arc<Device>, config: &Config) -> Result<Self, PipelineCreationError> {
        let resolution = config.graphics.global_illumination.resolution as _;
        let extent = vk::Extent3D::builder()
            .width(resolution)
            .height(resolution)
            .depth(resolution)
            .build();
        let image = ImmutableImage::uninitialized(
            device.clone(),
            vk::Format::R32G32B32A32_SFLOAT,
            extent,
            1,
            vk::ImageUsageFlags::STORAGE
                | vk::ImageUsageFlags::COLOR_ATTACHMENT
                | vk::ImageUsageFlags::SAMPLED,
        )?;

        let queue = device.graphics_queue();
        let mut builder = CommandBuffer::primary(
            device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            queue.family(),
        )?;
        builder.transition_image_layout(
            image.clone(),
            vk::ImageLayout::UNDEFINED,
            vk::ImageLayout::GENERAL,
            vk::AccessFlags::empty(),
            vk::AccessFlags::SHADER_WRITE,
            vk::PipelineStageFlags::FRAGMENT_SHADER,
            vk::PipelineStageFlags::FRAGMENT_SHADER,
        );
        let command_buffer = builder.build()?;

        queue.submit(command_buffer.clone(), vec![], vec![], None, None)?;
        queue.wait_idle()?;

        let view = ImageView::from_image(image.clone())?;
        let sampler = Sampler::builder().build(device.clone())?;
        Ok(Self {
            sampler: sampler,
            view,
            image,
        })
    }

    /// Returns the image of the volume.
    pub fn image(&self) -> &Arc<ImmutableImage> {
        &self.image
    }

    /// Returns the view to the volume.
    pub fn view(&self) -> &Arc<ImageView<Arc<ImmutableImage>>> {
        &self.view
    }

    /// Returns the sampler of the volume.
    pub fn sampler(&self) -> &Arc<Sampler> {
        &self.sampler
    }
}
