//! Camera component.

use serde::Deserialize;

use crate::math::Mat4;
use crate::Component;

/// Camera with a perspective projection.
#[derive(Component, Deserialize)]
pub struct Camera {
    fov_y: f32,
    near: f32,
    far: f32,
}

impl Camera {
    /// Returns the projection matrix.
    pub fn projection(&self, aspect_ratio: f32) -> Mat4 {
        Mat4::perspective(self.fov_y, aspect_ratio, self.near, self.far)
    }
}
