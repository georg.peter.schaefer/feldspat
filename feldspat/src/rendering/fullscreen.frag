#version 450

layout(binding = 0) uniform sampler2D image;

layout(origin_upper_left) in vec4 gl_FragCoord;

layout(location = 0) out vec4 out_color;

void main() {
    out_color = texelFetch(image, ivec2(gl_FragCoord.xy), 0);
}
