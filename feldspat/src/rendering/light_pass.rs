//! Light accumulation pass.

use std::sync::{Arc, Mutex};

use ash::vk;
use rayon::prelude::*;
use thread_local::ThreadLocal;

use crate::ecs::{Entities, Transform};
use crate::graphics::command_buffer::{CommandBuffer, CommandBufferBuilder};
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorSetLayoutCreationError,
    GraphicsPipelineCreationError,
};
use crate::graphics::{Device, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, Swapchain};
use crate::math::{Mat4, Vec3, Vec4};
use crate::rendering::camera::Camera;
use crate::rendering::fullscreen_pass::{FullscreenVertexBuffer, Vertex};
use crate::rendering::gi::GiData;
use crate::rendering::light::{DirectionalLight, PointLight};
use crate::rendering::{PipelineCreationError, PipelineExecutionError};

/// Light accumulation pass.
pub struct LightPass {
    gi_data: Arc<GiData>,
    fullscreen_vertex_buffer: Arc<FullscreenVertexBuffer>,
    dl_pipeline: Arc<GraphicsPipeline>,
    dl_descriptor_set_layout: Arc<DescriptorSetLayout>,
    pl_pipeline: Arc<GraphicsPipeline>,
    pl_descriptor_set_layout: Arc<DescriptorSetLayout>,
    render_pass: Arc<RenderPass>,
    device: Arc<Device>,
}

impl LightPass {
    /// Creates the light accumulation pass.
    pub fn new(
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        gi_data: Arc<GiData>,
    ) -> Result<Self, PipelineCreationError> {
        let vertex_shader_module = super::fullscreen_pass::vertex_shader::Shader::load(&device)?;
        let dl_fragment_shader_module = dl_fragment_shader::Shader::load(&device)?;
        let pl_fragment_shader_module = pl_fragment_shader::Shader::load(&device)?;
        let dl_descriptor_set_layout = Self::create_dl_descriptor_set_layout(device.clone())?;
        let pl_descriptor_set_layout = Self::create_pl_descriptor_set_layout(device.clone())?;
        let dl_pipeline = Self::create_pipeline(
            device.clone(),
            swapchain,
            render_pass.clone(),
            vertex_shader_module.clone(),
            dl_fragment_shader_module.clone(),
            &dl_descriptor_set_layout,
        )?;
        let pl_pipeline = Self::create_pipeline(
            device.clone(),
            swapchain,
            render_pass.clone(),
            vertex_shader_module.clone(),
            pl_fragment_shader_module.clone(),
            &pl_descriptor_set_layout,
        )?;
        let fullscreen_vertex_buffer = FullscreenVertexBuffer::new(device.clone())?;

        Ok(Self {
            gi_data,
            fullscreen_vertex_buffer,
            dl_pipeline,
            dl_descriptor_set_layout,
            pl_pipeline,
            pl_descriptor_set_layout,
            render_pass,
            device,
        })
    }

    fn create_dl_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(3)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pl_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(3)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipeline(
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        vertex_shader_module: Arc<ShaderModule>,
        fragment_shader_module: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        let extent = swapchain.extent();
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader_module)
            .fragment_shader(fragment_shader_module)
            .vertex_input::<Vertex>()
            .triangle_list()
            .viewports(vec![vk::Viewport::builder()
                .width(extent.width as _)
                .height(extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .blend(
                vk::PipelineColorBlendAttachmentState::builder()
                    .blend_enable(true)
                    .color_blend_op(vk::BlendOp::ADD)
                    .src_color_blend_factor(vk::BlendFactor::ONE)
                    .dst_color_blend_factor(vk::BlendFactor::ONE)
                    .alpha_blend_op(vk::BlendOp::MAX)
                    .src_alpha_blend_factor(vk::BlendFactor::ONE)
                    .dst_alpha_blend_factor(vk::BlendFactor::ONE)
                    .color_write_mask(vk::ColorComponentFlags::all())
                    .build(),
            )
            .render_pass(render_pass.clone(), 1)
            .push_constants::<shader::DirectionalLight>(vk::ShaderStageFlags::FRAGMENT)
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    /// Executes the sub pass.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
        framebuffer: &Arc<Framebuffer>,
        entities: &Entities,
    ) -> Result<(), PipelineExecutionError> {
        builder.next_subpass(vk::SubpassContents::SECONDARY_COMMAND_BUFFERS);

        let cameras = entities.components::<(Transform, Camera)>();
        if let Some((_, camera_transform, _)) = cameras.first() {
            let per_thread_builder = ThreadLocal::new();

            let mut results = entities
                .components::<(Transform, DirectionalLight)>()
                .par_iter()
                .map(|(_, transform, directional_light)| {
                    let mut builder = per_thread_builder
                        .get_or(|| {
                            Mutex::new(Some(
                                CommandBuffer::secondary(
                                    self.device.clone(),
                                    vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
                                    self.device.graphics_queue().family(),
                                    self.render_pass.clone(),
                                    1,
                                    framebuffer.clone(),
                                )
                                .unwrap(),
                            ))
                        })
                        .lock()
                        .unwrap();
                    self.directional_light(
                        builder.as_mut().unwrap(),
                        framebuffer,
                        camera_transform.position(),
                        &transform,
                        &directional_light,
                    )
                })
                .collect::<Vec<_>>();

            results.extend(
                entities
                    .components::<(Transform, PointLight)>()
                    .par_iter()
                    .map(|(_, transform, point_light)| {
                        let mut builder = per_thread_builder
                            .get_or(|| {
                                Mutex::new(Some(
                                    CommandBuffer::secondary(
                                        self.device.clone(),
                                        vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
                                        self.device.graphics_queue().family(),
                                        self.render_pass.clone(),
                                        1,
                                        framebuffer.clone(),
                                    )
                                    .unwrap(),
                                ))
                            })
                            .lock()
                            .unwrap();
                        self.point_light(
                            builder.as_mut().unwrap(),
                            framebuffer,
                            camera_transform.position(),
                            &transform,
                            &point_light,
                        )
                    })
                    .collect::<Vec<_>>(),
            );

            for result in results {
                result?;
            }

            for secondary_builder in per_thread_builder.iter() {
                let mut secondary_builder = secondary_builder.lock().unwrap();
                if let Some(secondary_builder) = secondary_builder.take() {
                    builder.execute_commands(secondary_builder.build()?);
                }
            }
        }

        Ok(())
    }

    fn directional_light(
        &self,
        builder: &mut CommandBufferBuilder,
        framebuffer: &Arc<Framebuffer>,
        eye: Vec3,
        transform: &Transform,
        directional_light: &DirectionalLight,
    ) -> Result<(), PipelineExecutionError> {
        let light_constants = shader::DirectionalLight {
            direction: Vec3::from(
                Mat4::from(transform.orientation()) * Vec4::new(0.0, 0.0, -1.0, 0.0),
            )
            .into(),
            dummy_0: 0.0,
            color: directional_light.color().into(),
            dummy_1: 0.0,
            intensity: directional_light.intensity(),
            eye: eye.into(),
        };
        let attachments = framebuffer.attachments();
        let descriptor_set = DescriptorSet::builder()
            .add_image(0, attachments[0].clone())
            .add_image(1, attachments[1].clone())
            .add_image(2, attachments[2].clone())
            .add_image(3, attachments[3].clone())
            .build(self.device.clone(), self.dl_descriptor_set_layout.clone())?;

        builder
            .bind_pipeline(self.dl_pipeline.clone())
            .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
            .push_constants(
                self.dl_pipeline.layout(),
                vk::ShaderStageFlags::FRAGMENT,
                light_constants,
            )
            .bind_descriptor_set(self.dl_pipeline.layout(), descriptor_set.clone())
            .draw(3, 1, 0, 0);

        Ok(())
    }

    fn point_light(
        &self,
        builder: &mut CommandBufferBuilder,
        framebuffer: &Arc<Framebuffer>,
        eye: Vec3,
        transform: &Transform,
        point_light: &PointLight,
    ) -> Result<(), PipelineExecutionError> {
        let light_constants = shader::PointLight {
            position: transform.position(),
            dummy_0: 0.0,
            color: point_light.color().into(),
            dummy_1: 0.0,
            intensity: point_light.intensity(),
            eye: eye.into(),
        };
        let attachments = framebuffer.attachments();
        let descriptor_set = DescriptorSet::builder()
            .add_image(0, attachments[0].clone())
            .add_image(1, attachments[1].clone())
            .add_image(2, attachments[2].clone())
            .add_image(3, attachments[3].clone())
            .build(self.device.clone(), self.pl_descriptor_set_layout.clone())?;

        builder
            .bind_pipeline(self.pl_pipeline.clone())
            .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
            .push_constants(
                self.pl_pipeline.layout(),
                vk::ShaderStageFlags::FRAGMENT,
                light_constants,
            )
            .bind_descriptor_set(self.pl_pipeline.layout(), descriptor_set.clone())
            .draw(3, 1, 0, 0);

        Ok(())
    }
}

mod dl_fragment_shader {
    feldspat_shaders::shader! {
        ty: "fragment",
        path: "src/rendering/directional_light.frag",
        include_dirs: ["src/rendering"]
    }
}

mod pl_fragment_shader {
    feldspat_shaders::shader! {
        ty: "fragment",
        path: "src/rendering/point_light.frag",
        include_dirs: ["src/rendering"]
    }
}

mod shader {
    use crate::math::Vec3;

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct DirectionalLight {
        pub direction: Vec3,
        pub dummy_0: f32,
        pub color: Vec3,
        pub intensity: f32,
        pub eye: Vec3,
        pub dummy_1: f32,
    }

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct PointLight {
        pub position: Vec3,
        pub dummy_0: f32,
        pub color: Vec3,
        pub intensity: f32,
        pub eye: Vec3,
        pub dummy_1: f32,
    }
}
