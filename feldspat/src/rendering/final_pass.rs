//! Final sub pass for gamma and color correction.

use crate::graphics::command_buffer::CommandBufferBuilder;
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorSetLayoutCreationError,
    GraphicsPipelineCreationError,
};
use crate::graphics::{Device, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, Swapchain};
use crate::rendering::fullscreen_pass::{FullscreenVertexBuffer, Vertex};
use crate::rendering::{PipelineCreationError, PipelineExecutionError};
use ash::vk;
use std::sync::Arc;

/// Final sub pass for gamma and color correction.
pub struct FinalPass {
    fullscreen_vertex_buffer: Arc<FullscreenVertexBuffer>,
    pipeline: Arc<GraphicsPipeline>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    device: Arc<Device>,
}

impl FinalPass {
    /// Creates the final pass.
    pub fn new(
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
    ) -> Result<Self, PipelineCreationError> {
        let vertex_shader_module = super::fullscreen_pass::vertex_shader::Shader::load(&device)?;
        let fragment_shader_module = fragment_shader::Shader::load(&device)?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(device.clone())?;
        let pipeline = Self::create_pipeline(
            device.clone(),
            swapchain,
            render_pass.clone(),
            vertex_shader_module.clone(),
            fragment_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let fullscreen_vertex_buffer = FullscreenVertexBuffer::new(device.clone())?;

        Ok(Self {
            fullscreen_vertex_buffer,
            pipeline,
            descriptor_set_layout,
            device,
        })
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipeline(
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        vertex_shader_module: Arc<ShaderModule>,
        fragment_shader_module: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        let extent = swapchain.extent();
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader_module)
            .fragment_shader(fragment_shader_module)
            .vertex_input::<Vertex>()
            .triangle_list()
            .viewports(vec![vk::Viewport::builder()
                .width(extent.width as _)
                .height(extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .render_pass(render_pass.clone(), 3)
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    /// Executes the sub pass.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
        framebuffer: &Arc<Framebuffer>,
    ) -> Result<(), PipelineExecutionError> {
        let descriptor_set = DescriptorSet::builder()
            .add_image(0, framebuffer.attachments()[6].clone())
            .build(self.device.clone(), self.descriptor_set_layout.clone())?;

        builder
            .next_subpass(vk::SubpassContents::INLINE)
            .bind_pipeline(self.pipeline.clone())
            .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
            .bind_descriptor_set(self.pipeline.layout(), descriptor_set)
            .draw(3, 1, 0, 0);

        Ok(())
    }
}

mod fragment_shader {
    feldspat_shaders::shader! {
        ty: "fragment",
        path: "src/rendering/final.frag"
    }
}
