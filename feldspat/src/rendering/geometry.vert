#version 450

layout(push_constant) uniform Transform {
    mat4 model;
    mat4 view;
    mat4 projection;
    vec4 base_color;
    float metallic;
    float roughness;
} transform;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 tangent;
layout(location = 3) in vec2 texcoord;

layout(location = 0) out vec3 frag_position;
layout(location = 1) out vec3 frag_normal;
layout(location = 2) out vec4 frag_tangent;
layout(location = 3) out vec2 frag_texcoord;

void main() {
    frag_position = vec3(transform.model * vec4(position, 1.0));
    frag_normal = mat3(transform.model) * normal;
    frag_tangent = vec4(mat3(transform.model) * tangent.xyz, tangent.w);
    frag_texcoord = texcoord;

    gl_Position = transform.projection * transform.view * transform.model * vec4(position, 1.0);
}
