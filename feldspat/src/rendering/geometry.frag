#version 450

layout(push_constant) uniform Transform {
    mat4 model;
    mat4 view;
    mat4 projection;
    vec4 base_color;
    float metallic;
    float roughness;
} transform;

layout(constant_id = 0) const bool base_color_texture = true;
layout(constant_id = 1) const bool normal_texture = true;
layout(constant_id = 2) const bool metallic_roughness_texture = true;

layout(binding = 0) uniform sampler2D base_color;
layout(binding = 1) uniform sampler2D normal;
layout(binding = 2) uniform sampler2D material;

layout(location = 0) in vec3 frag_position;
layout(location = 1) in vec3 frag_normal;
layout(location = 2) in vec4 frag_tangent;
layout(location = 3) in vec2 frag_texcoord;

layout(location = 0) out vec3 out_position;
layout(location = 1) out vec4 out_base_color;
layout(location = 2) out vec3 out_normal;
layout(location = 3) out vec3 out_metallic_roughness;

vec3 srgb_to_linear(vec3 x) {
    return pow(x, vec3(2.2));
}

mat3 tbn(vec4 tangent, vec3 normal) {
    vec3 T = normalize(tangent.xyz);
    vec3 N = normalize(normal);
    T = normalize(T - dot(T, N) * N);
    vec3 B = normalize(tangent.w * cross(N, T));
    return mat3(T, B, N);
}

vec4 get_base_color() {
    if (base_color_texture) {
        vec4 base_color = texture(base_color, frag_texcoord);
        return vec4(srgb_to_linear(base_color.rgb), base_color.a);
    }
    return transform.base_color;
}

vec3 get_normal() {
    if (normal_texture) {
        vec3 N = normalize(2.0 * texture(normal, frag_texcoord).xyz - 1.0);
        N = normalize(tbn(frag_tangent, frag_normal) * N);
        return N;
    }
    return normalize(frag_normal);
}

vec3 get_metallic_roughness() {
    if (metallic_roughness_texture) {
        return texture(material, frag_texcoord).rgb;
    }
    return vec3(1.0, transform.roughness, transform.metallic);
}

void main() {
//    vec4 albedo = get_base_color();
    vec4 albedo = vec4(1.0);
    if (albedo.a <= 0.0) {
        discard;
    }
    vec3 N = get_normal();

    out_position = frag_position;
    out_base_color = albedo;
    out_normal = N;
    out_metallic_roughness = get_metallic_roughness();
}
