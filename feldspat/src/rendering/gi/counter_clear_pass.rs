//! Pass for clearing all involved atomic counters.

use std::collections::HashMap;
use std::sync::Arc;

use ash::vk;

use crate::graphics::buffer::Buffer;
use crate::graphics::command_buffer::{CommandBuffer, CommandBufferBuilder};
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorSetLayoutCreationError,
    GraphicsPipelineCreationError,
};
use crate::graphics::render_pass::RenderPassCreationError;
use crate::graphics::{
    Device, Fence, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, SubpassDescription,
};
use crate::rendering::gi::GiData;
use crate::rendering::{PipelineCreationError, PipelineExecutionError};

/// Pass for clearing all involved atomic counters.
pub struct CounterClearPass {
    gi_data: Arc<GiData>,
    descriptor_set: Arc<DescriptorSet>,
    pipeline: Arc<GraphicsPipeline>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    framebuffer: Arc<Framebuffer>,
    render_pass: Arc<RenderPass>,
    device: Arc<Device>,
}

impl CounterClearPass {
    /// Creates a new thread manager pass.
    pub fn new(device: Arc<Device>, gi_data: Arc<GiData>) -> Result<Self, PipelineCreationError> {
        let vertex_shader_module = vertex_shader::Shader::load(&device)?;
        let render_pass = Self::create_render_pass(device.clone())?;
        let framebuffer = Framebuffer::empty(render_pass.clone(), 2, 2)?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(device.clone())?;
        let pipeline = Self::create_pipeline(
            device.clone(),
            render_pass.clone(),
            vertex_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let descriptor_set = DescriptorSet::builder()
            .add_buffer(
                0,
                gi_data.voxel_fragment_list.length.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_buffer(
                1,
                gi_data.octree.node_counts.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_buffer(
                2,
                gi_data.octree.node_pool_offsets.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_buffer(
                3,
                gi_data.octree.next_brick.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .build(device.clone(), descriptor_set_layout.clone())?;

        Ok(Self {
            gi_data,
            descriptor_set,
            pipeline,
            descriptor_set_layout,
            framebuffer,
            render_pass,
            device,
        })
    }

    fn create_render_pass(device: Arc<Device>) -> Result<Arc<RenderPass>, RenderPassCreationError> {
        Ok(RenderPass::new(
            device.clone(),
            vec![],
            vec![SubpassDescription {
                color_attachments: vec![],
                depth_attachment: None,
                input_attachments: vec![],
            }],
            vec![],
        )?)
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(3)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipeline(
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader)
            .point_list()
            .viewports(vec![vk::Viewport::builder()
                .width(2.0)
                .height(2.0)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .render_pass(render_pass.clone(), 0)
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    /// Executes the pass.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
    ) -> Result<(), PipelineExecutionError> {
        builder
            .pipeline_barrier(
                vk::PipelineStageFlags::VERTEX_SHADER | vk::PipelineStageFlags::FRAGMENT_SHADER,
                vk::PipelineStageFlags::VERTEX_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![
                    Self::create_memory_barrier(&self.gi_data.voxel_fragment_list.length),
                    Self::create_memory_barrier(&self.gi_data.octree.node_counts),
                    Self::create_memory_barrier(&self.gi_data.octree.node_pool_offsets),
                    Self::create_memory_barrier(&self.gi_data.octree.next_brick),
                ],
                vec![],
            )
            .begin_render_pass(
                self.framebuffer.clone(),
                vk::SubpassContents::INLINE,
                vec![],
            )
            .bind_pipeline(self.pipeline.clone())
            .bind_descriptor_set(self.pipeline.layout(), self.descriptor_set.clone())
            .draw(1, 1, 0, 0)
            .end_render_pass();

        Ok(())
    }

    fn create_memory_barrier<T>(buffer: &T) -> vk::BufferMemoryBarrier
    where
        T: Buffer,
    {
        vk::BufferMemoryBarrier::builder()
            .buffer(buffer.inner())
            .offset(0)
            .size(vk::WHOLE_SIZE)
            .src_access_mask(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE)
            .dst_access_mask(vk::AccessFlags::SHADER_WRITE)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .build()
    }
}

mod vertex_shader {
    feldspat_shaders::shader! {
        path: "src/rendering/gi/counter_clear.vert",
        ty: "vertex"
    }
}
