#version 450

#include <octree.glsl>

layout(binding = 0, std430) volatile coherent buffer NodePool {
    NodeTile pool[];
} node_pool;
layout(binding = 1, std430) volatile coherent buffer NextBrick {
    uint next;
} next_brick;

void main() {
    node_pool.pool[gl_VertexIndex].brick = atomicAdd(next_brick.next, 1);
    for (uint child = 0; child < 8; child++) {
        node_pool.pool[gl_VertexIndex].children[child] = 0;
    }
    for (uint neighbour = 0; neighbour < 26; neighbour++) {
        node_pool.pool[gl_VertexIndex].neighbours[neighbour] = 0;
    }
    gl_PointSize = 1.0;
}