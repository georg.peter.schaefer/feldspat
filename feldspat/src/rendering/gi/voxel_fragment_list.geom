#version 450

struct Material {
    vec4 base_color;
    float metallic;
    float roughness;
};

struct Model {
    mat4 transform;
    Material material;
};

struct Scene {
    uint resolution;
    float size;
};

layout(push_constant) uniform Constants {
    Model model;
    Scene scene;
} constants;

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

layout(location = 0) in vec3 geom_position[3];
layout(location = 1) in vec3 geom_normal[3];
layout(location = 2) in vec4 geom_tangent[3];
layout(location = 3) in vec2 geom_texcoord[3];

layout(location = 0) out vec3 frag_position;
layout(location = 1) out vec3 frag_normal;
layout(location = 2) out vec4 frag_tangent;
layout(location = 3) out vec2 frag_texcoord;
layout(location = 4) out uint dominant_axis;

void main() {
    vec3 e0 = geom_position[1] - geom_position[0];
    vec3 e1 = geom_position[2] - geom_position[0];
    vec3 contribution = abs(cross(e0, e1));

    for (int i = 0; i < 3; i++) {
        frag_position = geom_position[i];
        frag_normal = geom_normal[i];
        frag_tangent = geom_tangent[i];
        frag_texcoord = geom_texcoord[i];
        vec3 clip_space = geom_position[i] / (constants.scene.size / 2);

        if (contribution.z > contribution.x && contribution.z > contribution.y) {
            gl_Position = vec4(clip_space.xy, clip_space.z * 0.5 + 0.5, 1.0);
            dominant_axis = 2;
        } else if (contribution.x > contribution.y && contribution.x > contribution.z) {
            gl_Position = vec4(clip_space.yz, clip_space.x * 0.5 + 0.5, 1.0);
            dominant_axis = 0;
        } else {
            gl_Position = vec4(clip_space.xz, clip_space.y * 0.5 + 0.5, 1.0);
            dominant_axis = 1;
        }
        EmitVertex();
    }
    EndPrimitive();
}
