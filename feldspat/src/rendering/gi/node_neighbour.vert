#version 450

#include <octree.glsl>

const ivec3 NEIGHBOUR_OFFSET_COEFFICIENT[26] = ivec3[26](
    ivec3(-1, -1, -1),
    ivec3(-1, -1, 0),
    ivec3(-1, -1, 1),
    ivec3(-1, 0, -1),
    ivec3(-1, 0, 0),
    ivec3(-1, 0, 1),
    ivec3(-1, 1, -1),
    ivec3(-1, 1, 0),
    ivec3(-1, 1, 1),
    ivec3(0, -1, -1),
    ivec3(0, -1, 0),
    ivec3(0, -1, 1),
    ivec3(0, 0, -1),
    ivec3(0, 0, 1),
    ivec3(0, 1, -1),
    ivec3(0, 1, 0),
    ivec3(0, 1, 1),
    ivec3(1, -1, -1),
    ivec3(1, -1, 0),
    ivec3(1, -1, 1),
    ivec3(1, 0, -1),
    ivec3(1, 0, 0),
    ivec3(1, 0, 1),
    ivec3(1, 1, -1),
    ivec3(1, 1, 0),
    ivec3(1, 1, 1)
);

layout(push_constant) uniform Constants {
    float octree_length;
    uint octree_level;
} constants;

layout(binding = 0, std430) volatile coherent buffer NodePool {
    NodeTile pool[];
} node_pool;

void main() {
    vec3 node_extent = node_extent(constants.octree_length, constants.octree_level);
    vec3 node_half_extent = node_extent / 2;
    uint nodes_per_dimension = 1 << constants.octree_level;
    ivec3 node_offset_coefficient = node_offset_coefficient(nodes_per_dimension, gl_VertexIndex);
    vec3 node_center = vec3(-constants.octree_length / 2) + node_offset_coefficient * node_extent + node_half_extent;

    IntersectionResult result = intersect_octree(constants.octree_length, constants.octree_level + 1, node_center);
    if (result.intersected) {

        for (uint i = 0; i < NEIGHBOUR_OFFSET_COEFFICIENT.length(); i++) {
            vec3 neighbour_center = node_center + NEIGHBOUR_OFFSET_COEFFICIENT[i] * node_extent;

            IntersectionResult neighbour_result = intersect_octree(constants.octree_length, constants.octree_level + 1, neighbour_center);
            if (neighbour_result.intersected) {
                node_pool.pool[result.node].neighbours[i] = neighbour_result.node;
            }
        }
    }

    gl_PointSize = 1.0;
}

uint get_child(uint node_index, uint child_index) {
    return node_pool.pool[node_index].children[child_index];
}