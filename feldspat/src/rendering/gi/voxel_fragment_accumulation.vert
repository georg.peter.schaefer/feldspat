#version 450
#extension GL_EXT_shader_atomic_float : enable

#include <voxel_fragment.glsl>
#include <octree.glsl>

layout(push_constant) uniform Constants {
    float octree_length;
    uint octree_level;
} constants;

layout(binding = 0, std430) readonly buffer VoxelFragments {
    VoxelFragment fragments[];
} voxel_fragments;
layout(binding = 1, std430) readonly buffer NodePool {
    NodeTile pool[];
} node_pool;
layout(binding = 2, r32f) volatile coherent uniform image3D base_color_red_image;
layout(binding = 3, r32f) volatile coherent uniform image3D base_color_green_image;
layout(binding = 4, r32f) volatile coherent uniform image3D base_color_blue_image;
layout(binding = 5, r32f) volatile coherent uniform image3D visibility_image;
layout(binding = 6, r32f) volatile coherent uniform image3D fragment_count;

ivec3 brick_coords(uint node) {
    return 3 * index_3d(imageSize(base_color_red_image).x / 3, node_pool.pool[node].brick);
}

void voxel_fragment_atomic_add(ivec3 coords, VoxelFragment fragment) {
    imageAtomicAdd(fragment_count, coords, 1);
    imageAtomicAdd(base_color_red_image, coords, fragment.base_color.r);
    imageAtomicAdd(base_color_green_image, coords, fragment.base_color.g);
    imageAtomicAdd(base_color_blue_image, coords, fragment.base_color.b);
    imageStore(visibility_image, coords, 1.0.xxxx);
}

void voxel_atomic_add(uint node, uint child, VoxelFragment fragment) {
    ivec3 node_brick_coords = brick_coords(node);
    for (uint fragment_index = 0; fragment_index < 8; fragment_index++) {
        ivec3 fragment_coords = node_brick_coords + index_3d(3, NODE_VOXELS[child].fragments[fragment_index]);
        voxel_fragment_atomic_add(fragment_coords, fragment);
    }

    for (uint shared_fragments_index = 0; shared_fragments_index < 7; shared_fragments_index++) {
        SharedFragments shared_fragments = NODE_VOXELS[child].shared_fragments[shared_fragments_index];
        uint neighbour = node_pool.pool[node].neighbours[shared_fragments.neighbour];
        if (neighbour == 0) {
            continue;
        }

        ivec3 neighbour_brick_coords = brick_coords(neighbour);
        for (uint fragment_index = 0; fragment_index < shared_fragments.count; fragment_index++) {
            ivec3 fragment_coords = neighbour_brick_coords + index_3d(3, shared_fragments.fragments[fragment_index]);
            voxel_fragment_atomic_add(fragment_coords, fragment);
        }
    }
}

void main() {
    VoxelFragment fragment = voxel_fragments.fragments[gl_VertexIndex];
    IntersectionResult result = intersect_octree(constants.octree_length, constants.octree_level, fragment.position);
    if (result.intersected) {
        voxel_atomic_add(result.node, result.child, fragment);
    }
    gl_PointSize = 1.0;
}

uint get_child(uint node_index, uint child_index) {
    return node_pool.pool[node_index].children[child_index];
}