#version 450

#include <octree.glsl>

const float[8] VOXEL_WEIGHTS = float[8](0.03125, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.125);

const ivec3[8][8] CENTER_VOXEL_SAMPLES = ivec3[8][8](
    ivec3[8](ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2), ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(2, 2, 1), ivec3(2, 2, 2)),
    ivec3[8](ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0), ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(2, 2, 1), ivec3(2, 2, 0)),
    ivec3[8](ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2), ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(2, 0, 1), ivec3(2, 0, 2)),
    ivec3[8](ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0), ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(2, 0, 1), ivec3(2, 0, 0)),
    ivec3[8](ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2), ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(0, 2, 1), ivec3(0, 2, 2)),
    ivec3[8](ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0), ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(0, 2, 1), ivec3(0, 2, 0)),
    ivec3[8](ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2), ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(0, 0, 1), ivec3(0, 0, 2)),
    ivec3[8](ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0), ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(0, 0, 1), ivec3(0, 0, 0))
);
const float CENTER_VOXEL_WEIGHT = 0.015625;

const ivec3[8][8] CORNER_VOXEL_SAMPLES = ivec3[8][8](
    ivec3[8](ivec3(0, 0, 0), ivec3(0, 0, 1), ivec3(0, 1, 0), ivec3(0, 1, 1), ivec3(1, 0, 0), ivec3(1, 0, 1), ivec3(1, 1, 0), ivec3(1, 1, 1)),
    ivec3[8](ivec3(0, 0, 2), ivec3(0, 0, 1), ivec3(0, 1, 2), ivec3(0, 1, 1), ivec3(1, 0, 2), ivec3(1, 0, 1), ivec3(1, 1, 2), ivec3(1, 1, 1)),
    ivec3[8](ivec3(0, 2, 0), ivec3(0, 2, 1), ivec3(0, 1, 0), ivec3(0, 1, 1), ivec3(1, 2, 0), ivec3(1, 2, 1), ivec3(1, 1, 0), ivec3(1, 1, 1)),
    ivec3[8](ivec3(0, 2, 2), ivec3(0, 2, 1), ivec3(0, 1, 2), ivec3(0, 1, 1), ivec3(1, 2, 2), ivec3(1, 2, 1), ivec3(1, 1, 2), ivec3(1, 1, 1)),
    ivec3[8](ivec3(2, 0, 0), ivec3(2, 0, 1), ivec3(2, 1, 0), ivec3(2, 1, 1), ivec3(1, 0, 0), ivec3(1, 0, 1), ivec3(1, 1, 0), ivec3(1, 1, 1)),
    ivec3[8](ivec3(2, 0, 2), ivec3(2, 0, 1), ivec3(2, 1, 2), ivec3(2, 1, 1), ivec3(1, 0, 2), ivec3(1, 0, 1), ivec3(1, 1, 2), ivec3(1, 1, 1)),
    ivec3[8](ivec3(2, 2, 0), ivec3(2, 2, 1), ivec3(2, 1, 0), ivec3(2, 1, 1), ivec3(1, 2, 0), ivec3(1, 2, 1), ivec3(1, 1, 0), ivec3(1, 1, 1)),
    ivec3[8](ivec3(2, 2, 2), ivec3(2, 2, 1), ivec3(2, 1, 2), ivec3(2, 1, 1), ivec3(1, 2, 2), ivec3(1, 2, 1), ivec3(1, 1, 2), ivec3(1, 1, 1))
);
const float[8] CORNER_VOXEL_WEIGHTS = float[8](0.125, 0.0625, 0.0625, 0.03125, 0.0625, 0.03125, 0.03125, 0.015625);
const ivec3[8] CORNERS = ivec3[8](
    ivec3(0, 0, 0),
    ivec3(0, 0, 2),
    ivec3(0, 2, 0),
    ivec3(0, 2, 2),
    ivec3(2, 0, 0),
    ivec3(2, 0, 2),
    ivec3(2, 2, 0),
    ivec3(2, 2, 2)
);

const uint[6][4] FACE_NODES = uint[6][4](
    uint[4](0, 1, 2, 3),
    uint[4](4, 5, 6, 7),
    uint[4](0, 2, 4, 6),
    uint[4](1, 3, 5, 7),
    uint[4](0, 1, 4, 5),
    uint[4](2, 3, 6, 7)
);
const ivec3[6][4][8] FACE_VOXEL_SAMPLES = ivec3[6][4][8](
    ivec3[4][8](
        ivec3[8](ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(0, 2, 1), ivec3(0, 2, 2), ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2)),
        ivec3[8](ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(0, 2, 1), ivec3(0, 2, 0), ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0)),
        ivec3[8](ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(0, 0, 1), ivec3(0, 0, 2), ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2)),
        ivec3[8](ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(0, 0, 1), ivec3(0, 0, 0), ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0))
    ),
    ivec3[4][8](
        ivec3[8](ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(2, 2, 1), ivec3(2, 2, 2), ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2)),
        ivec3[8](ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(2, 2, 1), ivec3(2, 2, 0), ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0)),
        ivec3[8](ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(2, 0, 1), ivec3(2, 0, 2), ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2)),
        ivec3[8](ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(2, 0, 1), ivec3(2, 0, 0), ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0))
    ),
    ivec3[4][8](
        ivec3[8](ivec3(1, 1, 0), ivec3(2, 1, 0), ivec3(1, 2, 0), ivec3(2, 2, 0), ivec3(1, 1, 1), ivec3(2, 1, 1), ivec3(1, 2, 1), ivec3(2, 2, 1)),
        ivec3[8](ivec3(1, 1, 0), ivec3(0, 1, 0), ivec3(1, 2, 0), ivec3(0, 2, 0), ivec3(1, 1, 1), ivec3(0, 1, 1), ivec3(1, 2, 1), ivec3(0, 2, 1)),
        ivec3[8](ivec3(1, 1, 0), ivec3(2, 1, 0), ivec3(1, 0, 0), ivec3(2, 0, 0), ivec3(1, 1, 1), ivec3(2, 1, 1), ivec3(1, 0, 1), ivec3(2, 0, 1)),
        ivec3[8](ivec3(1, 1, 0), ivec3(0, 1, 0), ivec3(1, 0, 0), ivec3(0, 0, 0), ivec3(1, 1, 1), ivec3(0, 1, 1), ivec3(1, 0, 1), ivec3(0, 0, 1))
    ),
    ivec3[4][8](
        ivec3[8](ivec3(1, 1, 2), ivec3(2, 1, 2), ivec3(1, 2, 2), ivec3(2, 2, 2), ivec3(1, 1, 1), ivec3(2, 1, 1), ivec3(1, 2, 1), ivec3(2, 2, 1)),
        ivec3[8](ivec3(1, 1, 2), ivec3(0, 1, 2), ivec3(1, 2, 2), ivec3(0, 2, 2), ivec3(1, 1, 1), ivec3(0, 1, 1), ivec3(1, 2, 1), ivec3(0, 2, 1)),
        ivec3[8](ivec3(1, 1, 2), ivec3(2, 1, 2), ivec3(1, 0, 2), ivec3(2, 0, 2), ivec3(1, 1, 1), ivec3(2, 1, 1), ivec3(1, 0, 1), ivec3(2, 0, 1)),
        ivec3[8](ivec3(1, 1, 2), ivec3(0, 1, 2), ivec3(1, 0, 2), ivec3(0, 0, 2), ivec3(1, 1, 1), ivec3(0, 1, 1), ivec3(1, 0, 1), ivec3(0, 0, 1))
    ),
    ivec3[4][8](
        ivec3[8](ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(2, 0, 1), ivec3(2, 0, 2), ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2)),
        ivec3[8](ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(2, 0, 1), ivec3(2, 0, 0), ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0)),
        ivec3[8](ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(0, 0, 1), ivec3(0, 0, 2), ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2)),
        ivec3[8](ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(0, 0, 1), ivec3(0, 0, 0), ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0))
    ),
    ivec3[4][8](
        ivec3[8](ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(2, 2, 1), ivec3(2, 2, 2), ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2)),
        ivec3[8](ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(2, 2, 1), ivec3(2, 2, 0), ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0)),
        ivec3[8](ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(0, 2, 1), ivec3(0, 2, 2), ivec3(1, 1, 1), ivec3(1, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2)),
        ivec3[8](ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(0, 2, 1), ivec3(0, 2, 0), ivec3(1, 1, 1), ivec3(1, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0))
    )
);
const float[8] FACE_VOXEL_WEIGHTS = float[8](0.03125, 0.03125, 0.0625, 0.03125, 0.015625, 0.015625, 0.015625, 0.015625);
const ivec3[6] FACES = ivec3[6](
    ivec3(0, 1, 1),
    ivec3(2, 1, 1),
    ivec3(1, 1, 0),
    ivec3(1, 1, 2),
    ivec3(1, 0, 1),
    ivec3(1, 2, 1)
);

const uint[12][2] EDGE_NODES = uint[12][2](
    uint[2](0, 1),
    uint[2](2, 3),
    uint[2](4, 5),
    uint[2](6, 7),
    uint[2](0, 4),
    uint[2](2, 6),
    uint[2](1, 5),
    uint[2](3, 7),
    uint[2](0, 2),
    uint[2](1, 3),
    uint[2](4, 6),
    uint[2](5, 7)
);
const ivec3[12][2][8] EDGE_VOXEL_SAMPLES = ivec3[12][2][8](
    ivec3[2][8](
        ivec3[8](ivec3(0, 0, 1), ivec3(0, 0, 2), ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2), ivec3(1, 1, 1), ivec3(1, 1, 2)),
        ivec3[8](ivec3(0, 0, 1), ivec3(0, 0, 0), ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0), ivec3(1, 1, 1), ivec3(1, 1, 0))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(0, 2, 1), ivec3(0, 2, 2), ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2), ivec3(1, 1, 1), ivec3(1, 1, 2)),
        ivec3[8](ivec3(0, 2, 1), ivec3(0, 2, 0), ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0), ivec3(1, 1, 1), ivec3(1, 1, 0))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(2, 0, 1), ivec3(2, 0, 2), ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2), ivec3(1, 1, 1), ivec3(1, 1, 2)),
        ivec3[8](ivec3(2, 0, 1), ivec3(2, 0, 0), ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0), ivec3(1, 1, 1), ivec3(1, 1, 0))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(2, 2, 1), ivec3(2, 2, 2), ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2), ivec3(1, 1, 1), ivec3(1, 1, 2)),
        ivec3[8](ivec3(2, 2, 1), ivec3(2, 2, 0), ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0), ivec3(1, 1, 1), ivec3(1, 1, 0))
    ),

    ivec3[2][8](
        ivec3[8](ivec3(1, 0, 0), ivec3(2, 0, 0), ivec3(1, 1, 0), ivec3(2, 1, 0), ivec3(1, 0, 1), ivec3(2, 0, 1), ivec3(1, 1, 1), ivec3(2, 1, 1)),
        ivec3[8](ivec3(1, 0, 0), ivec3(0, 0, 0), ivec3(1, 1, 0), ivec3(0, 1, 0), ivec3(1, 0, 1), ivec3(0, 0, 1), ivec3(1, 1, 1), ivec3(0, 1, 1))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(1, 2, 0), ivec3(2, 2, 0), ivec3(1, 1, 0), ivec3(2, 1, 0), ivec3(1, 2, 1), ivec3(2, 2, 1), ivec3(1, 1, 1), ivec3(2, 1, 1)),
        ivec3[8](ivec3(1, 2, 0), ivec3(0, 2, 0), ivec3(1, 1, 0), ivec3(0, 1, 0), ivec3(1, 2, 1), ivec3(0, 2, 1), ivec3(1, 1, 1), ivec3(0, 1, 1))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(1, 0, 2), ivec3(2, 0, 2), ivec3(1, 1, 2), ivec3(2, 1, 2), ivec3(1, 0, 1), ivec3(2, 0, 1), ivec3(1, 1, 1), ivec3(2, 1, 1)),
        ivec3[8](ivec3(1, 0, 2), ivec3(0, 0, 2), ivec3(1, 1, 2), ivec3(0, 1, 2), ivec3(1, 0, 1), ivec3(0, 0, 1), ivec3(1, 1, 1), ivec3(0, 1, 1))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(1, 2, 2), ivec3(2, 2, 2), ivec3(1, 1, 2), ivec3(2, 1, 2), ivec3(1, 2, 1), ivec3(2, 2, 1), ivec3(1, 1, 1), ivec3(2, 1, 1)),
        ivec3[8](ivec3(1, 2, 2), ivec3(0, 2, 2), ivec3(1, 1, 2), ivec3(0, 1, 2), ivec3(1, 2, 1), ivec3(0, 2, 1), ivec3(1, 1, 1), ivec3(0, 1, 1))
    ),

    ivec3[2][8](
        ivec3[8](ivec3(0, 2, 1), ivec3(0, 2, 0), ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0), ivec3(1, 1, 1), ivec3(1, 1, 0)),
        ivec3[8](ivec3(0, 0, 1), ivec3(0, 0, 0), ivec3(0, 1, 1), ivec3(0, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0), ivec3(1, 1, 1), ivec3(1, 1, 0))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(0, 2, 1), ivec3(0, 2, 2), ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2), ivec3(1, 1, 1), ivec3(1, 1, 2)),
        ivec3[8](ivec3(0, 0, 1), ivec3(0, 0, 2), ivec3(0, 1, 1), ivec3(0, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2), ivec3(1, 1, 1), ivec3(1, 1, 2))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(2, 2, 1), ivec3(2, 2, 0), ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(1, 2, 1), ivec3(1, 2, 0), ivec3(1, 1, 1), ivec3(1, 1, 0)),
        ivec3[8](ivec3(2, 0, 1), ivec3(2, 0, 0), ivec3(2, 1, 1), ivec3(2, 1, 0), ivec3(1, 0, 1), ivec3(1, 0, 0), ivec3(1, 1, 1), ivec3(1, 1, 0))
    ),
    ivec3[2][8](
        ivec3[8](ivec3(2, 2, 1), ivec3(2, 2, 2), ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(1, 2, 1), ivec3(1, 2, 2), ivec3(1, 1, 1), ivec3(1, 1, 2)),
        ivec3[8](ivec3(2, 0, 1), ivec3(2, 0, 2), ivec3(2, 1, 1), ivec3(2, 1, 2), ivec3(1, 0, 1), ivec3(1, 0, 2), ivec3(1, 1, 1), ivec3(1, 1, 2))
    )
);
const float[8] EDGE_VOXEL_WEIGHTS = float[8](0.03125, 0.0625, 0.03125, 0.03125, 0.03125, 0.03125, 0.015625, 0.015625);
const ivec3[12] EDGES = ivec3[12](
    ivec3(0, 0, 1),
    ivec3(0, 1, 1),
    ivec3(2, 0, 1),
    ivec3(2, 1, 1),
    ivec3(1, 0, 0),
    ivec3(1, 2, 0),
    ivec3(1, 0, 2),
    ivec3(1, 2, 2),
    ivec3(0, 1, 0),
    ivec3(0, 1, 2),
    ivec3(2, 1, 0),
    ivec3(2, 1, 2)
);

layout(binding = 0, std430) readonly buffer NodePool {
    NodeTile pool[];
} node_pool;
layout(binding = 1, r32f) volatile coherent uniform image3D base_color_red_image;
layout(binding = 2, r32f) volatile coherent uniform image3D base_color_green_image;
layout(binding = 3, r32f) volatile coherent uniform image3D base_color_blue_image;
layout(binding = 4, r32f) volatile coherent uniform image3D occlusion_image;

vec4 load_brick_texel(ivec3 coords) {
    return vec4(
        imageLoad(base_color_red_image, coords).x,
        imageLoad(base_color_green_image, coords).x,
        imageLoad(base_color_blue_image, coords).x,
        imageLoad(occlusion_image, coords).x);
}

void store_brick_texel(ivec3 coords, vec4 value) {
    imageStore(base_color_red_image, coords, value.xxxx);
    imageStore(base_color_green_image, coords, value.yyyy);
    imageStore(base_color_blue_image, coords, value.zzzz);
    imageStore(occlusion_image, coords, value.wwww);
}

void main() {
    uint brick_pool_resolution = imageSize(base_color_red_image).x;
    uint brick = node_pool.pool[gl_VertexIndex].brick;
    ivec3 brick_offset = brick_offset_coefficient(brick_pool_resolution, brick);

//    vec4 center_color = vec4(0);
//    for (uint i = 0; i < 8; i++) {
//        uint child = node_pool.pool[gl_VertexIndex].children[i];
//        if (child == 0) {
//            continue;
//        }
//
//        uint child_brick = node_pool.pool[child].brick;
//        ivec3 child_brick_offset = brick_offset_coefficient(brick_pool_resolution, child_brick);
//        for (uint j = 0; j < 8; j++) {
//            center_color += CENTER_VOXEL_WEIGHT * load_brick_texel(child_brick_offset + CENTER_VOXEL_SAMPLES[i][j]);
//        }
//    }
//    store_brick_texel(brick_offset + ivec3(1, 1, 1), center_color);
//
//    for (uint i = 1; i < 8; i++) {
//        vec4 corner_color = vec4(0);
//        uint child = node_pool.pool[gl_VertexIndex].children[i];
//        if (child == 0) {
//            continue;
//        }
//
//        uint child_brick = node_pool.pool[child].brick;
//        ivec3 child_brick_offset = brick_offset_coefficient(brick_pool_resolution, child_brick);
//        for (uint j = 0; j < 8; j++) {
//            corner_color += CORNER_VOXEL_WEIGHTS[j] * load_brick_texel(child_brick_offset + CORNER_VOXEL_SAMPLES[i][j]);
//        }
//        store_brick_texel(brick_offset + CORNERS[i], corner_color);
//    }

    for (uint i = 0; i < MIP_MAP_FRAGMENTS.length(); i++) {
        vec4 color = vec4(0.0);
        for (uint j = 0; j < 27; j++) {
            NodeFragment node_fragment = MIP_MAP_FRAGMENTS[i][j];
            uint child = node_pool.pool[gl_VertexIndex].children[node_fragment.child];
            if (child == 0) {
                continue;
            }

            ivec3 sample_offset = index_3d(3, node_fragment.fragment);
            ivec3 child_brick_offset = ivec3(0);
            if (node_fragment.self) {
                child_brick_offset = brick_offset_coefficient(brick_pool_resolution, node_pool.pool[child].brick);
            } else {
                uint neighbour = node_pool.pool[child].neighbours[node_fragment.neighbour];
                if (neighbour == 0) {
                    continue;
                }
                child_brick_offset = brick_offset_coefficient(brick_pool_resolution, node_pool.pool[neighbour].brick);
            }

            color += MIP_MAP_FARGMENTS_WEIGHTS[j] * load_brick_texel(child_brick_offset + sample_offset);
        }
        store_brick_texel(brick_offset + index_3d(3, i), color);
    }

//    for (uint i = 0; i < 6; i++) {
//        vec4 face_color = vec4(0);
//        for (uint j = 0; j < 4; j++) {
//            uint child = node_pool.pool[gl_VertexIndex].children[FACE_NODES[i][j]];
//            if (child == 0) {
//                continue;
//            }
//
//            uint child_brick = node_pool.pool[child].brick;
//            ivec3 child_brick_offset = brick_offset_coefficient(brick_pool_resolution, child_brick);
//            for (uint k = 0; k < 8; k++) {
//                face_color += FACE_VOXEL_WEIGHTS[k] * load_brick_texel(child_brick_offset + FACE_VOXEL_SAMPLES[i][j][k]);
//            }
//        }
//        store_brick_texel(brick_offset + FACES[i], face_color);
//    }
//
//    for (uint i = 0; i < 12; i++) {
//        vec4 edge_color = vec4(0);
//        for (uint j = 0; j < 2; j++) {
//            uint child = node_pool.pool[gl_VertexIndex].children[EDGE_NODES[i][j]];
//            if (child == 0) {
//                continue;
//            }
//
//            uint child_brick = node_pool.pool[child].brick;
//            ivec3 child_brick_offset = brick_offset_coefficient(brick_pool_resolution, child_brick);
//            for (uint k = 0; k < 8; k++) {
//                edge_color += EDGE_VOXEL_WEIGHTS[k] * load_brick_texel(child_brick_offset + EDGE_VOXEL_SAMPLES[i][j][k]);
//            }
//        }
//        store_brick_texel(brick_offset + EDGES[i], edge_color);
//    }

    gl_PointSize = 1.0;
}