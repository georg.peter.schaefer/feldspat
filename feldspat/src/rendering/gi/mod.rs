//! Global illumination pipeline.

use std::convert::TryInto;
use std::sync::Arc;

use ash::vk;
use image::flat::View;
use itertools::Itertools;

use crate::ecs::{Entities, Transform};
use crate::engine::Config;
use crate::graphics::buffer::{CpuAccessibleBuffer, ImmutableBuffer};
use crate::graphics::command_buffer::CommandBuffer;
use crate::graphics::image::{Image, ImageView, ImageViewCreationError, ImmutableImage, Sampler};
use crate::graphics::Device;
use crate::math::Vec3;
use crate::rendering::gi::brick_pool_clear_pass::BrickPoolClearPass;
use crate::rendering::gi::counter_clear_pass::CounterClearPass;
use crate::rendering::gi::mip_mapping_pass::MipMappingPass;
use crate::rendering::gi::node_allocation_pass::NodeAllocationPass;
use crate::rendering::gi::node_initialization_pass::NodeInitializationPass;
use crate::rendering::gi::node_neighbour_pass::NodeNeighbourPass;
use crate::rendering::gi::node_subdivision_pass::NodeSubdivisionPass;
use crate::rendering::gi::thread_manager_pass::{Pass, ThreadManagerPass};
use crate::rendering::gi::voxel_fragment_accumulation_pass::VoxelFragmentAccumulationPass;
use crate::rendering::gi::voxel_fragment_list_pass::VoxelFragmentListPass;
use crate::rendering::gi::voxel_fragment_normalization_pass::VoxelFragmentNormalizationPass;
use crate::rendering::static_mesh::StaticMesh;
use crate::rendering::{PipelineCreationError, PipelineExecutionError};
use std::cell::{Cell, RefCell};

pub mod brick_pool_clear_pass;
pub mod counter_clear_pass;
pub mod mip_mapping_pass;
pub mod node_allocation_pass;
pub mod node_initialization_pass;
pub mod node_neighbour_pass;
pub mod node_subdivision_pass;
pub mod octree_debug_pass;
pub mod thread_manager_pass;
pub mod voxel_fragment_accumulation_pass;
pub mod voxel_fragment_list_pass;
pub mod voxel_fragment_normalization_pass;

/// Global illumination pipeline.
pub struct GiPipeline {
    built: Cell<bool>,
    mip_mapping_pass: MipMappingPass,
    voxel_fragment_normalization_pass: VoxelFragmentNormalizationPass,
    voxel_fragment_accumulation_pass: VoxelFragmentAccumulationPass,
    node_neighbour_pass: NodeNeighbourPass,
    node_initialization_pass: NodeInitializationPass,
    node_allocation_pass: NodeAllocationPass,
    node_subdivision_pass: NodeSubdivisionPass,
    voxel_fragment_list_pass: VoxelFragmentListPass,
    thread_manager_pass: ThreadManagerPass,
    brick_pool_clear_pass: BrickPoolClearPass,
    counter_clear_pass: CounterClearPass,
    gi_data: Arc<GiData>,
    device: Arc<Device>,
    config: Arc<Config>,
}

impl GiPipeline {
    /// Creates a new global illumination pipeline.
    pub fn new(config: Arc<Config>, device: Arc<Device>) -> Result<Self, PipelineCreationError> {
        let gi_data = GiData::new(&config, device.clone())?;
        let counter_clear_pass = CounterClearPass::new(device.clone(), gi_data.clone())?;
        let brick_pool_clear_pass = BrickPoolClearPass::new(device.clone(), gi_data.clone())?;
        let thread_manager_pass = ThreadManagerPass::new(device.clone(), gi_data.clone())?;
        let voxel_fragment_list_pass =
            VoxelFragmentListPass::new(config.clone(), device.clone(), gi_data.clone())?;
        let node_subdivision_pass =
            NodeSubdivisionPass::new(config.clone(), device.clone(), gi_data.clone())?;
        let node_allocation_pass = NodeAllocationPass::new(device.clone(), gi_data.clone())?;
        let node_initialization_pass =
            NodeInitializationPass::new(device.clone(), gi_data.clone())?;
        let node_neighbour_pass =
            NodeNeighbourPass::new(config.clone(), device.clone(), gi_data.clone())?;
        let voxel_fragment_accumulation_pass =
            VoxelFragmentAccumulationPass::new(config.clone(), device.clone(), gi_data.clone())?;
        let voxel_fragment_normalization_pass =
            VoxelFragmentNormalizationPass::new(device.clone(), gi_data.clone())?;
        let mip_mapping_pass = MipMappingPass::new(device.clone(), gi_data.clone())?;

        Ok(Self {
            built: Cell::new(false),
            mip_mapping_pass,
            voxel_fragment_normalization_pass,
            voxel_fragment_accumulation_pass,
            node_neighbour_pass,
            node_initialization_pass,
            node_allocation_pass,
            node_subdivision_pass,
            voxel_fragment_list_pass,
            thread_manager_pass,
            brick_pool_clear_pass,
            counter_clear_pass,
            gi_data,
            device,
            config,
        })
    }

    /// Executed the pipeline.
    pub fn execute(&self, entities: &Entities) -> Result<(), PipelineExecutionError> {
        let scene = entities.components::<(Transform, StaticMesh)>();
        if !self.built.get()
            && scene
                .iter()
                .all(|(_, _, static_mesh)| static_mesh.is_ready())
        {
            let queue = self.device.graphics_queue();

            let mut builder = CommandBuffer::primary(
                self.device.clone(),
                vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
                queue.family(),
            )?;
            self.counter_clear_pass.execute(&mut builder)?;
            self.brick_pool_clear_pass.execute(&mut builder)?;
            self.voxel_fragment_list_pass
                .execute(&mut builder, entities)?;
            self.thread_manager_pass.execute(
                &mut builder,
                Pass::Initialization,
                0u32.overflowing_sub(1u32).0,
            )?;
            self.node_initialization_pass.execute(&mut builder)?;

            for level in 0..self.gi_data.octree.max_height() {
                self.thread_manager_pass
                    .execute(&mut builder, Pass::Subdivision, level)?;
                self.node_subdivision_pass.execute(&mut builder, level)?;
                self.thread_manager_pass
                    .execute(&mut builder, Pass::Allocation, level)?;
                self.node_allocation_pass.execute(&mut builder, level)?;
                self.thread_manager_pass
                    .execute(&mut builder, Pass::Initialization, level)?;
                self.node_initialization_pass.execute(&mut builder)?;
                self.thread_manager_pass
                    .execute(&mut builder, Pass::Neighbour, level)?;
                self.node_neighbour_pass.execute(&mut builder, level);
            }
            self.thread_manager_pass.execute(
                &mut builder,
                Pass::Accumulation,
                self.gi_data.octree.max_height(),
            )?;
            self.voxel_fragment_accumulation_pass
                .execute(&mut builder)?;
            self.thread_manager_pass.execute(
                &mut builder,
                Pass::Normalization,
                self.gi_data.octree.max_height(),
            )?;
            self.voxel_fragment_normalization_pass
                .execute(&mut builder)?;

            self.thread_manager_pass.execute(
                &mut builder,
                Pass::Subdivision,
                self.gi_data.octree.max_height(),
            )?;
            self.node_subdivision_pass
                .execute(&mut builder, self.gi_data.octree.max_height())?;

            for level in (0..self.gi_data.octree.max_height()).rev() {
                self.thread_manager_pass
                    .execute(&mut builder, Pass::MipMapping, level)?;
                self.mip_mapping_pass.execute(&mut builder)?;
            }

            builder
                .transition_image_layout(
                    self.gi_data.octree.brick_pool.base_color.components[0]
                        .image()
                        .clone(),
                    vk::ImageLayout::GENERAL,
                    vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                    vk::AccessFlags::SHADER_WRITE,
                    vk::AccessFlags::SHADER_READ,
                    vk::PipelineStageFlags::VERTEX_SHADER,
                    vk::PipelineStageFlags::VERTEX_SHADER,
                )
                .transition_image_layout(
                    self.gi_data.octree.brick_pool.base_color.components[1]
                        .image()
                        .clone(),
                    vk::ImageLayout::GENERAL,
                    vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                    vk::AccessFlags::SHADER_WRITE,
                    vk::AccessFlags::SHADER_READ,
                    vk::PipelineStageFlags::VERTEX_SHADER,
                    vk::PipelineStageFlags::VERTEX_SHADER,
                )
                .transition_image_layout(
                    self.gi_data.octree.brick_pool.base_color.components[2]
                        .image()
                        .clone(),
                    vk::ImageLayout::GENERAL,
                    vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                    vk::AccessFlags::SHADER_WRITE,
                    vk::AccessFlags::SHADER_READ,
                    vk::PipelineStageFlags::VERTEX_SHADER,
                    vk::PipelineStageFlags::VERTEX_SHADER,
                )
                .transition_image_layout(
                    self.gi_data.octree.brick_pool.occlusion.components[0]
                        .image()
                        .clone(),
                    vk::ImageLayout::GENERAL,
                    vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                    vk::AccessFlags::SHADER_WRITE,
                    vk::AccessFlags::SHADER_READ,
                    vk::PipelineStageFlags::VERTEX_SHADER,
                    vk::PipelineStageFlags::VERTEX_SHADER,
                );

            let command_buffer = builder.build()?;
            queue.submit(command_buffer, vec![], vec![], None, None)?;
            self.built.replace(true);
        }

        Ok(())
    }

    /// Returns the gi data.
    pub fn gi_data(&self) -> &Arc<GiData> {
        &self.gi_data
    }
}

/// Contains all global illumination data.
pub struct GiData {
    octree: Octree,
    voxel_fragment_list: VoxelFragmentList,
    threads: Arc<ImmutableBuffer<vk::DrawIndirectCommand>>,
}

impl GiData {
    /// Creates new global illumination data.
    pub fn new(config: &Config, device: Arc<Device>) -> Result<Arc<Self>, PipelineCreationError> {
        let threads = ImmutableBuffer::uninitialized(
            device.clone(),
            std::mem::size_of::<vk::DrawIndirectCommand>() as _,
            vk::BufferUsageFlags::STORAGE_BUFFER | vk::BufferUsageFlags::INDIRECT_BUFFER,
        )?;
        let voxel_fragment_list = VoxelFragmentList::new(config, device.clone())?;
        let octree = Octree::new(config, device.clone())?;

        Ok(Arc::new(Self {
            octree,
            voxel_fragment_list,
            threads,
        }))
    }

    pub fn octree(&self) -> &Octree {
        &self.octree
    }
}

/// The voxel fragment list.
pub struct VoxelFragmentList {
    fragments: Arc<ImmutableBuffer<[VoxelFragment]>>,
    length: Arc<CpuAccessibleBuffer<u32>>,
}

impl VoxelFragmentList {
    /// Creates a new voxel fragment list.
    pub fn new(config: &Config, device: Arc<Device>) -> Result<Self, PipelineCreationError> {
        let length = CpuAccessibleBuffer::from_data(
            device.clone(),
            vk::BufferUsageFlags::STORAGE_BUFFER,
            0,
        )?;
        let resolution = config.graphics.global_illumination.resolution;
        let max_fragments_per_voxel = config.graphics.global_illumination.max_fragments_per_voxel;
        let max_fragments = resolution
            * resolution
            * (resolution / 4)
            * std::mem::size_of::<VoxelFragment>() as u64
            * max_fragments_per_voxel;
        let fragments = ImmutableBuffer::uninitialized(
            device.clone(),
            max_fragments as vk::DeviceSize,
            vk::BufferUsageFlags::STORAGE_BUFFER,
        )?;

        Ok(Self { length, fragments })
    }
}

/// A voxel fragment.
#[allow(unused)]
pub struct VoxelFragment {
    position: Vec3,
    metallic: f32,
    normal: Vec3,
    roughness: f32,
    base_color: Vec3,
    _dummy_0: f32,
}

pub struct Octree {
    brick_pool: BrickPool,
    next_brick: Arc<ImmutableBuffer<u32>>,
    node_pool: Arc<ImmutableBuffer<[NodeTile]>>,
    node_pool_offsets: Arc<ImmutableBuffer<[u32]>>,
    node_counts: Arc<ImmutableBuffer<[u32]>>,
    max_height: u32,
}

impl Octree {
    /// Creates a new octree.
    pub fn new(config: &Config, device: Arc<Device>) -> Result<Self, PipelineCreationError> {
        let resolution = config.graphics.global_illumination.resolution;
        let leaf_count = resolution * resolution * resolution;
        let max_height = f32::log(leaf_count as _, 8.0) as u32;
        let node_tile_count = 1 + (f32::powf(8.0, max_height as _) as u32 - 1) / 7;
        let nodes_in_pool =
            (config.graphics.global_illumination.occupied_nodes * node_tile_count as f32) as u32;
        let node_pool = ImmutableBuffer::uninitialized(
            device.clone(),
            (nodes_in_pool * std::mem::size_of::<NodeTile>() as u32) as vk::DeviceSize,
            vk::BufferUsageFlags::STORAGE_BUFFER,
        )?;
        let node_counts = ImmutableBuffer::uninitialized(
            device.clone(),
            (std::mem::size_of::<u32>() * (max_height + 1) as usize) as _,
            vk::BufferUsageFlags::STORAGE_BUFFER,
        )?;
        let node_pool_offsets = ImmutableBuffer::uninitialized(
            device.clone(),
            (std::mem::size_of::<u32>() * (max_height + 1) as usize) as _,
            vk::BufferUsageFlags::STORAGE_BUFFER,
        )?;

        let next_brick = ImmutableBuffer::uninitialized(
            device.clone(),
            std::mem::size_of::<u32>() as _,
            vk::BufferUsageFlags::STORAGE_BUFFER,
        )?;
        let brick_pool = BrickPool::new(device.clone(), nodes_in_pool)?;

        Ok(Self {
            brick_pool,
            next_brick,
            node_pool,
            node_pool_offsets,
            node_counts,
            max_height,
        })
    }

    pub fn brick_pool(&self) -> &BrickPool {
        &self.brick_pool
    }

    pub fn node_pool(&self) -> &Arc<ImmutableBuffer<[NodeTile]>> {
        &self.node_pool
    }

    /// Returns the maximum height of the octree.
    pub fn max_height(&self) -> u32 {
        self.max_height
    }
}

/// An octree node tile.
#[allow(unused)]
pub struct NodeTile {
    parent: u32,
    children: [u32; 8],
    neighbours: [u32; 26],
    brick: u32,
}

/// Different image views for the brick pool texture.
pub struct BrickPool {
    sampler: Arc<Sampler>,
    occlusion: SeparatedComponentsImage<1>,
    base_color: SeparatedComponentsImage<3>,
    fragment_count: SeparatedComponentsImage<1>,
}

impl BrickPool {
    /// Creates brick pool image views.
    pub fn new(device: Arc<Device>, node_tile_count: u32) -> Result<Self, PipelineCreationError> {
        let bricks_per_dimension = f32::cbrt(node_tile_count as _).ceil() as u32;
        let resolution = 3 * bricks_per_dimension;
        let fragment_count = SeparatedComponentsImage::new(device.clone(), resolution)?;
        let base_color = SeparatedComponentsImage::new(device.clone(), resolution)?;
        let visibility = SeparatedComponentsImage::new(device.clone(), resolution)?;
        let sampler = Sampler::builder().build(device.clone())?;

        Ok(Self {
            sampler,
            occlusion: visibility,
            base_color,
            fragment_count,
        })
    }

    pub fn sampler(&self) -> &Arc<Sampler> {
        &self.sampler
    }

    pub fn occlusion(&self) -> &SeparatedComponentsImage<1> {
        &self.occlusion
    }

    pub fn base_color(&self) -> &SeparatedComponentsImage<3> {
        &self.base_color
    }
}

/// Four component image separated as individual images.
pub struct SeparatedComponentsImage<const N: usize> {
    pub components: [Arc<ImageView<Arc<ImmutableImage>>>; N],
}

impl<const N: usize> SeparatedComponentsImage<N> {
    /// Creates a new separated components image.
    pub fn new(device: Arc<Device>, resolution: u32) -> Result<Self, PipelineCreationError> {
        let mut components = Vec::with_capacity(N);
        for _ in 0..N {
            components.push(Self::create_component(device.clone(), resolution)?);
        }
        Ok(Self {
            components: components.try_into().unwrap(),
        })
    }

    fn create_component(
        device: Arc<Device>,
        resolution: u32,
    ) -> Result<Arc<ImageView<Arc<ImmutableImage>>>, PipelineCreationError> {
        let image = ImmutableImage::uninitialized(
            device.clone(),
            vk::Format::R32_SFLOAT,
            vk::Extent3D::builder()
                .width(resolution)
                .height(resolution)
                .depth(resolution)
                .build(),
            1,
            vk::ImageUsageFlags::COLOR_ATTACHMENT
                | vk::ImageUsageFlags::STORAGE
                | vk::ImageUsageFlags::SAMPLED,
        )?;

        Ok(ImageView::new(
            image,
            vk::ImageViewType::TYPE_3D,
            vk::Format::R32_SFLOAT,
            vk::ComponentMapping::builder().build(),
            vk::ImageSubresourceRange::builder()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .base_mip_level(0)
                .level_count(1)
                .base_array_layer(0)
                .layer_count(1)
                .build(),
        )?)
    }
}
