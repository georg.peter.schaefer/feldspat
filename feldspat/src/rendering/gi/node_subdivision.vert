#version 450

#include <voxel_fragment.glsl>
#include <octree.glsl>

layout(push_constant) uniform Constants {
    float octree_length;
    uint octree_level;
} constants;

layout(binding = 0, std430) readonly buffer VoxelFragments {
    VoxelFragment fragments[];
} voxel_fragments;
layout(binding = 1, std430) volatile coherent buffer NodePool {
    NodeTile pool[];
} node_pool;

void main() {
    VoxelFragment fragment = voxel_fragments.fragments[gl_VertexIndex];
    IntersectionResult result = intersect_octree(constants.octree_length, constants.octree_level, fragment.position);
    if (result.intersected) {
        node_pool.pool[result.node].children[result.child] = TOUCHED;
    }
    gl_PointSize = 1.0;
}

uint get_child(uint node_index, uint child_index) {
    return node_pool.pool[node_index].children[child_index];
}