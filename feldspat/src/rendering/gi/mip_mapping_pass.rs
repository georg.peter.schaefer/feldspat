//! Mip mapping pass.

use std::sync::Arc;

use ash::vk;

use crate::engine::Config;
use crate::graphics::buffer::Buffer;
use crate::graphics::command_buffer::{CommandBuffer, CommandBufferBuilder};
use crate::graphics::image::Image;
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorSetLayoutCreationError,
    GraphicsPipelineCreationError,
};
use crate::graphics::render_pass::RenderPassCreationError;
use crate::graphics::{
    Device, Fence, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, SubpassDescription,
};
use crate::rendering::gi::GiData;
use crate::rendering::{PipelineCreationError, PipelineExecutionError};

/// Mip mapping pass.
pub struct MipMappingPass {
    gi_data: Arc<GiData>,
    descriptor_set: Arc<DescriptorSet>,
    pipeline: Arc<GraphicsPipeline>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    framebuffer: Arc<Framebuffer>,
    render_pass: Arc<RenderPass>,
    device: Arc<Device>,
}

impl MipMappingPass {
    /// Creates a new mip mapping pass.
    pub fn new(device: Arc<Device>, gi_data: Arc<GiData>) -> Result<Self, PipelineCreationError> {
        let vertex_shader_module = vertex_shader::Shader::load(&device)?;
        let render_pass = Self::create_render_pass(device.clone())?;
        let framebuffer = Framebuffer::empty(render_pass.clone(), 2, 2)?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(device.clone())?;
        let pipeline = Self::create_pipeline(
            device.clone(),
            render_pass.clone(),
            vertex_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let descriptor_set = DescriptorSet::builder()
            .add_buffer(
                0,
                gi_data.octree.node_pool.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_image(
                1,
                gi_data.octree.brick_pool.base_color.components[0].clone(),
            )
            .add_image(
                2,
                gi_data.octree.brick_pool.base_color.components[1].clone(),
            )
            .add_image(
                3,
                gi_data.octree.brick_pool.base_color.components[2].clone(),
            )
            .add_image(4, gi_data.octree.brick_pool.occlusion.components[0].clone())
            .build(device.clone(), descriptor_set_layout.clone())?;

        Ok(Self {
            gi_data,
            descriptor_set,
            pipeline,
            descriptor_set_layout,
            framebuffer,
            render_pass,
            device,
        })
    }

    fn create_render_pass(device: Arc<Device>) -> Result<Arc<RenderPass>, RenderPassCreationError> {
        Ok(RenderPass::new(
            device.clone(),
            vec![],
            vec![SubpassDescription {
                color_attachments: vec![],
                depth_attachment: None,
                input_attachments: vec![],
            }],
            vec![],
        )?)
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(3)
                    .descriptor_type(vk::DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(4)
                    .descriptor_type(vk::DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipeline(
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader)
            .point_list()
            .viewports(vec![vk::Viewport::builder()
                .width(2.0)
                .height(2.0)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .render_pass(render_pass.clone(), 0)
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    /// Executes the pass.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
    ) -> Result<(), PipelineExecutionError> {
        builder
            .pipeline_barrier(
                vk::PipelineStageFlags::VERTEX_SHADER,
                vk::PipelineStageFlags::DRAW_INDIRECT | vk::PipelineStageFlags::VERTEX_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![
                    Self::create_buffer_memory_barrier(
                        &self.gi_data.threads,
                        vk::AccessFlags::MEMORY_READ,
                    ),
                    Self::create_buffer_memory_barrier(
                        &self.gi_data.octree.node_pool,
                        vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE,
                    ),
                ],
                vec![
                    Self::create_image_memory_barrier(
                        &self.gi_data.octree.brick_pool.base_color.components[0].image(),
                    ),
                    Self::create_image_memory_barrier(
                        &self.gi_data.octree.brick_pool.base_color.components[1].image(),
                    ),
                    Self::create_image_memory_barrier(
                        &self.gi_data.octree.brick_pool.base_color.components[2].image(),
                    ),
                    Self::create_image_memory_barrier(
                        &self.gi_data.octree.brick_pool.occlusion.components[0].image(),
                    ),
                ],
            )
            .begin_render_pass(
                self.framebuffer.clone(),
                vk::SubpassContents::INLINE,
                vec![],
            )
            .bind_pipeline(self.pipeline.clone())
            .bind_descriptor_set(self.pipeline.layout(), self.descriptor_set.clone())
            .draw_indirect(
                self.gi_data.threads.clone(),
                0,
                1,
                std::mem::size_of::<vk::DrawIndirectCommand>() as _,
            )
            .end_render_pass();

        Ok(())
    }

    fn create_buffer_memory_barrier<T>(
        buffer: &T,
        dst_access_mask: vk::AccessFlags,
    ) -> vk::BufferMemoryBarrier
    where
        T: Buffer,
    {
        vk::BufferMemoryBarrier::builder()
            .buffer(buffer.inner())
            .offset(0)
            .size(vk::WHOLE_SIZE)
            .src_access_mask(vk::AccessFlags::SHADER_WRITE)
            .dst_access_mask(dst_access_mask)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .build()
    }

    fn create_image_memory_barrier<T>(image: &T) -> vk::ImageMemoryBarrier
    where
        T: Image,
    {
        vk::ImageMemoryBarrier::builder()
            .image(image.inner())
            .subresource_range(
                vk::ImageSubresourceRange::builder()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .base_mip_level(0)
                    .level_count(1)
                    .base_array_layer(0)
                    .layer_count(1)
                    .build(),
            )
            .src_access_mask(vk::AccessFlags::SHADER_WRITE)
            .dst_access_mask(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .old_layout(vk::ImageLayout::GENERAL)
            .new_layout(vk::ImageLayout::GENERAL)
            .build()
    }
}

mod vertex_shader {
    feldspat_shaders::shader! {
        path: "src/rendering/gi/mip_mapping.vert",
        ty: "vertex",
        include_dirs: ["src/rendering/gi"]
    }
}
