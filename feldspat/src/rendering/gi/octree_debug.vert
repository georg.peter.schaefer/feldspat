#version 450

#include <octree.glsl>

layout(push_constant) uniform Constants {
    mat4 view;
    mat4 projection;
    vec3 eye;
    uint resolution;
    float octree_length;
    uint octree_level;
} constants;

layout(binding = 0, std430) readonly buffer NodePool {
    NodeTile pool[];
} node_pool;
layout(binding = 1) uniform sampler3D base_color_red_image;
layout(binding = 2) uniform sampler3D base_color_green_image;
layout(binding = 3) uniform sampler3D base_color_blue_image;

layout(location = 0) in vec3 position;

layout(location = 0) out flat uint intersected;
layout(location = 1) out vec3 frag_color;

void main() {
    float step_size = constants.octree_length / constants.resolution;
    float half_step_size = step_size / 2;
    vec3 offset = vec3(-constants.octree_length / 2) + node_offset_coefficient(constants.resolution, gl_InstanceIndex) * vec3(step_size);
    vec3 center = offset + vec3(half_step_size);

    IntersectionResult result = intersect_octree(constants.octree_length, constants.octree_level, center);
    if (result.intersected && node_pool.pool[result.node].children[result.child] == TOUCHED) {
        intersected = 1;

        uint resolution = textureSize(base_color_red_image, 0).x;
        float texel_size = 1.0 / resolution;
        float half_texel_size = texel_size / 2;
        vec3 texture_offset = brick_offset_coefficient(resolution, node_pool.pool[result.node].brick) * texel_size;
        vec3 texture_child_offset = child_offset_coefficent(result.child) * texel_size;
        vec3 coords = texture_offset + texture_child_offset + vec3(half_texel_size);

        frag_color = vec3(
            texture(base_color_red_image, coords).r,
            texture(base_color_green_image, coords).r,
            texture(base_color_blue_image, coords).r);
    } else {
        intersected = 0;
    }

    gl_Position = constants.projection * constants.view * vec4(offset + position, 1.0);
}

uint get_child(uint node_index, uint child_index) {
    return node_pool.pool[node_index].children[child_index];
}