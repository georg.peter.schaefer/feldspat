struct VoxelFragment {
    vec3 position;
    float metallic;
    vec3 normal;
    float roughness;
    vec3 base_color;
};