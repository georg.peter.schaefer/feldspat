vec3 brick_coords(uint node_index, uint child_index, uint resolution, vec3 min, vec3 max, vec3 p) {
    float texel_size = 1.0 / resolution;
    float half_texel_size = texel_size / 2.0;
    vec3 texture_offset = brick_offset_coefficient(resolution, get_node(node_index).brick) * texel_size;
    vec3 texture_child_offset = child_offset_coefficent(child_index) * texel_size;
    vec3 extent = max - min;
    vec3 p_normalized = (p - min) / extent;
    return texture_offset + texture_child_offset + vec3(half_texel_size) + p_normalized * texel_size;
}

vec4 sample_brick(vec3 coords);

float f(float r) {
    float lambda = 0.2;
    return 1.0 / (1.0 + lambda * r);
}

vec4 cone_trace(vec3 origin, vec3 direction, float aperture, float max_distance, uint resolution, float length, uint max_level) {
    float distance = 0.0;
    int current_level = int(max_level);
    float alpha = 0.0;
    while (distance <= max_distance && current_level >= 0) {
        float voxel_size = node_size(length, current_level);
        float sample_radius = voxel_size * 0.5;
        float new_distance = sample_radius / tan(aperture);
        float step_size = new_distance - distance;
        distance += step_size;

        vec3 p = origin + distance * direction;
        IntersectionResult result = intersect_octree(length, current_level, p);
        if (result.intersected) {
            vec3 coords = brick_coords(result.node, result.child, resolution, result.min, result.max, p);
            float alpha_2 = sample_brick(coords).a;
            alpha_2 = 1.0 - pow(1.0 - alpha_2, step_size / voxel_size);
            alpha += (1.0 - alpha) * alpha_2;
        }

        current_level -= 1;
    }
//    alpha *= f(distance);

    return vec4(0.0, 0.0, 0.0, alpha);
}