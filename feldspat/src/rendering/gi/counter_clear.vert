#version 450

layout(binding = 0, std430) buffer VoxelFragmentCount {
    uint count;
} voxel_fragment_count;
layout(binding = 1, std430) buffer NodeCounts {
    uint level[];
} node_counts;
layout(binding = 2, std430) buffer NodePoolOffstes {
    uint level[];
} node_pool_offsets;
layout(binding = 3, std430) buffer NextBrick {
    uint next;
} next_brick;

void main() {
    voxel_fragment_count.count = 0;
    node_counts.level[0] = 1;
    node_pool_offsets.level[0] = 0;
    for (uint level = 1; level < node_counts.level.length(); level++) {
        node_counts.level[level] = 0;
        node_pool_offsets.level[level] = 0;
    }
    next_brick.next = 0;
    gl_PointSize = 1.0;
}