//! Brick pool clear pass.

use crate::graphics::command_buffer::{CommandBuffer, CommandBufferBuilder};
use crate::graphics::framebuffer::FramebufferCreationError;
use crate::graphics::image::{Image, ImageView, ImageViewCreationError, ImmutableImage};
use crate::graphics::{Device, Framebuffer, RenderPass, SubpassDescription};
use crate::rendering::gi::GiData;
use crate::rendering::{PipelineCreationError, PipelineExecutionError, Volume};
use ash::vk;
use std::sync::Arc;

/// Brick pool clear pass.
pub struct BrickPoolClearPass {
    framebuffer: Arc<Framebuffer>,
    gi_data: Arc<GiData>,
    device: Arc<Device>,
}

impl BrickPoolClearPass {
    /// Creates the gi clear pass.
    pub fn new(device: Arc<Device>, gi_data: Arc<GiData>) -> Result<Self, PipelineCreationError> {
        let render_pass = Self::create_render_pass(device.clone())?;
        let framebuffer = Self::create_frame_buffer(render_pass.clone(), gi_data.clone())?;

        Ok(Self {
            framebuffer,
            gi_data,
            device,
        })
    }

    fn create_render_pass(device: Arc<Device>) -> Result<Arc<RenderPass>, PipelineCreationError> {
        let attachment_description = vk::AttachmentDescription::builder()
            .format(vk::Format::R32_SFLOAT)
            .samples(vk::SampleCountFlags::TYPE_1)
            .load_op(vk::AttachmentLoadOp::CLEAR)
            .store_op(vk::AttachmentStoreOp::STORE)
            .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .final_layout(vk::ImageLayout::GENERAL)
            .build();

        Ok(RenderPass::new(
            device.clone(),
            vec![attachment_description; 5],
            vec![SubpassDescription {
                color_attachments: vec![
                    vk::AttachmentReference::builder()
                        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .attachment(0)
                        .build(),
                    vk::AttachmentReference::builder()
                        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .attachment(1)
                        .build(),
                    vk::AttachmentReference::builder()
                        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .attachment(2)
                        .build(),
                    vk::AttachmentReference::builder()
                        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .attachment(3)
                        .build(),
                    vk::AttachmentReference::builder()
                        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .attachment(4)
                        .build(),
                ],
                depth_attachment: None,
                input_attachments: vec![],
            }],
            vec![],
        )?)
    }

    fn create_frame_buffer(
        render_pass: Arc<RenderPass>,
        gi_data: Arc<GiData>,
    ) -> Result<Arc<Framebuffer>, PipelineCreationError> {
        Ok(Framebuffer::builder()
            .add(Self::create_view(
                gi_data.octree.brick_pool.base_color.components[0]
                    .image()
                    .clone(),
            )?)
            .add(Self::create_view(
                gi_data.octree.brick_pool.base_color.components[1]
                    .image()
                    .clone(),
            )?)
            .add(Self::create_view(
                gi_data.octree.brick_pool.base_color.components[2]
                    .image()
                    .clone(),
            )?)
            .add(Self::create_view(
                gi_data.octree.brick_pool.occlusion.components[0]
                    .image()
                    .clone(),
            )?)
            .add(Self::create_view(
                gi_data.octree.brick_pool.fragment_count.components[0]
                    .image()
                    .clone(),
            )?)
            .build(render_pass.clone())?)
    }

    fn create_view(
        image: Arc<ImmutableImage>,
    ) -> Result<Arc<ImageView<Arc<ImmutableImage>>>, ImageViewCreationError> {
        Ok(ImageView::new(
            image.clone(),
            vk::ImageViewType::TYPE_2D_ARRAY,
            image.format(),
            vk::ComponentMapping::builder().build(),
            vk::ImageSubresourceRange::builder()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .base_mip_level(0)
                .level_count(1)
                .base_array_layer(0)
                .layer_count(image.extent().depth)
                .build(),
        )?)
    }

    /// Records the pass into a primary command buffer.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
    ) -> Result<(), PipelineExecutionError> {
        builder
            .pipeline_barrier(
                vk::PipelineStageFlags::VERTEX_SHADER,
                vk::PipelineStageFlags::VERTEX_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    Self::create_memory_barrier(
                        &self.gi_data.octree.brick_pool.base_color.components[0].image(),
                    ),
                    Self::create_memory_barrier(
                        &self.gi_data.octree.brick_pool.base_color.components[1].image(),
                    ),
                    Self::create_memory_barrier(
                        &self.gi_data.octree.brick_pool.base_color.components[2].image(),
                    ),
                    Self::create_memory_barrier(
                        &self.gi_data.octree.brick_pool.occlusion.components[0].image(),
                    ),
                    Self::create_memory_barrier(
                        &self.gi_data.octree.brick_pool.fragment_count.components[0].image(),
                    ),
                ],
            )
            .begin_render_pass(
                self.framebuffer.clone(),
                vk::SubpassContents::INLINE,
                vec![
                    vk::ClearValue {
                        color: vk::ClearColorValue {
                            float32: [0.0, 0.0, 0.0, 0.0],
                        },
                    },
                    vk::ClearValue {
                        color: vk::ClearColorValue {
                            float32: [0.0, 0.0, 0.0, 0.0],
                        },
                    },
                    vk::ClearValue {
                        color: vk::ClearColorValue {
                            float32: [0.0, 0.0, 0.0, 0.0],
                        },
                    },
                    vk::ClearValue {
                        color: vk::ClearColorValue {
                            float32: [0.0, 0.0, 0.0, 0.0],
                        },
                    },
                    vk::ClearValue {
                        color: vk::ClearColorValue {
                            float32: [0.0, 0.0, 0.0, 0.0],
                        },
                    },
                ],
            );
        builder.end_render_pass();

        Ok(())
    }

    fn create_memory_barrier<T>(image: &T) -> vk::ImageMemoryBarrier
    where
        T: Image,
    {
        vk::ImageMemoryBarrier::builder()
            .image(image.inner())
            .subresource_range(
                vk::ImageSubresourceRange::builder()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .base_mip_level(0)
                    .level_count(1)
                    .base_array_layer(0)
                    .layer_count(1)
                    .build(),
            )
            .src_access_mask(vk::AccessFlags::SHADER_READ)
            .dst_access_mask(vk::AccessFlags::SHADER_WRITE)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .old_layout(vk::ImageLayout::UNDEFINED)
            .new_layout(vk::ImageLayout::GENERAL)
            .build()
    }
}
