struct SharedFragments {
    uint neighbour;
    uint count;
    uint fragments[4];
};

struct Voxel {
    uint fragments[8];
    SharedFragments shared_fragments[7];
};

const Voxel NODE_VOXELS[8] = Voxel[8](
    Voxel(
        uint[8](0, 1, 3, 4, 9, 10, 12, 13),
        SharedFragments[7](
            SharedFragments(0, 1, uint[4](26, 0, 0, 0)),
            SharedFragments(1, 2, uint[4](24, 25, 0, 0)),
            SharedFragments(3, 2, uint[4](20, 23, 0, 0)),
            SharedFragments(4, 4, uint[4](18, 19, 21, 22)),
            SharedFragments(9, 2, uint[4](8, 17, 0, 0)),
            SharedFragments(10, 4, uint[4](6, 7, 15, 16)),
            SharedFragments(12, 4, uint[4](2, 5, 11, 14))
        )
    ),
    Voxel(
        uint[8](1, 2, 4, 5, 10, 11, 13, 14),
        SharedFragments[7](
            SharedFragments(1, 2, uint[4](25, 26, 0, 0)),
            SharedFragments(2, 1, uint[4](24, 0, 0, 0)),
            SharedFragments(4, 4, uint[4](19, 20, 22, 23)),
            SharedFragments(5, 2, uint[4](18, 21, 0, 0)),
            SharedFragments(10, 4, uint[4](7, 8, 16, 17)),
            SharedFragments(11, 2, uint[4](6, 15, 0, 0)),
            SharedFragments(13, 4, uint[4](0, 3, 9, 12))
        )
    ),
    Voxel(
        uint[8](3, 4, 6, 7, 12, 13, 15, 16),
        SharedFragments[7](
            SharedFragments(3, 2, uint[4](23, 26, 0, 0)),
            SharedFragments(4, 4, uint[4](21, 22, 24, 25)),
            SharedFragments(6, 1, uint[4](20, 0, 0, 0)),
            SharedFragments(7, 2, uint[4](18, 19, 0, 0)),
            SharedFragments(12, 4, uint[4](5, 8, 14, 17)),
            SharedFragments(14, 2, uint[4](2, 11, 0, 0)),
            SharedFragments(15, 4, uint[4](0, 1, 9, 10))
        )
    ),
    Voxel(
        uint[8](4, 5, 7, 8, 13, 14, 16, 17),
        SharedFragments[7](
            SharedFragments(4, 4, uint[4](22, 23, 25, 26)),
            SharedFragments(5, 2, uint[4](21, 24, 0, 0)),
            SharedFragments(7, 2, uint[4](19, 20, 0, 0)),
            SharedFragments(8, 1, uint[4](18, 0, 0, 0)),
            SharedFragments(13, 4, uint[4](3, 6, 12, 15)),
            SharedFragments(15, 4, uint[4](1, 2, 10, 11)),
            SharedFragments(16, 2, uint[4](0, 9, 0, 0))
        )
    ),
    Voxel(
        uint[8](9, 10, 12, 13, 18, 19, 21, 22),
        SharedFragments[7](
            SharedFragments(9, 2, uint[4](17, 26, 0, 0)),
            SharedFragments(10, 4, uint[4](15, 16, 24, 25)),
            SharedFragments(12, 4, uint[4](11, 14, 20, 23)),
            SharedFragments(17, 1, uint[4](8, 0, 0, 0)),
            SharedFragments(18, 2, uint[4](6, 7, 0, 0)),
            SharedFragments(20, 2, uint[4](2, 5, 0, 0)),
            SharedFragments(21, 4, uint[4](0, 1, 3, 4))
        )
    ),
    Voxel(
        uint[8](10, 11, 13, 14, 19, 20, 22, 23),
        SharedFragments[7](
            SharedFragments(10, 4, uint[4](16, 17, 25, 26)),
            SharedFragments(11, 2, uint[4](15, 24, 0, 0)),
            SharedFragments(13, 4, uint[4](9, 12, 18, 21)),
            SharedFragments(18, 2, uint[4](7, 8, 0, 0)),
            SharedFragments(19, 1, uint[4](6, 0, 0, 0)),
            SharedFragments(21, 4, uint[4](1, 2, 4, 5)),
            SharedFragments(22, 2, uint[4](0, 3, 0, 0))
        )
    ),
    Voxel(
        uint[8](12, 13, 15, 16, 21, 22, 24, 25),
        SharedFragments[7](
            SharedFragments(12, 4, uint[4](14, 17, 23, 26)),
            SharedFragments(14, 2, uint[4](11, 20, 0, 0)),
            SharedFragments(15, 4, uint[4](9, 10, 18, 19)),
            SharedFragments(20, 2, uint[4](5, 8, 0, 0)),
            SharedFragments(21, 4, uint[4](3, 4, 6, 7)),
            SharedFragments(23, 1, uint[4](2, 0, 0, 0)),
            SharedFragments(24, 2, uint[4](0, 1, 0, 0))
        )
    ),
    Voxel(
        uint[8](13, 14, 16, 17, 22, 23, 25, 26),
        SharedFragments[7](
            SharedFragments(13, 4, uint[4](12, 15, 21, 24)),
            SharedFragments(15, 4, uint[4](10, 11, 19, 20)),
            SharedFragments(16, 2, uint[4](9, 18, 0, 0)),
            SharedFragments(21, 4, uint[4](4, 5, 7, 8)),
            SharedFragments(22, 2, uint[4](3, 6, 0, 0)),
            SharedFragments(24, 2, uint[4](1, 2, 0, 0)),
            SharedFragments(25, 1, uint[4](0, 0, 0, 0))
        )
    )
);

struct NodeFragment {
    bool self;
    uint child;
    uint neighbour;
    uint fragment;
};

const float CENTER_WEIGHT = 1.0 / 8.0;
const float FACE_WEIGHT = 1.0 / 16.0;
const float EDGE_WEIGHT = 1.0 / 32.0;
const float CORNER_WEIGHT = 1.0 / 64.0;
const float MIP_MAP_FARGMENTS_WEIGHTS[27] = float[27](
    CORNER_WEIGHT,
    EDGE_WEIGHT,
    CORNER_WEIGHT,
    EDGE_WEIGHT,
    FACE_WEIGHT,
    EDGE_WEIGHT,
    CORNER_WEIGHT,
    EDGE_WEIGHT,
    CORNER_WEIGHT,
    EDGE_WEIGHT,
    FACE_WEIGHT,
    EDGE_WEIGHT,
    FACE_WEIGHT,
    CENTER_WEIGHT,
    FACE_WEIGHT,
    EDGE_WEIGHT,
    FACE_WEIGHT,
    EDGE_WEIGHT,
    CORNER_WEIGHT,
    EDGE_WEIGHT,
    CORNER_WEIGHT,
    EDGE_WEIGHT,
    FACE_WEIGHT,
    EDGE_WEIGHT,
    CORNER_WEIGHT,
    EDGE_WEIGHT,
    CORNER_WEIGHT
);

// 0  9 18
// 1 10 19
// 2 11 20

// 3 12 21
// 4 13 22
// 5 14 23

// 6 15 24
// 7 16 25
// 8 17 26
//const NodeFragment MIP_MAP_FRAGMENTS[2][27] = NodeFragment[2][27](
//    NodeFragment[27](
//        NodeFragment(false, 0, 0, 13),
//        NodeFragment(false, 0, 0, 14),
//        NodeFragment(false, 0, 1, 13),
//        NodeFragment(false, 0, 0, 16),
//        NodeFragment(false, 0, 0, 17),
//        NodeFragment(false, 0, 1, 16),
//        NodeFragment(false, 0, 3, 13),
//        NodeFragment(false, 0, 3, 14),
//        NodeFragment(false, 0, 4, 13),
//        NodeFragment(false, 0, 0, 22),
//        NodeFragment(false, 0, 0, 23),
//        NodeFragment(false, 0, 1, 22),
//        NodeFragment(false, 0, 0, 25),
//        NodeFragment(true, 0, 0, 0),
//        NodeFragment(true, 0, 0, 1),
//        NodeFragment(false, 0, 3, 16),
//        NodeFragment(true, 0, 0, 3),
//        NodeFragment(true, 0, 0, 4),
//        NodeFragment(false, 0, 9, 13),
//        NodeFragment(false, 0, 9, 14),
//        NodeFragment(false, 0, 10, 13),
//        NodeFragment(false, 0, 9, 16),
//        NodeFragment(true, 0, 0, 9),
//        NodeFragment(true, 0, 0, 10),
//        NodeFragment(false, 0, 12, 13),
//        NodeFragment(true, 0, 0, 12),
//        NodeFragment(true, 0, 0, 13)
//    ),
//    NodeFragment[27](
//        NodeFragment(false, 0, 1, 13),
//        NodeFragment(false, 0, 1, 14),
//        NodeFragment(false, 0, 2, 13),
//        NodeFragment(false, 0, 1, 16),
//        NodeFragment(false, 0, 1, 17),
//        NodeFragment(false, 0, 2, 16),
//        NodeFragment(false, 0, 4, 13),
//        NodeFragment(false, 0, 4, 14),
//        NodeFragment(false, 0, 5, 13),
//        NodeFragment(false, 0, 1, 22),
//        NodeFragment(false, 0, 1, 23),
//        NodeFragment(false, 0, 2, 22),
//        NodeFragment(true, 0, 0, 1),
//        NodeFragment(true, 0, 0, 2),
//        NodeFragment(true, 1, 0, 1),
//        NodeFragment(true, 0, 0, 4),
//        NodeFragment(true, 0, 0, 5),
//        NodeFragment(true, 1, 0, 4),
//        NodeFragment(false, 0, 9, )
//    )
//);
const NodeFragment MIP_MAP_FRAGMENTS[27][27] = NodeFragment[27][27](
    NodeFragment[27](
        NodeFragment(false, 0, 0, 13),
        NodeFragment(false, 0, 0, 14),
        NodeFragment(false, 0, 1, 13),
        NodeFragment(false, 0, 0, 16),
        NodeFragment(false, 0, 0, 17),
        NodeFragment(false, 0, 1, 16),
        NodeFragment(false, 0, 3, 13),
        NodeFragment(false, 0, 3, 14),
        NodeFragment(false, 0, 4, 13),
        NodeFragment(false, 0, 0, 22),
        NodeFragment(false, 0, 0, 23),
        NodeFragment(false, 0, 1, 22),
        NodeFragment(false, 0, 0, 25),
        NodeFragment(true, 0, 0, 0),
        NodeFragment(true, 0, 0, 1),
        NodeFragment(false, 0, 3, 22),
        NodeFragment(true, 0, 0, 3),
        NodeFragment(true, 0, 0, 4),
        NodeFragment(false, 0, 9, 13),
        NodeFragment(false, 0, 9, 14),
        NodeFragment(false, 0, 10, 13),
        NodeFragment(false, 0, 9, 16),
        NodeFragment(true, 0, 0, 9),
        NodeFragment(true, 0, 0, 10),
        NodeFragment(false, 0, 12, 13),
        NodeFragment(true, 0, 0, 12),
        NodeFragment(true, 0, 0, 13)
    ),
        NodeFragment[27](
        NodeFragment(false, 0, 1, 13),
        NodeFragment(false, 0, 1, 12),
        NodeFragment(false, 0, 2, 13),
        NodeFragment(false, 0, 1, 16),
        NodeFragment(false, 0, 1, 15),
        NodeFragment(false, 0, 2, 16),
        NodeFragment(false, 0, 4, 13),
        NodeFragment(false, 0, 4, 12),
        NodeFragment(false, 0, 5, 13),
        NodeFragment(false, 0, 1, 22),
        NodeFragment(false, 0, 1, 21),
        NodeFragment(false, 0, 2, 22),
        NodeFragment(true, 0, 0, 1),
        NodeFragment(true, 0, 0, 2),
        NodeFragment(true, 1, 0, 1),
        NodeFragment(true, 0, 0, 4),
        NodeFragment(true, 0, 0, 5),
        NodeFragment(true, 1, 0, 4),
        NodeFragment(false, 0, 10, 13),
        NodeFragment(false, 0, 10, 12),
        NodeFragment(false, 0, 11, 13),
        NodeFragment(true, 0, 0, 10),
        NodeFragment(true, 0, 0, 11),
        NodeFragment(true, 1, 0, 10),
        NodeFragment(true, 0, 0, 13),
        NodeFragment(true, 0, 0, 14),
        NodeFragment(true, 1, 0, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 1, 1, 13),
        NodeFragment(false, 1, 2, 12),
        NodeFragment(false, 1, 2, 13),
        NodeFragment(false, 1, 1, 16),
        NodeFragment(false, 1, 2, 15),
        NodeFragment(false, 1, 2, 16),
        NodeFragment(false, 1, 4, 13),
        NodeFragment(false, 1, 5, 12),
        NodeFragment(false, 1, 5, 13),
        NodeFragment(false, 1, 1, 22),
        NodeFragment(false, 1, 2, 21),
        NodeFragment(false, 1, 2, 22),
        NodeFragment(true, 1, 0, 1),
        NodeFragment(true, 1, 0, 2),
        NodeFragment(false, 1, 2, 25),
        NodeFragment(true, 1, 0, 4),
        NodeFragment(true, 1, 0, 5),
        NodeFragment(false, 1, 5, 22),
        NodeFragment(false, 1, 10, 13),
        NodeFragment(false, 1, 11, 12),
        NodeFragment(false, 1, 11, 13),
        NodeFragment(true, 1, 0, 10),
        NodeFragment(true, 1, 0, 11),
        NodeFragment(false, 1, 11, 16),
        NodeFragment(true, 1, 0, 13),
        NodeFragment(true, 1, 0, 14),
        NodeFragment(false, 1, 13, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 0, 3, 13),
        NodeFragment(false, 0, 3, 14),
        NodeFragment(false, 0, 4, 13),
        NodeFragment(false, 0, 3, 10),
        NodeFragment(false, 0, 3, 11),
        NodeFragment(false, 0, 4, 10),
        NodeFragment(false, 0, 6, 13),
        NodeFragment(false, 0, 6, 14),
        NodeFragment(false, 0, 7, 13),
        NodeFragment(false, 0, 3, 22),
        NodeFragment(true, 0, 0, 3),
        NodeFragment(true, 0, 0, 4),
        NodeFragment(false, 0, 3, 19),
        NodeFragment(true, 0, 0, 6),
        NodeFragment(true, 0, 0, 7),
        NodeFragment(false, 0, 6, 22),
        NodeFragment(true, 2, 0, 3),
        NodeFragment(true, 2, 0, 4),
        NodeFragment(false, 0, 12, 13),
        NodeFragment(true, 0, 0, 12),
        NodeFragment(true, 0, 0, 13),
        NodeFragment(false, 0, 12, 10),
        NodeFragment(true, 0, 0, 15),
        NodeFragment(true, 0, 0, 16),
        NodeFragment(false, 0, 14, 13),
        NodeFragment(true, 2, 0, 12),
        NodeFragment(true, 2, 0, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 0, 4, 13),
        NodeFragment(false, 0, 4, 12),
        NodeFragment(false, 0, 5, 13),
        NodeFragment(false, 0, 4, 10),
        NodeFragment(false, 0, 4, 9),
        NodeFragment(false, 0, 5, 10),
        NodeFragment(false, 0, 7, 13),
        NodeFragment(false, 0, 7, 12),
        NodeFragment(false, 0, 8, 13),
        NodeFragment(true, 0, 0, 4),
        NodeFragment(true, 0, 0, 5),
        NodeFragment(true, 1, 0, 4),
        NodeFragment(true, 0, 0, 7),
        NodeFragment(true, 0, 0, 8),
        NodeFragment(true, 1, 0, 7),
        NodeFragment(true, 2, 0, 4),
        NodeFragment(true, 2, 0, 5),
        NodeFragment(true, 3, 0, 4),
        NodeFragment(true, 0, 0, 13),
        NodeFragment(true, 0, 0, 14),
        NodeFragment(true, 1, 0, 13),
        NodeFragment(true, 0, 0, 16),
        NodeFragment(true, 0, 0, 17),
        NodeFragment(true, 1, 0, 16),
        NodeFragment(true, 2, 0, 13),
        NodeFragment(true, 2, 0, 14),
        NodeFragment(true, 3, 0, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 1, 4, 13),
        NodeFragment(false, 1, 5, 12),
        NodeFragment(false, 1, 5, 13),
        NodeFragment(false, 1, 4, 10),
        NodeFragment(false, 1, 5, 9),
        NodeFragment(false, 1, 5, 10),
        NodeFragment(false, 1, 7, 13),
        NodeFragment(false, 1, 8, 12),
        NodeFragment(false, 1, 8, 13),
        NodeFragment(true, 1, 0, 4),
        NodeFragment(true, 1, 0, 5),
        NodeFragment(false, 1, 5, 22),
        NodeFragment(true, 1, 0, 7),
        NodeFragment(true, 1, 0, 8),
        NodeFragment(false, 1, 5, 19),
        NodeFragment(true, 3, 0, 4),
        NodeFragment(true, 3, 0, 5),
        NodeFragment(false, 1, 8, 22),
        NodeFragment(true, 1, 0, 13),
        NodeFragment(true, 1, 0, 14),
        NodeFragment(false, 1, 13, 13),
        NodeFragment(true, 1, 0, 16),
        NodeFragment(true, 1, 0, 17),
        NodeFragment(false, 1, 13, 10),
        NodeFragment(true, 3, 0, 13),
        NodeFragment(true, 3, 0, 14),
        NodeFragment(false, 1, 16, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 2, 3, 13),
        NodeFragment(false, 2, 3, 14),
        NodeFragment(false, 2, 4, 13),
        NodeFragment(false, 2, 6, 10),
        NodeFragment(false, 2, 6, 11),
        NodeFragment(false, 2, 7, 10),
        NodeFragment(false, 2, 6, 13),
        NodeFragment(false, 2, 6, 14),
        NodeFragment(false, 2, 7, 13),
        NodeFragment(false, 2, 3, 22),
        NodeFragment(true, 2, 0, 3),
        NodeFragment(true, 2, 0, 4),
        NodeFragment(false, 2, 6, 19),
        NodeFragment(true, 2, 0, 6),
        NodeFragment(true, 2, 0, 7),
        NodeFragment(false, 2, 6, 22),
        NodeFragment(false, 2, 6, 23),
        NodeFragment(false, 2, 7, 22),
        NodeFragment(false, 2, 12, 13),
        NodeFragment(true, 2, 0, 12),
        NodeFragment(true, 2, 0, 13),
        NodeFragment(false, 2, 14, 10),
        NodeFragment(true, 2, 0, 15),
        NodeFragment(true, 2, 0, 16),
        NodeFragment(false, 2, 14, 13),
        NodeFragment(false, 2, 14, 14),
        NodeFragment(false, 2, 15, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 2, 4, 13),
        NodeFragment(false, 2, 4, 12),
        NodeFragment(false, 2, 5, 13),
        NodeFragment(false, 2, 7, 10),
        NodeFragment(false, 2, 7, 9),
        NodeFragment(false, 2, 8, 10),
        NodeFragment(false, 2, 7, 13),
        NodeFragment(false, 2, 7, 12),
        NodeFragment(false, 2, 8, 13),
        NodeFragment(true, 2, 0, 4),
        NodeFragment(true, 2, 0, 5),
        NodeFragment(true, 3, 0, 4),
        NodeFragment(true, 2, 0, 7),
        NodeFragment(true, 2, 0, 8),
        NodeFragment(true, 3, 0, 7),
        NodeFragment(false, 2, 7, 22),
        NodeFragment(false, 2, 7, 21),
        NodeFragment(false, 2, 8, 22),
        NodeFragment(true, 2, 0, 13),
        NodeFragment(true, 2, 0, 14),
        NodeFragment(true, 3, 0, 13),
        NodeFragment(true, 2, 0, 16),
        NodeFragment(true, 2, 0, 17),
        NodeFragment(true, 3, 0, 16),
        NodeFragment(false, 2, 15, 13),
        NodeFragment(false, 2, 15, 12),
        NodeFragment(false, 2, 16, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 3, 4, 13),
        NodeFragment(false, 3, 5, 12),
        NodeFragment(false, 3, 5, 13),
        NodeFragment(false, 3, 7, 10),
        NodeFragment(false, 3, 8, 9),
        NodeFragment(false, 3, 8, 10),
        NodeFragment(false, 3, 7, 13),
        NodeFragment(false, 3, 8, 12),
        NodeFragment(false, 3, 8, 13),
        NodeFragment(true, 3, 0, 4),
        NodeFragment(true, 3, 0, 5),
        NodeFragment(false, 3, 5, 22),
        NodeFragment(true, 3, 0, 7),
        NodeFragment(true, 3, 0, 8),
        NodeFragment(false, 3, 8, 19),
        NodeFragment(false, 3, 7, 22),
        NodeFragment(false, 3, 8, 21),
        NodeFragment(false, 3, 8, 22),
        NodeFragment(true, 3, 0, 13),
        NodeFragment(true, 3, 0, 14),
        NodeFragment(false, 3, 13, 13),
        NodeFragment(true, 3, 0, 16),
        NodeFragment(true, 3, 0, 17),
        NodeFragment(false, 3, 16, 10),
        NodeFragment(false, 3, 15, 13),
        NodeFragment(false, 3, 16, 12),
        NodeFragment(false, 3, 16, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 0, 9, 13),
        NodeFragment(false, 0, 9, 14),
        NodeFragment(false, 0, 10, 13),
        NodeFragment(false, 0, 9, 16),
        NodeFragment(true, 0, 0, 9),
        NodeFragment(true, 0, 0, 10),
        NodeFragment(false, 0, 12, 13),
        NodeFragment(true, 0, 0, 12),
        NodeFragment(true, 0, 0, 13),
        NodeFragment(false, 0, 9, 4),
        NodeFragment(false, 0, 9, 5),
        NodeFragment(false, 0, 10, 4),
        NodeFragment(false, 0, 9, 7),
        NodeFragment(true, 0, 0, 18),
        NodeFragment(true, 0, 0, 19),
        NodeFragment(false, 0, 12, 4),
        NodeFragment(true, 0, 0, 21),
        NodeFragment(true, 0, 0, 22),
        NodeFragment(false, 0, 17, 13),
        NodeFragment(false, 0, 17, 14),
        NodeFragment(false, 0, 18, 13),
        NodeFragment(false, 0, 17, 16),
        NodeFragment(true, 4, 0, 9),
        NodeFragment(true, 4, 0, 10),
        NodeFragment(false, 0, 20, 13),
        NodeFragment(true, 4, 0, 12),
        NodeFragment(true, 4, 0, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 0, 10, 13),
        NodeFragment(false, 0, 10, 12),
        NodeFragment(false, 0, 11, 13),
        NodeFragment(true, 0, 0, 10),
        NodeFragment(true, 0, 0, 11),
        NodeFragment(true, 1, 0, 10),
        NodeFragment(true, 0, 0, 13),
        NodeFragment(true, 0, 0, 14),
        NodeFragment(true, 1, 0, 13),
        NodeFragment(false, 0, 10, 4),
        NodeFragment(false, 0, 10, 3),
        NodeFragment(false, 0, 11, 4),
        NodeFragment(true, 0, 0, 19),
        NodeFragment(true, 0, 0, 20),
        NodeFragment(true, 1, 0, 19),
        NodeFragment(true, 0, 0, 22),
        NodeFragment(true, 0, 0, 23),
        NodeFragment(true, 1, 0, 22),
        NodeFragment(false, 0, 18, 13),
        NodeFragment(false, 0, 18, 12),
        NodeFragment(false, 0, 19, 13),
        NodeFragment(true, 4, 0, 10),
        NodeFragment(true, 4, 0, 11),
        NodeFragment(true, 5, 0, 10),
        NodeFragment(true, 4, 0, 13),
        NodeFragment(true, 4, 0, 14),
        NodeFragment(true, 5, 0, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 1, 10, 13),
        NodeFragment(false, 1, 11, 12),
        NodeFragment(false, 1, 11, 13),
        NodeFragment(true, 1, 0, 10),
        NodeFragment(true, 1, 0, 11),
        NodeFragment(false, 1, 11, 16),
        NodeFragment(true, 1, 0, 13),
        NodeFragment(true, 1, 0, 14),
        NodeFragment(false, 1, 13, 13),
        NodeFragment(false, 1, 10, 4),
        NodeFragment(false, 1, 11, 3),
        NodeFragment(false, 1, 11, 4),
        NodeFragment(true, 1, 0, 19),
        NodeFragment(true, 1, 0, 20),
        NodeFragment(false, 1, 11, 7),
        NodeFragment(true, 1, 0, 22),
        NodeFragment(true, 1, 0, 23),
        NodeFragment(false, 1, 13, 4),
        NodeFragment(false, 1, 18, 13),
        NodeFragment(false, 1, 19, 12),
        NodeFragment(false, 1, 19, 13),
        NodeFragment(true, 5, 0, 10),
        NodeFragment(true, 5, 0, 11),
        NodeFragment(false, 1, 19, 16),
        NodeFragment(true, 5, 0, 13),
        NodeFragment(true, 5, 0, 14),
        NodeFragment(false, 1, 22, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 0, 12, 13),
        NodeFragment(true, 0, 0, 12),
        NodeFragment(true, 0, 0, 13),
        NodeFragment(false, 0, 12, 10),
        NodeFragment(true, 0, 0, 15),
        NodeFragment(true, 0, 0, 16),
        NodeFragment(false, 0, 14, 13),
        NodeFragment(true, 2, 0, 12),
        NodeFragment(true, 2, 0, 13),
        NodeFragment(false, 0, 12, 4),
        NodeFragment(true, 0, 0, 21),
        NodeFragment(true, 0, 0, 22),
        NodeFragment(false, 0, 12, 1),
        NodeFragment(true, 0, 0, 24),
        NodeFragment(true, 0, 0, 25),
        NodeFragment(false, 0, 14, 4),
        NodeFragment(true, 2, 0, 21),
        NodeFragment(true, 2, 0, 22),
        NodeFragment(false, 0, 20, 13),
        NodeFragment(true, 4, 0, 12),
        NodeFragment(true, 4, 0, 13),
        NodeFragment(false, 0, 20, 10),
        NodeFragment(true, 4, 0, 15),
        NodeFragment(true, 4, 0, 16),
        NodeFragment(false, 0, 23, 13),
        NodeFragment(true, 6, 0, 12),
        NodeFragment(true, 6, 0, 13)
    ),
    NodeFragment[27](
        NodeFragment(true, 0, 0, 13),
        NodeFragment(true, 0, 0, 14),
        NodeFragment(true, 1, 0, 13),
        NodeFragment(true, 0, 0, 16),
        NodeFragment(true, 0, 0, 17),
        NodeFragment(true, 1, 0, 16),
        NodeFragment(true, 2, 0, 13),
        NodeFragment(true, 2, 0, 14),
        NodeFragment(true, 3, 0, 13),
        NodeFragment(true, 0, 0, 22),
        NodeFragment(true, 0, 0, 23),
        NodeFragment(true, 1, 0, 22),
        NodeFragment(true, 0, 0, 25),
        NodeFragment(true, 0, 0, 26),
        NodeFragment(true, 1, 0, 25),
        NodeFragment(true, 2, 0, 22),
        NodeFragment(true, 2, 0, 23),
        NodeFragment(true, 3, 0, 22),
        NodeFragment(true, 4, 0, 13),
        NodeFragment(true, 4, 0, 14),
        NodeFragment(true, 5, 0, 13),
        NodeFragment(true, 4, 0, 16),
        NodeFragment(true, 4, 0, 17),
        NodeFragment(true, 5, 0, 16),
        NodeFragment(true, 6, 0, 13),
        NodeFragment(true, 6, 0, 14),
        NodeFragment(true, 7, 0, 13)
    ),
    NodeFragment[27](
        NodeFragment(true, 1, 0, 13),
        NodeFragment(true, 1, 0, 14),
        NodeFragment(false, 1, 13, 13),
        NodeFragment(true, 1, 0, 16),
        NodeFragment(true, 1, 0, 17),
        NodeFragment(false, 1, 13, 10),
        NodeFragment(true, 3, 0, 13),
        NodeFragment(true, 3, 0, 14),
        NodeFragment(false, 1, 16, 13),
        NodeFragment(true, 1, 0, 22),
        NodeFragment(true, 1, 0, 23),
        NodeFragment(false, 1, 13, 4),
        NodeFragment(true, 1, 0, 25),
        NodeFragment(true, 1, 0, 26),
        NodeFragment(false, 1, 13, 1),
        NodeFragment(true, 3, 0, 22),
        NodeFragment(true, 3, 0, 23),
        NodeFragment(false, 1, 16, 4),
        NodeFragment(true, 5, 0, 13),
        NodeFragment(true, 5, 0, 14),
        NodeFragment(false, 1, 22, 13),
        NodeFragment(true, 5, 0, 16),
        NodeFragment(true, 5, 0, 17),
        NodeFragment(false, 1, 22, 10),
        NodeFragment(true, 7, 0, 13),
        NodeFragment(true, 7, 0, 14),
        NodeFragment(false, 1, 25, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 2, 12, 13),
        NodeFragment(true, 2, 0, 12),
        NodeFragment(true, 2, 0, 13),
        NodeFragment(false, 2, 14, 10),
        NodeFragment(true, 2, 0, 15),
        NodeFragment(true, 2, 0, 16),
        NodeFragment(false, 2, 14, 13),
        NodeFragment(false, 2, 14, 14),
        NodeFragment(false, 2, 15, 13),
        NodeFragment(false, 2, 12, 4),
        NodeFragment(true, 2, 0, 21),
        NodeFragment(true, 2, 0, 22),
        NodeFragment(false, 2, 14, 1),
        NodeFragment(true, 2, 0, 24),
        NodeFragment(true, 2, 0, 25),
        NodeFragment(false, 2, 14, 4),
        NodeFragment(false, 2, 14, 5),
        NodeFragment(false, 2, 15, 4),
        NodeFragment(false, 2, 20, 13),
        NodeFragment(true, 6, 0, 12),
        NodeFragment(true, 6, 0, 13),
        NodeFragment(false, 2, 23, 10),
        NodeFragment(true, 6, 0, 15),
        NodeFragment(true, 6, 0, 16),
        NodeFragment(false, 2, 23, 13),
        NodeFragment(false, 2, 23, 14),
        NodeFragment(false, 2, 24, 13)
    ),
    NodeFragment[27](
        NodeFragment(true, 2, 0, 13),
        NodeFragment(true, 2, 0, 14),
        NodeFragment(true, 3, 0, 13),
        NodeFragment(true, 2, 0, 16),
        NodeFragment(true, 2, 0, 17),
        NodeFragment(true, 3, 0, 16),
        NodeFragment(false, 2, 15, 13),
        NodeFragment(false, 2, 15, 12),
        NodeFragment(false, 2, 16, 13),
        NodeFragment(true, 2, 0, 22),
        NodeFragment(true, 2, 0, 23),
        NodeFragment(true, 3, 0, 22),
        NodeFragment(true, 2, 0, 25),
        NodeFragment(true, 2, 0, 26),
        NodeFragment(true, 3, 0, 25),
        NodeFragment(false, 2, 15, 4),
        NodeFragment(false, 2, 15, 3),
        NodeFragment(false, 2, 16, 4),
        NodeFragment(true, 6, 0, 13),
        NodeFragment(true, 6, 0, 14),
        NodeFragment(true, 7, 0, 13),
        NodeFragment(true, 6, 0, 16),
        NodeFragment(true, 6, 0, 17),
        NodeFragment(true, 7, 0, 16),
        NodeFragment(false, 2, 24, 13),
        NodeFragment(false, 2, 24, 12),
        NodeFragment(false, 2, 25, 13)
    ),
    NodeFragment[27](
        NodeFragment(true, 3, 0, 13),
        NodeFragment(true, 3, 0, 14),
        NodeFragment(false, 3, 13, 13),
        NodeFragment(true, 3, 0, 16),
        NodeFragment(true, 3, 0, 17),
        NodeFragment(false, 3, 16, 10),
        NodeFragment(false, 3, 15, 13),
        NodeFragment(false, 3, 16, 12),
        NodeFragment(false, 3, 16, 13),
        NodeFragment(true, 3, 0, 22),
        NodeFragment(true, 3, 0, 23),
        NodeFragment(false, 3, 13, 4),
        NodeFragment(true, 3, 0, 25),
        NodeFragment(true, 3, 0, 26),
        NodeFragment(false, 3, 16, 1),
        NodeFragment(false, 3, 15, 4),
        NodeFragment(false, 3, 16, 3),
        NodeFragment(false, 3, 16, 4),
        NodeFragment(true, 7, 0, 13),
        NodeFragment(true, 7, 0, 14),
        NodeFragment(false, 3, 22, 13),
        NodeFragment(true, 7, 0, 16),
        NodeFragment(true, 7, 0, 17),
        NodeFragment(false, 3, 25, 10),
        NodeFragment(false, 3, 24, 13),
        NodeFragment(false, 3, 25, 12),
        NodeFragment(false, 3, 25, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 4, 9, 13),
        NodeFragment(false, 4, 9, 14),
        NodeFragment(false, 4, 10, 13),
        NodeFragment(false, 4, 9, 16),
        NodeFragment(true, 4, 0, 9),
        NodeFragment(true, 4, 0, 10),
        NodeFragment(false, 4, 12, 13),
        NodeFragment(true, 4, 0, 12),
        NodeFragment(true, 4, 0, 13),
        NodeFragment(false, 4, 17, 4),
        NodeFragment(false, 4, 17, 5),
        NodeFragment(false, 4, 18, 4),
        NodeFragment(false, 4, 17, 7),
        NodeFragment(true, 4, 0, 18),
        NodeFragment(true, 4, 0, 19),
        NodeFragment(false, 4, 20, 4),
        NodeFragment(true, 4, 0, 21),
        NodeFragment(true, 4, 0, 22),
        NodeFragment(false, 4, 17, 13),
        NodeFragment(false, 4, 17, 14),
        NodeFragment(false, 4, 18, 13),
        NodeFragment(false, 4, 17, 16),
        NodeFragment(false, 4, 17, 17),
        NodeFragment(false, 4, 18, 16),
        NodeFragment(false, 4, 20, 13),
        NodeFragment(false, 4, 20, 14),
        NodeFragment(false, 4, 21, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 4, 10, 13),
        NodeFragment(false, 4, 10, 12),
        NodeFragment(false, 4, 11, 13),
        NodeFragment(true, 4, 0, 10),
        NodeFragment(true, 4, 0, 11),
        NodeFragment(true, 5, 0, 10),
        NodeFragment(true, 4, 0, 13),
        NodeFragment(true, 4, 0, 14),
        NodeFragment(true, 5, 0, 13),
        NodeFragment(false, 4, 18, 4),
        NodeFragment(false, 4, 18, 3),
        NodeFragment(false, 4, 19, 4),
        NodeFragment(true, 4, 0, 19),
        NodeFragment(true, 4, 0, 20),
        NodeFragment(true, 5, 0, 19),
        NodeFragment(true, 4, 0, 22),
        NodeFragment(true, 4, 0, 23),
        NodeFragment(true, 5, 0, 22),
        NodeFragment(false, 4, 18, 13),
        NodeFragment(false, 4, 18, 12),
        NodeFragment(false, 4, 19, 13),
        NodeFragment(false, 4, 18, 16),
        NodeFragment(false, 4, 18, 15),
        NodeFragment(false, 4, 19, 16),
        NodeFragment(false, 4, 21, 13),
        NodeFragment(false, 4, 21, 12),
        NodeFragment(false, 4, 22, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 5, 10, 13),
        NodeFragment(false, 5, 11, 12),
        NodeFragment(false, 5, 11, 13),
        NodeFragment(true, 5, 0, 10),
        NodeFragment(true, 5, 0, 11),
        NodeFragment(false, 5, 11, 16),
        NodeFragment(true, 5, 0, 13),
        NodeFragment(true, 5, 0, 14),
        NodeFragment(false, 5, 13, 13),
        NodeFragment(false, 5, 18, 4),
        NodeFragment(false, 5, 19, 3),
        NodeFragment(false, 5, 19, 4),
        NodeFragment(true, 5, 0, 19),
        NodeFragment(true, 5, 0, 20),
        NodeFragment(false, 5, 19, 7),
        NodeFragment(true, 5, 0, 22),
        NodeFragment(true, 5, 0, 23),
        NodeFragment(false, 5, 22, 4),
        NodeFragment(false, 5, 18, 13),
        NodeFragment(false, 5, 19, 12),
        NodeFragment(false, 5, 19, 13),
        NodeFragment(false, 5, 18, 16),
        NodeFragment(false, 5, 19, 15),
        NodeFragment(false, 5, 19, 16),
        NodeFragment(false, 5, 21, 13),
        NodeFragment(false, 5, 22, 12),
        NodeFragment(false, 5, 22, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 4, 12, 13),
        NodeFragment(true, 4, 0, 12),
        NodeFragment(true, 4, 0, 13),
        NodeFragment(false, 4, 12, 10),
        NodeFragment(true, 4, 0, 15),
        NodeFragment(true, 4, 0, 16),
        NodeFragment(false, 4, 14, 13),
        NodeFragment(true, 6, 0, 12),
        NodeFragment(true, 6, 0, 13),
        NodeFragment(false, 4, 20, 4),
        NodeFragment(true, 4, 0, 21),
        NodeFragment(true, 4, 0, 22),
        NodeFragment(false, 4, 20, 1),
        NodeFragment(true, 4, 0, 24),
        NodeFragment(true, 4, 0, 25),
        NodeFragment(false, 4, 23, 4),
        NodeFragment(true, 6, 0, 21),
        NodeFragment(true, 6, 0, 22),
        NodeFragment(false, 4, 20, 13),
        NodeFragment(false, 4, 20, 14),
        NodeFragment(false, 4, 21, 13),
        NodeFragment(false, 4, 20, 10),
        NodeFragment(false, 4, 20, 11),
        NodeFragment(false, 4, 21, 10),
        NodeFragment(false, 4, 23, 13),
        NodeFragment(false, 4, 23, 14),
        NodeFragment(false, 4, 24, 13)
    ),
    NodeFragment[27](
        NodeFragment(true, 4, 0, 13),
        NodeFragment(true, 4, 0, 14),
        NodeFragment(true, 5, 0, 13),
        NodeFragment(true, 4, 0, 16),
        NodeFragment(true, 4, 0, 17),
        NodeFragment(true, 5, 0, 16),
        NodeFragment(true, 6, 0, 13),
        NodeFragment(true, 6, 0, 14),
        NodeFragment(true, 7, 0, 13),
        NodeFragment(true, 4, 0, 22),
        NodeFragment(true, 4, 0, 23),
        NodeFragment(true, 5, 0, 22),
        NodeFragment(true, 4, 0, 25),
        NodeFragment(true, 4, 0, 26),
        NodeFragment(true, 5, 0, 25),
        NodeFragment(true, 6, 0, 22),
        NodeFragment(true, 6, 0, 23),
        NodeFragment(true, 7, 0, 22),
        NodeFragment(false, 4, 21, 13),
        NodeFragment(false, 4, 21, 12),
        NodeFragment(false, 4, 22, 13),
        NodeFragment(false, 4, 21, 10),
        NodeFragment(false, 4, 21, 9),
        NodeFragment(false, 4, 22, 10),
        NodeFragment(false, 4, 24, 13),
        NodeFragment(false, 4, 24, 12),
        NodeFragment(false, 4, 25, 13)
    ),
    NodeFragment[27](
        NodeFragment(true, 5, 0, 13),
        NodeFragment(true, 5, 0, 14),
        NodeFragment(false, 5, 13, 13),
        NodeFragment(true, 5, 0, 16),
        NodeFragment(true, 5, 0, 17),
        NodeFragment(false, 5, 13, 10),
        NodeFragment(true, 7, 0, 13),
        NodeFragment(true, 7, 0, 14),
        NodeFragment(false, 5, 16, 13),
        NodeFragment(true, 5, 0, 22),
        NodeFragment(true, 5, 0, 23),
        NodeFragment(false, 5, 22, 4),
        NodeFragment(true, 5, 0, 25),
        NodeFragment(true, 5, 0, 26),
        NodeFragment(false, 5, 22, 1),
        NodeFragment(true, 7, 0, 22),
        NodeFragment(true, 7, 0, 23),
        NodeFragment(false, 5, 25, 4),
        NodeFragment(false, 5, 21, 13),
        NodeFragment(false, 5, 22, 12),
        NodeFragment(false, 5, 22, 13),
        NodeFragment(false, 5, 21, 10),
        NodeFragment(false, 5, 22, 9),
        NodeFragment(false, 5, 22, 10),
        NodeFragment(false, 5, 24, 13),
        NodeFragment(false, 5, 25, 12),
        NodeFragment(false, 5, 25, 13)
    ),
    NodeFragment[27](
        NodeFragment(false, 6, 12, 13),
        NodeFragment(true, 6, 0, 12),
        NodeFragment(true, 6, 0, 13),
        NodeFragment(false, 6, 14, 10),
        NodeFragment(true, 6, 0, 15),
        NodeFragment(true, 6, 0, 16),
        NodeFragment(false, 6, 14, 13),
        NodeFragment(false, 6, 14, 14),
        NodeFragment(false, 6, 15, 13),
        NodeFragment(false, 6, 20, 4),
        NodeFragment(true, 6, 0, 21),
        NodeFragment(true, 6, 0, 22),
        NodeFragment(false, 6, 23, 1),
        NodeFragment(true, 6, 0, 24),
        NodeFragment(true, 6, 0, 25),
        NodeFragment(false, 6, 23, 4),
        NodeFragment(false, 6, 23, 5),
        NodeFragment(false, 6, 24, 4),
        NodeFragment(false, 6, 20, 13),
        NodeFragment(false, 6, 20, 14),
        NodeFragment(false, 6, 21, 13),
        NodeFragment(false, 6, 23, 10),
        NodeFragment(false, 6, 23, 11),
        NodeFragment(false, 6, 24, 10),
        NodeFragment(false, 6, 23, 13),
        NodeFragment(false, 6, 23, 14),
        NodeFragment(false, 6, 24, 13)
    ),
    NodeFragment[27](
        NodeFragment(true, 6, 0, 13),
        NodeFragment(true, 6, 0, 14),
        NodeFragment(true, 7, 0, 13),
        NodeFragment(true, 6, 0, 16),
        NodeFragment(true, 6, 0, 17),
        NodeFragment(true, 7, 0, 16),
        NodeFragment(false, 6, 15, 13),
        NodeFragment(false, 6, 15, 12),
        NodeFragment(false, 6, 16, 13),
        NodeFragment(true, 6, 0, 22),
        NodeFragment(true, 6, 0, 23),
        NodeFragment(true, 7, 0, 22),
        NodeFragment(true, 6, 0, 25),
        NodeFragment(true, 6, 0, 26),
        NodeFragment(true, 7, 0, 25),
        NodeFragment(false, 6, 24, 4),
        NodeFragment(false, 6, 24, 3),
        NodeFragment(false, 6, 25, 4),
        NodeFragment(false, 6, 21, 13),
        NodeFragment(false, 6, 21, 12),
        NodeFragment(false, 6, 22, 13),
        NodeFragment(false, 6, 24, 10),
        NodeFragment(false, 6, 24, 9),
        NodeFragment(false, 6, 25, 10),
        NodeFragment(false, 6, 24, 13),
        NodeFragment(false, 6, 24, 12),
        NodeFragment(false, 6, 25, 13)
    ),
    NodeFragment[27](
        NodeFragment(true, 7, 0, 13),
        NodeFragment(true, 7, 0, 14),
        NodeFragment(false, 7, 13, 13),
        NodeFragment(true, 7, 0, 16),
        NodeFragment(true, 7, 0, 17),
        NodeFragment(false, 7, 16, 10),
        NodeFragment(false, 7, 15, 13),
        NodeFragment(false, 7, 16, 12),
        NodeFragment(false, 7, 16, 13),
        NodeFragment(true, 7, 0, 22),
        NodeFragment(true, 7, 0, 23),
        NodeFragment(false, 7, 22, 4),
        NodeFragment(true, 7, 0, 25),
        NodeFragment(true, 7, 0, 26),
        NodeFragment(false, 7, 25, 1),
        NodeFragment(false, 7, 24, 4),
        NodeFragment(false, 7, 25, 3),
        NodeFragment(false, 7, 25, 4),
        NodeFragment(false, 7, 21, 13),
        NodeFragment(false, 7, 22, 12),
        NodeFragment(false, 7, 22, 13),
        NodeFragment(false, 7, 24, 10),
        NodeFragment(false, 7, 25, 9),
        NodeFragment(false, 7, 25, 10),
        NodeFragment(false, 7, 24, 13),
        NodeFragment(false, 7, 25, 12),
        NodeFragment(false, 7, 25, 13)
    )
);

const uint TOUCHED = uint(0) - uint(1);

struct NodeTile {
    uint parent;
    uint children[8];
    uint neighbours[26];
    uint brick;
};

float node_size(float length, uint level) {
    return length / (1 << level);
}

vec3 node_extent(float length, uint level) {
    return vec3(node_size(length, level));
}

ivec3 index_3d(uint resolution, uint index) {
    uint z = index % resolution;
    uint y = (index / resolution) % resolution;
    uint x = index / (resolution * resolution);
    return ivec3(x, y, z);
}

ivec3 node_offset_coefficient(uint resolution, uint node) {
    return index_3d(resolution, node);
}

ivec3 child_offset_coefficent(uint child) {
    return index_3d(2, child);
}

ivec3 brick_offset_coefficient(uint resolution, uint brick) {
    return 3 * index_3d(resolution / 3, brick);
}

bool intersect_node(vec3 min_extent, vec3 max_extent, vec3 p) {
    return min_extent.x <= p.x && p.x <= max_extent.x
        && min_extent.y <= p.y && p.y <= max_extent.y
        && min_extent.z <= p.z && p.z <= max_extent.z;
}

NodeTile get_node(uint node_index);
uint get_child(uint node_index, uint child_index);

struct IntersectionResult {
    bool intersected;
    uint node;
    uint child;
    vec3 min;
    vec3 max;
};

IntersectionResult intersect_octree(float length, uint height, vec3 p) {
    uint node = 0;
    vec3 offset = vec3(-length / 2);

    for (uint level = 0; level <= height; level++) {
        vec3 child_extent = node_extent(length, level);
        uint max_children = level == 0 ? 1 : 8;

        for (uint child = 0; child < max_children; child++) {
            vec3 min_extent = offset + child_offset_coefficent(child) * child_extent;
            vec3 max_extent = min_extent + child_extent;

            if (intersect_node(min_extent, max_extent, p)) {
                if (level == height) {
                    return IntersectionResult(true, node, child, min_extent, max_extent);
                }

                node = get_child(node, child);
                offset = min_extent;
                break;
            }
        }

        if (node == 0) {
            return IntersectionResult(false, 0, 0, vec3(0), vec3(0));
        }
    }

    return IntersectionResult(false, 0, 0, vec3(0), vec3(0));
}