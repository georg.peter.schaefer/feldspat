#version 450

#include <octree.glsl>

layout(binding = 0, std430) readonly buffer NodePool {
    NodeTile pool[];
} node_pool;
layout(binding = 1, r32f) volatile coherent uniform image3D base_color_red_image;
layout(binding = 2, r32f) volatile coherent uniform image3D base_color_green_image;
layout(binding = 3, r32f) volatile coherent uniform image3D base_color_blue_image;
layout(binding = 4, r32f) volatile coherent uniform image3D fragment_count_image;

void main() {
    uint resolution = imageSize(base_color_red_image).x;
    ivec3 brick_offset = brick_offset_coefficient(resolution, node_pool.pool[gl_VertexIndex].brick);
    for (uint x = 0; x < 3; x++) {
        for (uint y = 0; y < 3; y++) {
            for (uint z = 0; z < 3; z++) {
                ivec3 voxel_offset = ivec3(x, y, z);
                ivec3 coords = brick_offset + voxel_offset;
                float fragment_count = imageLoad(fragment_count_image, coords).r;

                if (fragment_count >= 0.0) {
                    float base_color_red = imageLoad(base_color_red_image, coords).r;
                    imageStore(base_color_red_image, coords, vec4(base_color_red / fragment_count));

                    float base_color_green = imageLoad(base_color_green_image, coords).r;
                    imageStore(base_color_green_image, coords, vec4(base_color_green / fragment_count));

                    float base_color_blue = imageLoad(base_color_blue_image, coords).r;
                    imageStore(base_color_blue_image, coords, vec4(base_color_blue / fragment_count));
                }
            }
        }
    }
    gl_PointSize = 1.0;
}