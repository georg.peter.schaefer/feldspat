//! Octree debug pass.

use std::sync::Arc;

use ash::vk;

use crate::ecs::{Entities, Transform};
use crate::engine::Config;
use crate::graphics::buffer::ImmutableBuffer;
use crate::graphics::command_buffer::CommandBuffer;
use crate::graphics::image::{Image, ImageView, ImmutableImage};
use crate::graphics::pipeline::{DescriptorSet, DescriptorSetLayout};
use crate::graphics::{
    Device, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, SubpassDescription, Swapchain,
};
use crate::math::Vec3;
use crate::rendering::camera::Camera;
use crate::rendering::gi::GiData;
use crate::rendering::{PipelineCreationError, PipelineExecutionError, Volume};

/// Octree debug pass.
pub struct OctreeDebugPass {
    vertex_buffer: Arc<ImmutableBuffer<[Vertex]>>,
    gi_data: Arc<GiData>,
    descriptor_set: Arc<DescriptorSet>,
    pipeline: Arc<GraphicsPipeline>,
    _descriptor_set_layout: Arc<DescriptorSetLayout>,
    framebuffer: Arc<Framebuffer>,
    final_attachment: Arc<ImageView<Arc<ImmutableImage>>>,
    device: Arc<Device>,
    config: Arc<Config>,
}

impl OctreeDebugPass {
    /// Creates a new octree debug.
    pub fn new(
        config: Arc<Config>,
        device: Arc<Device>,
        swapchain: &Swapchain,
        gi_data: Arc<GiData>,
    ) -> Result<Self, PipelineCreationError> {
        let vertex_shader_module = vertex_shader::Shader::load(&device)?;
        let fragment_shader_module = fragment_shader::Shader::load(&device)?;
        let render_pass = Self::create_render_pass(device.clone())?;
        let final_attachment = ImageView::from_image(ImmutableImage::uninitialized(
            device.clone(),
            vk::Format::B8G8R8A8_SRGB,
            swapchain.images()[0].extent(),
            1,
            vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::SAMPLED,
        )?)?;
        let framebuffer =
            Self::create_framebuffer(&device, swapchain, &render_pass, final_attachment.clone())?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(device.clone())?;
        let pipeline = Self::create_pipeline(
            device.clone(),
            swapchain,
            render_pass.clone(),
            vertex_shader_module.clone(),
            fragment_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let descriptor_set =
            Self::create_descriptor_set(device.clone(), descriptor_set_layout.clone(), &gi_data)?;
        let vertex_buffer = Self::create_vertex_buffer(&config, device.clone())?;

        Ok(Self {
            vertex_buffer,
            gi_data,
            descriptor_set,
            pipeline,
            _descriptor_set_layout: descriptor_set_layout,
            framebuffer,
            final_attachment,
            device,
            config,
        })
    }

    fn create_render_pass(device: Arc<Device>) -> Result<Arc<RenderPass>, PipelineCreationError> {
        Ok(RenderPass::new(
            device.clone(),
            vec![
                vk::AttachmentDescription::builder()
                    .format(vk::Format::B8G8R8A8_SRGB)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::STORE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                    .build(),
                vk::AttachmentDescription::builder()
                    .format(vk::Format::D32_SFLOAT)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                    .build(),
            ],
            vec![SubpassDescription {
                color_attachments: vec![vk::AttachmentReference::builder()
                    .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .attachment(0)
                    .build()],
                depth_attachment: Some(
                    vk::AttachmentReference::builder()
                        .layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                        .attachment(1)
                        .build(),
                ),
                input_attachments: vec![],
            }],
            vec![],
        )?)
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, PipelineCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(3)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_framebuffer(
        device: &Arc<Device>,
        swapchain: &Swapchain,
        render_pass: &Arc<RenderPass>,
        final_attachment: Arc<ImageView<Arc<ImmutableImage>>>,
    ) -> Result<Arc<Framebuffer>, PipelineCreationError> {
        let extent = swapchain.images()[0].extent();
        Ok(Framebuffer::builder()
            .add(final_attachment)
            .add(ImageView::from_image(ImmutableImage::uninitialized(
                device.clone(),
                vk::Format::D32_SFLOAT,
                extent,
                1,
                vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
                    | vk::ImageUsageFlags::INPUT_ATTACHMENT,
            )?)?)
            .build(render_pass.clone())?)
    }

    fn create_pipeline(
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        fragment_shader: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, PipelineCreationError> {
        let extent = swapchain.extent();
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader)
            .fragment_shader(fragment_shader)
            .vertex_input::<Vertex>()
            .triangle_list()
            .cull_mode_back()
            .front_face_counter_clockwise()
            .depth_test_enabled()
            .viewports(vec![vk::Viewport::builder()
                .width(extent.width as _)
                .height(extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .render_pass(render_pass.clone(), 0)
            .push_constants::<shader::Constants>(vk::ShaderStageFlags::VERTEX)
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    fn create_descriptor_set(
        device: Arc<Device>,
        layout: Arc<DescriptorSetLayout>,
        gi_data: &GiData,
    ) -> Result<Arc<DescriptorSet>, PipelineCreationError> {
        Ok(DescriptorSet::builder()
            .add_buffer(
                0,
                gi_data.octree.node_pool.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_sampled_image(
                1,
                gi_data.octree.brick_pool.base_color.components[0].clone(),
                gi_data.octree.brick_pool.sampler.clone(),
            )
            .add_sampled_image(
                2,
                gi_data.octree.brick_pool.base_color.components[1].clone(),
                gi_data.octree.brick_pool.sampler.clone(),
            )
            .add_sampled_image(
                3,
                gi_data.octree.brick_pool.base_color.components[2].clone(),
                gi_data.octree.brick_pool.sampler.clone(),
            )
            .build(device.clone(), layout)?)
    }

    fn create_vertex_buffer(
        config: &Config,
        device: Arc<Device>,
    ) -> Result<Arc<ImmutableBuffer<[Vertex]>>, PipelineCreationError> {
        let resolution = config.graphics.global_illumination.resolution as usize;
        let node_size = config.graphics.global_illumination.size / resolution as f32;
        let vertices = Self::create_cube(Vec3::zero(), node_size);

        Ok(ImmutableBuffer::from_iter(
            device,
            vk::BufferUsageFlags::VERTEX_BUFFER,
            vertices,
        )?)
    }

    fn create_cube(p: Vec3, step_size: f32) -> Vec<Vertex> {
        vec![
            Self::create_triangle(
                p + Vec3::new(0.0, 0.0, step_size),
                p + Vec3::new(step_size, step_size, step_size),
                p + Vec3::new(0.0, step_size, step_size),
            ),
            Self::create_triangle(
                p + Vec3::new(0.0, 0.0, step_size),
                p + Vec3::new(step_size, 0.0, step_size),
                p + Vec3::new(step_size, step_size, step_size),
            ),
            Self::create_triangle(
                p + Vec3::new(0.0, 0.0, 0.0),
                p + Vec3::new(0.0, step_size, 0.0),
                p + Vec3::new(step_size, step_size, 0.0),
            ),
            Self::create_triangle(
                p + Vec3::new(0.0, 0.0, 0.0),
                p + Vec3::new(step_size, step_size, 0.0),
                p + Vec3::new(step_size, 0.0, 0.0),
            ),
            Self::create_triangle(
                p + Vec3::new(0.0, 0.0, 0.0),
                p + Vec3::new(step_size, 0.0, 0.0),
                p + Vec3::new(0.0, 0.0, step_size),
            ),
            Self::create_triangle(
                p + Vec3::new(step_size, 0.0, 0.0),
                p + Vec3::new(step_size, 0.0, step_size),
                p + Vec3::new(0.0, 0.0, step_size),
            ),
            Self::create_triangle(
                p + Vec3::new(0.0, step_size, 0.0),
                p + Vec3::new(0.0, step_size, step_size),
                p + Vec3::new(step_size, step_size, 0.0),
            ),
            Self::create_triangle(
                p + Vec3::new(step_size, step_size, 0.0),
                p + Vec3::new(0.0, step_size, step_size),
                p + Vec3::new(step_size, step_size, step_size),
            ),
            Self::create_triangle(
                p + Vec3::new(0.0, 0.0, 0.0),
                p + Vec3::new(0.0, step_size, step_size),
                p + Vec3::new(0.0, step_size, 0.0),
            ),
            Self::create_triangle(
                p + Vec3::new(0.0, 0.0, 0.0),
                p + Vec3::new(0.0, 0.0, step_size),
                p + Vec3::new(0.0, step_size, step_size),
            ),
            Self::create_triangle(
                p + Vec3::new(step_size, 0.0, 0.0),
                p + Vec3::new(step_size, step_size, 0.0),
                p + Vec3::new(step_size, step_size, step_size),
            ),
            Self::create_triangle(
                p + Vec3::new(step_size, 0.0, 0.0),
                p + Vec3::new(step_size, step_size, step_size),
                p + Vec3::new(step_size, 0.0, step_size),
            ),
        ]
        .into_iter()
        .flatten()
        .collect()
    }

    fn create_triangle(p0: Vec3, p1: Vec3, p2: Vec3) -> Vec<Vertex> {
        vec![
            Self::create_point(p0),
            Self::create_point(p1),
            Self::create_point(p2),
        ]
    }

    fn create_point(position: Vec3) -> Vertex {
        Vertex { position }
    }

    /// Records the pass into a primary command buffer.
    pub fn execute(
        &self,
        entities: &Entities,
    ) -> Result<Arc<CommandBuffer>, PipelineExecutionError> {
        let mut builder = CommandBuffer::primary(
            self.device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            self.device.graphics_queue().family(),
        )?;

        builder
            .pipeline_barrier(
                vk::PipelineStageFlags::VERTEX_SHADER,
                vk::PipelineStageFlags::VERTEX_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![],
            )
            .begin_render_pass(
                self.framebuffer.clone(),
                vk::SubpassContents::INLINE,
                vec![
                    vk::ClearValue {
                        color: vk::ClearColorValue {
                            uint32: [0, 0, 0, 1],
                        },
                    },
                    vk::ClearValue {
                        depth_stencil: vk::ClearDepthStencilValue {
                            depth: 1.0,
                            stencil: 0,
                        },
                    },
                ],
            );

        let camera = entities.components::<(Transform, Camera)>();
        if let Some((_, camera_transform, camera)) = camera.first() {
            let extent = self.framebuffer.extent();
            let aspect_ration = extent.width as f32 / extent.height as f32;
            let projection = camera.projection(aspect_ration);
            let view = camera_transform.model().inverse();
            let eye = camera_transform.position();
            let scene = shader::Constants {
                view,
                projection,
                eye,
                resolution: self.config.graphics.global_illumination.resolution as _,
                size: self.config.graphics.global_illumination.size,
                max_height: self.gi_data.octree.max_height(),
            };

            builder
                .bind_pipeline(self.pipeline.clone())
                .push_constants(self.pipeline.layout(), vk::ShaderStageFlags::VERTEX, scene)
                .bind_vertex_buffer(self.vertex_buffer.clone())
                .bind_descriptor_set(self.pipeline.layout(), self.descriptor_set.clone())
                .draw(
                    36 as _,
                    (self.config.graphics.global_illumination.resolution
                        * self.config.graphics.global_illumination.resolution
                        * self.config.graphics.global_illumination.resolution)
                        as _,
                    0,
                    0,
                );
        }

        builder.end_render_pass();

        Ok(builder.build()?)
    }

    /// Returns the final attachment.
    pub fn final_attachment(&self) -> &Arc<ImageView<Arc<ImmutableImage>>> {
        &self.final_attachment
    }
}

mod vertex_shader {
    feldspat_shaders::shader! {
        ty: "vertex",
        path: "src/rendering/gi/octree_debug.vert",
        include_dirs: ["src/rendering/gi"]
    }
}

mod fragment_shader {
    feldspat_shaders::shader! {
        ty: "fragment",
        path: "src/rendering/gi/octree_debug.frag"
    }
}

mod shader {
    use crate::math::{Mat4, Vec3};

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct Constants {
        pub view: Mat4,
        pub projection: Mat4,
        pub eye: Vec3,
        pub resolution: u32,
        pub size: f32,
        pub max_height: u32,
    }
}

/// A voxel vertex.
#[derive(Copy, Clone, Default)]
pub struct Vertex {
    /// The position.
    pub position: Vec3,
}

crate::vertex!(Vertex, position);
