#version 450

struct Material {
    vec4 base_color;
    float metallic;
    float roughness;
};

struct Model {
    mat4 transform;
    Material material;
};

struct Scene {
    float size;
};

layout(push_constant) uniform Constants {
    Model model;
    Scene scene;
} constants;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 tangent;
layout(location = 3) in vec2 texcoord;

layout(location = 0) out vec3 geom_position;
layout(location = 1) out vec3 geom_normal;
layout(location = 2) out vec4 geom_tangent;
layout(location = 3) out vec2 geom_texcoord;

void main() {
    geom_position = vec3(constants.model.transform * vec4(position, 1.0));
    geom_normal = mat3(constants.model.transform) * normal;
    geom_tangent = vec4(mat3(constants.model.transform) * tangent.xyz, tangent.w);
    geom_texcoord = texcoord;
}
