#version 450

const uint SUBDIVISION = 0;
const uint ALLOCATION = 1;
const uint INITIALIZATION = 2;
const uint NEIGHBOUR = 3;
const uint ACCUMULATION = 4;
const uint NOMRALIZATION = 5;
const uint MIP_MAPPING = 6;

layout(constant_id = 0) const uint pass = 0;

layout(push_constant) uniform Constants {
    uint level;
} constants;

layout(binding = 0, std430) readonly buffer VoxelFragmentCount {
    uint count;
} voxel_fragment_count;
layout(binding = 1, std430) readonly buffer NodeCounts {
    uint level[];
} node_counts;
layout(binding = 2, std430) buffer NodePoolOffstes {
    uint level[];
} node_pool_offsets;
layout(binding = 3, std430) buffer Threads {
    uint count;
    uint _dummy_0;
    uint offset;
    uint _dummy_2;
} threads;

void subdivision() {
    threads.count = voxel_fragment_count.count;
    threads._dummy_0 = 1;
    threads.offset = 0;
    threads._dummy_2 = 0;
}

void allocation() {
    threads.count = node_counts.level[constants.level];
    threads._dummy_0 = 1;
    threads.offset = node_pool_offsets.level[constants.level];
    threads._dummy_2 = 0;

    node_pool_offsets.level[constants.level + 1] = node_pool_offsets.level[constants.level] + node_counts.level[constants.level];
}

void initialization() {
    threads.count = node_counts.level[constants.level + 1];
    threads._dummy_0 = 1;
    threads.offset = node_pool_offsets.level[constants.level + 1];
    threads._dummy_2 = 0;
}

void neighbour() {
    threads.count = uint(pow(8, constants.level));
    threads._dummy_0 = 1;
    threads.offset = 0;
    threads._dummy_2 = 0;
}

void accumulation() {
    threads.count = voxel_fragment_count.count;
    threads._dummy_0 = 1;
    threads.offset = 0;
    threads._dummy_2 = 0;
}

void normalization() {
    threads.count = node_counts.level[node_counts.level.length() - 1];
    threads._dummy_0 = 1;
    threads.offset = node_pool_offsets.level[node_pool_offsets.level.length() - 1];
    threads._dummy_2 = 0;
}

void mip_mapping() {
    threads.count = node_counts.level[constants.level];
    threads._dummy_0 = 1;
    threads.offset = node_pool_offsets.level[constants.level];
    threads._dummy_2 = 0;
}

void main() {
    if (pass == SUBDIVISION) {
        subdivision();
    } else if (pass == ALLOCATION) {
        allocation();
    } else if (pass == INITIALIZATION) {
        initialization();
    } else if (pass == NEIGHBOUR) {
        neighbour();
    }  else if (pass == ACCUMULATION) {
        accumulation();
    } else if (pass == NOMRALIZATION) {
        normalization();
    } else if (pass == MIP_MAPPING) {
        mip_mapping();
    }
    gl_PointSize = 1.0;
}
