//! Voxel fragment list generation.

use std::sync::{Arc, Mutex};

use ash::vk;
use itertools::Itertools;
use rayon::prelude::*;
use thread_local::ThreadLocal;

use shader::{PushConstants, SpecializationConstants};

use crate::asset::join;
use crate::ecs::{Entities, Transform};
use crate::engine::Config;
use crate::graphics::buffer::Buffer;
use crate::graphics::command_buffer::{CommandBuffer, CommandBufferBuilder};
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorSetLayoutCreationError,
    GraphicsPipelineCreationError,
};
use crate::graphics::render_pass::RenderPassCreationError;
use crate::graphics::{
    Device, Fence, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, SubpassDescription,
};
use crate::rendering::gi::voxel_fragment_list_pass::shader::{Material, Model, Scene};
use crate::rendering::gi::GiData;
use crate::rendering::static_mesh::{StaticMesh, Vertex};
use crate::rendering::texture::Texture;
use crate::rendering::{PipelineCreationError, PipelineExecutionError};

/// Voxel fragment list generation pass.
pub struct VoxelFragmentListPass {
    gi_data: Arc<GiData>,
    default_texture: Texture,
    pipelines: Vec<Arc<GraphicsPipeline>>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    framebuffer: Arc<Framebuffer>,
    render_pass: Arc<RenderPass>,
    device: Arc<Device>,
    config: Arc<Config>,
}

impl VoxelFragmentListPass {
    /// Creates a new voxel fragment list pass.
    pub fn new(
        config: Arc<Config>,
        device: Arc<Device>,
        gi_data: Arc<GiData>,
    ) -> Result<Self, PipelineCreationError> {
        let volume_resolution = config.graphics.global_illumination.resolution as _;
        let vertex_shader_module = vertex_shader::Shader::load(&device)?;
        let geometry_shader_module = geometry_shader::Shader::load(&device)?;
        let fragment_shader_module = fragment_shader::Shader::load(&device)?;
        let render_pass = Self::create_render_pass(device.clone())?;
        let framebuffer =
            Framebuffer::empty(render_pass.clone(), volume_resolution, volume_resolution)?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(device.clone())?;
        let pipelines = Self::create_pipelines(
            &config,
            &device,
            render_pass.clone(),
            vertex_shader_module.clone(),
            geometry_shader_module.clone(),
            fragment_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let default_texture = Texture::two_by_two(device.clone())?;

        Ok(Self {
            gi_data,
            default_texture,
            pipelines,
            descriptor_set_layout,
            framebuffer,
            render_pass,
            device,
            config,
        })
    }

    fn create_render_pass(device: Arc<Device>) -> Result<Arc<RenderPass>, RenderPassCreationError> {
        Ok(RenderPass::new(
            device.clone(),
            vec![],
            vec![SubpassDescription {
                color_attachments: vec![],
                depth_attachment: None,
                input_attachments: vec![],
            }],
            vec![],
        )?)
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(3)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(4)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipelines(
        config: &Config,
        device: &Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        geometry_shader: Arc<ShaderModule>,
        fragment_shader: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Vec<Arc<GraphicsPipeline>>, GraphicsPipelineCreationError> {
        let mut pipelines = Vec::new();
        let specialization_constants =
            (0..3)
                .map(|_| 0..2)
                .multi_cartesian_product()
                .map(|specialization_constants| SpecializationConstants {
                    base_color_texture: specialization_constants[0],
                    normal_texture: specialization_constants[1],
                    metallic_roughness_texture: specialization_constants[2],
                });

        for constants in specialization_constants {
            pipelines.push(Self::create_pipeline(
                config,
                device.clone(),
                render_pass.clone(),
                vertex_shader.clone(),
                geometry_shader.clone(),
                fragment_shader.clone(),
                constants,
                descriptor_set_layout,
            )?);
        }

        Ok(pipelines)
    }

    fn create_pipeline(
        config: &Config,
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        geometry_shader: Arc<ShaderModule>,
        fragment_shader: Arc<ShaderModule>,
        specialization_constants: SpecializationConstants,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        let resolution = config.graphics.global_illumination.resolution as _;
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader)
            .geometry_shader(geometry_shader)
            .fragment_shader(fragment_shader)
            .fragment_shader_specialization_constants(specialization_constants)
            .vertex_input::<Vertex>()
            .triangle_list()
            .viewports(vec![vk::Viewport::builder()
                .width(resolution)
                .height(resolution)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .conservative_rasterization(vk::ConservativeRasterizationModeEXT::OVERESTIMATE)
            .render_pass(render_pass.clone(), 0)
            .push_constants::<PushConstants>(
                vk::ShaderStageFlags::VERTEX
                    | vk::ShaderStageFlags::GEOMETRY
                    | vk::ShaderStageFlags::FRAGMENT,
            )
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    /// Executes the pass.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
        entities: &Entities,
    ) -> Result<(), PipelineExecutionError> {
        builder
            .pipeline_barrier(
                vk::PipelineStageFlags::VERTEX_SHADER,
                vk::PipelineStageFlags::FRAGMENT_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![
                    Self::create_memory_barrier(&self.gi_data.voxel_fragment_list.length),
                    Self::create_memory_barrier(&self.gi_data.voxel_fragment_list.fragments),
                ],
                vec![],
            )
            .begin_render_pass(
                self.framebuffer.clone(),
                vk::SubpassContents::SECONDARY_COMMAND_BUFFERS,
                vec![],
            );
        let per_thread_builder = ThreadLocal::new();
        let results = entities
            .components::<(Transform, StaticMesh)>()
            .par_iter_mut()
            .map(|(_, transform, static_mesh)| {
                let mut builder = per_thread_builder
                    .get_or(|| {
                        Mutex::new(Some(
                            CommandBuffer::secondary(
                                self.device.clone(),
                                vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
                                self.device.graphics_queue().family(),
                                self.render_pass.clone(),
                                0,
                                self.framebuffer.clone(),
                            )
                            .unwrap(),
                        ))
                    })
                    .lock()
                    .unwrap();
                self.static_mesh(&mut builder.as_mut().unwrap(), &transform, &static_mesh)
            })
            .collect::<Vec<_>>();

        for result in results {
            result?;
        }

        for secondary_builder in per_thread_builder.iter() {
            let mut secondary_builder = secondary_builder.lock().unwrap();
            if let Some(secondary_builder) = secondary_builder.take() {
                builder.execute_commands(secondary_builder.build()?);
            }
        }

        builder.end_render_pass();

        Ok(())
    }

    fn create_memory_barrier<T>(buffer: &T) -> vk::BufferMemoryBarrier
    where
        T: Buffer,
    {
        vk::BufferMemoryBarrier::builder()
            .buffer(buffer.inner())
            .offset(0)
            .size(vk::WHOLE_SIZE)
            .src_access_mask(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE)
            .dst_access_mask(vk::AccessFlags::SHADER_WRITE)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .build()
    }

    fn static_mesh(
        &self,
        builder: &mut CommandBufferBuilder,
        transform: &Transform,
        static_mesh: &StaticMesh,
    ) -> Result<(), PipelineExecutionError> {
        static_mesh
            .mesh()
            .map_ok::<_, PipelineExecutionError>(|mesh| {
                builder
                    .bind_vertex_buffer(mesh.vertex_buffer.clone())
                    .bind_index_buffer(mesh.index_buffer.clone(), vk::IndexType::UINT32);

                for sub_mesh in &mesh.sub_meshes {
                    join((
                        sub_mesh.material.base_color_texture().clone(),
                        sub_mesh.material.normal_texture().clone(),
                        sub_mesh.material.metallic_roughness_texture().clone(),
                    ))
                    .map_ok::<_, PipelineExecutionError>(
                        |base_color, normal, metallic_roughness| {
                            let base_color = base_color.unwrap_or(&self.default_texture);
                            let normal = normal.unwrap_or(&self.default_texture);
                            let metallic_roughness =
                                metallic_roughness.unwrap_or(&self.default_texture);

                            let pipeline = &self.pipelines[sub_mesh.material.pipeline_index()];

                            let push_constants = PushConstants {
                                model: Model {
                                    transform: transform.model(),
                                    material: Material {
                                        base_color: sub_mesh.material.base_color(),
                                        metallic: sub_mesh.material.metallic(),
                                        roughness: sub_mesh.material.metallic(),
                                        _dummy_0: Default::default(),
                                        _dummy_1: Default::default(),
                                    },
                                },
                                scene: Scene {
                                    resolution: self.config.graphics.global_illumination.resolution
                                        as _,
                                    size: self.config.graphics.global_illumination.size,
                                },
                            };

                            let descriptor_set = DescriptorSet::builder()
                                .add_sampled_image(
                                    0,
                                    base_color.view().clone(),
                                    base_color.sampler().clone(),
                                )
                                .add_sampled_image(
                                    1,
                                    normal.view().clone(),
                                    normal.sampler().clone(),
                                )
                                .add_sampled_image(
                                    2,
                                    metallic_roughness.view().clone(),
                                    metallic_roughness.sampler().clone(),
                                )
                                .add_buffer(
                                    3,
                                    self.gi_data.voxel_fragment_list.fragments.clone(),
                                    vk::DescriptorType::STORAGE_BUFFER,
                                )
                                .add_buffer(
                                    4,
                                    self.gi_data.voxel_fragment_list.length.clone(),
                                    vk::DescriptorType::STORAGE_BUFFER,
                                )
                                .build(self.device.clone(), self.descriptor_set_layout.clone())?;

                            builder
                                .bind_pipeline(pipeline.clone())
                                .push_constants(
                                    pipeline.layout(),
                                    vk::ShaderStageFlags::VERTEX
                                        | vk::ShaderStageFlags::GEOMETRY
                                        | vk::ShaderStageFlags::FRAGMENT,
                                    push_constants,
                                )
                                .bind_descriptor_set(pipeline.layout(), descriptor_set.clone())
                                .draw_indexed(sub_mesh.count as _, 1, sub_mesh.offset as _, 0, 0);

                            Ok(())
                        },
                    )?
                }

                Ok(())
            })?;

        Ok(())
    }
}

mod vertex_shader {
    feldspat_shaders::shader! {
        path: "src/rendering/gi/voxel_fragment_list.vert",
        ty: "vertex"
    }
}

mod geometry_shader {
    feldspat_shaders::shader! {
        path: "src/rendering/gi/voxel_fragment_list.geom",
        ty: "geometry"
    }
}

mod fragment_shader {
    feldspat_shaders::shader! {
        path: "src/rendering/gi/voxel_fragment_list.frag",
        ty: "fragment",
        include_dirs: ["src/rendering/gi"]
    }
}

mod shader {
    use ash::vk;

    use crate::math::{Mat4, Vec4};

    #[allow(unused)]
    pub struct SpecializationConstants {
        pub base_color_texture: u32,
        pub normal_texture: u32,
        pub metallic_roughness_texture: u32,
    }

    impl crate::graphics::pipeline::SpecializationConstants for SpecializationConstants {
        fn specialization_map_entries(&self) -> Vec<vk::SpecializationMapEntry> {
            vec![
                vk::SpecializationMapEntry::builder()
                    .constant_id(0)
                    .offset(0)
                    .size(4)
                    .build(),
                vk::SpecializationMapEntry::builder()
                    .constant_id(1)
                    .offset(4)
                    .size(4)
                    .build(),
                vk::SpecializationMapEntry::builder()
                    .constant_id(2)
                    .offset(8)
                    .size(4)
                    .build(),
            ]
        }
    }

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct Material {
        pub base_color: Vec4,
        pub metallic: f32,
        pub roughness: f32,
        pub _dummy_0: f32,
        pub _dummy_1: f32,
    }

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct Model {
        pub transform: Mat4,
        pub material: Material,
    }

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct Scene {
        pub resolution: u32,
        pub size: f32,
    }

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct PushConstants {
        pub model: Model,
        pub scene: Scene,
    }
}
