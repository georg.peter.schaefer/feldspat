#version 450

#include <octree.glsl>

layout(push_constant) uniform Constants {
    uint level;
} constants;

layout(binding = 0, std430) volatile coherent buffer NodePool {
    NodeTile pool[];
} node_pool;
layout(binding = 1, std430) volatile coherent buffer NodeCounts {
    uint level[];
} node_counts;
layout(binding = 2, std430) readonly buffer NodePoolOffstes {
    uint level[];
} node_pool_offsets;

void main() {
    for (uint child = 0; child < 8; child++) {
        if (node_pool.pool[gl_VertexIndex].children[child] == TOUCHED) {
            uint next = atomicAdd(node_counts.level[constants.level + 1], 1);
            next += node_pool_offsets.level[constants.level + 1];
            node_pool.pool[gl_VertexIndex].children[child] = next;
            node_pool.pool[next].parent = gl_VertexIndex;
        }
    }
    gl_PointSize = 1.0;
}