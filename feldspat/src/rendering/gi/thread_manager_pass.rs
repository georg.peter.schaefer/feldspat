//! Pass for managing thread counts for indirect drawing.

use std::collections::HashMap;
use std::sync::Arc;

use ash::vk;

use crate::graphics::buffer::Buffer;
use crate::graphics::command_buffer::{CommandBuffer, CommandBufferBuilder};
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorSetLayoutCreationError,
    GraphicsPipelineCreationError,
};
use crate::graphics::render_pass::RenderPassCreationError;
use crate::graphics::{
    Device, Fence, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, SubpassDescription,
};
use crate::rendering::gi::thread_manager_pass::shader::{PushConstants, SpecializationConstants};
use crate::rendering::gi::GiData;
use crate::rendering::{PipelineCreationError, PipelineExecutionError};

///Pass for managing thread counts for indirect drawing.
pub struct ThreadManagerPass {
    gi_data: Arc<GiData>,
    descriptor_set: Arc<DescriptorSet>,
    pipelines: HashMap<Pass, Arc<GraphicsPipeline>>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    framebuffer: Arc<Framebuffer>,
    render_pass: Arc<RenderPass>,
    device: Arc<Device>,
}

impl ThreadManagerPass {
    /// Creates a new thread manager pass.
    pub fn new(device: Arc<Device>, gi_data: Arc<GiData>) -> Result<Self, PipelineCreationError> {
        let vertex_shader_module = vertex_shader::Shader::load(&device)?;
        let render_pass = Self::create_render_pass(device.clone())?;
        let framebuffer = Framebuffer::empty(render_pass.clone(), 2, 2)?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(device.clone())?;
        let pipelines = Self::create_pipelines(
            &device,
            render_pass.clone(),
            vertex_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let descriptor_set = DescriptorSet::builder()
            .add_buffer(
                0,
                gi_data.voxel_fragment_list.length.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_buffer(
                1,
                gi_data.octree.node_counts.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_buffer(
                2,
                gi_data.octree.node_pool_offsets.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_buffer(
                3,
                gi_data.threads.clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .build(device.clone(), descriptor_set_layout.clone())?;

        Ok(Self {
            gi_data,
            descriptor_set,
            pipelines,
            descriptor_set_layout,
            framebuffer,
            render_pass,
            device,
        })
    }

    fn create_render_pass(device: Arc<Device>) -> Result<Arc<RenderPass>, RenderPassCreationError> {
        Ok(RenderPass::new(
            device.clone(),
            vec![],
            vec![SubpassDescription {
                color_attachments: vec![],
                depth_attachment: None,
                input_attachments: vec![],
            }],
            vec![],
        )?)
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(3)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipelines(
        device: &Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<HashMap<Pass, Arc<GraphicsPipeline>>, GraphicsPipelineCreationError> {
        let mut pipelines = HashMap::new();

        for pass in [
            Pass::Subdivision,
            Pass::Allocation,
            Pass::Initialization,
            Pass::Neighbour,
            Pass::Accumulation,
            Pass::Normalization,
            Pass::MipMapping,
        ] {
            pipelines.insert(
                pass,
                Self::create_pipeline(
                    device.clone(),
                    render_pass.clone(),
                    vertex_shader.clone(),
                    SpecializationConstants { pass: pass as _ },
                    descriptor_set_layout,
                )?,
            );
        }

        Ok(pipelines)
    }

    fn create_pipeline(
        device: Arc<Device>,
        render_pass: Arc<RenderPass>,
        vertex_shader: Arc<ShaderModule>,
        specialization_constants: SpecializationConstants,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader)
            .vertex_shader_specialization_constants(specialization_constants)
            .point_list()
            .viewports(vec![vk::Viewport::builder()
                .width(2.0)
                .height(2.0)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .render_pass(render_pass.clone(), 0)
            .push_constants::<PushConstants>(vk::ShaderStageFlags::VERTEX)
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    /// Executes the pass.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
        pass: Pass,
        level: u32,
    ) -> Result<(), PipelineExecutionError> {
        let pipeline = &self.pipelines[&pass];
        let push_constants = PushConstants { level };

        builder
            .pipeline_barrier(
                vk::PipelineStageFlags::DRAW_INDIRECT | vk::PipelineStageFlags::VERTEX_SHADER,
                vk::PipelineStageFlags::VERTEX_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![
                    Self::create_memory_barrier(
                        &self.gi_data.voxel_fragment_list.length,
                        vk::AccessFlags::SHADER_WRITE,
                    ),
                    Self::create_memory_barrier(
                        &self.gi_data.octree.node_counts,
                        vk::AccessFlags::SHADER_WRITE,
                    ),
                    Self::create_memory_barrier(
                        &self.gi_data.octree.node_pool_offsets,
                        vk::AccessFlags::SHADER_WRITE,
                    ),
                    Self::create_memory_barrier(
                        &self.gi_data.threads,
                        vk::AccessFlags::MEMORY_READ,
                    ),
                ],
                vec![],
            )
            .begin_render_pass(
                self.framebuffer.clone(),
                vk::SubpassContents::INLINE,
                vec![],
            )
            .bind_pipeline(pipeline.clone())
            .push_constants(
                pipeline.layout(),
                vk::ShaderStageFlags::VERTEX,
                push_constants,
            )
            .bind_descriptor_set(pipeline.layout(), self.descriptor_set.clone())
            .draw(1, 1, 0, 0)
            .end_render_pass();

        Ok(())
    }

    fn create_memory_barrier<T>(
        buffer: &T,
        src_access_mask: vk::AccessFlags,
    ) -> vk::BufferMemoryBarrier
    where
        T: Buffer,
    {
        vk::BufferMemoryBarrier::builder()
            .buffer(buffer.inner())
            .offset(0)
            .size(vk::WHOLE_SIZE)
            .src_access_mask(src_access_mask)
            .dst_access_mask(vk::AccessFlags::SHADER_WRITE)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .build()
    }
}

/// Passes during scene voxelization.
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum Pass {
    /// Subdivision of intersected nodes.
    Subdivision = 0,
    /// Allocation of new nodes.
    Allocation = 1,
    /// Initialization of new nodes.
    Initialization = 2,
    /// Writing pointer to neighbour nodes.
    Neighbour = 3,
    /// Accumulation of voxel fragments in leaf bricks.
    Accumulation = 4,
    /// Normalization of voxel fragments in leaf bricks.
    Normalization = 5,
    /// Mip mapping of lower levels into parent nodes.
    MipMapping = 6,
}

mod vertex_shader {
    feldspat_shaders::shader! {
        path: "src/rendering/gi/thread_manager.vert",
        ty: "vertex"
    }
}

mod shader {
    use ash::vk;

    #[allow(unused)]
    pub struct SpecializationConstants {
        pub pass: u32,
    }

    impl crate::graphics::pipeline::SpecializationConstants for SpecializationConstants {
        fn specialization_map_entries(&self) -> Vec<vk::SpecializationMapEntry> {
            vec![vk::SpecializationMapEntry::builder()
                .constant_id(0)
                .offset(0)
                .size(4)
                .build()]
        }
    }

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct PushConstants {
        pub level: u32,
    }
}
