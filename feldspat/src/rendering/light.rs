//! Light sources.

use serde::Deserialize;

use crate::math::Vec3;
use crate::Component;

/// Point light.
#[derive(Component, Deserialize)]
pub struct PointLight {
    color: Vec3,
    intensity: f32,
}

impl PointLight {
    /// Creates a new point light with `color` and `intensity`.
    pub fn new(color: Vec3, intensity: f32) -> Self {
        Self { color, intensity }
    }

    /// Returns the color of the light.
    pub fn color(&self) -> Vec3 {
        self.color
    }

    /// Returns the intensity of the light.
    pub fn intensity(&self) -> f32 {
        self.intensity
    }
}

/// Directional light.
#[derive(Component, Deserialize)]
pub struct DirectionalLight {
    color: Vec3,
    intensity: f32,
}

impl DirectionalLight {
    /// Creates a new directional light with `color` and `intensity`.
    pub fn new(color: Vec3, intensity: f32) -> Self {
        Self { color, intensity }
    }

    /// Returns the color of the light.
    pub fn color(&self) -> Vec3 {
        self.color
    }

    /// Returns the intensity of the light.
    pub fn intensity(&self) -> f32 {
        self.intensity
    }
}
