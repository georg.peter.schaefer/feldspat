//! Static mesh component and serialization / deserialization implementation.
use std::iter;
use std::path::{Path, PathBuf};
use std::sync::Arc;

use ash::vk;
use gltf::mesh::util::ReadTexCoords;
use gltf::{Buffer, Gltf};
use serde::Deserialize;

use crate::asset::{Asset, AssetError, AssetRef, Assets, Blob};
use crate::graphics::buffer::ImmutableBuffer;
use crate::math::{Vec2, Vec3, Vec4};
use crate::rendering::material::Material;
use crate::Component;

/// Static mesh component.
#[derive(Component, Deserialize)]
pub struct StaticMesh {
    mesh: AssetRef<Mesh>,
}

impl StaticMesh {
    /// Returns the mesh.
    pub fn mesh(&self) -> &AssetRef<Mesh> {
        &self.mesh
    }

    /// Returns weather the mesh is fully loaded.
    pub fn is_ready(&self) -> bool {
        let mesh_ref = self.mesh.unwrap();
        let mesh = mesh_ref.as_ref().unwrap();
        self.mesh.is_ready()
            && mesh
                .sub_meshes
                .iter()
                .map(|sub_mesh| &sub_mesh.material)
                .flat_map(|material| {
                    vec![
                        material.base_color_texture(),
                        material.normal_texture(),
                        material.metallic_roughness_texture(),
                    ]
                })
                .filter(|texture| texture.is_some())
                .map(|texture| texture.unwrap())
                .all(|texture| texture.is_ready())
    }
}

/// Represents a static mesh.
pub struct Mesh {
    /// The sub meshes.
    pub sub_meshes: Vec<SubMesh>,
    /// The vertex buffer.
    pub vertex_buffer: Arc<ImmutableBuffer<[Vertex]>>,
    /// The index buffer.
    pub index_buffer: Arc<ImmutableBuffer<[u32]>>,
}

impl Mesh {
    fn read_buffer(assets: &Arc<Assets>, base: &Path, buffer: &Buffer) -> Vec<u8> {
        match buffer.source() {
            gltf::buffer::Source::Uri(uri) => assets
                .read::<Blob, _>(base.join(uri))
                .unwrap()
                .unwrap()
                .as_ref()
                .unwrap()
                .get()
                .clone(),
            _ => panic!("unsupported source"),
        }
    }

    fn read_texture_path(base: &Path, texture: &gltf::Texture) -> PathBuf {
        match texture.source().source() {
            gltf::image::Source::Uri { uri, .. } => base.join(&uri),
            _ => panic!("unsupported source"),
        }
    }
}

impl Asset for Mesh {
    fn try_from_bytes(
        assets: Arc<Assets>,
        path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, AssetError> {
        let base_path = path.parent().unwrap();
        let gltf = Gltf::from_slice(&bytes).unwrap();
        let buffers = gltf
            .buffers()
            .map(|buffer| Self::read_buffer(&assets, base_path, &buffer))
            .collect::<Vec<_>>();

        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let mut sub_meshes = Vec::new();
        for mesh in gltf.meshes() {
            for primitive in mesh.primitives() {
                let material = primitive.material();
                let pbr_metallic_roughness = material.pbr_metallic_roughness();

                let mut material_builder = Material::start(
                    pbr_metallic_roughness.base_color_factor().into(),
                    pbr_metallic_roughness.metallic_factor(),
                    pbr_metallic_roughness.roughness_factor(),
                );

                if let Some(base_color_texture) = &pbr_metallic_roughness
                    .base_color_texture()
                    .map(|info| info.texture())
                {
                    material_builder.with_base_color_texture(
                        assets.read(Self::read_texture_path(base_path, base_color_texture))?,
                    );
                }
                if let Some(normal_texture) = &material.normal_texture().map(|info| info.texture())
                {
                    material_builder.with_normal_texture(
                        assets.read(Self::read_texture_path(base_path, normal_texture))?,
                    );
                }
                if let Some(metallic_roughness_texture) = &pbr_metallic_roughness
                    .metallic_roughness_texture()
                    .map(|info| info.texture())
                {
                    material_builder.with_metallic_roughness_texture(assets.read(
                        Self::read_texture_path(base_path, metallic_roughness_texture),
                    )?);
                }
                let material = material_builder.build();

                let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()]));
                let index_offset = indices.len();
                let vertex_offset = vertices.len() as u32;

                indices.extend(
                    reader
                        .read_indices()
                        .unwrap()
                        .into_u32()
                        .map(|index| index + vertex_offset),
                );

                vertices.extend(
                    itertools::multizip((
                        reader.read_positions().unwrap(),
                        reader.read_normals().unwrap(),
                        reader
                            .read_tangents()
                            .map(|iter| Box::new(iter) as Box<dyn Iterator<Item = [f32; 4]>>)
                            .unwrap_or(Box::new(iter::repeat([1f32, 0f32, 0f32, 1f32]))
                                as Box<dyn Iterator<Item = [f32; 4]>>),
                        reader
                            .read_tex_coords(0)
                            .map(ReadTexCoords::into_f32)
                            .map(|iter| Box::new(iter) as Box<dyn Iterator<Item = [f32; 2]>>)
                            .unwrap_or(Box::new(iter::repeat([0f32; 2]))
                                as Box<dyn Iterator<Item = [f32; 2]>>),
                    ))
                    .map(|(position, normal, tangent, texcoord)| Vertex {
                        position: position.into(),
                        normal: normal.into(),
                        tangent: tangent.into(),
                        texcoord: texcoord.into(),
                    }),
                );

                sub_meshes.push(SubMesh {
                    offset: index_offset,
                    count: indices.len() - index_offset,
                    material,
                })
            }
        }

        let device = assets.device();
        let vertex_buffer = ImmutableBuffer::from_iter(
            device.clone(),
            vk::BufferUsageFlags::VERTEX_BUFFER,
            vertices,
        )
        .expect("Failed to create vertex buffer");
        let index_buffer =
            ImmutableBuffer::from_iter(device.clone(), vk::BufferUsageFlags::INDEX_BUFFER, indices)
                .expect("Failed to create index buffer");

        Ok(Mesh {
            sub_meshes,
            vertex_buffer,
            index_buffer,
        })
    }
}

/// Represents a sub mesh.
#[derive(Clone)]
pub struct SubMesh {
    /// Offset into the index buffer.
    pub offset: usize,
    /// Index count.
    pub count: usize,
    /// Material of the sub mesh.
    pub material: Material,
}

/// A static mesh vertex.
#[derive(Copy, Clone, Default)]
pub struct Vertex {
    /// The position.
    pub position: Vec3,
    /// The normal.
    pub normal: Vec3,
    /// The tangent.
    pub tangent: Vec4,
    /// The texture coordinates.
    pub texcoord: Vec2,
}

crate::vertex!(Vertex, position, normal, tangent, texcoord);
