#version 450

layout(input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput light;

layout(location = 0) out vec4 out_color;

void main() {
    vec3 color = subpassLoad(light).rgb;
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0 / 2.2));
    out_color = vec4(color, 1.0);
}
