//! Ambient occlusion sub pass.

use crate::engine::Config;
use crate::graphics::command_buffer::CommandBufferBuilder;
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorSetLayoutCreationError,
    GraphicsPipelineCreationError,
};
use crate::graphics::{Device, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, Swapchain};
use crate::rendering::ao_pass::shader::PushConstants;
use crate::rendering::fullscreen_pass::{FullscreenVertexBuffer, Vertex};
use crate::rendering::gi::GiData;
use crate::rendering::{PipelineCreationError, PipelineExecutionError};
use ash::vk;
use std::sync::Arc;

/// Final sub pass for gamma and color correction.
pub struct AOPass {
    gi_data: Arc<GiData>,
    fullscreen_vertex_buffer: Arc<FullscreenVertexBuffer>,
    pipeline: Arc<GraphicsPipeline>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    device: Arc<Device>,
    config: Arc<Config>,
}

impl AOPass {
    /// Creates the ao pass.
    pub fn new(
        config: Arc<Config>,
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        gi_data: Arc<GiData>,
    ) -> Result<Self, PipelineCreationError> {
        let vertex_shader_module = super::fullscreen_pass::vertex_shader::Shader::load(&device)?;
        let fragment_shader_module = fragment_shader::Shader::load(&device)?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(device.clone())?;
        let pipeline = Self::create_pipeline(
            device.clone(),
            swapchain,
            render_pass.clone(),
            vertex_shader_module.clone(),
            fragment_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let fullscreen_vertex_buffer = FullscreenVertexBuffer::new(device.clone())?;

        Ok(Self {
            gi_data,
            fullscreen_vertex_buffer,
            pipeline,
            descriptor_set_layout,
            device,
            config,
        })
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(1)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(2)
                    .descriptor_type(vk::DescriptorType::INPUT_ATTACHMENT)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(3)
                    .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(4)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(5)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(6)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(7)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipeline(
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        vertex_shader_module: Arc<ShaderModule>,
        fragment_shader_module: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        let extent = swapchain.extent();
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader_module)
            .fragment_shader(fragment_shader_module)
            .vertex_input::<Vertex>()
            .triangle_list()
            .viewports(vec![vk::Viewport::builder()
                .width(extent.width as _)
                .height(extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .render_pass(render_pass.clone(), 2)
            .push_constants::<PushConstants>(vk::ShaderStageFlags::FRAGMENT)
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    /// Executes the sub pass.
    pub fn execute(
        &self,
        builder: &mut CommandBufferBuilder,
        framebuffer: &Arc<Framebuffer>,
    ) -> Result<(), PipelineExecutionError> {
        let descriptor_set = DescriptorSet::builder()
            .add_image(0, framebuffer.attachments()[0].clone())
            .add_image(1, framebuffer.attachments()[2].clone())
            .add_image(2, framebuffer.attachments()[5].clone())
            .add_buffer(
                3,
                self.gi_data.octree().node_pool().clone(),
                vk::DescriptorType::STORAGE_BUFFER,
            )
            .add_sampled_image(
                4,
                self.gi_data.octree().brick_pool().base_color().components[0].clone(),
                self.gi_data.octree().brick_pool().sampler().clone(),
            )
            .add_sampled_image(
                5,
                self.gi_data.octree().brick_pool().base_color().components[1].clone(),
                self.gi_data.octree().brick_pool().sampler().clone(),
            )
            .add_sampled_image(
                6,
                self.gi_data.octree().brick_pool().base_color().components[2].clone(),
                self.gi_data.octree().brick_pool().sampler().clone(),
            )
            .add_sampled_image(
                7,
                self.gi_data.octree().brick_pool().occlusion().components[0].clone(),
                self.gi_data.octree().brick_pool().sampler().clone(),
            )
            .build(self.device.clone(), self.descriptor_set_layout.clone())?;

        let push_constants = PushConstants {
            size: self.config.graphics.global_illumination.size,
            level: self.gi_data.octree().max_height(),
        };

        builder
            .next_subpass(vk::SubpassContents::INLINE)
            .bind_pipeline(self.pipeline.clone())
            .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
            .push_constants(
                self.pipeline.layout(),
                vk::ShaderStageFlags::FRAGMENT,
                push_constants,
            )
            .bind_descriptor_set(self.pipeline.layout(), descriptor_set)
            .draw(3, 1, 0, 0);

        Ok(())
    }
}

mod fragment_shader {
    feldspat_shaders::shader! {
        ty: "fragment",
        path: "src/rendering/ao.frag",
        include_dirs: ["src/rendering"]
    }
}

mod shader {
    use ash::vk;

    #[derive(Copy, Clone)]
    #[allow(unused)]
    pub struct PushConstants {
        pub size: f32,
        pub level: u32,
    }
}
