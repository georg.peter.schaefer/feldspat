//! Physically based rendering pipeline.

use std::sync::Arc;

use ash::vk;

use crate::ecs::Entities;
use crate::engine::Config;
use crate::graphics::command_buffer::CommandBuffer;
use crate::graphics::image::{Image, ImageView, ImmutableImage};
use crate::graphics::render_pass::RenderPassCreationError;
use crate::graphics::{Device, Framebuffer, RenderPass, SubpassDescription, Swapchain};
use crate::rendering::ao_pass::AOPass;
use crate::rendering::final_pass::FinalPass;
use crate::rendering::geometry_pass::GeometryPass;
use crate::rendering::gi::GiData;
use crate::rendering::light_pass::LightPass;
use crate::rendering::{PipelineCreationError, PipelineExecutionError};

/// Physical based rendering pipeline.
pub struct PbrPipeline {
    final_pass: FinalPass,
    ao_pass: AOPass,
    light_pass: LightPass,
    geometry_pass: GeometryPass,
    framebuffer: Arc<Framebuffer>,
    final_attachment: Arc<ImageView<Arc<ImmutableImage>>>,
    _render_pass: Arc<RenderPass>,
    device: Arc<Device>,
}

impl PbrPipeline {
    /// Create a new pbr pipeline.
    pub fn new(
        config: Arc<Config>,
        device: Arc<Device>,
        swapchain: &Swapchain,
        gi_data: Arc<GiData>,
    ) -> Result<PbrPipeline, PipelineCreationError> {
        let render_pass = Self::create_render_pass(&device)?;
        let final_attachment = ImageView::from_image(ImmutableImage::uninitialized(
            device.clone(),
            vk::Format::R8G8B8A8_SRGB,
            swapchain.images()[0].extent(),
            1,
            vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::SAMPLED,
        )?)?;
        let framebuffer =
            Self::create_framebuffer(&device, &swapchain, &render_pass, final_attachment.clone())?;
        let geometry_pass = GeometryPass::new(device.clone(), &swapchain, render_pass.clone())?;
        let light_pass = LightPass::new(
            device.clone(),
            &swapchain,
            render_pass.clone(),
            gi_data.clone(),
        )?;
        let ao_pass = AOPass::new(
            config.clone(),
            device.clone(),
            &swapchain,
            render_pass.clone(),
            gi_data.clone(),
        )?;
        let final_pass = FinalPass::new(device.clone(), &swapchain, render_pass.clone())?;

        Ok(Self {
            final_pass,
            ao_pass,
            light_pass,
            geometry_pass,
            framebuffer,
            final_attachment,
            _render_pass: render_pass,
            device,
        })
    }

    fn create_render_pass(
        device: &Arc<Device>,
    ) -> Result<Arc<RenderPass>, RenderPassCreationError> {
        RenderPass::new(
            device.clone(),
            vec![
                vk::AttachmentDescription::builder()
                    .format(vk::Format::R32G32B32A32_SFLOAT)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .build(),
                vk::AttachmentDescription::builder()
                    .format(vk::Format::A2B10G10R10_UNORM_PACK32)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .build(),
                vk::AttachmentDescription::builder()
                    .format(vk::Format::R32G32B32A32_SFLOAT)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .build(),
                vk::AttachmentDescription::builder()
                    .format(vk::Format::R32G32B32A32_SFLOAT)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .build(),
                vk::AttachmentDescription::builder()
                    .format(vk::Format::D32_SFLOAT)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                    .build(),
                vk::AttachmentDescription::builder()
                    .format(vk::Format::R32G32B32A32_SFLOAT)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .build(),
                vk::AttachmentDescription::builder()
                    .format(vk::Format::R32G32B32A32_SFLOAT)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .build(),
                vk::AttachmentDescription::builder()
                    .format(vk::Format::R8G8B8A8_SRGB)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .load_op(vk::AttachmentLoadOp::CLEAR)
                    .store_op(vk::AttachmentStoreOp::STORE)
                    .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                    .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                    .initial_layout(vk::ImageLayout::UNDEFINED)
                    .final_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                    .build(),
            ],
            vec![
                SubpassDescription {
                    color_attachments: vec![
                        vk::AttachmentReference::builder()
                            .attachment(0)
                            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                            .build(),
                        vk::AttachmentReference::builder()
                            .attachment(1)
                            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                            .build(),
                        vk::AttachmentReference::builder()
                            .attachment(2)
                            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                            .build(),
                        vk::AttachmentReference::builder()
                            .attachment(3)
                            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                            .build(),
                    ],
                    depth_attachment: Some(
                        vk::AttachmentReference::builder()
                            .attachment(4)
                            .layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                            .build(),
                    ),
                    input_attachments: vec![],
                },
                SubpassDescription {
                    color_attachments: vec![vk::AttachmentReference::builder()
                        .attachment(5)
                        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .build()],
                    depth_attachment: None,
                    input_attachments: vec![
                        vk::AttachmentReference::builder()
                            .attachment(0)
                            .layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                            .build(),
                        vk::AttachmentReference::builder()
                            .attachment(1)
                            .layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                            .build(),
                        vk::AttachmentReference::builder()
                            .attachment(2)
                            .layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                            .build(),
                        vk::AttachmentReference::builder()
                            .attachment(3)
                            .layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                            .build(),
                    ],
                },
                SubpassDescription {
                    color_attachments: vec![vk::AttachmentReference::builder()
                        .attachment(6)
                        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .build()],
                    depth_attachment: None,
                    input_attachments: vec![
                        vk::AttachmentReference::builder()
                            .attachment(0)
                            .layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                            .build(),
                        vk::AttachmentReference::builder()
                            .attachment(2)
                            .layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                            .build(),
                        vk::AttachmentReference::builder()
                            .attachment(5)
                            .layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                            .build(),
                    ],
                },
                SubpassDescription {
                    color_attachments: vec![vk::AttachmentReference::builder()
                        .attachment(7)
                        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .build()],
                    depth_attachment: None,
                    input_attachments: vec![vk::AttachmentReference::builder()
                        .attachment(6)
                        .layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                        .build()],
                },
            ],
            vec![
                vk::SubpassDependency::builder()
                    .src_subpass(vk::SUBPASS_EXTERNAL)
                    .dst_subpass(0)
                    .src_stage_mask(
                        vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
                            | vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
                    )
                    .src_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ)
                    .dst_stage_mask(
                        vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
                            | vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
                    )
                    .dst_access_mask(
                        vk::AccessFlags::COLOR_ATTACHMENT_WRITE
                            | vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE,
                    )
                    .build(),
                vk::SubpassDependency::builder()
                    .src_subpass(0)
                    .dst_subpass(1)
                    .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                    .src_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ)
                    .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                    .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
                    .build(),
                vk::SubpassDependency::builder()
                    .src_subpass(1)
                    .dst_subpass(2)
                    .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                    .src_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ)
                    .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                    .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
                    .build(),
                vk::SubpassDependency::builder()
                    .src_subpass(2)
                    .dst_subpass(3)
                    .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                    .src_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ)
                    .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                    .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
                    .build(),
            ],
        )
    }

    fn create_framebuffer(
        device: &Arc<Device>,
        swapchain: &Swapchain,
        render_pass: &Arc<RenderPass>,
        final_attachment: Arc<ImageView<Arc<ImmutableImage>>>,
    ) -> Result<Arc<Framebuffer>, PipelineCreationError> {
        let extent = swapchain.images()[0].extent();
        Ok(Framebuffer::builder()
            .add(ImageView::from_image(ImmutableImage::uninitialized(
                device.clone(),
                vk::Format::R32G32B32A32_SFLOAT,
                extent,
                1,
                vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::INPUT_ATTACHMENT,
            )?)?)
            .add(ImageView::from_image(ImmutableImage::uninitialized(
                device.clone(),
                vk::Format::A2B10G10R10_UNORM_PACK32,
                extent,
                1,
                vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::INPUT_ATTACHMENT,
            )?)?)
            .add(ImageView::from_image(ImmutableImage::uninitialized(
                device.clone(),
                vk::Format::R32G32B32A32_SFLOAT,
                extent,
                1,
                vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::INPUT_ATTACHMENT,
            )?)?)
            .add(ImageView::from_image(ImmutableImage::uninitialized(
                device.clone(),
                vk::Format::R32G32B32A32_SFLOAT,
                extent,
                1,
                vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::INPUT_ATTACHMENT,
            )?)?)
            .add(ImageView::from_image(ImmutableImage::uninitialized(
                device.clone(),
                vk::Format::D32_SFLOAT,
                extent,
                1,
                vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
                    | vk::ImageUsageFlags::INPUT_ATTACHMENT,
            )?)?)
            .add(ImageView::from_image(ImmutableImage::uninitialized(
                device.clone(),
                vk::Format::R32G32B32A32_SFLOAT,
                extent,
                1,
                vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::INPUT_ATTACHMENT,
            )?)?)
            .add(ImageView::from_image(ImmutableImage::uninitialized(
                device.clone(),
                vk::Format::R32G32B32A32_SFLOAT,
                extent,
                1,
                vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::INPUT_ATTACHMENT,
            )?)?)
            .add(final_attachment)
            .build(render_pass.clone())?)
    }

    /// Records the pipeline execution into a primary command buffer.
    pub fn execute(
        &self,
        entities: &Entities,
    ) -> Result<Arc<CommandBuffer>, PipelineExecutionError> {
        let mut builder = CommandBuffer::primary(
            self.device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            self.device.graphics_queue().family(),
        )?;
        builder.begin_render_pass(
            self.framebuffer.clone(),
            vk::SubpassContents::SECONDARY_COMMAND_BUFFERS,
            vec![
                vk::ClearValue {
                    color: vk::ClearColorValue {
                        float32: [0.0, 0.0, 0.0, 1.0],
                    },
                },
                vk::ClearValue {
                    color: vk::ClearColorValue {
                        float32: [0.0, 0.0, 0.0, 1.0],
                    },
                },
                vk::ClearValue {
                    color: vk::ClearColorValue {
                        float32: [0.0, 0.0, 0.0, 1.0],
                    },
                },
                vk::ClearValue {
                    color: vk::ClearColorValue {
                        float32: [0.0, 0.0, 0.0, 1.0],
                    },
                },
                vk::ClearValue {
                    depth_stencil: vk::ClearDepthStencilValue {
                        depth: 1.0,
                        stencil: 0,
                    },
                },
                vk::ClearValue {
                    color: vk::ClearColorValue {
                        float32: [0.0, 0.0, 0.0, 1.0],
                    },
                },
                vk::ClearValue {
                    color: vk::ClearColorValue {
                        float32: [0.0, 0.0, 0.0, 1.0],
                    },
                },
                vk::ClearValue {
                    color: vk::ClearColorValue {
                        int32: [0, 0, 0, 1],
                    },
                },
            ],
        );

        self.geometry_pass
            .execute(&mut builder, &self.framebuffer, entities)?;
        self.light_pass
            .execute(&mut builder, &self.framebuffer, entities)?;
        self.ao_pass.execute(&mut builder, &self.framebuffer)?;
        self.final_pass.execute(&mut builder, &self.framebuffer)?;

        builder.end_render_pass();

        Ok(builder.build()?)
    }

    /// Returns the final attachment.
    pub fn final_attachment(&self) -> &Arc<ImageView<Arc<ImmutableImage>>> {
        &self.final_attachment
    }
}
