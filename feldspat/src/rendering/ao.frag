#version 450

#include <gi/octree.glsl>
#include <gi/cone_tracing.glsl>

const float PI = 3.14159265358979323846;

layout(push_constant) uniform Constants {
    float octree_length;
    uint octree_level;
} constants;

layout(input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput position;
layout(input_attachment_index = 1, set = 0, binding = 1) uniform subpassInput normal;
layout(input_attachment_index = 2, set = 0, binding = 2) uniform subpassInput light;
layout(binding = 3, std430) buffer NodePool {
    NodeTile pool[];
} node_pool;
layout(binding = 4) uniform sampler3D base_color_red_image;
layout(binding = 5) uniform sampler3D base_color_green_image;
layout(binding = 6) uniform sampler3D base_color_blue_image;
layout(binding = 7) uniform sampler3D occlusion_image;

layout(location = 0) out vec4 out_color;

void main() {
    vec3 color = subpassLoad(light).rgb;
    vec3 N = subpassLoad(normal).xyz;

    if (length(N) <= 0.01) {
        out_color = vec4(color, 1.0);
    } else {
        N = normalize(N);
        vec3 P = subpassLoad(position).xyz;

        uint resolution = textureSize(base_color_red_image, 0).x;
        vec3 tangent = normalize(vec3(0.0, -N.z, N.y));
        vec3 bitangent = normalize(cross(N, tangent));
        float occlusion = 0.0;
        float max_distance = 20.0;
        float aperture = (PI / 2) * 0.5;
        vec3 N0 = normalize(N + normalize(2 * bitangent / 3 - tangent / 3));
        vec3 N1 = normalize(N + normalize(2 * -bitangent / 3 - tangent / 3));
        vec3 N2 = normalize(N + tangent);
        vec3 offset = N * (constants.octree_length / resolution) * 1.0;
        occlusion += 0.33 * cone_trace(offset + P, N0, aperture, max_distance, resolution, constants.octree_length, constants.octree_level).a;
        occlusion += 0.33 * cone_trace(offset + P, N1, aperture, max_distance, resolution, constants.octree_length, constants.octree_level).a;
        occlusion += 0.33 * cone_trace(offset + P, N2, aperture, max_distance, resolution, constants.octree_length, constants.octree_level).a;
        float ao = clamp(1.0 - occlusion, 0.0, 1.0);
        out_color = vec4(ao * color, 1.0);
    }
}

NodeTile get_node(uint node_index) {
    return node_pool.pool[node_index];
}

uint get_child(uint node_index, uint child_index) {
    return node_pool.pool[node_index].children[child_index];
}

vec4 sample_brick(vec3 coords) {
    return vec4(
    texture(base_color_red_image, coords).r,
    texture(base_color_green_image, coords).r,
    texture(base_color_blue_image, coords).r,
    texture(occlusion_image, coords).r);
}
