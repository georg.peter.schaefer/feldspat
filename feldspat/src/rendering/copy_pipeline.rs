//! Pipeline that copies an input image to an output image.

use std::sync::Arc;

use ash::vk;

use crate::graphics::command_buffer::CommandBuffer;
use crate::graphics::image::{ImageView, ImmutableImage, Sampler, SamplerCreationError};
use crate::graphics::pipeline::{
    DescriptorSet, DescriptorSetCreationError, DescriptorSetLayout,
    DescriptorSetLayoutCreationError, GraphicsPipelineCreationError,
};
use crate::graphics::render_pass::RenderPassCreationError;
use crate::graphics::{
    Device, Framebuffer, GraphicsPipeline, RenderPass, ShaderModule, SubpassDescription, Swapchain,
};
use crate::rendering::fullscreen_pass::{FullscreenVertexBuffer, Vertex};
use crate::rendering::{PipelineCreationError, PipelineExecutionError};

/// Pipeline that copies an input image to an output image.
pub struct CopyPipeline {
    sampler: Arc<Sampler>,
    fullscreen_vertex_buffer: Arc<FullscreenVertexBuffer>,
    framebuffers: Vec<Arc<Framebuffer>>,
    pipeline: Arc<GraphicsPipeline>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    _render_pass: Arc<RenderPass>,
    device: Arc<Device>,
}

impl CopyPipeline {
    /// Creates the copy pipeline.
    pub fn new(
        device: Arc<Device>,
        swapchain: &Swapchain,
    ) -> Result<CopyPipeline, PipelineCreationError> {
        let render_pass = Self::create_render_pass(&device, swapchain)?;
        let descriptor_set_layout = Self::create_descriptor_set_layout(device.clone())?;
        let vertex_shader_module = super::fullscreen_pass::vertex_shader::Shader::load(&device)?;
        let fragment_shader_module = fragment_shader::Shader::load(&device)?;
        let pipeline = Self::create_pipeline(
            device.clone(),
            swapchain,
            render_pass.clone(),
            vertex_shader_module.clone(),
            fragment_shader_module.clone(),
            &descriptor_set_layout,
        )?;
        let framebuffers = Self::create_framebuffers(swapchain, &render_pass)?;
        let fullscreen_vertex_buffer = FullscreenVertexBuffer::new(device.clone())?;
        let sampler = Self::create_sampler(&device)?;

        Ok(Self {
            sampler,
            fullscreen_vertex_buffer,
            framebuffers,
            pipeline,
            descriptor_set_layout,
            _render_pass: render_pass,
            device,
        })
    }

    fn create_render_pass(
        device: &Arc<Device>,
        swapchain: &Swapchain,
    ) -> Result<Arc<RenderPass>, RenderPassCreationError> {
        RenderPass::new(
            device.clone(),
            vec![vk::AttachmentDescription::builder()
                .format(swapchain.format())
                .samples(vk::SampleCountFlags::TYPE_1)
                .load_op(vk::AttachmentLoadOp::CLEAR)
                .store_op(vk::AttachmentStoreOp::STORE)
                .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
                .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
                .initial_layout(vk::ImageLayout::UNDEFINED)
                .final_layout(vk::ImageLayout::PRESENT_SRC_KHR)
                .build()],
            vec![SubpassDescription {
                color_attachments: vec![vk::AttachmentReference::builder()
                    .attachment(0)
                    .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .build()],
                depth_attachment: None,
                input_attachments: vec![],
            }],
            vec![vk::SubpassDependency::builder()
                .src_subpass(vk::SUBPASS_EXTERNAL)
                .dst_subpass(0)
                .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                .src_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ)
                .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
                .build()],
        )
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        Ok(DescriptorSetLayout::builder()
            .add(
                vk::DescriptorSetLayoutBinding::builder()
                    .binding(0)
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .descriptor_count(1)
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .build(),
            )
            .build(device.clone())?)
    }

    fn create_pipeline(
        device: Arc<Device>,
        swapchain: &Swapchain,
        render_pass: Arc<RenderPass>,
        vertex_shader_module: Arc<ShaderModule>,
        fragment_shader_module: Arc<ShaderModule>,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        let extent = swapchain.extent();
        Ok(GraphicsPipeline::builder()
            .vertex_shader(vertex_shader_module)
            .fragment_shader(fragment_shader_module)
            .vertex_input::<Vertex>()
            .triangle_list()
            .viewports(vec![vk::Viewport::builder()
                .width(extent.width as _)
                .height(extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)
                .build()])
            .render_pass(render_pass.clone(), 0)
            .descriptor_set_layout(descriptor_set_layout)
            .build(device.clone())?)
    }

    fn create_framebuffers(
        swapchain: &Swapchain,
        render_pass: &Arc<RenderPass>,
    ) -> Result<Vec<Arc<Framebuffer>>, PipelineCreationError> {
        let mut framebuffers = vec![];
        for view in swapchain.image_views() {
            framebuffers.push(
                Framebuffer::builder()
                    .add(view.clone())
                    .build(render_pass.clone())?,
            );
        }
        Ok(framebuffers)
    }

    fn create_sampler(device: &Arc<Device>) -> Result<Arc<Sampler>, SamplerCreationError> {
        Ok(Sampler::builder().build(device.clone())?)
    }

    /// Executes the pipeline for the passed swapchain image.
    pub fn execute(
        &self,
        src_image: Arc<ImageView<Arc<ImmutableImage>>>,
        target_swapchain_image_index: usize,
    ) -> Result<Arc<CommandBuffer>, PipelineExecutionError> {
        let queue = self.device.graphics_queue();
        let framebuffer = &self.framebuffers[target_swapchain_image_index];
        let descriptor_set = Self::create_descriptor_set(
            &self.device,
            &self.descriptor_set_layout,
            &src_image,
            &self.sampler,
        )?;

        let mut builder = CommandBuffer::primary(
            self.device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            queue.family(),
        )?;

        builder
            .begin_render_pass(
                framebuffer.clone(),
                vk::SubpassContents::INLINE,
                vec![vk::ClearValue {
                    color: vk::ClearColorValue {
                        uint32: [0, 0, 0, 1],
                    },
                }],
            )
            .bind_pipeline(self.pipeline.clone())
            .bind_descriptor_set(self.pipeline.layout(), descriptor_set.clone())
            .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
            .draw(3, 1, 0, 0)
            .end_render_pass();

        Ok(builder.build()?)
    }

    fn create_descriptor_set(
        device: &Arc<Device>,
        layout: &Arc<DescriptorSetLayout>,
        view: &Arc<ImageView<Arc<ImmutableImage>>>,
        sampler: &Arc<Sampler>,
    ) -> Result<Arc<DescriptorSet>, DescriptorSetCreationError> {
        Ok(DescriptorSet::builder()
            .add_sampled_image(0, view.clone(), sampler.clone())
            .build(device.clone(), layout.clone())?)
    }
}

mod fragment_shader {
    feldspat_shaders::shader! {
        ty: "fragment",
        path: "src/rendering/fullscreen.frag"
    }
}
