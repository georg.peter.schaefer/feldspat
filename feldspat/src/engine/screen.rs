//! Types and traits for game state (`Screen`) handling.

use std::cell::UnsafeCell;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use log::debug;

use crate::engine::Engine;

/// The mode of a screen.
#[derive(Debug, Eq, PartialEq)]
pub enum Mode {
    /// Covers all screens under this screen.
    ///
    /// Lower screens are not updated and not drawn.
    Opaque,

    /// Overlays all screens under this screen.
    ///
    /// Lower screens are not updated but drawn.
    Modal,

    /// Overlays all screens under this screen.
    ///
    /// Lower screens are updated and drawn.
    Overlay,
}

/// Describes a screen.
/// TODO: Error handling.
pub trait Screen {
    /// Returns the name of the screen.
    fn name(&self) -> &'static str;

    /// Returns the mode of the screen.
    fn mode(&self) -> Mode;

    /// Handles on entering the screen.
    fn on_enter(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>>;

    /// Updates the screen.
    fn update(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>>;

    /// Draws the screen.
    fn draw(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>>;

    /// Handles on leaving the screen.
    fn on_leave(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>>;
}

/// Stack based state machine for screens.
pub struct Screens {
    screens: HashMap<&'static str, Arc<dyn Screen>>,
    queue: Mutex<Vec<StackOp>>,
    stack: UnsafeCell<Vec<Arc<dyn Screen>>>,
}

impl Screens {
    /// Creates a new state machine with `screens`.
    pub fn new<I>(screens: I) -> Self
    where
        I: IntoIterator<Item = Arc<dyn Screen>>,
    {
        Self {
            screens: screens
                .into_iter()
                .map(|screen| (screen.name(), screen))
                .collect(),
            queue: Mutex::new(Vec::new()),
            stack: UnsafeCell::new(Vec::new()),
        }
    }

    /// Adds a screen.
    pub fn add(&mut self, screen: Arc<dyn Screen>) {
        let name = screen.name();
        self.screens.insert(name, screen);
        if self.get().is_empty() {
            self.push(name).unwrap();
        }
    }

    /// Updates the current frame.
    pub fn update(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>> {
        for screen in Self::get_frame(self.get()) {
            screen.update(engine)?;
        }
        Ok(())
    }

    /// Draws the current visible frame.
    pub fn draw(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>> {
        for screen in Self::get_visible_frame(self.get()) {
            screen.draw(engine)?;
        }
        Ok(())
    }

    /// Pushes a new screen onto the stack.
    pub fn push(&self, screen: &str) -> Result<(), ()> {
        self.queue
            .lock()
            .unwrap()
            .push(StackOp::Push(self.screens.get(screen).cloned().ok_or(())?));
        Ok(())
    }

    /// Pops a screen from the stack.
    pub fn pop(&self) {
        self.queue.lock().unwrap().push(StackOp::Pop);
    }

    /// Commits queued operations.
    pub(crate) fn commit(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>> {
        let mut queue = self.queue.lock().unwrap();
        for op in queue.iter() {
            match op {
                StackOp::Push(screen) => {
                    self._push(screen, engine)?;
                }
                StackOp::Pop => {
                    self._pop(engine)?;
                }
            }
        }
        queue.clear();
        Ok(())
    }

    fn _push(
        &self,
        screen: &Arc<dyn Screen>,
        engine: &Engine,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let stack = self.get_mut();

        if screen.mode() == Mode::Opaque {
            for screen in Self::get_frame(&stack) {
                debug!("Leaving screen '{}'", screen.name());
                screen.on_leave(engine)?;
            }
        }

        stack.push(screen.clone());

        debug!("Entering screen '{}'", screen.name());
        screen.on_enter(engine)?;
        Ok(())
    }

    fn _pop(&self, engine: &Engine) -> Result<(), Box<dyn std::error::Error>> {
        let stack = self.get_mut();
        let frame_length = {
            let frame = Self::get_frame(&stack);

            for screen in frame.iter().rev() {
                debug!("Leaving screen '{}'", screen.name());
                screen.on_leave(engine)?;
            }

            frame.len()
        };
        let stack_length = stack.len();

        stack.truncate(stack_length - frame_length);

        for screen in Self::get_frame(&stack) {
            debug!("Entering screen '{}'", screen.name());
            screen.on_enter(engine)?;
        }
        Ok(())
    }

    fn get_frame(stack: &Vec<Arc<dyn Screen>>) -> &[Arc<dyn Screen>] {
        stack
            .iter()
            .rposition(|screen| matches!(screen.mode(), Mode::Opaque | Mode::Modal))
            .map(|index| &stack[index..])
            .unwrap_or(stack.as_slice())
    }

    fn get_visible_frame(stack: &Vec<Arc<dyn Screen>>) -> &[Arc<dyn Screen>] {
        stack
            .iter()
            .rposition(|screen| screen.mode() == Mode::Opaque)
            .map(|index| &stack[index..])
            .unwrap_or(stack.as_slice())
    }

    fn get(&self) -> &Vec<Arc<dyn Screen>> {
        unsafe { &*self.stack.get() }
    }

    fn get_mut(&self) -> &mut Vec<Arc<dyn Screen>> {
        unsafe { &mut *self.stack.get() }
    }
}

unsafe impl Send for Screens {}

unsafe impl Sync for Screens {}

enum StackOp {
    Push(Arc<dyn Screen>),
    Pop,
}
