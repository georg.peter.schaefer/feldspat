use std::cell::RefCell;
use std::sync::Arc;
use std::time::{SystemTime, UNIX_EPOCH};

/// Keeps track of time.
pub struct Time {
    inner: RefCell<InnerTime>,
}

impl Time {
    /// Creates a new `Time`.
    pub fn new() -> Arc<Self> {
        Arc::new(Self {
            inner: RefCell::new(InnerTime {
                last: SystemTime::now(),
                current: SystemTime::now(),
                elapsed: 0.0,
                delta: 0.0,
            }),
        })
    }

    /// Returns the elapsed since the engine has been started time in seconds.
    pub fn elapsed(&self) -> f32 {
        self.inner.borrow().elapsed
    }

    /// Returns the time since the last frame in seconds.
    pub fn delta(&self) -> f32 {
        self.inner.borrow().delta
    }

    /// Updates the time.
    pub fn update(&self) {
        let mut inner = self.inner.borrow_mut();
        inner.last = inner.current;
        inner.current = SystemTime::now();
        inner.delta = (inner.current.duration_since(UNIX_EPOCH).unwrap()
            - inner.last.duration_since(UNIX_EPOCH).unwrap())
        .as_secs_f32();
        inner.elapsed += inner.delta;
    }
}

unsafe impl Send for Time {}

unsafe impl Sync for Time {}

struct InnerTime {
    last: SystemTime,
    current: SystemTime,
    elapsed: f32,
    delta: f32,
}
