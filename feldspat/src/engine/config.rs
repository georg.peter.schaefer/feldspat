use std::path::PathBuf;

use serde::Deserialize;

/// Engine configuration.
#[derive(Debug, Deserialize, Default)]
pub struct Config {
    /// System configuration.
    pub system: System,
    /// Video configuration.
    pub video: Video,
    /// Graphics configuration.
    pub graphics: Graphics,
}

/// System configuration.
#[derive(Debug, Deserialize)]
pub struct System {
    pub title: String,
    pub assets: PathBuf,
}

impl Default for System {
    fn default() -> Self {
        Self {
            title: "feldspat engine".to_string(),
            assets: "feldspat-demo/rsc/assets".into(),
        }
    }
}

/// Video configuration.
#[derive(Debug, Deserialize, Default)]
pub struct Video {
    pub resolution: Resolution,
}

/// Resolution.
#[derive(Debug, Deserialize)]
pub struct Resolution {
    pub width: u32,
    pub height: u32,
}

impl Default for Resolution {
    fn default() -> Self {
        Self {
            width: 1024,
            height: 768,
        }
    }
}

/// Graphics configuration.
#[derive(Debug, Deserialize, Default)]
pub struct Graphics {
    pub shadow: Shadow,
    pub global_illumination: GlobalIllumination,
}

/// Shadow properties.
#[derive(Debug, Deserialize)]
pub struct Shadow {
    pub resolution: u32,
    pub light_bleeding_reduction: f32,
    pub c_0: f32,
    pub c_1: f32,
}

impl Default for Shadow {
    fn default() -> Self {
        Self {
            resolution: 1024,
            light_bleeding_reduction: 0.0,
            c_0: 40.0,
            c_1: 5.0,
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct GlobalIllumination {
    pub resolution: u64,
    pub size: f32,
    pub max_fragments_per_voxel: u64,
    pub occupied_nodes: f32,
}

impl Default for GlobalIllumination {
    fn default() -> Self {
        Self {
            resolution: 512,
            size: 30.0,
            max_fragments_per_voxel: 1,
            occupied_nodes: 0.1,
        }
    }
}
