use log::info;

include!(concat!(env!("OUT_DIR"), "/built.rs"));

pub struct Info {
    version: &'static str,
    commit_hash: Option<&'static str>,
    target: &'static str,
    profile: &'static str,
    rustc_version: &'static str,
}

impl Info {
    pub fn new() -> Self {
        Self {
            version: PKG_VERSION,
            commit_hash: GIT_COMMIT_HASH,
            target: TARGET,
            profile: PROFILE,
            rustc_version: RUSTC_VERSION,
        }
    }

    pub fn log() {
        let info = Info::new();
        info!(
            "Running feldspat version {} ({})",
            info.version,
            info.commit_hash.unwrap()
        );
        info!("Target OS: {}", info.target);
        info!("Profile: {}", info.profile);
        info!("Compiled with {}", info.rustc_version)
    }
}
