use std::error::Error;
use std::sync::Arc;

use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};

use crate::asset::Assets;
use crate::engine::info::Info;
use crate::engine::screen::{Screen, Screens};
use crate::engine::splash::Splash;
use crate::engine::{Config, Time};
use crate::graphics::GraphicsContext;
use crate::input::Input;
use crate::rendering::Renderer;

/// The engine.
pub struct Engine {
    screens: Screens,
    input: Arc<Input>,
    renderer: Arc<Renderer>,
    assets: Arc<Assets>,
    graphics_context: Arc<GraphicsContext>,
    time: Arc<Time>,
    config: Arc<Config>,
}

impl Engine {
    /// Starts building a new engine.
    pub fn builder() -> EngineBuilder {
        EngineBuilder {
            screen_constructors: vec![],
        }
    }

    /// Returns the config.
    pub fn config(&self) -> &Arc<Config> {
        &self.config
    }

    /// Returns the time.
    pub fn time(&self) -> &Arc<Time> {
        &self.time
    }

    /// Returns the graphics context.
    pub fn graphics_context(&self) -> &Arc<GraphicsContext> {
        &self.graphics_context
    }

    /// Returns the asset manager.
    pub fn assets(&self) -> &Arc<Assets> {
        &self.assets
    }

    /// Returns the renderer.
    pub fn renderer(&self) -> &Arc<Renderer> {
        &self.renderer
    }

    /// Returns the input manager.
    pub fn input(&self) -> &Arc<Input> {
        &self.input
    }

    /// Updates the engine.
    fn update(&self) -> Result<(), Box<dyn std::error::Error>> {
        self.screens.update(&self)?;
        self.screens.draw(&self)?;
        self.screens.commit(&self)?;
        self.input.reset();
        unsafe {
            self.graphics_context.swapchain().handle_recreate()?;
        }
        self.time.update();
        Ok(())
    }
}

/// Engine builder.
pub struct EngineBuilder {
    screen_constructors: Vec<Box<dyn Fn(&Engine) -> Arc<dyn Screen>>>,
}

impl EngineBuilder {
    /// Adds a screen to the engine.
    pub fn screen<C>(mut self, screen_constructor: C) -> Self
    where
        C: Fn(&Engine) -> Arc<dyn Screen> + 'static,
    {
        self.screen_constructors.push(Box::new(screen_constructor));
        self
    }

    /// Builds the engine and executes the main loop.
    pub fn run(self) -> Result<(), Box<dyn Error + 'static>> {
        Splash::log();
        Info::log();

        let config = Arc::new(Config::default());
        let event_loop = EventLoop::new();
        let graphics_context = GraphicsContext::new(&event_loop, &config)?;
        let mut engine = Engine {
            screens: Screens::new([]),
            input: Input::new(),
            assets: Assets::new(
                graphics_context.device().clone(),
                config.system.assets.clone(),
            ),
            renderer: Renderer::new(config.clone(), graphics_context.clone())?,
            graphics_context,
            time: Time::new(),
            config: config.clone(),
        };

        for screen_constructor in &self.screen_constructors {
            engine.screens.add(screen_constructor(&engine));
        }

        event_loop.run(move |event, _, flow| {
            *flow = ControlFlow::Poll;

            match event {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => {
                    *flow = ControlFlow::Exit;
                }
                Event::WindowEvent {
                    event: WindowEvent::Resized(_),
                    ..
                } => unsafe {
                    engine.graphics_context().swapchain().recreate();
                },
                Event::WindowEvent { event, .. } => {
                    engine.input.handle_window_event(&event);
                }
                Event::MainEventsCleared => {
                    if let Err(e) = engine.update() {
                        log::error!("{}", e);
                        *flow = ControlFlow::Exit;
                    }
                }
                Event::LoopDestroyed => {
                    crate::asset::ASSETS.lock().unwrap().take();
                    engine.graphics_context().device().wait_idle().unwrap();
                    unsafe {
                        engine.graphics_context().clean_up();
                    }
                }
                _ => {}
            }
        });
    }
}
