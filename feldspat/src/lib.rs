#![warn(missing_docs)]
#![warn(missing_doc_code_examples)]
#![warn(broken_intra_doc_links)]

//! Multiplatform game engine.

#[cfg(test)]
#[macro_use]
extern crate approx;
#[macro_use]
extern crate lalrpop_util;
extern crate self as feldspat;

pub use feldspat_derive::Component;

pub mod asset;
pub mod ecs;
pub mod engine;
pub mod graphics;
pub mod input;
pub mod math;
pub mod rendering;
