//! Types for loading shader modules.

use crate::graphics::device::Device;
use crate::graphics::VulkanError;
use ash::vk;
use std::ffi::{CStr, CString};
use std::fmt::{Display, Formatter};
use std::sync::Arc;

/// A shader module.
pub struct ShaderModule {
    main_entrypoint: CString,
    stage: vk::ShaderStageFlags,
    inner: vk::ShaderModule,
    device: Arc<Device>,
}

impl ShaderModule {
    /// Creates a new shader module from the compiled SPIR-V `code` with the `main` entrypoint for
    /// a specific shader `stage`.
    pub fn new(
        device: &Arc<Device>,
        code: &[u32],
        stage: vk::ShaderStageFlags,
        main_entrypoint: &str,
    ) -> Result<Arc<Self>, ShaderModuleCreationError> {
        let shader_module_create_info = vk::ShaderModuleCreateInfo::builder().code(code).build();
        let inner = unsafe { device.create_shader_module(&shader_module_create_info, None) }?;
        Ok(Arc::new(Self {
            main_entrypoint: CString::new(main_entrypoint).unwrap(),
            stage,
            inner,
            device: device.clone(),
        }))
    }

    /// Returns the name of the main entry point.
    pub fn main_entrypoint(&self) -> &CStr {
        &self.main_entrypoint
    }

    /// Returns the shader stage.
    pub fn stage(&self) -> vk::ShaderStageFlags {
        self.stage
    }

    /// Returns the vulkan shader module.
    pub fn inner(&self) -> vk::ShaderModule {
        self.inner
    }
}

impl Drop for ShaderModule {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_shader_module(self.inner, None);
        }
    }
}

/// Error that can occur when creating a device.
#[derive(Debug)]
pub enum ShaderModuleCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// One or more shaders failed to compile or link.
    InvalidShader(VulkanError),
}

impl Display for ShaderModuleCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            ShaderModuleCreationError::OutOfHostMemory(e)
            | ShaderModuleCreationError::OutOfDeviceMemory(e)
            | ShaderModuleCreationError::InvalidShader(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for ShaderModuleCreationError {}

impl From<VulkanError> for ShaderModuleCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => ShaderModuleCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => ShaderModuleCreationError::OutOfDeviceMemory(e),
            VulkanError::InvalidShader => ShaderModuleCreationError::InvalidShader(e),
            _ => unreachable!("Unknown create shader module error"),
        }
    }
}

impl From<vk::Result> for ShaderModuleCreationError {
    fn from(result: vk::Result) -> Self {
        ShaderModuleCreationError::from(VulkanError::from(result))
    }
}
