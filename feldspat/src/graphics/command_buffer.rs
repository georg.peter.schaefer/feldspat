//! Utilities for recording command buffers.

use std::collections::HashMap;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::sync::{Arc, Mutex, Weak};

use ash::vk;

use crate::graphics::buffer::Buffer;
use crate::graphics::image::Image;
use crate::graphics::pipeline::{DescriptorSet, PipelineLayout};
use crate::graphics::{Device, Framebuffer, GraphicsPipeline, RenderPass, VulkanError};

/// A recorded command buffer.
pub struct CommandBuffer {
    copy_buffer_resources: Vec<CopyBufferResources>,
    copy_buffer_image_resources: Vec<CopyBufferImageResources>,
    descriptor_sets: Vec<Arc<DescriptorSet>>,
    secondary_command_buffers: Vec<Arc<CommandBuffer>>,
    inner: vk::CommandBuffer,
    command_pool: Arc<CommandPool>,
    device: Arc<Device>,
}

impl CommandBuffer {
    /// Starts recording a primary command buffer.
    pub fn primary(
        device: Arc<Device>,
        usage: vk::CommandBufferUsageFlags,
        queue_family: u32,
    ) -> Result<CommandBufferBuilder, CommandBufferCreationError> {
        CommandBufferBuilder::new(device, queue_family, vk::CommandBufferLevel::PRIMARY, usage)
    }

    /// Starts recording a secondary command buffer.
    pub fn secondary(
        device: Arc<Device>,
        usage: vk::CommandBufferUsageFlags,
        queue_family: u32,
        render_pass: Arc<RenderPass>,
        subpass: u32,
        framebuffer: Arc<Framebuffer>,
    ) -> Result<CommandBufferBuilder, CommandBufferCreationError> {
        CommandBufferBuilder::new_secondary(
            device,
            queue_family,
            vk::CommandBufferLevel::SECONDARY,
            usage | vk::CommandBufferUsageFlags::RENDER_PASS_CONTINUE,
            render_pass,
            subpass,
            framebuffer,
        )
    }

    /// Returns the vulkan command buffer.
    pub fn inner(&self) -> vk::CommandBuffer {
        self.inner
    }

    /// Returns the device.
    pub fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

impl Drop for CommandBuffer {
    #[allow(unused_must_use)]
    fn drop(&mut self) {
        let command_pool = self.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.device
                .reset_command_buffer(self.inner, vk::CommandBufferResetFlags::empty());
        }
    }
}

/// Builder for recording a command buffer.
pub struct CommandBufferBuilder {
    command_buffer: CommandBuffer,
}

impl CommandBufferBuilder {
    fn new(
        device: Arc<Device>,
        queue_family: u32,
        level: vk::CommandBufferLevel,
        usage: vk::CommandBufferUsageFlags,
    ) -> Result<CommandBufferBuilder, CommandBufferCreationError> {
        let command_pool = device.command_pool(queue_family)?;
        let inner_command_pool = command_pool.inner().lock().unwrap();
        let allocate_info = vk::CommandBufferAllocateInfo::builder()
            .command_pool(*inner_command_pool)
            .level(level)
            .command_buffer_count(1);
        let inner = unsafe { device.allocate_command_buffers(&allocate_info) }?
            .first()
            .cloned()
            .unwrap();
        let begin_info = vk::CommandBufferBeginInfo::builder().flags(usage);
        unsafe { device.begin_command_buffer(inner, &begin_info) }?;

        Ok(Self {
            command_buffer: CommandBuffer {
                copy_buffer_resources: vec![],
                copy_buffer_image_resources: vec![],
                descriptor_sets: vec![],
                secondary_command_buffers: vec![],
                inner,
                command_pool: command_pool.clone(),
                device: device.clone(),
            },
        })
    }

    fn new_secondary(
        device: Arc<Device>,
        queue_family: u32,
        level: vk::CommandBufferLevel,
        usage: vk::CommandBufferUsageFlags,
        render_pass: Arc<RenderPass>,
        subpass: u32,
        framebuffer: Arc<Framebuffer>,
    ) -> Result<CommandBufferBuilder, CommandBufferCreationError> {
        let command_pool = device.command_pool(queue_family)?;
        let inner_command_pool = command_pool.inner().lock().unwrap();
        let allocate_info = vk::CommandBufferAllocateInfo::builder()
            .command_pool(*inner_command_pool)
            .level(level)
            .command_buffer_count(1);
        let inner = unsafe { device.allocate_command_buffers(&allocate_info) }?
            .first()
            .cloned()
            .unwrap();
        let inheritance_info = vk::CommandBufferInheritanceInfo::builder()
            .render_pass(render_pass.inner())
            .subpass(subpass)
            .framebuffer(framebuffer.inner());
        let begin_info = vk::CommandBufferBeginInfo::builder()
            .flags(usage)
            .inheritance_info(&inheritance_info);
        unsafe { device.begin_command_buffer(inner, &begin_info) }?;

        Ok(Self {
            command_buffer: CommandBuffer {
                copy_buffer_resources: vec![],
                copy_buffer_image_resources: vec![],
                descriptor_sets: vec![],
                secondary_command_buffers: vec![],
                inner,
                command_pool: command_pool.clone(),
                device: device.clone(),
            },
        })
    }

    /// Begins a render pass.
    pub fn begin_render_pass(
        &mut self,
        framebuffer: Arc<Framebuffer>,
        subpass_contents: vk::SubpassContents,
        clear_values: Vec<vk::ClearValue>,
    ) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        let render_pass_info = vk::RenderPassBeginInfo::builder()
            .render_pass(framebuffer.render_pass().inner())
            .framebuffer(framebuffer.inner())
            .render_area(
                vk::Rect2D::builder()
                    .offset(vk::Offset2D::default())
                    .extent(framebuffer.extent())
                    .build(),
            )
            .clear_values(&clear_values);
        unsafe {
            self.command_buffer.device.cmd_begin_render_pass(
                self.command_buffer.inner(),
                &render_pass_info,
                subpass_contents,
            )
        };
        self
    }

    /// Binds a descriptor set.
    pub fn bind_descriptor_set(
        &mut self,
        pipeline_layout: &PipelineLayout,
        descriptor_set: Arc<DescriptorSet>,
    ) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_bind_descriptor_sets(
                self.command_buffer.inner(),
                vk::PipelineBindPoint::GRAPHICS,
                pipeline_layout.inner(),
                0,
                &[descriptor_set.inner()],
                &[],
            );
        }
        &mut self.command_buffer.descriptor_sets.push(descriptor_set);
        self
    }

    /// Binds a graphics pipeline.
    pub fn bind_pipeline(&mut self, pipeline: Arc<GraphicsPipeline>) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_bind_pipeline(
                self.command_buffer.inner,
                vk::PipelineBindPoint::GRAPHICS,
                pipeline.inner(),
            )
        };
        self
    }

    /// Binds an index buffer.
    pub fn bind_index_buffer<T>(&mut self, buffer: T, index_type: vk::IndexType) -> &mut Self
    where
        T: Buffer,
    {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_bind_index_buffer(
                self.command_buffer.inner(),
                buffer.inner(),
                0,
                index_type,
            );
        }
        self
    }

    /// Binds a vertex buffer.
    pub fn bind_vertex_buffer<T>(&mut self, buffer: T) -> &mut Self
    where
        T: Buffer,
    {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        let buffers = [buffer.inner()];
        let offsets = [0];
        unsafe {
            self.command_buffer.device.cmd_bind_vertex_buffers(
                self.command_buffer.inner,
                0,
                &buffers,
                &offsets,
            )
        };
        self
    }

    /// Blits an image from `src` to `dst`.
    pub fn blit_image<A, B>(
        &mut self,
        src: A,
        src_layout: vk::ImageLayout,
        dst: B,
        dst_layout: vk::ImageLayout,
        regions: Vec<vk::ImageBlit>,
        filter: vk::Filter,
    ) -> &mut Self
    where
        A: Image,
        B: Image,
    {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_blit_image(
                self.command_buffer.inner(),
                src.inner(),
                src_layout,
                dst.inner(),
                dst_layout,
                &regions,
                filter,
            );
        }
        self
    }

    /// Clear regions of a color image.
    pub fn clear_color_image<I>(
        &mut self,
        image: I,
        layout: vk::ImageLayout,
        clear_color: vk::ClearColorValue,
        ranges: Vec<vk::ImageSubresourceRange>,
    ) -> &mut Self
    where
        I: Image,
    {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_clear_color_image(
                self.command_buffer.inner(),
                image.inner(),
                layout,
                &clear_color,
                &ranges,
            );
        }
        self
    }

    /// Copies a buffer from `src` to `dst`.
    pub fn copy_buffer<A, B>(&mut self, src: A, dst: B, region: vk::BufferCopy) -> &mut Self
    where
        A: Buffer + 'static,
        B: Buffer + 'static,
    {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_copy_buffer(
                self.command_buffer.inner(),
                src.inner(),
                dst.inner(),
                &[region],
            );
        };
        self.command_buffer
            .copy_buffer_resources
            .push(CopyBufferResources {
                src: Box::new(src),
                dst: Box::new(dst),
            });
        self
    }

    /// Copies a buffer to an image.
    pub fn copy_buffer_to_image<A, B>(
        &mut self,
        src: A,
        dst: B,
        dst_image_layout: vk::ImageLayout,
        region: vk::BufferImageCopy,
    ) -> &mut Self
    where
        A: Buffer + 'static,
        B: Image + 'static,
    {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_copy_buffer_to_image(
                self.command_buffer.inner(),
                src.inner(),
                dst.inner(),
                dst_image_layout,
                &[region],
            );
        }
        self.command_buffer
            .copy_buffer_image_resources
            .push(CopyBufferImageResources {
                src: Box::new(src),
                dst: Box::new(dst),
            });
        self
    }

    /// Draws.
    pub fn draw(
        &mut self,
        vertex_count: u32,
        instance_count: u32,
        first_vertex: u32,
        first_instance: u32,
    ) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_draw(
                self.command_buffer.inner(),
                vertex_count,
                instance_count,
                first_vertex,
                first_instance,
            )
        };
        self
    }

    /// Draws indexed.
    pub fn draw_indexed(
        &mut self,
        index_count: u32,
        instance_count: u32,
        first_index: u32,
        vertex_offset: i32,
        first_instance: u32,
    ) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_draw_indexed(
                self.command_buffer.inner(),
                index_count,
                instance_count,
                first_index,
                vertex_offset,
                first_instance,
            )
        }
        self
    }

    /// DRaw with indirect parameters.
    pub fn draw_indirect<T>(
        &mut self,
        buffer: T,
        offset: vk::DeviceSize,
        draw_count: u32,
        stride: u32,
    ) -> &mut Self
    where
        T: Buffer,
    {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_draw_indirect(
                self.command_buffer.inner(),
                buffer.inner(),
                offset,
                draw_count,
                stride,
            );
        }
        self
    }

    /// Ends the current render pass.
    pub fn end_render_pass(&mut self) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer
                .device
                .cmd_end_render_pass(self.command_buffer.inner())
        };
        self
    }

    /// Executes a secondary command buffer.
    pub fn execute_commands(&mut self, secondary_command_buffer: Arc<CommandBuffer>) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        self.command_buffer
            .secondary_command_buffers
            .push(secondary_command_buffer.clone());
        unsafe {
            self.command_buffer.device.cmd_execute_commands(
                self.command_buffer.inner(),
                &[secondary_command_buffer.inner()],
            )
        }
        self
    }

    /// Executes the next subpass.
    pub fn next_subpass(&mut self, subpass_contents: vk::SubpassContents) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer
                .device
                .cmd_next_subpass(self.command_buffer.inner(), subpass_contents)
        };
        self
    }

    /// Adds a pipeline barrier.
    pub fn pipeline_barrier(
        &mut self,
        src_stage_mask: vk::PipelineStageFlags,
        dst_stage_mask: vk::PipelineStageFlags,
        dependency_flags: vk::DependencyFlags,
        memory_barriers: Vec<vk::MemoryBarrier>,
        buffer_memory_barriers: Vec<vk::BufferMemoryBarrier>,
        image_memory_barriers: Vec<vk::ImageMemoryBarrier>,
    ) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer.device.cmd_pipeline_barrier(
                self.command_buffer.inner(),
                src_stage_mask,
                dst_stage_mask,
                dependency_flags,
                &memory_barriers,
                &buffer_memory_barriers,
                &image_memory_barriers,
            )
        };
        self
    }

    /// Sets the push constants for a shader stage.
    pub fn push_constants<T>(
        &mut self,
        pipeline_layout: &PipelineLayout,
        stage: vk::ShaderStageFlags,
        push_constants: T,
    ) -> &mut Self {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            let data = std::slice::from_raw_parts(
                ((&push_constants) as *const T).cast(),
                std::mem::size_of::<T>(),
            );
            self.command_buffer.device.cmd_push_constants(
                self.command_buffer.inner(),
                pipeline_layout.inner(),
                stage,
                0,
                data,
            );
        }
        self
    }

    /// Transitions an image layout from `old_layout` to `new_layout`.
    pub fn deprected_transition_image_layout<T>(
        &mut self,
        image: T,
        old_layout: vk::ImageLayout,
        new_layout: vk::ImageLayout,
    ) -> &mut Self
    where
        T: Image,
    {
        let (src_access_mask, dst_access_mask, src_stage_mask, dst_stage_mask) =
            match (old_layout, new_layout) {
                (vk::ImageLayout::UNDEFINED, vk::ImageLayout::TRANSFER_DST_OPTIMAL) => (
                    vk::AccessFlags::empty(),
                    vk::AccessFlags::TRANSFER_WRITE,
                    vk::PipelineStageFlags::TOP_OF_PIPE,
                    vk::PipelineStageFlags::TRANSFER,
                ),
                (
                    vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                    vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                ) => (
                    vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
                    vk::AccessFlags::SHADER_READ,
                    vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                    vk::PipelineStageFlags::FRAGMENT_SHADER,
                ),
                (
                    vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                    vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                ) => (
                    vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
                    vk::AccessFlags::TRANSFER_READ,
                    vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                    vk::PipelineStageFlags::TRANSFER,
                ),
                (
                    vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                    vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                ) => (
                    vk::AccessFlags::TRANSFER_WRITE,
                    vk::AccessFlags::SHADER_READ,
                    vk::PipelineStageFlags::TRANSFER,
                    vk::PipelineStageFlags::FRAGMENT_SHADER,
                ),
                (vk::ImageLayout::TRANSFER_DST_OPTIMAL, vk::ImageLayout::PRESENT_SRC_KHR) => (
                    vk::AccessFlags::TRANSFER_WRITE,
                    vk::AccessFlags::MEMORY_READ,
                    vk::PipelineStageFlags::TRANSFER,
                    vk::PipelineStageFlags::TRANSFER,
                ),
                _ => panic!("Unsupported layout transition"),
            };
        let subresource_range = vk::ImageSubresourceRange::builder()
            .aspect_mask(vk::ImageAspectFlags::COLOR)
            .base_mip_level(0)
            .level_count(image.mip_levels())
            .base_array_layer(0)
            .layer_count(1)
            .build();
        let barrier = vk::ImageMemoryBarrier::builder()
            .old_layout(old_layout)
            .new_layout(new_layout)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .image(image.inner())
            .subresource_range(subresource_range)
            .src_access_mask(src_access_mask)
            .dst_access_mask(dst_access_mask)
            .build();
        self.pipeline_barrier(
            src_stage_mask,
            dst_stage_mask,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![barrier],
        )
    }

    /// Transitions an image layout from `old_layout` to `new_layout`.
    pub fn transition_image_layout<T>(
        &mut self,
        image: T,
        old_layout: vk::ImageLayout,
        new_layout: vk::ImageLayout,
        src_access_mask: vk::AccessFlags,
        dst_access_mask: vk::AccessFlags,
        src_stage_mask: vk::PipelineStageFlags,
        dst_stage_mask: vk::PipelineStageFlags,
    ) -> &mut Self
    where
        T: Image,
    {
        let subresource_range = vk::ImageSubresourceRange::builder()
            .aspect_mask(vk::ImageAspectFlags::COLOR)
            .base_mip_level(0)
            .level_count(image.mip_levels())
            .base_array_layer(0)
            .layer_count(1)
            .build();
        let barrier = vk::ImageMemoryBarrier::builder()
            .old_layout(old_layout)
            .new_layout(new_layout)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .image(image.inner())
            .subresource_range(subresource_range)
            .src_access_mask(src_access_mask)
            .dst_access_mask(dst_access_mask)
            .build();
        self.pipeline_barrier(
            src_stage_mask,
            dst_stage_mask,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![barrier],
        )
    }

    /// Finishes command buffer recording.
    pub fn build(self) -> Result<Arc<CommandBuffer>, CommandBufferCreationError> {
        let command_pool = self.command_buffer.command_pool.clone();
        let _guard = command_pool.inner.lock().unwrap();
        unsafe {
            self.command_buffer
                .device
                .end_command_buffer(self.command_buffer.inner())
        }?;
        Ok(Arc::new(self.command_buffer))
    }
}

struct CopyBufferResources {
    src: Box<dyn Buffer>,
    dst: Box<dyn Buffer>,
}

unsafe impl Send for CopyBufferResources {}

unsafe impl Sync for CopyBufferResources {}

struct CopyBufferImageResources {
    src: Box<dyn Buffer>,
    dst: Box<dyn Image>,
}

unsafe impl Send for CopyBufferImageResources {}

unsafe impl Sync for CopyBufferImageResources {}

/// Error that can occur during command buffer creation.
#[derive(Debug)]
pub enum CommandBufferCreationError {
    /// Command pool creation has failed.
    CommandPoolCreationFailed(CommandPoolCreationError),
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for CommandBufferCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            CommandBufferCreationError::CommandPoolCreationFailed(e) => Display::fmt(e, f),
            CommandBufferCreationError::OutOfHostMemory(e)
            | CommandBufferCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for CommandBufferCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            CommandBufferCreationError::CommandPoolCreationFailed(e) => Some(e),
            _ => None,
        }
    }
}

impl From<CommandPoolCreationError> for CommandBufferCreationError {
    fn from(e: CommandPoolCreationError) -> Self {
        CommandBufferCreationError::CommandPoolCreationFailed(e)
    }
}

impl From<VulkanError> for CommandBufferCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => CommandBufferCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => CommandBufferCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown command buffer creation error"),
        }
    }
}

impl From<vk::Result> for CommandBufferCreationError {
    fn from(result: vk::Result) -> Self {
        CommandBufferCreationError::from(VulkanError::from(result))
    }
}

/// Per thread command pool.
#[derive(Debug)]
pub struct PerThreadCommandPool {
    per_thread: Mutex<HashMap<std::thread::ThreadId, Weak<CommandPool>>>,
    queue_family: u32,
}

impl PerThreadCommandPool {
    pub(crate) fn new(queue_family: u32) -> Self {
        Self {
            per_thread: Default::default(),
            queue_family,
        }
    }

    pub(crate) fn get(
        &self,
        device: Arc<Device>,
    ) -> Result<Arc<CommandPool>, CommandPoolCreationError> {
        let mut per_thread = self.per_thread.lock().unwrap();
        let current_thread = std::thread::current().id();
        let command_pool =
            if let Some(command_pool) = per_thread.get(&current_thread).and_then(Weak::upgrade) {
                command_pool
            } else {
                let command_pool = CommandPool::new(device, self.queue_family)?;
                per_thread.insert(current_thread, Arc::downgrade(&command_pool));
                command_pool
            };
        Ok(command_pool)
    }
}

/// A single command pool.
#[derive(Debug)]
pub struct CommandPool {
    inner: Mutex<vk::CommandPool>,
    device: Arc<Device>,
}

impl CommandPool {
    fn new(
        device: Arc<Device>,
        queue_family: u32,
    ) -> Result<Arc<CommandPool>, CommandPoolCreationError> {
        let create_info = vk::CommandPoolCreateInfo::builder()
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .queue_family_index(queue_family);
        let inner = Mutex::new(unsafe { device.create_command_pool(&create_info, None) }?);
        Ok(Arc::new(Self { inner, device }))
    }

    /// Returns the vulkan command pool
    pub fn inner(&self) -> &Mutex<vk::CommandPool> {
        &self.inner
    }
}

impl Drop for CommandPool {
    fn drop(&mut self) {
        unsafe {
            self.device
                .destroy_command_pool(*self.inner.lock().unwrap(), None);
        }
    }
}

/// Error that can occur during command pool creation.
#[derive(Debug)]
pub enum CommandPoolCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for CommandPoolCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            CommandPoolCreationError::OutOfHostMemory(e)
            | CommandPoolCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for CommandPoolCreationError {}

impl From<VulkanError> for CommandPoolCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => CommandPoolCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => CommandPoolCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown command pool creation error"),
        }
    }
}

impl From<vk::Result> for CommandPoolCreationError {
    fn from(result: vk::Result) -> Self {
        CommandPoolCreationError::from(VulkanError::from(result))
    }
}
