//! Types for window creation.

use std::error::Error;
use std::fmt::{Display, Formatter};
use std::sync::Arc;

use ash::vk::{PresentModeKHR, SurfaceCapabilitiesKHR, SurfaceFormatKHR, SurfaceKHR};
use ash::{vk, Entry};
use winit::dpi::{PhysicalPosition, PhysicalSize};
use winit::error::OsError;
use winit::event_loop::EventLoop;
use winit::window::{Window as InnerWindow, WindowBuilder};

use crate::engine::Config;
use crate::graphics::context::Instance;
use crate::graphics::device::PhysicalDevice;
use crate::graphics::VulkanError;

/// Represents a window.
pub struct Window {
    surface: Surface,
    inner: InnerWindow,
}

impl Window {
    pub(crate) fn from_config(
        event_loop: &EventLoop<()>,
        entry: &Entry,
        instance: &Instance,
        config: &Arc<Config>,
    ) -> Result<Arc<Self>, WindowCreationError> {
        let inner = WindowBuilder::new()
            .with_title(config.system.title.clone())
            .with_inner_size(PhysicalSize::new(
                config.video.resolution.width,
                config.video.resolution.height,
            ))
            .build(event_loop)
            .unwrap();
        let surface = Surface::new(entry, instance, &inner)?;

        Ok(Arc::new(Self { inner, surface }))
    }

    /// Returns the resolution of the window.
    pub fn resolution(&self) -> [u32; 2] {
        self.inner.inner_size().into()
    }

    pub(super) fn surface(&self) -> &Surface {
        &self.surface
    }

    /// Sets the cursor grab state.
    pub fn set_cursor_grab(&self, grab: bool) {
        self.inner.set_cursor_grab(grab).unwrap();
        self.inner.set_cursor_visible(!grab);
    }

    /// Centers the cursor inside the window.
    pub fn center_cursor_position(&self) {
        let resolution = self.resolution();

        self.inner
            .set_cursor_position(PhysicalPosition::new(resolution[0] / 2, resolution[1] / 2))
            .unwrap();
    }
}

/// Error that can occur during window creation.
#[derive(Debug)]
pub enum WindowCreationError {
    /// Window creation has failed.
    WindowCreationFailed(OsError),
    /// Surface creation has failed.
    SurfaceCreationFailed(SurfaceCreationError),
}

impl Display for WindowCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            WindowCreationError::WindowCreationFailed(e) => Display::fmt(e, f),
            WindowCreationError::SurfaceCreationFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for WindowCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            WindowCreationError::WindowCreationFailed(e) => Some(e),
            WindowCreationError::SurfaceCreationFailed(e) => Some(e),
        }
    }
}

impl From<OsError> for WindowCreationError {
    fn from(e: OsError) -> Self {
        WindowCreationError::WindowCreationFailed(e)
    }
}

impl From<SurfaceCreationError> for WindowCreationError {
    fn from(e: SurfaceCreationError) -> Self {
        WindowCreationError::SurfaceCreationFailed(e)
    }
}

pub(super) struct Surface {
    inner: SurfaceKHR,
    loader: ash::extensions::khr::Surface,
}

impl Surface {
    fn new(
        entry: &Entry,
        instance: &Instance,
        window: &InnerWindow,
    ) -> Result<Self, SurfaceCreationError> {
        Ok(Self {
            loader: ash::extensions::khr::Surface::new(entry, instance),
            inner: Self::create_surface(entry, instance, window)?,
        })
    }

    #[cfg(target_os = "linux")]
    fn create_surface(
        entry: &Entry,
        instance: &Instance,
        window: &InnerWindow,
    ) -> Result<SurfaceKHR, SurfaceCreationError> {
        use ash::extensions::khr::XlibSurface;
        use winit::platform::unix::WindowExtUnix;

        let xlib_display = window.xlib_display().unwrap();
        let xlib_window = window.xlib_window().unwrap();
        let xlib_surface_create_info = vk::XlibSurfaceCreateInfoKHR::builder()
            .window(xlib_window as vk::Window)
            .dpy(xlib_display as *mut vk::Display)
            .build();
        let xlib_surface_loader = XlibSurface::new(entry, instance);
        Ok(unsafe { xlib_surface_loader.create_xlib_surface(&xlib_surface_create_info, None) }?)
    }

    #[cfg(target_os = "windows")]
    fn create_surface(
        entry: &Entry,
        instance: &Instance,
        window: &InnerWindow,
    ) -> Result<SurfaceKHR, SurfaceCreationError> {
        use ash::extensions::khr::Win32Surface;
        use std::os::raw::c_void;
        use std::ptr;
        use winapi::shared::windef::HWND;
        use winapi::um::libloaderapi::GetModuleHandleW;
        use winit::platform::windows::WindowExtWindows;

        let hwnd = window.hwnd() as HWND;
        let hinstance = unsafe { GetModuleHandleW(ptr::null()) } as *const c_void;
        let win32_create_info = vk::Win32SurfaceCreateInfoKHR {
            s_type: vk::StructureType::WIN32_SURFACE_CREATE_INFO_KHR,
            p_next: ptr::null(),
            flags: Default::default(),
            hinstance,
            hwnd: hwnd as *const c_void,
        };
        let win32_surface_loader = Win32Surface::new(entry, instance);
        Ok(unsafe { win32_surface_loader.create_win32_surface(&win32_create_info, None) }?)
    }

    pub(super) fn is_supported(
        &self,
        physical_device: vk::PhysicalDevice,
        queue_family_index: usize,
    ) -> bool {
        unsafe {
            self.loader.get_physical_device_surface_support(
                physical_device,
                queue_family_index as u32,
                self.inner,
            )
        }
        .expect("Failed to get physical device surface support")
    }

    pub(super) fn capabilities(&self, physical_device: &PhysicalDevice) -> SurfaceCapabilitiesKHR {
        unsafe {
            self.loader
                .get_physical_device_surface_capabilities(physical_device.inner(), self.inner)
        }
        .expect("Failed to get physical device surface capabilities")
    }

    pub(super) fn formats(&self, physical_device: &PhysicalDevice) -> Vec<SurfaceFormatKHR> {
        unsafe {
            self.loader
                .get_physical_device_surface_formats(physical_device.inner(), self.inner)
        }
        .expect("Failed to get physical device surface formats")
    }

    pub(super) fn present_modes(&self, physical_device: &PhysicalDevice) -> Vec<PresentModeKHR> {
        unsafe {
            self.loader
                .get_physical_device_surface_present_modes(physical_device.inner(), self.inner)
        }
        .expect("Failed to get physical device present modes")
    }

    pub(super) fn inner(&self) -> SurfaceKHR {
        self.inner
    }
}

impl Drop for Surface {
    fn drop(&mut self) {
        unsafe { self.loader.destroy_surface(self.inner, None) }
    }
}

/// Error that can occur during surface creation.
#[derive(Debug)]
pub enum SurfaceCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for SurfaceCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            SurfaceCreationError::OutOfHostMemory(e)
            | SurfaceCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for SurfaceCreationError {}

impl From<VulkanError> for SurfaceCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => SurfaceCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => SurfaceCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown create VkSurfaceKHR error"),
        }
    }
}

impl From<vk::Result> for SurfaceCreationError {
    fn from(result: vk::Result) -> Self {
        SurfaceCreationError::from(VulkanError::from(result))
    }
}
