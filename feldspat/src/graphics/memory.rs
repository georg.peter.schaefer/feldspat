//! Allocator and memory management.

use std::fmt::{Debug, Display, Formatter};
use std::sync::{Arc, Mutex};

use ash::vk;
use num::Integer;

use crate::graphics::buffer::Buffer;
use crate::graphics::context::Instance;
use crate::graphics::device::PhysicalDevice;
use crate::graphics::image::Image;
use crate::graphics::{Device, VulkanError};

/// Standard allocator.
pub struct Allocator {
    memory_types: Mutex<Vec<MemoryType>>,
}

impl Allocator {
    pub(crate) fn new(instance: &Instance, physical_device: &PhysicalDevice) -> Arc<Self> {
        let device_memory_properties =
            unsafe { instance.get_physical_device_memory_properties(physical_device.inner()) };
        let memory_types = device_memory_properties.memory_types
            [0..device_memory_properties.memory_type_count as usize]
            .iter()
            .enumerate()
            .map(|(type_index, memory_type)| {
                MemoryType::new(type_index as u32, memory_type.property_flags)
            })
            .collect();
        Arc::new(Self {
            memory_types: Mutex::new(memory_types),
        })
    }

    /// Allocates memory.
    pub fn allocate(
        &self,
        device: Arc<Device>,
        memory_requirements: vk::MemoryRequirements,
        properties: vk::MemoryPropertyFlags,
    ) -> Result<Memory, AllocationError> {
        let mut memory_types = self.memory_types.lock().unwrap();
        let type_index = Self::find_memory_type(
            &memory_types,
            memory_requirements.memory_type_bits,
            properties,
        )?;
        let memory_type = &mut memory_types[type_index as usize];
        let block = memory_type.find_or_create_block(device, memory_requirements)?;
        let memory = block.lock().unwrap().sub_allocate(
            block.clone(),
            memory_requirements.size,
            memory_requirements.alignment,
        );
        Ok(memory)
    }

    fn find_memory_type(
        memory_types: &Vec<MemoryType>,
        filter: u32,
        properties: vk::MemoryPropertyFlags,
    ) -> Result<u32, AllocationError> {
        memory_types
            .iter()
            .enumerate()
            .find(|(type_index, memory_type)| {
                (filter & (1 << type_index) > 0) && memory_type.properties.contains(properties)
            })
            .map(|(type_index, _)| type_index as u32)
            .ok_or(AllocationError::NoSuitableMemoryType)
    }

    pub(crate) fn clean_up(&self, device: &Device) {
        let memory_types = self.memory_types.lock().unwrap();
        for memory_type in memory_types.iter() {
            for block in memory_type.blocks.iter() {
                let block = block.lock().unwrap();
                unsafe {
                    device.free_memory(block.inner, None);
                }
            }
        }
    }
}

impl Debug for Allocator {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Allocator")
    }
}

/// A bindable memory range.
pub struct Memory {
    range: Range,
    inner: vk::DeviceMemory,
    block: Arc<Mutex<MemoryBlock>>,
}

impl Memory {
    fn new(block: Arc<Mutex<MemoryBlock>>, inner: vk::DeviceMemory, range: Range) -> Self {
        Self {
            range,
            inner,
            block,
        }
    }

    /// Binds a buffer to the memory.
    pub fn bind_buffer<T>(&self, device: &Device, buffer: &T) -> Result<(), BindMemoryError>
    where
        T: Buffer,
    {
        unsafe { device.bind_buffer_memory(buffer.inner(), self.inner, self.range.offset) }?;
        Ok(())
    }

    /// Binds an image to the memory.
    pub fn bind_image<T>(&self, device: &Device, image: &T) -> Result<(), BindMemoryError>
    where
        T: Image,
    {
        unsafe { device.bind_image_memory(image.inner(), self.inner, self.range.offset) }?;
        Ok(())
    }

    /// Writes some data to the memory.
    pub fn write<I>(&self, device: &Device, data: I) -> Result<(), WriteMemoryError>
    where
        I: IntoIterator,
        I::Item: Sized,
    {
        let data = data.into_iter().collect::<Vec<_>>();
        let block = self.block.lock().unwrap();
        unsafe {
            let mapped_memory = device.map_memory(
                block.inner,
                self.range.offset,
                self.range.size,
                vk::MemoryMapFlags::empty(),
            )?;
            mapped_memory.copy_from_nonoverlapping(
                data.as_ptr() as _,
                std::mem::size_of::<I::Item>() * data.len(),
            );
            device.unmap_memory(block.inner)
        }
        Ok(())
    }
}

impl Drop for Memory {
    fn drop(&mut self) {
        self.block.lock().unwrap().sub_free(self.range);
    }
}

impl Debug for Memory {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Memory")
    }
}

struct MemoryType {
    blocks: Vec<Arc<Mutex<MemoryBlock>>>,
    properties: vk::MemoryPropertyFlags,
    type_index: u32,
}

impl MemoryType {
    const DEFAULT_BLOCK_SIZE: vk::DeviceSize = 1024 * 1024 * 512;

    fn new(type_index: u32, properties: vk::MemoryPropertyFlags) -> Self {
        Self {
            blocks: Vec::default(),
            properties,
            type_index,
        }
    }

    fn find_or_create_block(
        &mut self,
        device: Arc<Device>,
        memory_requirements: vk::MemoryRequirements,
    ) -> Result<Arc<Mutex<MemoryBlock>>, AllocationError> {
        let size = Self::DEFAULT_BLOCK_SIZE.max(memory_requirements.size);
        let block = if let Some(block) = self
            .blocks
            .iter()
            .find(|block| {
                let block = block.lock().unwrap();
                block.contains_free_range(memory_requirements.size, memory_requirements.alignment)
            })
            .cloned()
        {
            block
        } else {
            self.blocks
                .push(MemoryBlock::new(device, self.type_index, size)?);
            self.blocks.last().unwrap().clone()
        };
        Ok(block)
    }
}

struct MemoryBlock {
    free: Vec<Range>,
    _size: vk::DeviceSize,
    inner: vk::DeviceMemory,
}

impl MemoryBlock {
    fn new(
        device: Arc<Device>,
        type_index: u32,
        size: vk::DeviceSize,
    ) -> Result<Arc<Mutex<MemoryBlock>>, AllocationError> {
        let allocation_info = vk::MemoryAllocateInfo::builder()
            .memory_type_index(type_index)
            .allocation_size(size);
        let inner = unsafe { device.allocate_memory(&allocation_info, None) }?;
        Ok(Arc::new(Mutex::new(Self {
            free: vec![Range { offset: 0, size }],
            _size: size,
            inner,
        })))
    }

    fn contains_free_range(&self, size: vk::DeviceSize, alignment: vk::DeviceSize) -> bool {
        self.free
            .iter()
            .find(|range| {
                let aligned_offset = range.offset.div_ceil(&alignment) * alignment;
                let diff = aligned_offset - range.offset;
                if diff > range.size {
                    return false;
                }
                let aligned_size = range.size - diff;
                aligned_size >= size
            })
            .is_some()
    }

    fn sub_allocate(
        &mut self,
        self_ref: Arc<Mutex<MemoryBlock>>,
        size: vk::DeviceSize,
        alignment: vk::DeviceSize,
    ) -> Memory {
        let free_index = self.free.iter().position(|range| {
            let aligned_offset = range.offset.div_ceil(&alignment) * alignment;
            let diff = aligned_offset - range.offset;
            if diff > range.size {
                return false;
            }
            let aligned_size = range.size - diff;
            aligned_size >= size
        }).expect("Failed to find index of free memory sub range. This indicates a race condition in the allocator.");
        let range = self.free.remove(free_index);
        let aligned_offset = range.offset.div_ceil(&alignment) * alignment;
        let diff = aligned_offset - range.offset;
        let aligned_size = range.size - diff;
        assert!(aligned_size >= size);

        if diff > 0 {
            self.free.push(Range {
                offset: range.offset,
                size: diff,
            });
        }
        if aligned_size > size {
            self.free.push(Range {
                offset: aligned_offset + size,
                size: aligned_size - size,
            });
        }

        Memory::new(
            self_ref,
            self.inner,
            Range {
                offset: aligned_offset,
                size,
            },
        )
    }

    fn sub_free(&mut self, range: Range) {
        self.free.push(range);
    }
}

#[derive(Copy, Clone)]
struct Range {
    offset: vk::DeviceSize,
    size: vk::DeviceSize,
}

/// Error that can occur during allocation.
#[derive(Debug)]
pub enum AllocationError {
    /// No suitable memory type found.
    NoSuitableMemoryType,
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// An external handle is not a valid handle of the specified type.
    InvalidExternalHandle(VulkanError),
    /// A buffer creation or memory allocation failed because the requested address is not available.
    InvalidOpaqueCaptureAddress(VulkanError),
}

impl Display for AllocationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            AllocationError::NoSuitableMemoryType => {
                write!(f, "Failed to find suitable memory type for allocation")
            }
            AllocationError::OutOfHostMemory(e)
            | AllocationError::OutOfDeviceMemory(e)
            | AllocationError::InvalidExternalHandle(e)
            | AllocationError::InvalidOpaqueCaptureAddress(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for AllocationError {}

impl From<VulkanError> for AllocationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => AllocationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => AllocationError::OutOfDeviceMemory(e),
            VulkanError::InvalidExternalHandle => AllocationError::InvalidExternalHandle(e),
            VulkanError::InvalidOpaqueCaptureAddress => {
                AllocationError::InvalidOpaqueCaptureAddress(e)
            }
            _ => unreachable!("Unknown allocate memory error"),
        }
    }
}

impl From<vk::Result> for AllocationError {
    fn from(result: vk::Result) -> Self {
        AllocationError::from(VulkanError::from(result))
    }
}

/// Error that can occur while binding the memory.
#[derive(Debug)]
pub enum BindMemoryError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// A buffer creation or memory allocation failed because the requested address is not available.
    InvalidOpaqueCaptureAddress(VulkanError),
}

impl Display for BindMemoryError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            BindMemoryError::OutOfHostMemory(e)
            | BindMemoryError::OutOfDeviceMemory(e)
            | BindMemoryError::InvalidOpaqueCaptureAddress(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for BindMemoryError {}

impl From<VulkanError> for BindMemoryError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => BindMemoryError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => BindMemoryError::OutOfDeviceMemory(e),
            VulkanError::InvalidOpaqueCaptureAddress => {
                BindMemoryError::InvalidOpaqueCaptureAddress(e)
            }
            _ => unreachable!("Unknown bind memory error"),
        }
    }
}

impl From<vk::Result> for BindMemoryError {
    fn from(result: vk::Result) -> Self {
        BindMemoryError::from(VulkanError::from(result))
    }
}

/// Error that can occur while writing to the memory.
#[derive(Debug)]
pub enum WriteMemoryError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// Mapping of a memory object has failed.
    MemoryMapFailed(VulkanError),
}

impl Display for WriteMemoryError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            WriteMemoryError::OutOfHostMemory(e)
            | WriteMemoryError::OutOfDeviceMemory(e)
            | WriteMemoryError::MemoryMapFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for WriteMemoryError {}

impl From<VulkanError> for WriteMemoryError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => WriteMemoryError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => WriteMemoryError::OutOfDeviceMemory(e),
            VulkanError::InvalidOpaqueCaptureAddress => WriteMemoryError::MemoryMapFailed(e),
            _ => unreachable!("Unknown map memory error"),
        }
    }
}

impl From<vk::Result> for WriteMemoryError {
    fn from(result: vk::Result) -> Self {
        WriteMemoryError::from(VulkanError::from(result))
    }
}
