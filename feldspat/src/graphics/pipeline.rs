//! Types for pipeline creation.

use std::collections::HashMap;
use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::sync::{Arc, Mutex, Weak};

use ash::vk;

use crate::graphics::buffer::Buffer;
use crate::graphics::image::{Image, ImageViewAbstract, Sampler};
use crate::graphics::{Device, RenderPass, ShaderModule, VulkanError};

/// A graphics pipeline.
pub struct GraphicsPipeline {
    inner: vk::Pipeline,
    layout: PipelineLayout,
    device: Arc<Device>,
}

impl<'a> GraphicsPipeline {
    /// Starts building a new graphics pipeline.
    pub fn builder() -> GraphicsPipelineBuilder<'a, (), ()> {
        GraphicsPipelineBuilder {
            render_pass: None,
            push_constants: None,
            descriptor_set_layouts: Default::default(),
            color_blending: None,
            depth_test: None,
            conservative_rasterization_mode: None,
            front_face: None,
            cull_mode: None,
            scissors: None,
            viewports: None,
            topology: None,
            vertex_stride: None,
            vertex_input: None,
            fragment_shader_specialization_constants: None,
            fragment_shader: None,
            geometry_shader: None,
            vertex_shader_specialization_constants: None,
            vertex_shader: None,
        }
    }

    /// Returns the layout of the pipeline.
    pub fn layout(&self) -> &PipelineLayout {
        &self.layout
    }

    /// Returns the vulkan pipeline.
    pub fn inner(&self) -> vk::Pipeline {
        self.inner
    }
}

impl Drop for GraphicsPipeline {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_pipeline(self.inner, None);
        }
    }
}

/// Graphics pipeline builder.
pub struct GraphicsPipelineBuilder<'a, VSSC, FSSC> {
    render_pass: Option<(Arc<RenderPass>, u32)>,
    push_constants: Option<vk::PushConstantRange>,
    descriptor_set_layouts: Vec<&'a DescriptorSetLayout>,
    color_blending: Option<vk::PipelineColorBlendAttachmentState>,
    depth_test: Option<()>,
    conservative_rasterization_mode: Option<vk::ConservativeRasterizationModeEXT>,
    front_face: Option<vk::FrontFace>,
    cull_mode: Option<vk::CullModeFlags>,
    scissors: Option<Vec<vk::Rect2D>>,
    viewports: Option<Vec<vk::Viewport>>,
    topology: Option<vk::PrimitiveTopology>,
    vertex_stride: Option<usize>,
    vertex_input: Option<Vec<vk::VertexInputAttributeDescription>>,
    fragment_shader_specialization_constants: Option<FSSC>,
    fragment_shader: Option<Arc<ShaderModule>>,
    geometry_shader: Option<Arc<ShaderModule>>,
    vertex_shader_specialization_constants: Option<VSSC>,
    vertex_shader: Option<Arc<ShaderModule>>,
}

impl<'a, VSSC, FSSC> GraphicsPipelineBuilder<'a, VSSC, FSSC>
where
    VSSC: SpecializationConstants,
    FSSC: SpecializationConstants,
{
    /// Enabled sets the color blend state for all color attachments.
    pub fn blend(mut self, blend_state: vk::PipelineColorBlendAttachmentState) -> Self {
        self.color_blending = Some(blend_state);
        self
    }

    /// Sets the vertex shader.
    pub fn vertex_shader(mut self, vertex_shader: Arc<ShaderModule>) -> Self {
        self.vertex_shader = Some(vertex_shader);
        self
    }

    /// Sets the specialization constants for the vertex shader.
    pub fn vertex_shader_specialization_constants<T>(
        self,
        specialization_constants: T,
    ) -> GraphicsPipelineBuilder<'a, T, FSSC>
    where
        T: SpecializationConstants,
    {
        GraphicsPipelineBuilder {
            render_pass: self.render_pass,
            push_constants: self.push_constants,
            descriptor_set_layouts: self.descriptor_set_layouts,
            color_blending: self.color_blending,
            depth_test: self.depth_test,
            conservative_rasterization_mode: self.conservative_rasterization_mode,
            front_face: self.front_face,
            cull_mode: self.cull_mode,
            scissors: self.scissors,
            viewports: self.viewports,
            topology: self.topology,
            vertex_stride: self.vertex_stride,
            vertex_input: self.vertex_input,
            fragment_shader_specialization_constants: self.fragment_shader_specialization_constants,
            fragment_shader: self.fragment_shader,
            geometry_shader: self.geometry_shader,
            vertex_shader_specialization_constants: Some(specialization_constants),
            vertex_shader: self.vertex_shader,
        }
    }

    /// Sets the geometry shader.
    pub fn geometry_shader(mut self, geometry_shader: Arc<ShaderModule>) -> Self {
        self.geometry_shader = Some(geometry_shader);
        self
    }

    /// Sets the fragment shader.
    pub fn fragment_shader(mut self, fragment_shader: Arc<ShaderModule>) -> Self {
        self.fragment_shader = Some(fragment_shader);
        self
    }

    /// Sets the specialization constants for the fragment shader.
    pub fn fragment_shader_specialization_constants<T>(
        self,
        specialization_constants: T,
    ) -> GraphicsPipelineBuilder<'a, VSSC, T>
    where
        T: SpecializationConstants,
    {
        GraphicsPipelineBuilder {
            render_pass: self.render_pass,
            push_constants: self.push_constants,
            descriptor_set_layouts: self.descriptor_set_layouts,
            color_blending: self.color_blending,
            depth_test: self.depth_test,
            conservative_rasterization_mode: self.conservative_rasterization_mode,
            front_face: self.front_face,
            cull_mode: self.cull_mode,
            scissors: self.scissors,
            viewports: self.viewports,
            topology: self.topology,
            vertex_stride: self.vertex_stride,
            vertex_input: self.vertex_input,
            fragment_shader_specialization_constants: Some(specialization_constants),
            fragment_shader: self.fragment_shader,
            geometry_shader: self.geometry_shader,
            vertex_shader_specialization_constants: self.vertex_shader_specialization_constants,
            vertex_shader: self.vertex_shader,
        }
    }

    /// Sets the input vertex description.
    pub fn vertex_input<V>(mut self) -> Self
    where
        V: Vertex,
    {
        self.vertex_input = Some(V::attributes());
        self.vertex_stride = Some(V::size());
        self
    }

    /// Sets the input assemblies topology to triangle list.
    pub fn triangle_list(mut self) -> Self {
        self.topology = Some(vk::PrimitiveTopology::TRIANGLE_LIST);
        self
    }

    /// Sets the input assemblies topology to point list.
    pub fn point_list(mut self) -> Self {
        self.topology = Some(vk::PrimitiveTopology::POINT_LIST);
        self
    }

    /// Sets the viewports.
    pub fn viewports(mut self, viewports: Vec<vk::Viewport>) -> Self {
        self.scissors = Some(
            viewports
                .iter()
                .map(|viewport| {
                    vk::Rect2D::builder()
                        .extent(
                            vk::Extent2D::builder()
                                .width(viewport.width as u32)
                                .height(viewport.height as u32)
                                .build(),
                        )
                        .build()
                })
                .collect(),
        );
        self.viewports = Some(viewports);
        self
    }

    /// Enables back face culling.
    pub fn cull_mode_back(mut self) -> Self {
        self.cull_mode = Some(vk::CullModeFlags::BACK);
        self
    }

    /// Defines the front face as clockwise.
    pub fn front_face_clockwise(mut self) -> Self {
        self.front_face = Some(vk::FrontFace::CLOCKWISE);
        self
    }

    /// Defines the front face as counter clockwise.
    pub fn front_face_counter_clockwise(mut self) -> Self {
        self.front_face = Some(vk::FrontFace::COUNTER_CLOCKWISE);
        self
    }

    /// Enables conservative rasterization.
    pub fn conservative_rasterization(
        mut self,
        mode: vk::ConservativeRasterizationModeEXT,
    ) -> Self {
        self.conservative_rasterization_mode = Some(mode);
        self
    }

    /// Enables the depth test with default settings.
    pub fn depth_test_enabled(mut self) -> Self {
        self.depth_test = Some(());
        self
    }

    /// Adds a descriptor set layout.
    pub fn descriptor_set_layout(mut self, layout: &'a DescriptorSetLayout) -> Self {
        self.descriptor_set_layouts.push(layout);
        self
    }

    /// Adds push constants.
    pub fn push_constants<T>(mut self, stage: vk::ShaderStageFlags) -> Self {
        self.push_constants = Some(
            vk::PushConstantRange::builder()
                .stage_flags(stage)
                .offset(0)
                .size(std::mem::size_of::<T>() as _)
                .build(),
        );
        self
    }

    /// Sets the render pass.
    pub fn render_pass(mut self, render_pass: Arc<RenderPass>, subpass: u32) -> Self {
        self.render_pass = Some((render_pass, subpass));
        self
    }

    /// Builds the graphics pipeline.
    pub fn build(
        self,
        device: Arc<Device>,
    ) -> Result<Arc<GraphicsPipeline>, GraphicsPipelineCreationError> {
        let vertex_shader = self.vertex_shader.expect("Missing vertex shader");
        let vertex_shader_specialization_constants_map_entries = self
            .vertex_shader_specialization_constants
            .as_ref()
            .map(|constants| constants.specialization_map_entries())
            .unwrap_or(().specialization_map_entries());
        let vertex_shader_specialization_constants_data = self
            .vertex_shader_specialization_constants
            .as_ref()
            .map(|constants| constants.as_bytes())
            .unwrap_or(().as_bytes());
        let vertex_shader_specialization_constants = vk::SpecializationInfo::builder()
            .map_entries(&vertex_shader_specialization_constants_map_entries)
            .data(vertex_shader_specialization_constants_data)
            .build();
        let fragment_shader_specialization_constants_map_entries = self
            .fragment_shader_specialization_constants
            .as_ref()
            .map(|constants| constants.specialization_map_entries())
            .unwrap_or(().specialization_map_entries());
        let fragment_shader_specialization_constants_data = self
            .fragment_shader_specialization_constants
            .as_ref()
            .map(|constants| constants.as_bytes())
            .unwrap_or(().as_bytes());
        let fragment_shader_specialization_constants = vk::SpecializationInfo::builder()
            .map_entries(&fragment_shader_specialization_constants_map_entries)
            .data(fragment_shader_specialization_constants_data)
            .build();
        let mut stages = vec![vk::PipelineShaderStageCreateInfo::builder()
            .stage(vertex_shader.stage())
            .module(vertex_shader.inner())
            .name(vertex_shader.main_entrypoint())
            .specialization_info(&vertex_shader_specialization_constants)
            .build()];
        if let Some(fragment_shader) = self.fragment_shader {
            stages.push(
                vk::PipelineShaderStageCreateInfo::builder()
                    .stage(fragment_shader.stage())
                    .module(fragment_shader.inner())
                    .name(fragment_shader.main_entrypoint())
                    .specialization_info(&fragment_shader_specialization_constants)
                    .build(),
            )
        }
        if let Some(geometry_shader) = self.geometry_shader {
            stages.push(
                vk::PipelineShaderStageCreateInfo::builder()
                    .stage(geometry_shader.stage())
                    .module(geometry_shader.inner())
                    .name(geometry_shader.main_entrypoint())
                    .build(),
            );
        }
        let vertex_binding_descriptions = self
            .vertex_stride
            .as_ref()
            .map(|vertex_stride| {
                vec![vk::VertexInputBindingDescription::builder()
                    .binding(0)
                    .input_rate(vk::VertexInputRate::VERTEX)
                    .stride(*vertex_stride as _)
                    .build()]
            })
            .unwrap_or(Vec::new());
        let vertex_input = vk::PipelineVertexInputStateCreateInfo::builder()
            .vertex_binding_descriptions(&vertex_binding_descriptions)
            .vertex_attribute_descriptions(
                self.vertex_input
                    .as_ref()
                    .map(|vertex_input| vertex_input.as_slice())
                    .unwrap_or(&[]),
            );
        let input_assembly = vk::PipelineInputAssemblyStateCreateInfo::builder()
            .topology(self.topology.expect("Missing topology"))
            .primitive_restart_enable(false);
        let viewport_state = vk::PipelineViewportStateCreateInfo::builder()
            .viewports(self.viewports.as_ref().expect("Missing viewports"))
            .scissors(self.scissors.as_ref().expect("Missing scissors"));
        let mut conservative_rasterization =
            vk::PipelineRasterizationConservativeStateCreateInfoEXT::builder()
                .conservative_rasterization_mode(
                    self.conservative_rasterization_mode
                        .unwrap_or(vk::ConservativeRasterizationModeEXT::DISABLED),
                )
                .build();
        let rasterizer = vk::PipelineRasterizationStateCreateInfo::builder()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .line_width(1.0)
            .cull_mode(self.cull_mode.unwrap_or(vk::CullModeFlags::NONE))
            .front_face(self.front_face.unwrap_or(vk::FrontFace::COUNTER_CLOCKWISE))
            .depth_bias_enable(false)
            .push_next(&mut conservative_rasterization);
        let depth_stencil = vk::PipelineDepthStencilStateCreateInfo::builder()
            .depth_test_enable(self.depth_test.is_some())
            .depth_write_enable(self.depth_test.is_some())
            .depth_compare_op(vk::CompareOp::LESS)
            .depth_bounds_test_enable(false)
            .min_depth_bounds(0.0)
            .max_depth_bounds(1.0)
            .stencil_test_enable(false);
        let multisampling = vk::PipelineMultisampleStateCreateInfo::builder()
            .sample_shading_enable(false)
            .rasterization_samples(vk::SampleCountFlags::TYPE_1);
        let (render_pass, subpass) = self.render_pass.expect("Missing render pass");
        let subpass_description = render_pass.subpass(subpass as _);
        let color_blend_state = self.color_blending.unwrap_or(
            vk::PipelineColorBlendAttachmentState::builder()
                .color_write_mask(vk::ColorComponentFlags::all())
                .blend_enable(false)
                .build(),
        );
        let color_blend_attachments = (0..subpass_description.color_attachment_count)
            .map(|_| color_blend_state)
            .collect::<Vec<_>>();
        let color_blending = vk::PipelineColorBlendStateCreateInfo::builder()
            .logic_op_enable(false)
            .attachments(&color_blend_attachments);
        let descriptor_set_layouts = self
            .descriptor_set_layouts
            .iter()
            .map(|layout| layout.inner())
            .collect::<Vec<_>>();
        let push_constants = self
            .push_constants
            .map(|push_constants| vec![push_constants])
            .unwrap_or(vec![]);
        let layout = PipelineLayout::new(
            device.clone(),
            vk::PipelineLayoutCreateInfo::builder()
                .set_layouts(&descriptor_set_layouts)
                .push_constant_ranges(&push_constants)
                .build(),
        )?;

        let create_infos = [vk::GraphicsPipelineCreateInfo::builder()
            .stages(&stages)
            .vertex_input_state(&vertex_input)
            .input_assembly_state(&input_assembly)
            .viewport_state(&viewport_state)
            .rasterization_state(&rasterizer)
            .depth_stencil_state(&depth_stencil)
            .multisample_state(&multisampling)
            .color_blend_state(&color_blending)
            .layout(layout.inner())
            .render_pass(render_pass.inner())
            .subpass(subpass)
            .build()];

        let inner = unsafe {
            device.create_graphics_pipelines(vk::PipelineCache::null(), &create_infos, None)
        }
        .map_err(|(_, e)| e)?[0];

        Ok(Arc::new(GraphicsPipeline {
            inner,
            layout,
            device,
        }))
    }
}

/// Error that can occur during graphics pipeline creation.
#[derive(Debug)]
pub enum GraphicsPipelineCreationError {
    /// Pipeline layout creation has failed.
    PipelineLayoutCreationFailed(PipelineLayoutCreationError),
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// One or more shaders failed to compile or link.
    InvalidShader(VulkanError),
}

impl Display for GraphicsPipelineCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            GraphicsPipelineCreationError::PipelineLayoutCreationFailed(e) => Display::fmt(e, f),
            GraphicsPipelineCreationError::OutOfHostMemory(e)
            | GraphicsPipelineCreationError::OutOfDeviceMemory(e)
            | GraphicsPipelineCreationError::InvalidShader(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for GraphicsPipelineCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            GraphicsPipelineCreationError::PipelineLayoutCreationFailed(e) => Some(e),
            _ => None,
        }
    }
}

impl From<PipelineLayoutCreationError> for GraphicsPipelineCreationError {
    fn from(e: PipelineLayoutCreationError) -> Self {
        GraphicsPipelineCreationError::PipelineLayoutCreationFailed(e)
    }
}

impl From<VulkanError> for GraphicsPipelineCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => GraphicsPipelineCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => GraphicsPipelineCreationError::OutOfDeviceMemory(e),
            VulkanError::InvalidShader => GraphicsPipelineCreationError::InvalidShader(e),
            _ => unreachable!("Unknown graphics pipeline creation error"),
        }
    }
}

impl From<vk::Result> for GraphicsPipelineCreationError {
    fn from(result: vk::Result) -> Self {
        GraphicsPipelineCreationError::from(VulkanError::from(result))
    }
}

/// A pipeline layout.
pub struct PipelineLayout {
    inner: vk::PipelineLayout,
    device: Arc<Device>,
}

impl PipelineLayout {
    /// Creates a pipeline layout from a create info.
    pub fn new(
        device: Arc<Device>,
        create_info: vk::PipelineLayoutCreateInfo,
    ) -> Result<Self, PipelineLayoutCreationError> {
        Ok(Self {
            inner: unsafe { device.create_pipeline_layout(&create_info, None) }?,
            device,
        })
    }

    /// Returns the vulkan pipeline layout.
    pub fn inner(&self) -> vk::PipelineLayout {
        self.inner
    }
}

impl Drop for PipelineLayout {
    fn drop(&mut self) {
        unsafe { self.device.destroy_pipeline_layout(self.inner, None) }
    }
}

/// Error that can occur during pipeline layout creation.
#[derive(Debug)]
pub enum PipelineLayoutCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for PipelineLayoutCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            PipelineLayoutCreationError::OutOfHostMemory(e)
            | PipelineLayoutCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for PipelineLayoutCreationError {}

impl From<VulkanError> for PipelineLayoutCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => PipelineLayoutCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => PipelineLayoutCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown pipeline layout creation error"),
        }
    }
}

impl From<vk::Result> for PipelineLayoutCreationError {
    fn from(result: vk::Result) -> Self {
        PipelineLayoutCreationError::from(VulkanError::from(result))
    }
}

/// A descriptor set layout.
pub struct DescriptorSetLayout {
    inner: vk::DescriptorSetLayout,
    bindings: Vec<vk::DescriptorSetLayoutBinding>,
    device: Arc<Device>,
}

impl DescriptorSetLayout {
    /// Starts building a descriptor set layout.
    pub fn builder() -> DescriptorSetLayoutBuilder {
        DescriptorSetLayoutBuilder {
            bindings: Default::default(),
        }
    }

    /// Returns the vulkan handle of the descriptor set layout.
    pub fn inner(&self) -> vk::DescriptorSetLayout {
        self.inner
    }
}

impl Drop for DescriptorSetLayout {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_descriptor_set_layout(self.inner, None);
        }
    }
}

unsafe impl Sync for DescriptorSetLayout {}

unsafe impl Send for DescriptorSetLayout {}

/// Builder for descriptor set layouts.
pub struct DescriptorSetLayoutBuilder {
    bindings: Vec<vk::DescriptorSetLayoutBinding>,
}

impl DescriptorSetLayoutBuilder {
    /// Adds a layout binding.
    pub fn add(mut self, binding: vk::DescriptorSetLayoutBinding) -> Self {
        self.bindings.push(binding);
        self
    }

    /// Builds the descriptor set layout.
    pub fn build(
        self,
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, DescriptorSetLayoutCreationError> {
        let create_info = vk::DescriptorSetLayoutCreateInfo::builder().bindings(&self.bindings);
        let inner = unsafe { device.create_descriptor_set_layout(&create_info, None) }?;
        Ok(Arc::new(DescriptorSetLayout {
            inner,
            bindings: self.bindings,
            device,
        }))
    }
}

/// Error that can occur during descriptor set layout creation.
#[derive(Debug)]
pub enum DescriptorSetLayoutCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for DescriptorSetLayoutCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            DescriptorSetLayoutCreationError::OutOfHostMemory(e)
            | DescriptorSetLayoutCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for DescriptorSetLayoutCreationError {}

impl From<VulkanError> for DescriptorSetLayoutCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => DescriptorSetLayoutCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => {
                DescriptorSetLayoutCreationError::OutOfDeviceMemory(e)
            }
            _ => unreachable!("Unknown descriptor set layout creation error"),
        }
    }
}

impl From<vk::Result> for DescriptorSetLayoutCreationError {
    fn from(result: vk::Result) -> Self {
        DescriptorSetLayoutCreationError::from(VulkanError::from(result))
    }
}

/// A collection of descriptor pools.
pub struct DescriptorPools {
    pools: Arc<Mutex<Vec<Arc<DescriptorPool>>>>,
    default_pool_size: usize,
}

impl DescriptorPools {
    /// Creates a new descriptor pool collection.
    pub fn new(default_pool_size: usize) -> Self {
        Self {
            pools: Arc::new(Mutex::new(vec![])),
            default_pool_size,
        }
    }

    /// Allocates a descriptor set.
    pub fn alloc(
        &self,
        device: Arc<Device>,
        layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<DescriptorSet>, DescriptorSetCreationError> {
        let mut pools = self.pools.lock().unwrap();
        let pool = if let Some(pool) = pools
            .iter()
            .find(|pool| self.is_pool_suitable(pool, &layout))
            .cloned()
        {
            pool
        } else {
            let pool = self.create_pool(device.clone())?;
            pools.push(pool.clone());
            pool
        };

        let device = pool.device.upgrade().unwrap().clone();
        let layouts = [layout.inner()];
        let inner_pool = pool.inner().lock().unwrap();
        let allocate_info = vk::DescriptorSetAllocateInfo::builder()
            .descriptor_pool(*inner_pool)
            .set_layouts(&layouts);
        let inner = unsafe { device.allocate_descriptor_sets(&allocate_info) }?[0];

        Self::reduce_free_descriptors(&pool, &layout);

        Ok(Arc::new(DescriptorSet {
            inner,
            layout,
            pool: pool.clone(),
            device,
        }))
    }

    fn is_pool_suitable(&self, pool: &DescriptorPool, layout: &DescriptorSetLayout) -> bool {
        let free_descriptors = pool.free_descriptors.lock().unwrap();
        self.has_pool_enough_free_descriptors_of_type(
            layout,
            &free_descriptors,
            vk::DescriptorType::UNIFORM_BUFFER,
        ) && self.has_pool_enough_free_descriptors_of_type(
            layout,
            &free_descriptors,
            vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
        ) && self.has_pool_enough_free_descriptors_of_type(
            layout,
            &free_descriptors,
            vk::DescriptorType::INPUT_ATTACHMENT,
        )
    }

    fn has_pool_enough_free_descriptors_of_type(
        &self,
        layout: &DescriptorSetLayout,
        free_descriptors: &HashMap<vk::DescriptorType, u32>,
        descriptor_type: vk::DescriptorType,
    ) -> bool {
        let required = layout
            .bindings
            .iter()
            .filter(|bindings| bindings.descriptor_type == descriptor_type)
            .map(|binding| binding.descriptor_count)
            .sum();
        let available = free_descriptors.get(&descriptor_type).cloned().unwrap_or(0);
        available >= required
    }

    fn create_pool(
        &self,
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorPool>, DescriptorPoolCreationError> {
        let pool_sizes = vec![
            vk::DescriptorPoolSize::builder()
                .ty(vk::DescriptorType::UNIFORM_BUFFER)
                .descriptor_count(self.default_pool_size as _)
                .build(),
            vk::DescriptorPoolSize::builder()
                .ty(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                .descriptor_count(self.default_pool_size as _)
                .build(),
            vk::DescriptorPoolSize::builder()
                .ty(vk::DescriptorType::INPUT_ATTACHMENT)
                .descriptor_count(self.default_pool_size as _)
                .build(),
        ];
        DescriptorPool::new(device, pool_sizes, self.default_pool_size as _)
    }

    fn reduce_free_descriptors(pool: &DescriptorPool, layout: &DescriptorSetLayout) {
        let mut free_descriptors = pool.free_descriptors.lock().unwrap();
        let combined_image_samplers: u32 = layout
            .bindings
            .iter()
            .filter(|bindings| {
                bindings.descriptor_type == vk::DescriptorType::COMBINED_IMAGE_SAMPLER
            })
            .map(|binding| binding.descriptor_count)
            .sum();
        let free_combined_image_samplers =
            free_descriptors[&vk::DescriptorType::COMBINED_IMAGE_SAMPLER];
        free_descriptors.insert(
            vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            free_combined_image_samplers - combined_image_samplers,
        );
        let images: u32 = layout
            .bindings
            .iter()
            .filter(|bindings| bindings.descriptor_type == vk::DescriptorType::INPUT_ATTACHMENT)
            .map(|binding| binding.descriptor_count)
            .sum();
        let free_images = free_descriptors[&vk::DescriptorType::INPUT_ATTACHMENT];
        free_descriptors.insert(vk::DescriptorType::INPUT_ATTACHMENT, free_images - images);
    }

    /// Clean up.
    pub unsafe fn clean_up(&self, device: &Device) {
        let mut pools = self.pools.lock().unwrap();
        for pool in pools.iter() {
            device.destroy_descriptor_pool(*pool.inner().lock().unwrap(), None);
        }
        pools.clear();
    }
}

impl Debug for DescriptorPools {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "DescriptorPools")
    }
}

/// Pool for allocating descriptors.
pub struct DescriptorPool {
    inner: Arc<Mutex<vk::DescriptorPool>>,
    free_descriptors: Mutex<HashMap<vk::DescriptorType, u32>>,
    device: Weak<Device>,
}

impl DescriptorPool {
    /// Creates a new descriptor pool.
    pub fn new(
        device: Arc<Device>,
        pool_sizes: Vec<vk::DescriptorPoolSize>,
        max_sets: u32,
    ) -> Result<Arc<Self>, DescriptorPoolCreationError> {
        let create_info = vk::DescriptorPoolCreateInfo::builder()
            .flags(vk::DescriptorPoolCreateFlags::FREE_DESCRIPTOR_SET)
            .pool_sizes(&pool_sizes)
            .max_sets(max_sets);
        let inner = Arc::new(Mutex::new(unsafe {
            device.create_descriptor_pool(&create_info, None)
        }?));
        let free_descriptors = Mutex::new(
            pool_sizes
                .iter()
                .map(|pool_size| (pool_size.ty, pool_size.descriptor_count))
                .collect(),
        );
        Ok(Arc::new(Self {
            inner,
            free_descriptors,
            device: Arc::downgrade(&device),
        }))
    }

    /// Returns the vulkan handle to the descriptor pool.
    pub fn inner(&self) -> &Arc<Mutex<vk::DescriptorPool>> {
        &self.inner
    }
}

/// Error that can occur during descriptor pool creation.
#[derive(Debug)]
pub enum DescriptorPoolCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// A descriptor pool creation has failed due to fragmentation.
    Fragmentation(VulkanError),
}

impl Display for DescriptorPoolCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            DescriptorPoolCreationError::OutOfHostMemory(e)
            | DescriptorPoolCreationError::OutOfDeviceMemory(e)
            | DescriptorPoolCreationError::Fragmentation(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for DescriptorPoolCreationError {}

impl From<VulkanError> for DescriptorPoolCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => DescriptorPoolCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => DescriptorPoolCreationError::OutOfDeviceMemory(e),
            VulkanError::Fragmentation => DescriptorPoolCreationError::Fragmentation(e),
            _ => unreachable!("Unknown descriptor pool creation error"),
        }
    }
}

impl From<vk::Result> for DescriptorPoolCreationError {
    fn from(result: vk::Result) -> Self {
        DescriptorPoolCreationError::from(VulkanError::from(result))
    }
}

/// A descriptor set.
pub struct DescriptorSet {
    inner: vk::DescriptorSet,
    layout: Arc<DescriptorSetLayout>,
    pool: Arc<DescriptorPool>,
    device: Arc<Device>,
}

impl DescriptorSet {
    /// Starts building a descriptor set.
    pub fn builder() -> DescriptorSetBuilder {
        DescriptorSetBuilder {
            buffers: Default::default(),
            sampled_images: Default::default(),
            images: Default::default(),
        }
    }

    /// Returns the vulkan handle to the descriptor set.
    pub fn inner(&self) -> vk::DescriptorSet {
        self.inner
    }

    fn increase_free_descriptors(pool: &DescriptorPool, layout: &DescriptorSetLayout) {
        let mut free_descriptors = pool.free_descriptors.lock().unwrap();
        let combined_image_samplers: u32 = layout
            .bindings
            .iter()
            .filter(|bindings| {
                bindings.descriptor_type == vk::DescriptorType::COMBINED_IMAGE_SAMPLER
            })
            .map(|binding| binding.descriptor_count)
            .sum();
        let free_combined_image_samplers =
            free_descriptors[&vk::DescriptorType::COMBINED_IMAGE_SAMPLER];
        free_descriptors.insert(
            vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            free_combined_image_samplers + combined_image_samplers,
        );
        let images: u32 = layout
            .bindings
            .iter()
            .filter(|bindings| bindings.descriptor_type == vk::DescriptorType::INPUT_ATTACHMENT)
            .map(|binding| binding.descriptor_count)
            .sum();
        let free_images = free_descriptors[&vk::DescriptorType::INPUT_ATTACHMENT];
        free_descriptors.insert(vk::DescriptorType::INPUT_ATTACHMENT, free_images + images);
    }
}

impl Drop for DescriptorSet {
    fn drop(&mut self) {
        let pool = self.pool.inner().lock().unwrap();
        Self::increase_free_descriptors(&self.pool, &self.layout);

        let descriptor_sets = [self.inner()];
        unsafe {
            self.device
                .free_descriptor_sets(*pool, &descriptor_sets)
                .unwrap();
        }
    }
}

/// Builder for creating descriptor sets.
pub struct DescriptorSetBuilder {
    buffers: Vec<(u32, Arc<dyn Buffer>, vk::DescriptorType)>,
    sampled_images: Vec<(u32, Arc<dyn ImageViewAbstract>, Arc<Sampler>)>,
    images: Vec<(u32, Arc<dyn ImageViewAbstract>)>,
}

impl DescriptorSetBuilder {
    /// Adds a buffer with a binding to the descriptor set.
    pub fn add_buffer<T>(
        mut self,
        binding: u32,
        buffer: Arc<T>,
        descriptor_type: vk::DescriptorType,
    ) -> Self
    where
        T: Buffer + 'static,
    {
        self.buffers.push((binding, buffer, descriptor_type));
        self
    }

    /// Adds a sampled image with a binding to the descriptor set.
    pub fn add_sampled_image<T>(mut self, binding: u32, view: Arc<T>, sampler: Arc<Sampler>) -> Self
    where
        T: ImageViewAbstract + 'static,
    {
        self.sampled_images.push((binding, view, sampler));
        self
    }

    /// Adds a image with a binding to the descriptor set.
    pub fn add_image(mut self, binding: u32, view: Arc<dyn ImageViewAbstract>) -> Self {
        self.images.push((binding, view));
        self
    }

    /// Builds the descriptor set.
    pub fn build(
        self,
        device: Arc<Device>,
        layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<DescriptorSet>, DescriptorSetCreationError> {
        let descriptor_set = device.descriptor_pools().alloc(device.clone(), layout)?;

        for (binding, buffer, descriptor_type) in &self.buffers {
            let buffer_info = vk::DescriptorBufferInfo::builder()
                .buffer(buffer.inner())
                .offset(0)
                .range(vk::WHOLE_SIZE)
                .build();
            let descriptor_write = vk::WriteDescriptorSet::builder()
                .dst_set(descriptor_set.inner())
                .dst_binding(*binding)
                .dst_array_element(0)
                .descriptor_type(*descriptor_type)
                .buffer_info(&[buffer_info])
                .build();
            unsafe { device.update_descriptor_sets(&[descriptor_write], &[]) };
        }

        for (binding, image, sampler) in &self.sampled_images {
            let image_info = vk::DescriptorImageInfo::builder()
                .image_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                .image_view(image.inner())
                .sampler(sampler.inner())
                .build();
            let descriptor_write = vk::WriteDescriptorSet::builder()
                .dst_set(descriptor_set.inner())
                .dst_binding(*binding)
                .dst_array_element(0)
                .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                .image_info(&[image_info])
                .build();
            unsafe { device.update_descriptor_sets(&[descriptor_write], &[]) };
        }

        for (binding, image) in &self.images {
            let image_info = vk::DescriptorImageInfo::builder()
                .image_layout(Self::layout(image.image()))
                .image_view(image.inner())
                .build();
            let descriptor_write = vk::WriteDescriptorSet::builder()
                .dst_set(descriptor_set.inner())
                .dst_binding(*binding)
                .dst_array_element(0)
                .descriptor_type(Self::image_descriptor_type(image.image()))
                .image_info(&[image_info])
                .build();
            unsafe { device.update_descriptor_sets(&[descriptor_write], &[]) };
        }

        Ok(descriptor_set)
    }

    fn layout(image: &dyn Image) -> vk::ImageLayout {
        if image
            .usage()
            .contains(vk::ImageUsageFlags::INPUT_ATTACHMENT)
        {
            vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL
        } else if image.usage().contains(vk::ImageUsageFlags::STORAGE) {
            vk::ImageLayout::GENERAL
        } else {
            panic!("Unknown image usage")
        }
    }

    fn image_descriptor_type(image: &dyn Image) -> vk::DescriptorType {
        if image
            .usage()
            .contains(vk::ImageUsageFlags::INPUT_ATTACHMENT)
        {
            vk::DescriptorType::INPUT_ATTACHMENT
        } else if image.usage().contains(vk::ImageUsageFlags::STORAGE) {
            vk::DescriptorType::STORAGE_IMAGE
        } else {
            panic!("Unknown image usage")
        }
    }
}

/// Error that can occur during descriptor set creation.
#[derive(Debug)]
pub enum DescriptorSetCreationError {
    /// Descriptor pool creation has failed.
    DescriptorPoolCreationFailed(DescriptorPoolCreationError),
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// A pool allocation has failed due to fragmentation of the pool’s memory.
    FragmentedPool(VulkanError),
    /// A pool memory allocation has failed.
    OutOfPoolMemory(VulkanError),
}

impl Display for DescriptorSetCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            DescriptorSetCreationError::DescriptorPoolCreationFailed(e) => Display::fmt(e, f),
            DescriptorSetCreationError::OutOfHostMemory(e)
            | DescriptorSetCreationError::OutOfDeviceMemory(e)
            | DescriptorSetCreationError::FragmentedPool(e)
            | DescriptorSetCreationError::OutOfPoolMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for DescriptorSetCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            DescriptorSetCreationError::DescriptorPoolCreationFailed(e) => Some(e),
            _ => None,
        }
    }
}

impl From<DescriptorPoolCreationError> for DescriptorSetCreationError {
    fn from(e: DescriptorPoolCreationError) -> Self {
        DescriptorSetCreationError::DescriptorPoolCreationFailed(e)
    }
}

impl From<VulkanError> for DescriptorSetCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => DescriptorSetCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => DescriptorSetCreationError::OutOfDeviceMemory(e),
            VulkanError::FragmentedPool => DescriptorSetCreationError::FragmentedPool(e),
            VulkanError::OutOfPoolMemory => DescriptorSetCreationError::OutOfPoolMemory(e),
            _ => unreachable!("Unknown descriptor set creation error"),
        }
    }
}

impl From<vk::Result> for DescriptorSetCreationError {
    fn from(result: vk::Result) -> Self {
        DescriptorSetCreationError::from(VulkanError::from(result))
    }
}

/// Trait for providing the specialization info for shader specialization constants.
pub trait SpecializationConstants: Sized {
    /// Returns the specialization info for the constants.
    fn specialization_map_entries(&self) -> Vec<vk::SpecializationMapEntry>;
    /// Returns the constants as slice of bytes.
    fn as_bytes(&self) -> &[u8] {
        unsafe {
            std::slice::from_raw_parts((self as *const Self) as _, std::mem::size_of::<Self>())
        }
    }
}

impl SpecializationConstants for () {
    fn specialization_map_entries(&self) -> Vec<vk::SpecializationMapEntry> {
        Default::default()
    }
}

/// Trait for vertex types.
pub trait Vertex: Default {
    /// Returns the vertex input attribute descriptions.
    fn attributes() -> Vec<vk::VertexInputAttributeDescription>;
    /// Returns the size of the vertex in bytes.
    fn size() -> usize;
}

/// Trait for vertex attributes.
pub trait VertexField {
    /// Returns the format of the attribute.
    fn format() -> vk::Format;
    /// Returns the size of the attribute in bytes.
    fn size() -> usize;
}
