//! Types for creating images, image views asn samplers.

use std::error::Error;
use std::fmt::{Display, Formatter};
use std::ops::Deref;
use std::sync::Arc;

use ash::vk;
use ash::vk::{Extent3D, Format, ImageType, ImageUsageFlags};

use crate::graphics::buffer::{BufferCreationError, CpuAccessibleBuffer};
use crate::graphics::command_buffer::{
    CommandBuffer, CommandBufferBuilder, CommandBufferCreationError,
};
use crate::graphics::device::Device;
use crate::graphics::memory::{AllocationError, BindMemoryError, Memory};
use crate::graphics::queue::{SubmitError, WaitIdleError};
use crate::graphics::VulkanError;

/// An image with bound device local memory.
#[derive(Debug)]
pub struct ImmutableImage {
    _memory: Memory,
    raw_image: RawImage,
}

impl ImmutableImage {
    /// Creates an image from an itertaor.
    pub fn from_iter<I>(
        device: Arc<Device>,
        format: vk::Format,
        extent: vk::Extent3D,
        mip_maps: bool,
        data: I,
    ) -> Result<Arc<Self>, ImageCreationError>
    where
        I: IntoIterator,
        I::IntoIter: ExactSizeIterator,
        I::Item: Sized + 'static,
    {
        let data = data.into_iter().collect::<Vec<_>>();
        let staging_buffer = CpuAccessibleBuffer::from_iter(
            device.clone(),
            vk::BufferUsageFlags::TRANSFER_SRC,
            data,
        )?;
        let mip_levels = if mip_maps {
            f32::floor(f32::log2(u32::max(extent.width, extent.height) as _)) as _
        } else {
            1
        };
        let image = Self::uninitialized(
            device.clone(),
            format,
            extent,
            mip_levels,
            vk::ImageUsageFlags::TRANSFER_SRC
                | vk::ImageUsageFlags::TRANSFER_DST
                | vk::ImageUsageFlags::SAMPLED,
        )?;

        let queue = device.graphics_queue();

        let subresource = vk::ImageSubresourceLayers::builder()
            .aspect_mask(vk::ImageAspectFlags::COLOR)
            .mip_level(0)
            .base_array_layer(0)
            .layer_count(1)
            .build();
        let image_copy = vk::BufferImageCopy::builder()
            .buffer_offset(0)
            .buffer_row_length(0)
            .buffer_image_height(0)
            .image_subresource(subresource)
            .image_offset(vk::Offset3D::default())
            .image_extent(extent)
            .build();
        let mut builder = CommandBuffer::primary(
            device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            queue.family(),
        )?;
        builder
            .deprected_transition_image_layout(
                image.clone(),
                vk::ImageLayout::UNDEFINED,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            )
            .copy_buffer_to_image(
                staging_buffer.clone(),
                image.clone(),
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                image_copy,
            );

        if mip_maps {
            image.add_generate_mip_maps_commands(&mut builder);
        } else {
            builder.deprected_transition_image_layout(
                image.clone(),
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            );
        }
        let command_buffer = builder.build()?;

        queue.submit(command_buffer.clone(), vec![], vec![], None, None)?;

        Ok(image)
    }

    /// Creates an an image with no data.
    pub fn uninitialized(
        device: Arc<Device>,
        format: vk::Format,
        extent: vk::Extent3D,
        mip_levels: u32,
        usage: vk::ImageUsageFlags,
    ) -> Result<Arc<Self>, ImageCreationError> {
        let raw_image = RawImage::new(device.clone(), format, extent, mip_levels, usage)?;
        let memory_requirements =
            unsafe { device.get_image_memory_requirements(raw_image.inner()) };
        let memory = device.allocator().allocate(
            device.clone(),
            memory_requirements,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        )?;
        memory.bind_image(&device, &raw_image)?;
        Ok(Arc::new(Self {
            _memory: memory,
            raw_image,
        }))
    }

    /// Creates a uninitialized image with a specific layout.
    pub fn with_layout(
        device: Arc<Device>,
        format: vk::Format,
        extent: vk::Extent3D,
        mip_levels: u32,
        usage: vk::ImageUsageFlags,
        layout: vk::ImageLayout,
    ) -> Result<Arc<Self>, ImageCreationError> {
        let image = Self::uninitialized(device.clone(), format, extent, mip_levels, usage)?;
        let queue = device.graphics_queue();
        let mut builder = CommandBuffer::primary(
            device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            queue.family(),
        )?;
        builder.deprected_transition_image_layout(
            image.clone(),
            vk::ImageLayout::UNDEFINED,
            layout,
        );
        let command_buffer = builder.build()?;
        queue.submit(command_buffer.clone(), vec![], vec![], None, None)?;
        Ok(image)
    }

    /// Generates mip maps for the image.
    pub fn generate_mip_maps(self: &Arc<Self>) -> Result<(), MipMapGenerationError> {
        let device = self.raw_image.device();
        let queue = device.graphics_queue();
        let mut builder = CommandBuffer::primary(
            device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            queue.family(),
        )?;
        self.add_generate_mip_maps_commands(&mut builder);
        let command_buffer = builder.build()?;
        queue.submit(command_buffer.clone(), vec![], vec![], None, None)?;

        Ok(())
    }

    fn add_generate_mip_maps_commands(self: &Arc<Self>, builder: &mut CommandBufferBuilder) {
        let mut width = self.extent().width;
        let mut height = self.extent().height;

        let subresource_range = vk::ImageSubresourceRange::builder()
            .aspect_mask(vk::ImageAspectFlags::COLOR)
            .level_count(1)
            .base_array_layer(0)
            .layer_count(1)
            .build();
        let mut barrier = vk::ImageMemoryBarrier::builder()
            .image(self.inner())
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .subresource_range(subresource_range)
            .build();

        for current_mip_level in 1..self.mip_levels() {
            barrier.subresource_range.base_mip_level = current_mip_level - 1;
            barrier.old_layout = vk::ImageLayout::TRANSFER_DST_OPTIMAL;
            barrier.new_layout = vk::ImageLayout::TRANSFER_SRC_OPTIMAL;
            barrier.src_access_mask = vk::AccessFlags::TRANSFER_WRITE;
            barrier.dst_access_mask = vk::AccessFlags::TRANSFER_READ;

            builder.pipeline_barrier(
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::TRANSFER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![barrier],
            );

            let src_subresource = vk::ImageSubresourceLayers::builder()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .mip_level(current_mip_level - 1)
                .base_array_layer(0)
                .layer_count(1)
                .build();
            let dst_subresource = vk::ImageSubresourceLayers::builder()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .mip_level(current_mip_level)
                .base_array_layer(0)
                .layer_count(1)
                .build();
            let blit = vk::ImageBlit::builder()
                .src_offsets([
                    vk::Offset3D::default(),
                    vk::Offset3D::builder()
                        .x(width as _)
                        .y(height as _)
                        .z(1)
                        .build(),
                ])
                .src_subresource(src_subresource)
                .dst_offsets([
                    vk::Offset3D::default(),
                    vk::Offset3D::builder()
                        .x(if width > 1 { width / 2 } else { 1 } as _)
                        .y(if height > 1 { height / 2 } else { 1 } as _)
                        .z(1)
                        .build(),
                ])
                .dst_subresource(dst_subresource)
                .build();

            builder.blit_image(
                self.clone(),
                vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                self.clone(),
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                vec![blit],
                vk::Filter::LINEAR,
            );

            barrier.old_layout = vk::ImageLayout::TRANSFER_SRC_OPTIMAL;
            barrier.new_layout = vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL;
            barrier.src_access_mask = vk::AccessFlags::TRANSFER_READ;
            barrier.dst_access_mask = vk::AccessFlags::SHADER_READ;

            builder.pipeline_barrier(
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::FRAGMENT_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![barrier],
            );

            if width > 1 {
                width /= 2
            }
            if height > 1 {
                height /= 2
            }
        }

        barrier.subresource_range.base_mip_level = self.mip_levels() - 1;
        barrier.old_layout = vk::ImageLayout::TRANSFER_DST_OPTIMAL;
        barrier.new_layout = vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL;
        barrier.src_access_mask = vk::AccessFlags::TRANSFER_WRITE;
        barrier.dst_access_mask = vk::AccessFlags::SHADER_READ;

        builder.pipeline_barrier(
            vk::PipelineStageFlags::TRANSFER,
            vk::PipelineStageFlags::FRAGMENT_SHADER,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![barrier],
        );
    }
}

/// An error that can occur during mip map generation.
#[derive(Debug)]
pub enum MipMapGenerationError {
    /// Command buffer creation has failed.
    CommandBufferCreationFailed(CommandBufferCreationError),
    /// Command buffer submission has failed.
    SubmitFailed(SubmitError),
    /// Waiting for the queue to become idle has failed.
    WaitIdleFailed(WaitIdleError),
}

impl Display for MipMapGenerationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            MipMapGenerationError::CommandBufferCreationFailed(e) => Display::fmt(e, f),
            MipMapGenerationError::SubmitFailed(e) => Display::fmt(e, f),
            MipMapGenerationError::WaitIdleFailed(e) => Display::fmt(e, f),
        }
    }
}

impl From<CommandBufferCreationError> for MipMapGenerationError {
    fn from(e: CommandBufferCreationError) -> Self {
        MipMapGenerationError::CommandBufferCreationFailed(e)
    }
}

impl From<SubmitError> for MipMapGenerationError {
    fn from(e: SubmitError) -> Self {
        MipMapGenerationError::SubmitFailed(e)
    }
}

impl From<WaitIdleError> for MipMapGenerationError {
    fn from(e: WaitIdleError) -> Self {
        MipMapGenerationError::WaitIdleFailed(e)
    }
}

impl Image for ImmutableImage {
    fn image_type(&self) -> ImageType {
        self.raw_image.image_type()
    }

    fn extent(&self) -> Extent3D {
        self.raw_image.extent()
    }

    fn mip_levels(&self) -> u32 {
        self.raw_image.mip_levels()
    }

    fn device(&self) -> &Arc<Device> {
        self.raw_image.device()
    }

    fn inner(&self) -> vk::Image {
        self.raw_image.inner()
    }

    fn format(&self) -> Format {
        self.raw_image.format()
    }

    fn usage(&self) -> ImageUsageFlags {
        self.raw_image.usage()
    }
}

/// Image without bound memory.
#[derive(Debug)]
pub struct RawImage {
    usage: vk::ImageUsageFlags,
    image_type: vk::ImageType,
    extent: vk::Extent3D,
    mip_levels: u32,
    format: vk::Format,
    inner: vk::Image,
    device: Arc<Device>,
}

impl RawImage {
    /// Creates a new image.
    pub fn new(
        device: Arc<Device>,
        format: vk::Format,
        extent: vk::Extent3D,
        mip_levels: u32,
        usage: vk::ImageUsageFlags,
    ) -> Result<RawImage, ImageCreationError> {
        let (image_type, create_flags) = match extent {
            vk::Extent3D {
                height: 1,
                depth: 1,
                ..
            } => (vk::ImageType::TYPE_1D, vk::ImageCreateFlags::empty()),
            vk::Extent3D { depth: 1, .. } => {
                (vk::ImageType::TYPE_2D, vk::ImageCreateFlags::empty())
            }
            _ => (
                vk::ImageType::TYPE_3D,
                vk::ImageCreateFlags::TYPE_2D_ARRAY_COMPATIBLE
                    | vk::ImageCreateFlags::MUTABLE_FORMAT,
            ),
        };
        let create_info = vk::ImageCreateInfo::builder()
            .image_type(image_type)
            .flags(create_flags)
            .extent(extent)
            .mip_levels(mip_levels)
            .array_layers(1)
            .format(format)
            .tiling(vk::ImageTiling::OPTIMAL)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .usage(usage)
            .sharing_mode(vk::SharingMode::EXCLUSIVE)
            .samples(vk::SampleCountFlags::TYPE_1);
        let inner = unsafe { device.create_image(&create_info, None) }?;
        Ok(Self {
            usage,
            image_type,
            extent,
            mip_levels,
            format,
            inner,
            device,
        })
    }
}

impl Image for RawImage {
    fn image_type(&self) -> ImageType {
        self.image_type
    }

    fn extent(&self) -> vk::Extent3D {
        self.extent
    }

    fn mip_levels(&self) -> u32 {
        self.mip_levels
    }

    fn device(&self) -> &Arc<Device> {
        &self.device
    }

    fn inner(&self) -> vk::Image {
        self.inner
    }

    fn format(&self) -> vk::Format {
        self.format
    }

    fn usage(&self) -> ImageUsageFlags {
        self.usage
    }
}

/// Error that can occur during image creation.
#[derive(Debug)]
pub enum ImageCreationError {
    /// Buffer creation has failed.
    BufferCreationFailed(BufferCreationError),
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A host memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// Memory allocation has failed.
    AllocationFailed(AllocationError),
    /// Binding memory to image has failed.
    BindMemoryFailed(BindMemoryError),
    /// Command buffer creation has failed.
    CommandBufferCreationFailed(CommandBufferCreationError),
    /// Command buffer submission has failed.
    SubmitFailed(SubmitError),
    /// Waiting for the queue to become idle has failed.
    WaitIdleFailed(WaitIdleError),
}

impl Display for ImageCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            ImageCreationError::BufferCreationFailed(e) => Display::fmt(e, f),
            ImageCreationError::OutOfHostMemory(e) | ImageCreationError::OutOfDeviceMemory(e) => {
                Display::fmt(e, f)
            }
            ImageCreationError::AllocationFailed(e) => Display::fmt(e, f),
            ImageCreationError::BindMemoryFailed(e) => Display::fmt(e, f),
            ImageCreationError::CommandBufferCreationFailed(e) => Display::fmt(e, f),
            ImageCreationError::SubmitFailed(e) => Display::fmt(e, f),
            ImageCreationError::WaitIdleFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for ImageCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            ImageCreationError::BufferCreationFailed(e) => Some(e),
            ImageCreationError::AllocationFailed(e) => Some(e),
            ImageCreationError::BindMemoryFailed(e) => Some(e),
            ImageCreationError::CommandBufferCreationFailed(e) => Some(e),
            ImageCreationError::SubmitFailed(e) => Some(e),
            ImageCreationError::WaitIdleFailed(e) => Some(e),
            _ => None,
        }
    }
}

impl From<BufferCreationError> for ImageCreationError {
    fn from(e: BufferCreationError) -> Self {
        ImageCreationError::BufferCreationFailed(e)
    }
}

impl From<VulkanError> for ImageCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => ImageCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => ImageCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown create image error"),
        }
    }
}

impl From<vk::Result> for ImageCreationError {
    fn from(result: vk::Result) -> Self {
        ImageCreationError::from(VulkanError::from(result))
    }
}

impl From<AllocationError> for ImageCreationError {
    fn from(e: AllocationError) -> Self {
        ImageCreationError::AllocationFailed(e)
    }
}

impl From<BindMemoryError> for ImageCreationError {
    fn from(e: BindMemoryError) -> Self {
        ImageCreationError::BindMemoryFailed(e)
    }
}

impl From<CommandBufferCreationError> for ImageCreationError {
    fn from(e: CommandBufferCreationError) -> Self {
        ImageCreationError::CommandBufferCreationFailed(e)
    }
}

impl From<SubmitError> for ImageCreationError {
    fn from(e: SubmitError) -> Self {
        ImageCreationError::SubmitFailed(e)
    }
}

impl From<WaitIdleError> for ImageCreationError {
    fn from(e: WaitIdleError) -> Self {
        ImageCreationError::WaitIdleFailed(e)
    }
}

impl Drop for RawImage {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_image(self.inner, None);
        }
    }
}

/// A view to an image.
#[derive(Debug)]
pub struct ImageView<I>
where
    I: Image,
{
    inner: vk::ImageView,
    image: I,
}

impl<I> ImageView<I>
where
    I: Image,
{
    /// Creates a new image view for an image.
    pub fn new(
        image: I,
        view_type: vk::ImageViewType,
        format: vk::Format,
        components: vk::ComponentMapping,
        range: vk::ImageSubresourceRange,
    ) -> Result<Arc<Self>, ImageViewCreationError> {
        let create_info = vk::ImageViewCreateInfo::builder()
            .image(image.inner())
            .view_type(view_type)
            .format(format)
            .components(components)
            .subresource_range(range)
            .build();
        let inner = unsafe { image.device().create_image_view(&create_info, None) }?;
        Ok(Arc::new(Self { inner, image }))
    }

    /// Crates a new view for an image.
    pub fn from_image(image: I) -> Result<Arc<Self>, ImageViewCreationError> {
        let view_type = match image.image_type() {
            vk::ImageType::TYPE_1D => vk::ImageViewType::TYPE_1D,
            vk::ImageType::TYPE_2D => vk::ImageViewType::TYPE_2D,
            vk::ImageType::TYPE_3D => vk::ImageViewType::TYPE_3D,
            _ => unreachable!("Unknown image type"),
        };
        let create_info = vk::ImageViewCreateInfo::builder()
            .image(image.inner())
            .view_type(view_type)
            .format(image.format())
            .components(
                vk::ComponentMapping::builder()
                    .r(vk::ComponentSwizzle::R)
                    .g(vk::ComponentSwizzle::G)
                    .b(vk::ComponentSwizzle::B)
                    .a(vk::ComponentSwizzle::A)
                    .build(),
            )
            .subresource_range(
                vk::ImageSubresourceRange::builder()
                    .aspect_mask(if image.format() == vk::Format::D32_SFLOAT {
                        vk::ImageAspectFlags::DEPTH
                    } else {
                        vk::ImageAspectFlags::COLOR
                    })
                    .base_mip_level(0)
                    .level_count(image.mip_levels())
                    .base_array_layer(0)
                    .layer_count(1)
                    .build(),
            )
            .build();
        let inner = unsafe { image.device().create_image_view(&create_info, None) }?;
        Ok(Arc::new(Self { inner, image }))
    }

    /// Returns the vulkan image view.
    pub fn inner(&self) -> vk::ImageView {
        self.inner
    }

    /// Returns the image.
    pub fn image(&self) -> &I {
        &self.image
    }
}

impl<I> Drop for ImageView<I>
where
    I: Image,
{
    fn drop(&mut self) {
        unsafe { self.image.device().destroy_image_view(self.inner, None) }
    }
}

impl<I> ImageViewAbstract for ImageView<I>
where
    I: Image,
{
    fn inner(&self) -> vk::ImageView {
        self.inner
    }

    fn image(&self) -> &dyn Image {
        &self.image
    }
}

/// Error that can occur during image view creation.
#[derive(Debug)]
pub enum ImageViewCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for ImageViewCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            ImageViewCreationError::OutOfHostMemory(e)
            | ImageViewCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for ImageViewCreationError {}

impl From<VulkanError> for ImageViewCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => ImageViewCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => ImageViewCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown create image view error"),
        }
    }
}

impl From<vk::Result> for ImageViewCreationError {
    fn from(result: vk::Result) -> Self {
        ImageViewCreationError::from(VulkanError::from(result))
    }
}

/// Trait for using `ImageView`s as trait objects.
pub trait ImageViewAbstract {
    /// Returns the vulkan image view.
    fn inner(&self) -> vk::ImageView;

    /// Returns the image.
    fn image(&self) -> &dyn Image;
}

/// Trait for an image.
pub trait Image {
    /// Returns the image type.
    fn image_type(&self) -> vk::ImageType;
    /// Returns the extent of the image.
    fn extent(&self) -> vk::Extent3D;
    /// Returns the mip levels of the image.
    fn mip_levels(&self) -> u32;
    /// Returns the device which owns the image.
    fn device(&self) -> &Arc<Device>;
    /// Returns the vulkan image handle.
    fn inner(&self) -> vk::Image;
    /// Returns the format of the image.
    fn format(&self) -> vk::Format;
    /// Returns the usage.
    fn usage(&self) -> vk::ImageUsageFlags;
}

impl<T> Image for T
where
    T: Deref,
    T::Target: Image,
{
    fn image_type(&self) -> ImageType {
        self.deref().image_type()
    }

    fn extent(&self) -> vk::Extent3D {
        self.deref().extent()
    }

    fn mip_levels(&self) -> u32 {
        self.deref().mip_levels()
    }

    fn device(&self) -> &Arc<Device> {
        self.deref().device()
    }

    fn inner(&self) -> vk::Image {
        self.deref().inner()
    }

    fn format(&self) -> vk::Format {
        self.deref().format()
    }

    fn usage(&self) -> ImageUsageFlags {
        self.deref().usage()
    }
}

/// A sampler.
pub struct Sampler {
    inner: vk::Sampler,
    device: Arc<Device>,
}

impl Sampler {
    /// Starts building a new sampler.
    pub fn builder() -> SamplerBuilder {
        SamplerBuilder { max_lod: None }
    }

    /// Returns the vulkan handle of the sampler.
    pub fn inner(&self) -> vk::Sampler {
        self.inner
    }
}

impl Drop for Sampler {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_sampler(self.inner, None);
        }
    }
}

/// Builder for creating a `Sampler`.
pub struct SamplerBuilder {
    max_lod: Option<f32>,
}

impl SamplerBuilder {
    /// Sets the maximum lod for the sampler.
    pub fn max_lod(mut self, max_lod: f32) -> Self {
        self.max_lod = Some(max_lod);
        self
    }

    /// Builds the sampler.
    pub fn build(self, device: Arc<Device>) -> Result<Arc<Sampler>, SamplerCreationError> {
        let create_info = vk::SamplerCreateInfo::builder()
            .mag_filter(vk::Filter::LINEAR)
            .min_filter(vk::Filter::LINEAR)
            .address_mode_u(vk::SamplerAddressMode::REPEAT)
            .address_mode_v(vk::SamplerAddressMode::REPEAT)
            .address_mode_w(vk::SamplerAddressMode::REPEAT)
            .anisotropy_enable(true)
            .max_anisotropy(
                device
                    .physical_device()
                    .properties()
                    .limits
                    .max_sampler_anisotropy,
            )
            .border_color(vk::BorderColor::INT_OPAQUE_BLACK)
            .unnormalized_coordinates(false)
            .compare_enable(false)
            .compare_op(vk::CompareOp::ALWAYS)
            .mipmap_mode(vk::SamplerMipmapMode::LINEAR)
            .mip_lod_bias(0.0)
            .min_lod(0.0)
            .max_lod(self.max_lod.unwrap_or(0.0));
        let inner = unsafe { device.create_sampler(&create_info, None) }?;
        Ok(Arc::new(Sampler { inner, device }))
    }
}

/// Error that can occur during sampler creation.
#[derive(Debug)]
pub enum SamplerCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for SamplerCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            SamplerCreationError::OutOfHostMemory(e)
            | SamplerCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for SamplerCreationError {}

impl From<VulkanError> for SamplerCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => SamplerCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => SamplerCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown create image view error"),
        }
    }
}

impl From<vk::Result> for SamplerCreationError {
    fn from(result: vk::Result) -> Self {
        SamplerCreationError::from(VulkanError::from(result))
    }
}
