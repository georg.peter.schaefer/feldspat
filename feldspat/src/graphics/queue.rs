//! Queues.

use crate::graphics::command_buffer::CommandBuffer;
use crate::graphics::swapchain::{PresentError, Swapchain};
use crate::graphics::{Fence, Semaphore, VulkanError};
use ash::vk;
use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::{Debug, Display, Formatter};
use std::sync::{Arc, Mutex};

/// A queue.
pub struct Queue {
    submitted: RefCell<HashMap<Arc<Fence>, Arc<CommandBuffer>>>,
    family: u32,
    inner: Arc<Mutex<vk::Queue>>,
    device: ash::Device,
}

impl Queue {
    pub(super) fn new(device: ash::Device, inner: vk::Queue, family: u32) -> Arc<Self> {
        Arc::new(Self {
            submitted: RefCell::new(HashMap::new()),
            family,
            inner: Arc::new(Mutex::new(inner)),
            device,
        })
    }

    /// Submits a command buffer to a queue.
    pub fn submit(
        &self,
        command_buffer: Arc<CommandBuffer>,
        wait_semaphore: Vec<&Semaphore>,
        wait_stage: Vec<vk::PipelineStageFlags>,
        signal_semaphore: Option<&Semaphore>,
        fence: Option<Arc<Fence>>,
    ) -> Result<(), SubmitError> {
        let command_buffers = [command_buffer.inner()];
        let wait_semaphores = wait_semaphore
            .iter()
            .map(|semaphore| semaphore.inner())
            .collect::<Vec<_>>();
        let wait_stages = wait_stage.iter().cloned().collect::<Vec<_>>();
        let signal_semaphore = signal_semaphore
            .iter()
            .map(|semaphore| semaphore.inner())
            .collect::<Vec<_>>();
        let submits = [vk::SubmitInfo::builder()
            .command_buffers(&command_buffers)
            .wait_semaphores(&wait_semaphores)
            .wait_dst_stage_mask(&wait_stages)
            .signal_semaphores(&signal_semaphore)
            .build()];
        let fence = fence.unwrap_or(
            Fence::new(
                command_buffer.device().clone(),
                vk::FenceCreateFlags::empty(),
            )
            .unwrap(),
        );
        let inner = self.inner.lock().unwrap();
        self.submitted
            .borrow_mut()
            .insert(fence.clone(), command_buffer.clone());
        self.remove_executed_command_buffers();
        Ok(unsafe { self.device.queue_submit(*inner, &submits, fence.inner()) }?)
    }

    fn remove_executed_command_buffers(&self) {
        self.submitted
            .borrow_mut()
            .retain(|fence, _| !fence.is_signaled());
    }

    pub(crate) fn clear_command_buffers(&self) {
        let _guard = self.inner.lock().unwrap();
        self.submitted.borrow_mut().clear();
    }

    /// Presents an image to the swapchain.
    pub fn present(
        &self,
        swapchain: &Swapchain,
        image_index: u32,
        wait_semaphore: &Semaphore,
    ) -> Result<bool, PresentError> {
        let swapchains = [swapchain.inner()];
        let image_indices = [image_index];
        let wait_semaphores = [wait_semaphore.inner()];
        let present_info = vk::PresentInfoKHR::builder()
            .swapchains(&swapchains)
            .image_indices(&image_indices)
            .wait_semaphores(&wait_semaphores);
        swapchain.present(&self, &present_info)
    }

    /// Waits until the queue has become idle.
    pub fn wait_idle(&self) -> Result<(), WaitIdleError> {
        let inner = self.inner.lock().unwrap();
        unsafe { self.device.queue_wait_idle(*inner) }?;
        Ok(())
    }

    /// Returns the family index of the queue.
    pub fn family(&self) -> u32 {
        self.family
    }

    /// Returns the vulkan handle of the queue.
    pub fn inner(&self) -> &Arc<Mutex<vk::Queue>> {
        &self.inner
    }
}

unsafe impl Send for Queue {}

unsafe impl Sync for Queue {}

impl Debug for Queue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Queue")
    }
}

/// Error that can occur when submitting commands to a queue.
#[derive(Debug)]
pub enum SubmitError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// The logical or physical device has been lost.
    DeviceLost(VulkanError),
}

impl Display for SubmitError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            SubmitError::OutOfHostMemory(e)
            | SubmitError::OutOfDeviceMemory(e)
            | SubmitError::DeviceLost(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for SubmitError {}

impl From<VulkanError> for SubmitError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => SubmitError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => SubmitError::OutOfDeviceMemory(e),
            VulkanError::DeviceLost => SubmitError::DeviceLost(e),
            _ => unreachable!("Unknown wait for fence error"),
        }
    }
}

impl From<vk::Result> for SubmitError {
    fn from(result: vk::Result) -> Self {
        SubmitError::from(VulkanError::from(result))
    }
}

/// Error that can occur when waiting for a queue to become idle.
#[derive(Debug)]
pub enum WaitIdleError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// The logical or physical device has been lost.
    DeviceLost(VulkanError),
}

impl Display for WaitIdleError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            WaitIdleError::OutOfHostMemory(e)
            | WaitIdleError::OutOfDeviceMemory(e)
            | WaitIdleError::DeviceLost(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for WaitIdleError {}

impl From<VulkanError> for WaitIdleError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => WaitIdleError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => WaitIdleError::OutOfDeviceMemory(e),
            VulkanError::DeviceLost => WaitIdleError::DeviceLost(e),
            _ => unreachable!("Unknown device wait idle error"),
        }
    }
}

impl From<vk::Result> for WaitIdleError {
    fn from(result: vk::Result) -> Self {
        WaitIdleError::from(VulkanError::from(result))
    }
}
