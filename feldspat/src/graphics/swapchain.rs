//! Types for swapchain creation.

use std::cell::UnsafeCell;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::sync::Arc;

use ash::extensions::khr;
use ash::vk;
use ash::vk::{
    ColorSpaceKHR, CompositeAlphaFlagsKHR, Extent2D, Extent3D, Format, ImageType, ImageUsageFlags,
    PresentModeKHR, SharingMode, SurfaceFormatKHR, SwapchainCreateInfoKHR, SwapchainKHR,
};

use crate::graphics::device::{Device, WaitIdleError};
use crate::graphics::image::{Image, ImageView, ImageViewCreationError};
use crate::graphics::window::{Surface, Window};
use crate::graphics::{Queue, Semaphore, VulkanError};

/// The swapchain.
pub struct Swapchain {
    recreate_pending: UnsafeCell<bool>,
    recreation_callback:
        UnsafeCell<Vec<Box<dyn FnMut(&Swapchain) -> Result<(), Box<dyn std::error::Error>>>>>,
    inner: UnsafeCell<Option<InnerSwapchain>>,
    window: Arc<Window>,
    device: Arc<Device>,
}

impl Swapchain {
    pub(super) fn new(
        device: Arc<Device>,
        window: Arc<Window>,
    ) -> Result<Self, SwapchainCreationError> {
        Ok(Swapchain {
            recreate_pending: UnsafeCell::new(false),
            recreation_callback: UnsafeCell::new(Vec::new()),
            inner: UnsafeCell::new(Some(InnerSwapchain::new(device.clone(), &window)?)),
            window,
            device,
        })
    }

    /// Acquires the next image to render to.
    pub fn acquire_next_image(
        &self,
        semaphore: &Semaphore,
    ) -> Result<(u32, bool), AcquireNextImageError> {
        Ok(unsafe {
            self.get().loader.acquire_next_image(
                self.get().inner,
                u64::MAX,
                semaphore.inner(),
                vk::Fence::null(),
            )
        }?)
    }

    /// Presents the current image.
    pub fn present(
        &self,
        queue: &Queue,
        present_info: &vk::PresentInfoKHR,
    ) -> Result<bool, PresentError> {
        let queue = queue.inner().lock().unwrap();
        Ok(unsafe { self.get().loader.queue_present(*queue, &present_info) }?)
    }

    /// Marks the swapchain for recreation.
    pub unsafe fn recreate(&self) {
        *self.recreate_pending.get() = true;
    }

    /// Recreates the swapchain if needed.
    pub unsafe fn handle_recreate(&self) -> Result<(), Box<dyn std::error::Error>> {
        let recreate_pending = &mut *self.recreate_pending.get();
        if *recreate_pending {
            self.device.wait_idle()?;
            *self.inner.get() = None;
            *self.inner.get() = Some(InnerSwapchain::new(self.device.clone(), &self.window)?);
            for callback in &mut *self.recreation_callback.get() {
                callback(&self)?;
            }
            *recreate_pending = false;
        }
        Ok(())
    }

    /// Registers a recreation call back.
    pub unsafe fn register_recreate_callback<F>(&self, callback: F)
    where
        F: FnMut(&Swapchain) -> Result<(), Box<dyn std::error::Error>> + 'static,
    {
        (*self.recreation_callback.get()).push(Box::new(callback))
    }

    /// Clears the recreation callbacks.
    pub unsafe fn clean_up(&self) {
        (*self.recreation_callback.get()).clear();
    }

    /// Returns the swapchain extent.
    pub fn extent(&self) -> vk::Extent2D {
        self.get().extent
    }

    /// Returns the swapchain format.
    pub fn format(&self) -> vk::Format {
        self.get().format
    }

    /// Returns the swapchain image views.
    pub fn image_views(&self) -> &Vec<Arc<ImageView<Arc<SwapchainImage>>>> {
        &self.get().image_views
    }

    /// Returns the swapchain images.
    pub fn images(&self) -> &Vec<Arc<SwapchainImage>> {
        &self.get().images
    }

    /// Returns the vulkan handle of the swapchain.
    pub fn inner(&self) -> vk::SwapchainKHR {
        self.get().inner
    }

    fn get(&self) -> &InnerSwapchain {
        unsafe { &*self.inner.get() }.as_ref().unwrap()
    }
}

unsafe impl Sync for Swapchain {}

unsafe impl Send for Swapchain {}

struct InnerSwapchain {
    extent: Extent2D,
    format: vk::Format,
    image_views: Vec<Arc<ImageView<Arc<SwapchainImage>>>>,
    images: Vec<Arc<SwapchainImage>>,
    inner: SwapchainKHR,
    loader: khr::Swapchain,
}

impl InnerSwapchain {
    pub(super) fn new(
        device: Arc<Device>,
        window: &Window,
    ) -> Result<Self, SwapchainCreationError> {
        let surface = window.surface();
        let image_count = Self::calculate_image_count(&device, surface);
        let surface_format = Self::select_surface_format(&device, surface);
        let extent = Self::select_swapchain_extent(&device, &window);
        let sharing_mode = Self::select_sharing_mode(&device);
        let present_mode = Self::select_present_mode(&device, surface);
        let create_info = SwapchainCreateInfoKHR::builder()
            .surface(surface.inner())
            .min_image_count(image_count)
            .image_format(surface_format.format)
            .image_color_space(surface_format.color_space)
            .image_extent(extent)
            .image_array_layers(1)
            .image_usage(ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::TRANSFER_DST)
            .queue_family_indices(&sharing_mode.1)
            .pre_transform(
                surface
                    .capabilities(device.physical_device())
                    .current_transform,
            )
            .composite_alpha(CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(present_mode)
            .clipped(true)
            .build();

        let loader = khr::Swapchain::new(device.instance(), &device);
        let inner = unsafe { loader.create_swapchain(&create_info, None) }?;
        let images: Vec<_> = unsafe { loader.get_swapchain_images(inner) }?
            .iter()
            .map(|image| {
                SwapchainImage::new(
                    device.clone(),
                    *image,
                    ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::TRANSFER_DST,
                    extent,
                    surface_format.format,
                )
            })
            .collect();
        let image_views = Self::create_image_views(&images)?;

        Ok(Self {
            extent,
            format: surface_format.format,
            image_views,
            images,
            inner,
            loader,
        })
    }

    fn calculate_image_count(device: &Device, surface: &Surface) -> u32 {
        let capabilities = surface.capabilities(device.physical_device());
        let mut image_count = capabilities.min_image_count + 1;
        if capabilities.max_image_count > 0 && image_count > capabilities.max_image_count {
            image_count = capabilities.max_image_count
        }
        image_count
    }

    fn select_surface_format(device: &Device, surface: &Surface) -> SurfaceFormatKHR {
        let supported_formats = surface.formats(device.physical_device());
        supported_formats
            .iter()
            .find(|surface_format| {
                surface_format.format == Format::B8G8R8A8_SRGB
                    && surface_format.color_space == ColorSpaceKHR::SRGB_NONLINEAR
            })
            .cloned()
            .unwrap_or(supported_formats[0])
    }

    fn select_swapchain_extent(device: &Device, window: &Window) -> Extent2D {
        let surface = window.surface();
        let resolution = window.resolution();
        let capabilities = surface.capabilities(device.physical_device());
        if capabilities.current_extent.width != u32::MAX {
            capabilities.current_extent
        } else {
            Extent2D {
                width: resolution[0].clamp(
                    capabilities.min_image_extent.width,
                    capabilities.max_image_extent.width,
                ),
                height: resolution[1].clamp(
                    capabilities.min_image_extent.height,
                    capabilities.max_image_extent.height,
                ),
            }
        }
    }

    fn select_sharing_mode(device: &Device) -> (SharingMode, Vec<u32>) {
        let queue_family_indices = device.physical_device().queue_family_indices();
        if queue_family_indices.graphics() != queue_family_indices.present() {
            (
                SharingMode::CONCURRENT,
                vec![
                    queue_family_indices.graphics(),
                    queue_family_indices.present(),
                ],
            )
        } else {
            (SharingMode::CONCURRENT, vec![])
        }
    }

    fn select_present_mode(device: &Device, surface: &Surface) -> PresentModeKHR {
        surface
            .present_modes(device.physical_device())
            .iter()
            .find(|present_mode| **present_mode == PresentModeKHR::MAILBOX)
            .cloned()
            .unwrap_or(PresentModeKHR::FIFO)
    }

    fn create_image_views(
        images: &Vec<Arc<SwapchainImage>>,
    ) -> Result<Vec<Arc<ImageView<Arc<SwapchainImage>>>>, SwapchainCreationError> {
        let mut image_views = Vec::with_capacity(images.len());
        for image in images {
            image_views.push(ImageView::from_image(image.clone())?);
        }
        Ok(image_views)
    }
}

impl Drop for InnerSwapchain {
    fn drop(&mut self) {
        unsafe {
            self.loader.destroy_swapchain(self.inner, None);
        }
    }
}

/// A swapchain image.
pub struct SwapchainImage {
    usage: vk::ImageUsageFlags,
    format: vk::Format,
    extent: vk::Extent2D,
    inner: vk::Image,
    device: Arc<Device>,
}

impl SwapchainImage {
    fn new(
        device: Arc<Device>,
        inner: vk::Image,
        usage: vk::ImageUsageFlags,
        extent: vk::Extent2D,
        format: vk::Format,
    ) -> Arc<Self> {
        Arc::new(Self {
            usage,
            format,
            extent,
            inner,
            device,
        })
    }
}

impl Image for SwapchainImage {
    fn image_type(&self) -> ImageType {
        vk::ImageType::TYPE_2D
    }

    fn extent(&self) -> Extent3D {
        vk::Extent3D::builder()
            .width(self.extent.width)
            .height(self.extent.height)
            .depth(1)
            .build()
    }

    fn mip_levels(&self) -> u32 {
        1
    }

    fn device(&self) -> &Arc<Device> {
        &self.device
    }

    fn inner(&self) -> vk::Image {
        self.inner
    }

    fn format(&self) -> Format {
        self.format
    }

    fn usage(&self) -> ImageUsageFlags {
        self.usage
    }
}

/// Error that can occur during swapchain creation.
#[derive(Debug)]
pub enum SwapchainCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// The logical or physical device has been lost.
    DeviceLost(VulkanError),
    /// A surface is no longer available.
    SurfaceLost(VulkanError),
    /// The requested window is already in use by Vulkan or another API in a manner which prevents it from being used again.
    NativeWindowInUse(VulkanError),
    /// Initialization of an object could not be completed for implementation-specific reasons.
    InitializationFailed(VulkanError),
    /// Image view creation for the swapchain images has failed.
    ImageViewCreationFailed(ImageViewCreationError),
    /// Wait for the device to become idle has failed.
    WaitIdleFailed(WaitIdleError),
}

impl Display for SwapchainCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            SwapchainCreationError::OutOfHostMemory(e)
            | SwapchainCreationError::OutOfDeviceMemory(e)
            | SwapchainCreationError::DeviceLost(e)
            | SwapchainCreationError::SurfaceLost(e)
            | SwapchainCreationError::NativeWindowInUse(e)
            | SwapchainCreationError::InitializationFailed(e) => Display::fmt(e, f),
            SwapchainCreationError::ImageViewCreationFailed(e) => Display::fmt(e, f),
            SwapchainCreationError::WaitIdleFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for SwapchainCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            SwapchainCreationError::ImageViewCreationFailed(e) => Some(e),
            SwapchainCreationError::WaitIdleFailed(e) => Some(e),
            _ => None,
        }
    }
}

impl From<VulkanError> for SwapchainCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => SwapchainCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => SwapchainCreationError::OutOfDeviceMemory(e),
            VulkanError::InitializationFailed => SwapchainCreationError::InitializationFailed(e),
            VulkanError::DeviceLost => SwapchainCreationError::DeviceLost(e),
            VulkanError::SurfaceLost => SwapchainCreationError::SurfaceLost(e),
            VulkanError::NativeWindowInUse => SwapchainCreationError::NativeWindowInUse(e),
            _ => unreachable!("Unknown vkCreateSwapchainKHR error"),
        }
    }
}

impl From<vk::Result> for SwapchainCreationError {
    fn from(result: vk::Result) -> Self {
        SwapchainCreationError::from(VulkanError::from(result))
    }
}

impl From<ImageViewCreationError> for SwapchainCreationError {
    fn from(e: ImageViewCreationError) -> Self {
        SwapchainCreationError::ImageViewCreationFailed(e)
    }
}

impl From<WaitIdleError> for SwapchainCreationError {
    fn from(e: WaitIdleError) -> Self {
        SwapchainCreationError::WaitIdleFailed(e)
    }
}

/// Error that can occur during acquiring the next image.
#[derive(Debug)]
pub enum AcquireNextImageError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// The logical or physical device has been lost.
    DeviceLost(VulkanError),
    /// A surface is no longer available.
    SurfaceLost(VulkanError),
    /// A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail.
    OutOfDate(VulkanError),
    /// An operation on a swapchain created with VK_FULL_SCREEN_EXCLUSIVE_APPLICATION_CONTROLLED_EXT failed as it did not have exclusive full-screen access.
    FullScreenExclusiveModeLost(VulkanError),
}

impl Display for AcquireNextImageError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            AcquireNextImageError::OutOfHostMemory(e)
            | AcquireNextImageError::OutOfDeviceMemory(e)
            | AcquireNextImageError::DeviceLost(e)
            | AcquireNextImageError::OutOfDate(e)
            | AcquireNextImageError::SurfaceLost(e)
            | AcquireNextImageError::FullScreenExclusiveModeLost(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for AcquireNextImageError {}

impl From<VulkanError> for AcquireNextImageError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => AcquireNextImageError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => AcquireNextImageError::OutOfDeviceMemory(e),
            VulkanError::DeviceLost => AcquireNextImageError::DeviceLost(e),
            VulkanError::OutOfDate => AcquireNextImageError::OutOfDate(e),
            VulkanError::SurfaceLost => AcquireNextImageError::SurfaceLost(e),
            VulkanError::FullScreenExclusiveModeLost => {
                AcquireNextImageError::FullScreenExclusiveModeLost(e)
            }
            _ => unreachable!("Unknown acquire next image error"),
        }
    }
}

impl From<vk::Result> for AcquireNextImageError {
    fn from(result: vk::Result) -> Self {
        AcquireNextImageError::from(VulkanError::from(result))
    }
}

/// Error that can occur during presentation.
#[derive(Debug)]
pub enum PresentError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// The logical or physical device has been lost.
    DeviceLost(VulkanError),
    /// A surface is no longer available.
    SurfaceLost(VulkanError),
    /// A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail.
    OutOfDate(VulkanError),
    /// An operation on a swapchain created with VK_FULL_SCREEN_EXCLUSIVE_APPLICATION_CONTROLLED_EXT failed as it did not have exclusive full-screen access.
    FullScreenExclusiveModeLost(VulkanError),
}

impl Display for PresentError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            PresentError::OutOfHostMemory(e)
            | PresentError::OutOfDeviceMemory(e)
            | PresentError::DeviceLost(e)
            | PresentError::OutOfDate(e)
            | PresentError::SurfaceLost(e)
            | PresentError::FullScreenExclusiveModeLost(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for PresentError {}

impl From<VulkanError> for PresentError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => PresentError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => PresentError::OutOfDeviceMemory(e),
            VulkanError::DeviceLost => PresentError::DeviceLost(e),
            VulkanError::OutOfDate => PresentError::OutOfDate(e),
            VulkanError::SurfaceLost => PresentError::SurfaceLost(e),
            VulkanError::FullScreenExclusiveModeLost => {
                PresentError::FullScreenExclusiveModeLost(e)
            }
            _ => unreachable!("Unknown acquire next image error"),
        }
    }
}

impl From<vk::Result> for PresentError {
    fn from(result: vk::Result) -> Self {
        PresentError::from(VulkanError::from(result))
    }
}
