//! Types for render pass creation.

use std::fmt::{Display, Formatter};
use std::sync::Arc;

use ash::vk;

use crate::graphics::{Device, VulkanError};

/// A render pass.
pub struct RenderPass {
    subpasses: Vec<vk::SubpassDescription>,
    inner: vk::RenderPass,
    device: Arc<Device>,
}

impl RenderPass {
    /// Creates a new render pass.
    pub fn new(
        device: Arc<Device>,
        attachments: Vec<vk::AttachmentDescription>,
        subpasses: Vec<SubpassDescription>,
        dependencies: Vec<vk::SubpassDependency>,
    ) -> Result<Arc<Self>, RenderPassCreationError> {
        let subpasses: Vec<_> = subpasses
            .iter()
            .map(|subpass| {
                let mut builder = vk::SubpassDescription::builder()
                    .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS);
                if let Some(depth_stencil_attachment) = &subpass.depth_attachment {
                    builder = builder.depth_stencil_attachment(depth_stencil_attachment);
                }
                builder
                    .color_attachments(&subpass.color_attachments)
                    .input_attachments(&subpass.input_attachments)
                    .build()
            })
            .collect();
        let create_info = vk::RenderPassCreateInfo::builder()
            .attachments(&attachments)
            .subpasses(&subpasses)
            .dependencies(&dependencies)
            .build();
        let inner = unsafe { device.create_render_pass(&create_info, None) }?;
        Ok(Arc::new(Self {
            subpasses,
            inner,
            device: device.clone(),
        }))
    }

    /// Returns the subpass description.
    pub fn subpass(&self, subpass: usize) -> vk::SubpassDescription {
        self.subpasses[subpass]
    }

    /// Returns the vulkan render pass.
    pub fn inner(&self) -> vk::RenderPass {
        self.inner
    }

    /// Returns the owning device.
    pub fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

impl Drop for RenderPass {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_render_pass(self.inner, None);
        }
    }
}

unsafe impl Sync for RenderPass {}

unsafe impl Send for RenderPass {}

/// Describes the dependencies of a subpass.
pub struct SubpassDescription {
    /// The color attachments the subpass writes to.
    pub color_attachments: Vec<vk::AttachmentReference>,
    /// The depth attachemnt the subpass writes to.
    pub depth_attachment: Option<vk::AttachmentReference>,
    /// The input attachments the subpass uses.
    pub input_attachments: Vec<vk::AttachmentReference>,
}

/// Error that can occur during render pass creation.
#[derive(Debug)]
pub enum RenderPassCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for RenderPassCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            RenderPassCreationError::OutOfHostMemory(e)
            | RenderPassCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for RenderPassCreationError {}

impl From<VulkanError> for RenderPassCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => RenderPassCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => RenderPassCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown pipeline layout creation error"),
        }
    }
}

impl From<vk::Result> for RenderPassCreationError {
    fn from(result: vk::Result) -> Self {
        RenderPassCreationError::from(VulkanError::from(result))
    }
}
