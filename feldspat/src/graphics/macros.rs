//! Helper macros.

/// Implements the trait `Vertex` for the given type.
#[macro_export]
macro_rules! vertex {
    ($vt:ty $(, $field:ident)*) => {
        impl $crate::graphics::pipeline::Vertex for $vt {
            #[allow(unused_assignments)]
            fn attributes() -> Vec<ash::vk::VertexInputAttributeDescription> {
                fn format<T: $crate::graphics::pipeline::VertexField>(_: &T) -> ash::vk::Format {
                    T::format()
                }

                fn size<T: $crate::graphics::pipeline::VertexField>(_: &T) -> usize {
                    T::size()
                }

                let mut attributes = Vec::new();
                let mut location = 0;
                let mut offset = 0;
                let dummy = <$vt>::default();
                $(
                    attributes.push(
                        ash::vk::VertexInputAttributeDescription::builder()
                            .binding(0)
                            .location(location)
                            .format(format(&dummy.$field))
                            .offset(offset)
                            .build()
                    );
                    location += 1;
                    offset += size(&dummy.$field) as u32;
                )*
                attributes
            }

            fn size() -> usize {
                fn _size<T: $crate::graphics::pipeline::VertexField>(_: &T) -> usize {
                    T::size()
                }

                let mut size = 0;
                let dummy = <$vt>::default();
                $(
                    size += _size(&dummy.$field);
                )*
                size
            }
        }
    };
}
