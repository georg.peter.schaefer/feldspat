//! Types for synchronization.

use std::fmt::{Display, Formatter};
use std::hash::{Hash, Hasher};
use std::sync::Arc;

use ash::vk;

use crate::graphics::{Device, VulkanError};

/// A semaphore.
pub struct Semaphore {
    inner: vk::Semaphore,
    device: Arc<Device>,
}

impl Semaphore {
    /// Creates a new semaphore.
    pub fn new(device: Arc<Device>) -> Result<Self, SemaphoreCreationError> {
        Ok(Self {
            inner: unsafe { device.create_semaphore(&vk::SemaphoreCreateInfo::builder(), None) }?,
            device,
        })
    }

    /// Returns the vulkan handle to the semaphore.
    pub fn inner(&self) -> vk::Semaphore {
        self.inner
    }
}

impl Drop for Semaphore {
    fn drop(&mut self) {
        unsafe { self.device.destroy_semaphore(self.inner, None) }
    }
}

/// Error that can occur during semaphore creation.
#[derive(Debug)]
pub enum SemaphoreCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for SemaphoreCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            SemaphoreCreationError::OutOfHostMemory(e)
            | SemaphoreCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for SemaphoreCreationError {}

impl From<VulkanError> for SemaphoreCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => SemaphoreCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => SemaphoreCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown semaphore creation error"),
        }
    }
}

impl From<vk::Result> for SemaphoreCreationError {
    fn from(result: vk::Result) -> Self {
        SemaphoreCreationError::from(VulkanError::from(result))
    }
}

/// A fence.
pub struct Fence {
    inner: vk::Fence,
    device: Arc<Device>,
}

impl Fence {
    /// Creates a new fence.
    pub fn new(
        device: Arc<Device>,
        flags: vk::FenceCreateFlags,
    ) -> Result<Arc<Fence>, FenceCreationError> {
        Ok(Arc::new(Self {
            inner: unsafe {
                device.create_fence(&vk::FenceCreateInfo::builder().flags(flags), None)
            }?,
            device,
        }))
    }

    /// Waits for the fence.
    pub fn wait(&self) -> Result<(), WaitError> {
        unsafe { self.device.wait_for_fences(&[self.inner], true, u64::MAX) }?;
        Ok(())
    }

    /// Resets the fence.
    pub fn reset(&self) -> Result<(), ResetError> {
        unsafe { self.device.reset_fences(&[self.inner]) }?;
        Ok(())
    }

    /// Returns weither the fence has been signaled.
    pub fn is_signaled(&self) -> bool {
        unsafe { self.device.get_fence_status(self.inner).unwrap() }
    }

    /// Returns the vulkan handle of the fence.
    pub fn inner(&self) -> vk::Fence {
        self.inner
    }
}

impl Drop for Fence {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_fence(self.inner, None);
        }
    }
}

impl PartialEq for Fence {
    fn eq(&self, other: &Self) -> bool {
        PartialEq::eq(&self.inner, &other.inner)
    }
}

impl Eq for Fence {}

impl Hash for Fence {
    fn hash<H: Hasher>(&self, state: &mut H) {
        Hash::hash(&self.inner, state)
    }
}

/// Error tha can occur during fence creation.
#[derive(Debug)]
pub enum FenceCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for FenceCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            FenceCreationError::OutOfHostMemory(e) | FenceCreationError::OutOfDeviceMemory(e) => {
                Display::fmt(e, f)
            }
        }
    }
}

impl std::error::Error for FenceCreationError {}

impl From<VulkanError> for FenceCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => FenceCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => FenceCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown fence creation error"),
        }
    }
}

impl From<vk::Result> for FenceCreationError {
    fn from(result: vk::Result) -> Self {
        FenceCreationError::from(VulkanError::from(result))
    }
}

/// Error that can occur when waiting for a fence.
#[derive(Debug)]
pub enum WaitError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// The logical or physical device has been lost.
    DeviceLost(VulkanError),
}

impl Display for WaitError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            WaitError::OutOfHostMemory(e)
            | WaitError::OutOfDeviceMemory(e)
            | WaitError::DeviceLost(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for WaitError {}

impl From<VulkanError> for WaitError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => WaitError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => WaitError::OutOfDeviceMemory(e),
            VulkanError::DeviceLost => WaitError::DeviceLost(e),
            _ => unreachable!("Unknown wait for fence error"),
        }
    }
}

impl From<vk::Result> for WaitError {
    fn from(result: vk::Result) -> Self {
        WaitError::from(VulkanError::from(result))
    }
}

/// Error that can occur when resetting for a fence.
#[derive(Debug)]
pub enum ResetError {
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for ResetError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            ResetError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for ResetError {}

impl From<VulkanError> for ResetError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfDeviceMemory => ResetError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown wait for fence error"),
        }
    }
}

impl From<vk::Result> for ResetError {
    fn from(result: vk::Result) -> Self {
        ResetError::from(VulkanError::from(result))
    }
}
