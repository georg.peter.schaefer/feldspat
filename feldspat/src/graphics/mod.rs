//! Graphics setup.

use ash::vk;
pub use context::GraphicsContext;
use std::ffi::CStr;
use std::fmt::{Display, Formatter};

pub mod buffer;
pub mod command_buffer;
pub mod context;
pub mod device;
pub mod framebuffer;
pub mod image;
pub mod macros;
pub mod memory;
pub mod pipeline;
pub mod queue;
pub mod render_pass;
pub mod shader;
pub mod swapchain;
pub mod sync;
pub mod window;

pub use device::Device;
pub use framebuffer::Framebuffer;
pub use pipeline::GraphicsPipeline;
pub use queue::Queue;
pub use render_pass::RenderPass;
pub use render_pass::SubpassDescription;
pub use shader::ShaderModule;
pub use shader::ShaderModuleCreationError;
pub use swapchain::AcquireNextImageError;
pub use swapchain::PresentError;
pub use swapchain::Swapchain;
pub use sync::Fence;
pub use sync::Semaphore;

/// Provides a function to convert a vulkan string to a string slice.
pub trait FromVulkanStr {
    /// Converts the string.
    fn from_vk_str<'a>(vk_str: *const i8) -> &'a str;
}

impl FromVulkanStr for str {
    fn from_vk_str<'a>(vk_str: *const i8) -> &'a str {
        unsafe { CStr::from_ptr(vk_str) }.to_str().unwrap()
    }
}

/// General vulkan error.
#[derive(Debug)]
pub enum VulkanError {
    /// A host memory allocation has failed.
    OutOfHostMemory,
    /// A device memory allocation has failed.
    OutOfDeviceMemory,
    /// Initialization of an object could not be completed for implementation-specific reasons.
    InitializationFailed,
    /// The logical or physical device has been lost.
    DeviceLost,
    /// Mapping of a memory object has failed.
    MemoryMapFailed,
    /// A requested layer is not present or could not be loaded.
    LayerNotPresent,
    /// Initialization of an object could not be completed for implementation-specific reasons.
    ExtensionNotPresent,
    /// A requested feature is not supported.
    FeatureNotPresent,
    /// The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons.
    IncompatibleDriver,
    /// Too many objects of the type have already been created.
    TooManyObjects,
    /// A requested format is not supported on this device.
    FormatNotSupported,
    /// A pool allocation has failed due to fragmentation of the pool’s memory.
    FragmentedPool,
    /// A surface is no longer available.
    SurfaceLost,
    /// The requested window is already in use by Vulkan or another API in a manner which prevents it from being used again.
    NativeWindowInUse,
    /// A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail.
    OutOfDate,
    /// The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image.
    IncompatibleDisplay,
    /// One or more shaders failed to compile or link.
    InvalidShader,
    /// A pool memory allocation has failed.
    OutOfPoolMemory,
    /// An external handle is not a valid handle of the specified type.
    InvalidExternalHandle,
    /// A descriptor pool creation has failed due to fragmentation.
    Fragmentation,
    /// A buffer creation or memory allocation failed because the requested address is not available.
    InvalidOpaqueCaptureAddress,
    /// An operation on a swapchain created with VK_FULL_SCREEN_EXCLUSIVE_APPLICATION_CONTROLLED_EXT failed as it did not have exclusive full-screen access.
    FullScreenExclusiveModeLost,
    /// An unknown error has occurred; either the application has provided invalid input, or an implementation failure has occurred.
    Unknown,
}

impl Display for VulkanError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            VulkanError::OutOfHostMemory => write!(f, "A host memory allocation has failed"),
            VulkanError::OutOfDeviceMemory => write!(f, "A device memory allocation has failed"),
            VulkanError::InitializationFailed => write!(f, "Initialization of an object could not be completed for implementation-specific reasons"),
            VulkanError::DeviceLost => write!(f, "The logical or physical device has been lost"),
            VulkanError::MemoryMapFailed => write!(f, "Mapping of a memory object has failed"),
            VulkanError::LayerNotPresent => write!(f, "A requested layer is not present or could not be loaded"),
            VulkanError::ExtensionNotPresent => write!(f, "A requested extension is not supported"),
            VulkanError::FeatureNotPresent => write!(f, "A requested feature is not supported"),
            VulkanError::IncompatibleDriver => write!(f, "The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons"),
            VulkanError::TooManyObjects => write!(f, "Too many objects of the type have already been created"),
            VulkanError::FormatNotSupported => write!(f, "A requested format is not supported on this device"),
            VulkanError::FragmentedPool => write!(f, "A pool allocation has failed due to fragmentation of the pool’s memory"),
            VulkanError::SurfaceLost => write!(f, "A surface is no longer available"),
            VulkanError::NativeWindowInUse => write!(f, "The requested window is already in use by Vulkan or another API in a manner which prevents it from being used again"),
            VulkanError::OutOfDate => write!(f, "A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail"),
            VulkanError::IncompatibleDisplay => write!(f, "The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image"),
            VulkanError::InvalidShader => write!(f, "One or more shaders failed to compile or link"),
            VulkanError::OutOfPoolMemory => write!(f, "A pool memory allocation has failed"),
            VulkanError::InvalidExternalHandle => write!(f, "An external handle is not a valid handle of the specified type"),
            VulkanError::Fragmentation => write!(f, "A descriptor pool creation has failed due to fragmentation"),
            VulkanError::InvalidOpaqueCaptureAddress => write!(f, "A buffer creation or memory allocation failed because the requested address is not available"),
            VulkanError::FullScreenExclusiveModeLost => write!(f, "An operation on a swapchain created with VK_FULL_SCREEN_EXCLUSIVE_APPLICATION_CONTROLLED_EXT failed as it did not have exclusive full-screen access"),
            VulkanError::Unknown => write!(f, "An unknown error has occurred; either the application has provided invalid input, or an implementation failure has occurred"),
        }
    }
}

impl std::error::Error for VulkanError {}

impl From<vk::Result> for VulkanError {
    fn from(result: vk::Result) -> Self {
        match result {
            vk::Result::ERROR_OUT_OF_HOST_MEMORY => Self::OutOfHostMemory,
            vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => Self::OutOfDeviceMemory,
            vk::Result::ERROR_INITIALIZATION_FAILED => Self::InitializationFailed,
            vk::Result::ERROR_DEVICE_LOST => Self::DeviceLost,
            vk::Result::ERROR_MEMORY_MAP_FAILED => Self::MemoryMapFailed,
            vk::Result::ERROR_LAYER_NOT_PRESENT => Self::LayerNotPresent,
            vk::Result::ERROR_EXTENSION_NOT_PRESENT => Self::ExtensionNotPresent,
            vk::Result::ERROR_FEATURE_NOT_PRESENT => Self::FeatureNotPresent,
            vk::Result::ERROR_INCOMPATIBLE_DRIVER => Self::IncompatibleDriver,
            vk::Result::ERROR_TOO_MANY_OBJECTS => Self::TooManyObjects,
            vk::Result::ERROR_FORMAT_NOT_SUPPORTED => Self::FormatNotSupported,
            vk::Result::ERROR_FRAGMENTED_POOL => Self::FragmentedPool,
            vk::Result::ERROR_SURFACE_LOST_KHR => Self::SurfaceLost,
            vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR => Self::NativeWindowInUse,
            vk::Result::ERROR_OUT_OF_DATE_KHR => Self::OutOfDate,
            vk::Result::ERROR_INCOMPATIBLE_DISPLAY_KHR => Self::IncompatibleDisplay,
            vk::Result::ERROR_INVALID_SHADER_NV => Self::InvalidShader,
            vk::Result::ERROR_OUT_OF_POOL_MEMORY => Self::OutOfPoolMemory,
            vk::Result::ERROR_INVALID_EXTERNAL_HANDLE => Self::InvalidExternalHandle,
            vk::Result::ERROR_FRAGMENTATION => Self::Fragmentation,
            vk::Result::ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS => Self::InvalidOpaqueCaptureAddress,
            vk::Result::ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT => {
                Self::FullScreenExclusiveModeLost
            }
            vk::Result::ERROR_UNKNOWN => Self::Unknown,
            _ => unreachable!("Unhandled error type"),
        }
    }
}
