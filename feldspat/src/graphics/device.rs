//! Logical device.

use std::collections::{HashMap, HashSet};
use std::fmt::{Debug, Display, Formatter};
use std::ops::Deref;
use std::os::raw::c_char;
use std::sync::{Arc, Mutex};

use ash::extensions::khr;
use ash::vk;
use ash::vk::{
    DeviceCreateInfo, DeviceQueueCreateInfo, ExtensionProperties, PhysicalDeviceProperties,
    PhysicalDeviceType, QueueFlags,
};
use itertools::Itertools;
use vk::PhysicalDeviceFeatures;

use crate::graphics::command_buffer::{
    CommandPool, CommandPoolCreationError, PerThreadCommandPool,
};
use crate::graphics::context::Instance;
use crate::graphics::memory::Allocator;
use crate::graphics::pipeline::DescriptorPools;
use crate::graphics::window::Window;
use crate::graphics::{FromVulkanStr, Queue, VulkanError};

/// Logical device.
pub struct Device {
    descriptor_pools: DescriptorPools,
    command_pools: Mutex<HashMap<u32, PerThreadCommandPool>>,
    allocator: Arc<Allocator>,
    present_queue: Arc<Queue>,
    graphics_queue: Arc<Queue>,
    inner: ash::Device,
    physical_device: PhysicalDevice,
    instance: Arc<Instance>,
}

impl Device {
    pub(super) fn new(
        instance: &Arc<Instance>,
        window: &Window,
    ) -> Result<Arc<Self>, DeviceCreationError> {
        let required_properties = RequiredProperties::new();
        let physical_device =
            Self::select_physical_device(&instance, &window, &required_properties)?;
        let queue_family_indices = physical_device.queue_family_indices();
        let unique_queue_families: Vec<_> = [
            queue_family_indices.graphics(),
            queue_family_indices.present(),
        ]
        .iter()
        .unique()
        .cloned()
        .collect();
        let queue_create_infos: Vec<_> = unique_queue_families
            .iter()
            .map(|queue_family| {
                DeviceQueueCreateInfo::builder()
                    .queue_family_index(*queue_family)
                    .queue_priorities(&[1.0f32])
                    .build()
            })
            .collect();
        let mut shader_atomic_float_features =
            vk::PhysicalDeviceShaderAtomicFloatFeaturesEXT::builder()
                .shader_image_float32_atomic_add(true)
                .build();
        let device_create_info = DeviceCreateInfo::builder()
            .queue_create_infos(&queue_create_infos)
            .enabled_features(required_properties.features())
            .enabled_extension_names(required_properties.extensions())
            .push_next(&mut shader_atomic_float_features)
            .build();
        let inner =
            unsafe { instance.create_device(physical_device.inner(), &device_create_info, None) }?;

        let graphics_queue = Queue::new(
            inner.clone(),
            unsafe { inner.get_device_queue(queue_family_indices.graphics(), 0) },
            queue_family_indices.graphics(),
        );
        let present_queue = if queue_family_indices.graphics() == queue_family_indices.present() {
            graphics_queue.clone()
        } else {
            Queue::new(
                inner.clone(),
                unsafe { inner.get_device_queue(queue_family_indices.present(), 0) },
                queue_family_indices.graphics(),
            )
        };
        Ok(Arc::new(Self {
            descriptor_pools: DescriptorPools::new(40),
            command_pools: Default::default(),
            allocator: Allocator::new(&instance, &physical_device),
            present_queue,
            graphics_queue,
            inner,
            physical_device,
            instance: instance.clone(),
        }))
    }

    fn select_physical_device(
        instance: &Arc<Instance>,
        window: &Window,
        required_properties: &RequiredProperties,
    ) -> Result<PhysicalDevice, DeviceCreationError> {
        let suitable_devices = PhysicalDevice::suitable(instance, window, required_properties);

        if suitable_devices.is_empty() {
            return Err(DeviceCreationError::NoSuitablePhysicalDevice);
        }

        log::debug!("Suitable devices:");
        if log::log_enabled!(log::Level::Debug) {
            for physical_device in &suitable_devices {
                log::debug!("{}", physical_device);
            }
        }

        let selected_device = suitable_devices.first().cloned().unwrap();
        log::info!("Selected physical device {}", selected_device);
        Ok(selected_device)
    }

    pub(super) fn instance(&self) -> &Arc<Instance> {
        &self.instance
    }

    pub(super) fn physical_device(&self) -> &PhysicalDevice {
        &self.physical_device
    }

    /// Returns the queue families of this device.
    pub fn queue_families(&self) -> &QueueFamilyIndices {
        self.physical_device.queue_family_indices()
    }

    /// Returns the graphics queue.
    pub fn graphics_queue(&self) -> &Arc<Queue> {
        &self.graphics_queue
    }

    /// Returns the present queue.
    pub fn present_queue(&self) -> &Arc<Queue> {
        &self.present_queue
    }

    /// Returns the allocator.
    pub fn allocator(&self) -> &Arc<Allocator> {
        &self.allocator
    }

    /// Waits until the device has become idle.
    pub fn wait_idle(&self) -> Result<(), WaitIdleError> {
        unsafe { self.inner.device_wait_idle()? }
        self.graphics_queue.clear_command_buffers();
        self.present_queue.clear_command_buffers();
        Ok(())
    }

    /// Returns a command pool for the `queue_family`.
    pub fn command_pool(
        self: &Arc<Self>,
        queue_family: u32,
    ) -> Result<Arc<CommandPool>, CommandPoolCreationError> {
        let mut command_pools = self.command_pools.lock().unwrap();
        command_pools
            .entry(queue_family)
            .or_insert(PerThreadCommandPool::new(queue_family))
            .get(self.clone())
    }

    /// Returns the descriptor pool collection.
    pub fn descriptor_pools(&self) -> &DescriptorPools {
        &self.descriptor_pools
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe {
            self.descriptor_pools.clean_up(self);
        }
        self.allocator.clean_up(&self);
        unsafe {
            self.inner.destroy_device(None);
        }
    }
}

impl Deref for Device {
    type Target = ash::Device;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl Debug for Device {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "feldspat::graphics::Device")
    }
}

/// Error that can occur during device creation.
#[derive(Debug)]
pub enum DeviceCreationError {
    /// No suitable physical device found.
    NoSuitablePhysicalDevice,
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// Initialization of an object could not be completed for implementation-specific reasons.
    InitializationFailed(VulkanError),
    /// Initialization of an object could not be completed for implementation-specific reasons.
    ExtensionNotPresent(VulkanError),
    /// A requested feature is not supported.
    FeatureNotPresent(VulkanError),
    /// Too many objects of the type have already been created.
    TooManyObjects(VulkanError),
    /// The logical or physical device has been lost.
    DeviceLost(VulkanError),
}

impl Display for DeviceCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            DeviceCreationError::NoSuitablePhysicalDevice => {
                write!(f, "No suitable physical device")
            }
            DeviceCreationError::OutOfHostMemory(error)
            | DeviceCreationError::OutOfDeviceMemory(error)
            | DeviceCreationError::InitializationFailed(error)
            | DeviceCreationError::ExtensionNotPresent(error)
            | DeviceCreationError::FeatureNotPresent(error)
            | DeviceCreationError::TooManyObjects(error)
            | DeviceCreationError::DeviceLost(error) => Display::fmt(error, f),
        }
    }
}

impl std::error::Error for DeviceCreationError {}

impl From<VulkanError> for DeviceCreationError {
    fn from(error: VulkanError) -> Self {
        match error {
            VulkanError::OutOfHostMemory => DeviceCreationError::OutOfHostMemory(error),
            VulkanError::OutOfDeviceMemory => DeviceCreationError::OutOfDeviceMemory(error),
            VulkanError::InitializationFailed => DeviceCreationError::InitializationFailed(error),
            VulkanError::ExtensionNotPresent => DeviceCreationError::ExtensionNotPresent(error),
            VulkanError::FeatureNotPresent => DeviceCreationError::FeatureNotPresent(error),
            VulkanError::TooManyObjects => DeviceCreationError::TooManyObjects(error),
            VulkanError::DeviceLost => DeviceCreationError::DeviceLost(error),
            _ => unreachable!("Unknown vkCreateDevice error"),
        }
    }
}

impl From<vk::Result> for DeviceCreationError {
    fn from(result: vk::Result) -> Self {
        DeviceCreationError::from(VulkanError::from(result))
    }
}

/// Error that can occur when waiting for the device to become idle.
#[derive(Debug)]
pub enum WaitIdleError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// The logical or physical device has been lost.
    DeviceLost(VulkanError),
}

impl Display for WaitIdleError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            WaitIdleError::OutOfHostMemory(e)
            | WaitIdleError::OutOfDeviceMemory(e)
            | WaitIdleError::DeviceLost(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for WaitIdleError {}

impl From<VulkanError> for WaitIdleError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => WaitIdleError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => WaitIdleError::OutOfDeviceMemory(e),
            VulkanError::DeviceLost => WaitIdleError::DeviceLost(e),
            _ => unreachable!("Unknown device wait idle error"),
        }
    }
}

impl From<vk::Result> for WaitIdleError {
    fn from(result: vk::Result) -> Self {
        WaitIdleError::from(VulkanError::from(result))
    }
}

/// A physical device.
#[derive(Clone)]
pub struct PhysicalDevice {
    queue_family_indices: QueueFamilyIndices,
    index: usize,
    instance: Arc<Instance>,
}

impl PhysicalDevice {
    pub(super) fn suitable(
        instance: &Arc<Instance>,
        window: &Window,
        required_properties: &RequiredProperties,
    ) -> Vec<PhysicalDevice> {
        unsafe { instance.enumerate_physical_devices() }
            .expect("Failed to enumerate physical devices")
            .iter()
            .cloned()
            .enumerate()
            .map(|inner| Self::new(instance, inner, window))
            .filter(|physical_device| required_properties.is_suitable(physical_device))
            .map(|physical_device| required_properties.rate(physical_device))
            .sorted_by_key(|(_, rating)| *rating)
            .map(|(physical_device, _)| physical_device)
            .collect()
    }

    fn new(instance: &Arc<Instance>, inner: (usize, vk::PhysicalDevice), window: &Window) -> Self {
        Self {
            queue_family_indices: QueueFamilyIndices::new(instance, inner.1, window),
            index: inner.0,
            instance: instance.clone(),
        }
    }

    pub(super) fn queue_family_indices(&self) -> &QueueFamilyIndices {
        &self.queue_family_indices
    }

    /// Returns the properties of the physical device.
    pub fn properties(&self) -> PhysicalDeviceProperties {
        unsafe { self.instance.get_physical_device_properties(self.inner()) }
    }

    /// Returns the physical device properties for conservative rasterization.
    pub fn conservative_rasterization_properties(
        &self,
    ) -> vk::PhysicalDeviceConservativeRasterizationPropertiesEXT {
        let mut conservative_rasterization_properties =
            vk::PhysicalDeviceConservativeRasterizationPropertiesEXT::default();
        let mut properties2 = vk::PhysicalDeviceProperties2::builder()
            .push_next(&mut conservative_rasterization_properties)
            .build();

        unsafe {
            self.instance
                .get_physical_device_properties2(self.inner(), &mut properties2)
        }

        conservative_rasterization_properties
    }

    fn extensions(&self) -> Vec<ExtensionProperties> {
        unsafe {
            self.instance
                .enumerate_device_extension_properties(self.inner())
                .expect("Failed to enumerate device extension properties")
        }
    }

    fn features(&self) -> vk::PhysicalDeviceFeatures {
        unsafe { self.instance.get_physical_device_features(self.inner()) }
    }

    pub(super) fn inner(&self) -> vk::PhysicalDevice {
        unsafe { self.instance.enumerate_physical_devices() }
            .expect("Failed to enumerate physical devices")
            .iter()
            .nth(self.index)
            .cloned()
            .expect("Failed to get physical device")
    }
}

impl Display for PhysicalDevice {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let name = str::from_vk_str(
            unsafe { self.instance.get_physical_device_properties(self.inner()) }
                .device_name
                .as_ptr(),
        );
        write!(f, "{}", name)
    }
}

/// Contains the indices of the selected queue families.
#[derive(Copy, Clone)]
pub struct QueueFamilyIndices {
    graphics: Option<u32>,
    present: Option<u32>,
}

impl QueueFamilyIndices {
    fn new(instance: &Instance, physical_device: vk::PhysicalDevice, window: &Window) -> Self {
        let mut queue_familiy_indices = Self {
            graphics: None,
            present: None,
        };
        let queue_families =
            unsafe { instance.get_physical_device_queue_family_properties(physical_device) };

        for (index, queue_family) in queue_families.iter().enumerate() {
            if queue_family.queue_flags.contains(QueueFlags::GRAPHICS) {
                queue_familiy_indices.graphics = Some(index as u32);
            }

            if window.surface().is_supported(physical_device, index) {
                queue_familiy_indices.present = Some(index as u32);
            }

            if queue_familiy_indices.is_complete() {
                break;
            }
        }

        queue_familiy_indices
    }

    /// Returns the graphics queue family.
    pub fn graphics(&self) -> u32 {
        self.graphics.expect("Incomplete queue families")
    }

    /// Returns the present queue family.
    pub fn present(&self) -> u32 {
        self.present.expect("Incomplete queue families")
    }

    fn is_complete(&self) -> bool {
        self.graphics.is_some() && self.present.is_some()
    }
}

pub(super) struct RequiredProperties {
    features: PhysicalDeviceFeatures,
    extensions: Vec<*const c_char>,
}

impl RequiredProperties {
    const TYPE_DISCRETE_GPU_RATING: usize = 1000;
    const TYPE_INTEGRATED_GPU_RATING: usize = 500;
    const TYPE_VIRTUAL_GPU_RATING: usize = 200;
    const TYPE_CPU_RATING: usize = 100;
    const TYPE_OTHER_RATING: usize = 0;

    pub(super) fn new() -> Self {
        Self {
            features: PhysicalDeviceFeatures {
                sampler_anisotropy: vk::TRUE,
                fragment_stores_and_atomics: vk::TRUE,
                geometry_shader: vk::TRUE,
                vertex_pipeline_stores_and_atomics: vk::TRUE,
                ..Default::default()
            },
            extensions: vec![
                khr::Swapchain::name().as_ptr(),
                ash::vk::ExtConservativeRasterizationFn::name().as_ptr(),
                ash::vk::ExtShaderAtomicFloatFn::name().as_ptr(),
            ],
        }
    }

    fn features(&self) -> &PhysicalDeviceFeatures {
        &self.features
    }

    fn extensions(&self) -> &Vec<*const c_char> {
        &self.extensions
    }

    fn is_suitable(&self, physical_device: &PhysicalDevice) -> bool {
        self.supports_required_extensions(physical_device)
            && self.supports_required_features(physical_device)
            && physical_device.queue_family_indices().is_complete()
    }

    fn supports_required_extensions(&self, physical_device: &PhysicalDevice) -> bool {
        let supported_extensions: HashSet<&str> = physical_device
            .extensions()
            .iter()
            .map(|extension| str::from_vk_str(extension.extension_name.as_ptr()))
            .collect();
        let required_extensions: HashSet<&str> = self
            .extensions()
            .iter()
            .map(|extension| str::from_vk_str(*extension))
            .collect();
        !required_extensions
            .intersection(&supported_extensions)
            .collect::<HashSet<_>>()
            .is_empty()
    }

    fn supports_required_features(&self, physical_device: &PhysicalDevice) -> bool {
        let supported_features = physical_device.features();
        Self::contains_enabled_features(self.features, supported_features)
    }

    fn contains_enabled_features(
        required_features: vk::PhysicalDeviceFeatures,
        supported_features: vk::PhysicalDeviceFeatures,
    ) -> bool {
        (required_features.sampler_anisotropy == vk::TRUE
            && supported_features.sampler_anisotropy == vk::TRUE)
            || required_features.sampler_anisotropy == vk::FALSE
    }

    fn rate(&self, physical_device: PhysicalDevice) -> (PhysicalDevice, usize) {
        let mut rating = 0;

        rating += self.rate_type(physical_device.properties());

        (physical_device, rating)
    }

    fn rate_type(&self, properties: PhysicalDeviceProperties) -> usize {
        match properties.device_type {
            PhysicalDeviceType::DISCRETE_GPU => Self::TYPE_DISCRETE_GPU_RATING,
            PhysicalDeviceType::INTEGRATED_GPU => Self::TYPE_INTEGRATED_GPU_RATING,
            PhysicalDeviceType::VIRTUAL_GPU => Self::TYPE_VIRTUAL_GPU_RATING,
            PhysicalDeviceType::CPU => Self::TYPE_CPU_RATING,
            PhysicalDeviceType::OTHER => Self::TYPE_OTHER_RATING,
            _ => unreachable!("Unhandled physical device type"),
        }
    }
}
