//! Types for creating framebuffers.

use std::fmt::{Display, Formatter};
use std::sync::Arc;

use ash::vk;

use crate::graphics::image::{Image, ImageView, ImageViewAbstract};
use crate::graphics::{Device, RenderPass, VulkanError};

/// A framebuffer.
pub struct Framebuffer {
    inner: vk::Framebuffer,
    extent: vk::Extent2D,
    render_pass: Arc<RenderPass>,
    attachments: Vec<Arc<dyn ImageViewAbstract>>,
    device: Arc<Device>,
}

impl Framebuffer {
    /// Starts building a framebuffer.
    pub fn builder() -> FramebufferBuilder {
        FramebufferBuilder {
            attachments: vec![],
        }
    }

    /// Creates an empty framebuffer.
    pub fn empty(
        render_pass: Arc<RenderPass>,
        width: u32,
        height: u32,
    ) -> Result<Arc<Framebuffer>, FramebufferCreationError> {
        let device = render_pass.device().clone();
        let create_info = vk::FramebufferCreateInfo::builder()
            .render_pass(render_pass.inner())
            .width(width)
            .height(height)
            .layers(1);
        let inner = unsafe { device.create_framebuffer(&create_info, None) }?;
        Ok(Arc::new(Self {
            inner,
            extent: vk::Extent2D::builder().width(width).height(height).build(),
            render_pass,
            attachments: vec![],
            device,
        }))
    }

    /// Returns the vulkan framebuffer.
    pub fn inner(&self) -> vk::Framebuffer {
        self.inner
    }

    /// Returns the extent of the framebuffer.
    pub fn extent(&self) -> vk::Extent2D {
        self.extent
    }

    /// Returns the render pass the framebuffer has been built for.
    pub fn render_pass(&self) -> &Arc<RenderPass> {
        &self.render_pass
    }

    /// Returns the attachments of the framebuffer.
    pub fn attachments(&self) -> &Vec<Arc<dyn ImageViewAbstract>> {
        &self.attachments
    }
}

impl Drop for Framebuffer {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_framebuffer(self.inner, None);
        }
    }
}

unsafe impl Sync for Framebuffer {}

unsafe impl Send for Framebuffer {}

/// Builder for creating a framebuffer.
pub struct FramebufferBuilder {
    attachments: Vec<Arc<dyn ImageViewAbstract>>,
}

impl FramebufferBuilder {
    /// Adds an attachment.
    pub fn add<I>(mut self, attachment: Arc<ImageView<I>>) -> Self
    where
        I: Image + 'static,
    {
        self.attachments.push(attachment);
        self
    }

    /// Builds the framebuffer.
    pub fn build(
        self,
        render_pass: Arc<RenderPass>,
    ) -> Result<Arc<Framebuffer>, FramebufferCreationError> {
        let device = render_pass.device().clone();
        let extent = self
            .attachments
            .first()
            .map(|view| view.image())
            .map(|image| image.extent())
            .unwrap_or(vk::Extent3D::builder().width(1).height(1).depth(1).build());
        let attachments: Vec<_> = self
            .attachments
            .iter()
            .map(|attachment| attachment.inner())
            .collect();
        let create_info = vk::FramebufferCreateInfo::builder()
            .render_pass(render_pass.inner())
            .attachments(&attachments)
            .width(extent.width)
            .height(extent.height)
            .layers(extent.depth);
        let inner = unsafe { device.create_framebuffer(&create_info, None) }?;
        Ok(Arc::new(Framebuffer {
            inner,
            extent: vk::Extent2D::builder()
                .width(extent.width)
                .height(extent.height)
                .build(),
            render_pass,
            attachments: self.attachments,
            device,
        }))
    }
}

/// Error that can occur during framebuffer creation.
#[derive(Debug)]
pub enum FramebufferCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
}

impl Display for FramebufferCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            FramebufferCreationError::OutOfHostMemory(e)
            | FramebufferCreationError::OutOfDeviceMemory(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for FramebufferCreationError {}

impl From<VulkanError> for FramebufferCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => FramebufferCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => FramebufferCreationError::OutOfDeviceMemory(e),
            _ => unreachable!("Unknown framebuffer creation error"),
        }
    }
}

impl From<vk::Result> for FramebufferCreationError {
    fn from(result: vk::Result) -> Self {
        FramebufferCreationError::from(VulkanError::from(result))
    }
}
