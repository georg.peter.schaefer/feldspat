//! Types for buffer creation.

use std::error::Error;
use std::fmt::{Display, Formatter};
use std::marker::PhantomData;
use std::sync::Arc;

use ash::vk;

use crate::graphics::command_buffer::{CommandBuffer, CommandBufferCreationError};
use crate::graphics::memory::{AllocationError, BindMemoryError, Memory, WriteMemoryError};
use crate::graphics::queue::{SubmitError, WaitIdleError};
use crate::graphics::{Device, VulkanError};

/// A buffer with bound memory.
pub struct ImmutableBuffer<T>
where
    T: ?Sized,
{
    _memory: Memory,
    raw_buffer: RawBuffer,
    _phantom: PhantomData<Box<T>>,
}

impl<T> ImmutableBuffer<T>
where
    T: ?Sized,
{
    /// Creates an unintialized buffer with `size`.
    pub fn uninitialized(
        device: Arc<Device>,
        size: vk::DeviceSize,
        usage: vk::BufferUsageFlags,
    ) -> Result<Arc<Self>, BufferCreationError> {
        let raw_buffer = RawBuffer::new(device.clone(), size, usage)?;

        let memory_requirements =
            unsafe { device.get_buffer_memory_requirements(raw_buffer.inner()) };
        let memory = device.allocator().allocate(
            device.clone(),
            memory_requirements,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        )?;
        memory.bind_buffer(&device, &raw_buffer)?;

        Ok(Arc::new(Self {
            _memory: memory,
            raw_buffer,
            _phantom: Default::default(),
        }))
    }
}

impl<T> ImmutableBuffer<[T]> {
    /// Creates a buffer from an iterator.
    pub fn from_iter<I>(
        device: Arc<Device>,
        usage: vk::BufferUsageFlags,
        data: I,
    ) -> Result<Arc<Self>, BufferCreationError>
    where
        I: IntoIterator<Item = T>,
        I::IntoIter: ExactSizeIterator,
        T: Sized + Clone + 'static,
    {
        let data: Vec<_> = data.into_iter().collect();
        let size = (data.len() * std::mem::size_of::<T>()) as vk::DeviceSize;
        let staging_buffer = CpuAccessibleBuffer::from_iter(
            device.clone(),
            vk::BufferUsageFlags::TRANSFER_SRC,
            data.clone(),
        )?;

        let buffer = Self::uninitialized(
            device.clone(),
            size,
            vk::BufferUsageFlags::TRANSFER_DST | usage,
        )?;

        let mut builder = CommandBuffer::primary(
            device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            device.graphics_queue().family(),
        )?;
        builder.copy_buffer(
            staging_buffer.clone(),
            buffer.clone(),
            vk::BufferCopy::builder()
                .src_offset(0)
                .dst_offset(0)
                .size(size)
                .build(),
        );
        let command_buffer = builder.build()?;
        let queue = device.graphics_queue();
        queue.submit(command_buffer.clone(), vec![], vec![], None, None)?;

        Ok(buffer)
    }
}

impl<T> ImmutableBuffer<T>
where
    T: Sized + Clone + 'static,
{
    /// Creates a buffer from data.
    pub fn from_data(
        device: Arc<Device>,
        usage: vk::BufferUsageFlags,
        data: T,
    ) -> Result<Arc<Self>, BufferCreationError> {
        let data = [data];
        let size = (data.len() * std::mem::size_of::<T>()) as vk::DeviceSize;
        let staging_buffer = CpuAccessibleBuffer::from_iter(
            device.clone(),
            vk::BufferUsageFlags::TRANSFER_SRC,
            data.clone(),
        )?;

        let buffer = Self::uninitialized(
            device.clone(),
            size,
            vk::BufferUsageFlags::TRANSFER_DST | usage,
        )?;

        let mut builder = CommandBuffer::primary(
            device.clone(),
            vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            device.graphics_queue().family(),
        )?;
        builder.copy_buffer(
            staging_buffer.clone(),
            buffer.clone(),
            vk::BufferCopy::builder()
                .src_offset(0)
                .dst_offset(0)
                .size(size)
                .build(),
        );
        let command_buffer = builder.build()?;
        let queue = device.graphics_queue();
        queue.submit(command_buffer.clone(), vec![], vec![], None, None)?;

        Ok(buffer)
    }
}

impl<T> Buffer for ImmutableBuffer<T>
where
    T: ?Sized,
{
    fn inner(&self) -> vk::Buffer {
        self.raw_buffer.inner()
    }
}

/// A buffer with bound memory that is accessible from the host.
pub struct CpuAccessibleBuffer<T>
where
    T: ?Sized,
{
    memory: Memory,
    raw_buffer: RawBuffer,
    _device: Arc<Device>,
    _phantom: PhantomData<Box<T>>,
}

impl<T> CpuAccessibleBuffer<T> {
    /// Returns the memory.
    pub fn memory(&self) -> &Memory {
        &self.memory
    }
}

impl<T> CpuAccessibleBuffer<T>
where
    T: Sized,
{
    /// Creates a buffer with data.
    pub fn from_data(
        device: Arc<Device>,
        usage: vk::BufferUsageFlags,
        data: T,
    ) -> Result<Arc<Self>, BufferCreationError> {
        let data = [data];
        let raw_buffer = RawBuffer::new(
            device.clone(),
            (data.len() * std::mem::size_of::<T>()) as vk::DeviceSize,
            usage,
        )?;

        let memory_requirements =
            unsafe { device.get_buffer_memory_requirements(raw_buffer.inner()) };
        let memory = device.allocator().allocate(
            device.clone(),
            memory_requirements,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
        )?;
        memory.bind_buffer(&device, &raw_buffer)?;
        memory.write(&device, data)?;

        Ok(Arc::new(Self {
            memory,
            raw_buffer,
            _device: device,
            _phantom: Default::default(),
        }))
    }
}

impl<T> CpuAccessibleBuffer<[T]> {
    /// Creates a buffer from an iterator.
    pub fn from_iter<I>(
        device: Arc<Device>,
        usage: vk::BufferUsageFlags,
        data: I,
    ) -> Result<Arc<Self>, BufferCreationError>
    where
        I: IntoIterator<Item = T>,
        I::IntoIter: ExactSizeIterator,
        T: Sized,
    {
        let data: Vec<_> = data.into_iter().collect();
        let raw_buffer = RawBuffer::new(
            device.clone(),
            (data.len() * std::mem::size_of::<T>()) as vk::DeviceSize,
            usage,
        )?;

        let memory_requirements =
            unsafe { device.get_buffer_memory_requirements(raw_buffer.inner()) };
        let memory = device.allocator().allocate(
            device.clone(),
            memory_requirements,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
        )?;
        memory.bind_buffer(&device, &raw_buffer)?;
        memory.write(&device, data)?;

        Ok(Arc::new(Self {
            memory,
            raw_buffer,
            _device: device,
            _phantom: Default::default(),
        }))
    }
}

impl<T> Buffer for CpuAccessibleBuffer<T>
where
    T: ?Sized,
{
    fn inner(&self) -> vk::Buffer {
        self.raw_buffer.inner()
    }
}

/// A raw buffer object without bound memory.
pub struct RawBuffer {
    inner: vk::Buffer,
    device: Arc<Device>,
}

impl RawBuffer {
    /// Creates a new buffer.
    pub fn new(
        device: Arc<Device>,
        size: vk::DeviceSize,
        usage: vk::BufferUsageFlags,
    ) -> Result<Self, BufferCreationError> {
        let buffer_info = vk::BufferCreateInfo::builder()
            .size(size)
            .usage(usage)
            .sharing_mode(vk::SharingMode::EXCLUSIVE);
        let inner = unsafe { device.create_buffer(&buffer_info, None) }?;
        Ok(Self { inner, device })
    }
}

impl Drop for RawBuffer {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_buffer(self.inner, None);
        }
    }
}

impl Buffer for RawBuffer {
    fn inner(&self) -> vk::Buffer {
        self.inner
    }
}

/// Error that can occur during buffer creation.
#[derive(Debug)]
pub enum BufferCreationError {
    /// A host memory allocation has failed.
    OutOfHostMemory(VulkanError),
    /// A device memory allocation has failed.
    OutOfDeviceMemory(VulkanError),
    /// A buffer creation or memory allocation failed because the requested address is not available.
    InvalidOpaqueCaptureAddress(VulkanError),
    /// Command buffer creation failed.
    CommandBufferCreationFailed(CommandBufferCreationError),
    /// Failed to submit the command buffer for transferring from host to device memory.
    SubmitFailed(SubmitError),
    /// Failed to wait for the transfer to complete.
    WaitIdleFailed(WaitIdleError),
    /// Memory allocation failed.
    AllocationFailed(AllocationError),
    /// Bind memory to buffer failed.
    BindMemoryFailed(BindMemoryError),
    /// Write to memory failed.
    WriteMemoryFailed(WriteMemoryError),
}

impl Display for BufferCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            BufferCreationError::OutOfHostMemory(e)
            | BufferCreationError::OutOfDeviceMemory(e)
            | BufferCreationError::InvalidOpaqueCaptureAddress(e) => Display::fmt(e, f),
            BufferCreationError::CommandBufferCreationFailed(e) => Display::fmt(e, f),
            BufferCreationError::SubmitFailed(e) => Display::fmt(e, f),
            BufferCreationError::WaitIdleFailed(e) => Display::fmt(e, f),
            BufferCreationError::AllocationFailed(e) => Display::fmt(e, f),
            BufferCreationError::BindMemoryFailed(e) => Display::fmt(e, f),
            BufferCreationError::WriteMemoryFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for BufferCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            BufferCreationError::CommandBufferCreationFailed(e) => Some(e),
            BufferCreationError::SubmitFailed(e) => Some(e),
            BufferCreationError::WaitIdleFailed(e) => Some(e),
            BufferCreationError::AllocationFailed(e) => Some(e),
            BufferCreationError::BindMemoryFailed(e) => Some(e),
            BufferCreationError::WriteMemoryFailed(e) => Some(e),
            _ => None,
        }
    }
}

impl From<VulkanError> for BufferCreationError {
    fn from(e: VulkanError) -> Self {
        match e {
            VulkanError::OutOfHostMemory => BufferCreationError::OutOfHostMemory(e),
            VulkanError::OutOfDeviceMemory => BufferCreationError::OutOfDeviceMemory(e),
            VulkanError::InvalidOpaqueCaptureAddress => {
                BufferCreationError::InvalidOpaqueCaptureAddress(e)
            }
            _ => unreachable!("Unknown buffer creation error"),
        }
    }
}

impl From<vk::Result> for BufferCreationError {
    fn from(result: vk::Result) -> Self {
        BufferCreationError::from(VulkanError::from(result))
    }
}

impl From<SubmitError> for BufferCreationError {
    fn from(e: SubmitError) -> Self {
        BufferCreationError::SubmitFailed(e)
    }
}

impl From<CommandBufferCreationError> for BufferCreationError {
    fn from(e: CommandBufferCreationError) -> Self {
        BufferCreationError::CommandBufferCreationFailed(e)
    }
}

impl From<WaitIdleError> for BufferCreationError {
    fn from(e: WaitIdleError) -> Self {
        BufferCreationError::WaitIdleFailed(e)
    }
}

impl From<AllocationError> for BufferCreationError {
    fn from(e: AllocationError) -> Self {
        BufferCreationError::AllocationFailed(e)
    }
}

impl From<BindMemoryError> for BufferCreationError {
    fn from(e: BindMemoryError) -> Self {
        BufferCreationError::BindMemoryFailed(e)
    }
}

impl From<WriteMemoryError> for BufferCreationError {
    fn from(e: WriteMemoryError) -> Self {
        BufferCreationError::WriteMemoryFailed(e)
    }
}

impl<T> Buffer for Arc<T>
where
    T: Buffer,
{
    fn inner(&self) -> vk::Buffer {
        (**self).inner()
    }
}

/// Trait for buffers.
pub trait Buffer {
    /// Returns the inner buffer handle.
    fn inner(&self) -> vk::Buffer;
}
