//! Graphics API initialization.

use std::error::Error;
use std::ffi::{c_void, CStr, CString};
use std::fmt::{Debug, Display, Formatter};
use std::ops::Deref;
use std::os::raw::c_char;
use std::sync::Arc;

use ash::extensions::ext::DebugUtils;
use ash::extensions::khr::Surface;
use ash::vk::{
    make_api_version, ApplicationInfo, Bool32, DebugUtilsMessageSeverityFlagsEXT,
    DebugUtilsMessageTypeFlagsEXT, DebugUtilsMessengerCallbackDataEXT,
    DebugUtilsMessengerCreateInfoEXT, DebugUtilsMessengerEXT, InstanceCreateInfo,
};
use ash::{Entry, InstanceError, LoadingError};
use winit::event_loop::EventLoop;

use crate::engine::Config;
use crate::graphics::device::{Device, DeviceCreationError};
use crate::graphics::swapchain::{Swapchain, SwapchainCreationError};
use crate::graphics::window::{Window, WindowCreationError};

use super::FromVulkanStr;

/// Graphics context.
pub struct GraphicsContext {
    swapchain: Swapchain,
    device: Arc<Device>,
    window: Arc<Window>,
    _debug_callback: DebugCallback,
    _instance: Arc<Instance>,
    _entry: Entry,
}

impl GraphicsContext {
    pub(crate) fn new(
        event_loop: &EventLoop<()>,
        config: &Arc<Config>,
    ) -> Result<Arc<Self>, ContextCreationError> {
        let entry = unsafe { Entry::new()? };
        let instance = Self::create_instance(&entry, config)?;
        let debug_callback = DebugCallback::new(&entry, &instance);
        let window = Window::from_config(event_loop, &entry, &instance, config)?;
        let device = Device::new(&instance, &window)?;
        let swapchain = Swapchain::new(device.clone(), window.clone())?;

        Ok(Arc::new(GraphicsContext {
            _entry: entry,
            _instance: instance,
            _debug_callback: debug_callback,
            window,
            device,
            swapchain,
        }))
    }

    fn create_instance(
        entry: &Entry,
        config: &Arc<Config>,
    ) -> Result<Arc<Instance>, ContextCreationError> {
        let app_name = CString::new(config.system.title.as_str()).unwrap();
        let engine_name = CString::new("Feldspat").unwrap();
        let app_info = ApplicationInfo::builder()
            .api_version(make_api_version(0, 1, 2, 180))
            .application_name(&app_name)
            .engine_name(&engine_name)
            .build();
        let extensions = Self::required_extension_names(entry)?;
        let layers = Self::required_layer_names(entry)?;
        let mut debug_utils_messenger_create_info =
            DebugCallback::debug_utils_messenger_create_info();
        let create_info = InstanceCreateInfo::builder()
            .application_info(&app_info)
            .enabled_extension_names(&extensions)
            .enabled_layer_names(&layers)
            .push_next(&mut debug_utils_messenger_create_info)
            .build();
        Instance::new(entry, &create_info)
    }

    fn required_extension_names(entry: &Entry) -> Result<Vec<*const c_char>, ContextCreationError> {
        let instance_extension_properties = entry
            .enumerate_instance_extension_properties()
            .expect("Failed to enumerate instance extension properties");
        let required_extension_names = vec![
            Surface::name().as_ptr(),
            #[cfg(target_os = "linux")]
            ash::extensions::khr::XlibSurface::name().as_ptr(),
            #[cfg(target_os = "windows")]
            ash::extensions::khr::Win32Surface::name().as_ptr(),
            DebugUtils::name().as_ptr(),
        ];

        if log::log_enabled!(log::Level::Debug) {
            log::debug!("Available instance extensions:");
            for extension_properties in &instance_extension_properties {
                log::debug!(
                    "{}",
                    str::from_vk_str(extension_properties.extension_name.as_ptr())
                );
            }
        }

        if required_extension_names
            .iter()
            .map(|extension_name| str::from_vk_str(*extension_name))
            .all(|extension_name| {
                let predicate = instance_extension_properties
                    .iter()
                    .map(|extension_properties| {
                        str::from_vk_str(extension_properties.extension_name.as_ptr())
                    })
                    .find(|extension_properties| &extension_name == extension_properties)
                    .is_none();
                if predicate {
                    log::error!("Missing instance extension: {}", extension_name);
                }
                predicate
            })
        {
            return Err(ContextCreationError::MissingRequiredExtensions);
        }

        Ok(required_extension_names)
    }

    fn required_layer_names(entry: &Entry) -> Result<Vec<*const c_char>, ContextCreationError> {
        let instance_layer_properties = entry
            .enumerate_instance_layer_properties()
            .expect("Failed to enumerate instance layer properties");
        let required_layer_names =
            vec![CStr::from_bytes_with_nul(b"VK_LAYER_KHRONOS_validation\0")
                .unwrap()
                .as_ptr()];

        if log::log_enabled!(log::Level::Debug) {
            log::debug!("Available instance layers:");
            for layer_properties in &instance_layer_properties {
                log::debug!("{}", str::from_vk_str(layer_properties.layer_name.as_ptr()));
            }
        }

        if required_layer_names
            .iter()
            .map(|layer_name| str::from_vk_str(*layer_name))
            .all(|layer_name| {
                let predicate = instance_layer_properties
                    .iter()
                    .map(|layer_properties| str::from_vk_str(layer_properties.layer_name.as_ptr()))
                    .find(|layer_properties| &layer_name == layer_properties)
                    .is_none();
                if predicate {
                    log::error!("Missing instance layer: {}", layer_name);
                }
                predicate
            })
        {
            return Err(ContextCreationError::MissingRequiredLayer);
        }

        Ok(required_layer_names)
    }

    /// Cleanup.
    pub unsafe fn clean_up(&self) {
        self.swapchain.clean_up();
    }

    /// Returns the window.
    pub fn window(&self) -> &Arc<Window> {
        &self.window
    }

    /// Returns the device.
    pub fn device(&self) -> &Arc<Device> {
        &self.device
    }

    /// Returns the swapchain.
    pub fn swapchain(&self) -> &Swapchain {
        &self.swapchain
    }
}

pub(crate) struct Instance {
    inner: ash::Instance,
}

impl Instance {
    fn new(
        entry: &Entry,
        create_info: &InstanceCreateInfo,
    ) -> Result<Arc<Self>, ContextCreationError> {
        let inner = unsafe { entry.create_instance(&create_info, None) }?;
        Ok(Arc::new(Self { inner }))
    }
}

impl Drop for Instance {
    fn drop(&mut self) {
        unsafe {
            self.inner.destroy_instance(None);
        }
    }
}

impl Deref for Instance {
    type Target = ash::Instance;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

struct DebugCallback {
    debug_utils: DebugUtils,
    debug_messenger: DebugUtilsMessengerEXT,
}

impl DebugCallback {
    fn new(entry: &Entry, instance: &Instance) -> Self {
        let debug_utils = DebugUtils::new(&entry, instance);
        let debug_utils_messenger_create_info = Self::debug_utils_messenger_create_info();
        let debug_messenger = unsafe {
            debug_utils.create_debug_utils_messenger(&debug_utils_messenger_create_info, None)
        }
        .expect("Failed to create debug utils messenger extension");

        Self {
            debug_utils,
            debug_messenger,
        }
    }

    fn debug_utils_messenger_create_info() -> DebugUtilsMessengerCreateInfoEXT {
        DebugUtilsMessengerCreateInfoEXT::builder()
            .message_severity(DebugUtilsMessageSeverityFlagsEXT::all())
            .message_type(DebugUtilsMessageTypeFlagsEXT::all())
            .pfn_user_callback(Some(debug_callback))
            .build()
    }
}

impl Drop for DebugCallback {
    fn drop(&mut self) {
        unsafe {
            self.debug_utils
                .destroy_debug_utils_messenger(self.debug_messenger, None);
        }
    }
}

unsafe extern "system" fn debug_callback(
    message_severity: DebugUtilsMessageSeverityFlagsEXT,
    _message_type: DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> Bool32 {
    let message = str::from_vk_str((*p_callback_data).p_message);
    match message_severity {
        DebugUtilsMessageSeverityFlagsEXT::VERBOSE => {
            log::trace!("{}", message);
        }
        DebugUtilsMessageSeverityFlagsEXT::WARNING => {
            log::warn!("{}", message);
        }
        DebugUtilsMessageSeverityFlagsEXT::ERROR => {
            log::error!("{}", message);
        }
        DebugUtilsMessageSeverityFlagsEXT::INFO => {
            log::info!("{}", message);
        }
        _ => unreachable!("Unhandled severity"),
    }

    ash::vk::FALSE
}

/// Error that can occur during context creation-
#[derive(Debug)]
pub enum ContextCreationError {
    /// Loading vulkan has failed.
    LoadingFailed(LoadingError),
    /// The instance is missing a required extension.
    MissingRequiredExtensions,
    /// Instance creation has failed.
    InstanceCreationFailed(InstanceError),
    /// The instance is missing a required layer.
    MissingRequiredLayer,
    /// Window creation has failed.
    WindowCreationFailed(WindowCreationError),
    /// Device creation has failed.
    DeviceCreationFailed(DeviceCreationError),
    /// Swapchain creation has failed.
    SwapchainCreationFailed(SwapchainCreationError),
}

impl Display for ContextCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            ContextCreationError::LoadingFailed(e) => Display::fmt(e, f),
            ContextCreationError::MissingRequiredExtensions => {
                write!(f, "Missing a required instance extension")
            }
            ContextCreationError::InstanceCreationFailed(e) => Display::fmt(e, f),
            ContextCreationError::MissingRequiredLayer => {
                write!(f, "Missing a required instance layer")
            }
            ContextCreationError::WindowCreationFailed(e) => Display::fmt(e, f),
            ContextCreationError::DeviceCreationFailed(e) => Display::fmt(e, f),
            ContextCreationError::SwapchainCreationFailed(e) => Display::fmt(e, f),
        }
    }
}

impl std::error::Error for ContextCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self {
            ContextCreationError::LoadingFailed(e) => Some(e),
            ContextCreationError::MissingRequiredExtensions => None,
            ContextCreationError::InstanceCreationFailed(e) => Some(e),
            ContextCreationError::MissingRequiredLayer => None,
            ContextCreationError::WindowCreationFailed(e) => Some(e),
            ContextCreationError::DeviceCreationFailed(e) => Some(e),
            ContextCreationError::SwapchainCreationFailed(e) => Some(e),
        }
    }
}

impl From<LoadingError> for ContextCreationError {
    fn from(e: LoadingError) -> Self {
        ContextCreationError::LoadingFailed(e)
    }
}

impl From<InstanceError> for ContextCreationError {
    fn from(e: InstanceError) -> Self {
        ContextCreationError::InstanceCreationFailed(e)
    }
}

impl From<WindowCreationError> for ContextCreationError {
    fn from(e: WindowCreationError) -> Self {
        ContextCreationError::WindowCreationFailed(e)
    }
}

impl From<DeviceCreationError> for ContextCreationError {
    fn from(e: DeviceCreationError) -> Self {
        ContextCreationError::DeviceCreationFailed(e)
    }
}

impl From<SwapchainCreationError> for ContextCreationError {
    fn from(e: SwapchainCreationError) -> Self {
        ContextCreationError::SwapchainCreationFailed(e)
    }
}
