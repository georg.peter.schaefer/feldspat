extern crate lalrpop;

use built::write_built_file;

fn main() {
    write_built_file().expect("Failed to acquire build-time information");

    lalrpop::Configuration::new()
        .always_use_colors()
        .process_current_dir()
        .unwrap();
}
