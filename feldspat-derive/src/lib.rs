//! Drive macros.

#![warn(missing_docs)]
#![warn(missing_doc_code_examples)]
#![warn(broken_intra_doc_links)]

use proc_macro::TokenStream;
use quote::quote;
use syn::parse_macro_input;
use syn::ItemStruct;

/// Marks a struct as a component.
///
/// Implements the trait `Component`.
#[proc_macro_derive(Component)]
pub fn component(input: TokenStream) -> TokenStream {
    let item_struct = parse_macro_input!(input as ItemStruct);
    let ident = item_struct.ident;
    return quote! {
        impl feldspat::ecs::Component for #ident {
            fn as_any(self: std::sync::Arc<Self>) -> std::sync::Arc<dyn std::any::Any + Sync + Send> {
                self
            }

            fn register(
                factories: &mut std::collections::HashMap<
                    String,
                    std::sync::Arc<
                        Fn(
                            &feldspat::ecs::Facade,
                            &mut feldspat::ecs::Deserializer
                        ) -> Result<(), ()> + Sync + Send
                    >
                >
            ) {
                factories.insert(
                    stringify!(#ident).to_string(),
                    std::sync::Arc::new(|entity, deserializer| {
                        let component = #ident::deserialize(deserializer).unwrap();
                        entity.insert(component);
                        Ok(())
                    })
                );
            }
        }
    }
    .into();
}
